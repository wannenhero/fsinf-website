---
title: Impressum
permalink: /impressum/
lang: de
ref: impressum
---



Fachschaftsrat Informatik  
Universität Leipzig  
Augustusplatz 10  
04109 Leipzig  
Raum A 541   
Email: [{{ site.email }}](mailto:{{ site.email }})  

Bitte beachten Sie ferner auch das Impressum der Universität Leipzig.

https://www.uni-leipzig.de/impressum/
