---
title: Kontakt
permalink: /kontakt/
lang: de
ref: kontakt
---

E-Mail: [`{{ site.email }}`](mailto:{{ site.email }})  
Telefon: [`+49 341 97 322 97`](tel:+493419732297)  
Unternehmen bitten wir von Werbe- und Kooperationsangeboten abzusehen. Da wir mit Unternehmen nicht kooperieren, bleiben diese unbeantwortet.

Für **Jobangebote** haben wir einen Mailverteiler eingerichtet. Bitte senden Sie Jobangebote direkt an:[`jobs[at]fsinf.informatik.uni-leipzig.de`](mailto:jobs@fsinf.informatik.uni-leipzig.de)

Sprechzeiten und Sitzungen siehe [Kalender]({{ "/kalender/" | absolute_url }}). Zu den Sitzungen ist Besuch herzlich willkommen.

### FSR-Raum

Fachschaftsrat Informatik
**Raum A-541**   
Institut für Informatik  
Augustusplatz 10  
04109 Leipzig

### Post-Adresse

Universität Leipzig  
Fakultät für Mathematik und Informatik  
Fachschaftsrat Informatik  
PF 100920  
04009 Leipzig

Bitte bedenkt, dass Post zu uns manchmal etwas länger braucht.

### Mitglieder

  * Graubner, Phillip
  * Hackel, Georg
  * Hakimi, Ahmad Dawar
  * Lange, Josephine
  * Kartashova, Natasha
  * Kodytek, Moritz,
  * Martin Irene
  * Meinecke, Christofer
  * Mieth, Eric
  * Reinhardt, Paul