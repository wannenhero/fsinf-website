---


---

## Tagesordnung
##### 1 Orga
##### 2 Emails
##### 3 Softwaretechnik Vorlesung
##### 4 Auswertung
##### 5 Offener Informatikraum
##### 6 Rechnernetze Seminar
##### 7 35C3
##### 8 Neues zu P-402 Zugang?
##### 9 Feierliche Exmatrikulation

Anwesend: Natascha, Denise, Philipp, Paul, Eric, Josy, Christofer, Georg, Sofia, Moritz, Irene
Sitzungsführung: Georg
Protokoll: Moritz


## 1 Orga
* leere Batterie der Tür
	- Tür piept
	- Batterie vermutlich leer --> Hausmeister rufen
	-
* Raum Buchen (A531) für Sitzungen
	- Paul hat Fr. Wenzel geschrieben -> nichts ist frei
	- Selbststudiumszeit erst ab 14 Uhr
	
* Termine im Kalender eintragen
	- Denise trägt es ein
* FAQ und Service - neue Version auf die Seite stellen? (Christofers Änderungen sind bereits im repo Kommunikation)
	- Christofer hatte Änderungen gepostet
	- Christopher stellt es rein

* WeiFei 
	- Sofia ist bereit für Einkauf und Bestellung, nicht für Deko usw.
	- Irene würde vor Ort helfen
	- Donnerstag 13.12. FKH?
	- Christofer, Sofia, Moritz planen
	- Musik-Hut muss aufgesetzt werden
	- Paul macht Awareness
	- Putzkräften Bescheid geben
* Sofa
	- bisher keine eindeutige Entscheidung
	- längerfristig nach Sofas Ausschau halten
	- Alle halten Ausschau
	- Sofa trotz Kosten reinigen?
* TI Sachen im git - Paul berichtet
	- TI revidiert Entscheidung nicht
	- Bogdan könnte FSR verklagen
	- TI stellt Altklausuren zur Verfügung --> Forderungen könnten sich ausweiten
* Newsletterarchiv über Mailman abrufbar. 
	- Eric kümmert sich
* Israelvortrag
	- Es fehlte an Transparenz -> keine Aufschlüsselung von Finanzen
	- Keine Antworten auf Emails
	- Halbes Jahr später Rechnung über 200€
	- In Zukunft nicht fördern ohne zu wissen, was man fördert

## 2 Emails
* Geld für MHG
	- 30€, Rechnung in Anhang zu Email
	- Geld wird überwiesen
* Treffen mit der Mathematik für den Lehrbericht
	- Anfrage vom FSR Mathe
	- Stellungnahme zum Lehrbericht auf Fak.ebene gefordert
	- Paul und Christofer kümmern sich
* Anfrage aus dem HoPo Referat
	- 
* Antrag von Denise über Förderung von Ihr, Irene und Sonja für einen Informatikkongress in Kopenhagen
    - Bezuschussung von je 100€ unter der Voraussetzung ihre Eindrücke vorzutragen und die Möglichkeit zu bewerben, dass der FSR so etwas fördert
    - Bewilligt (6/2/3)

## 3 Softwaretechnik Vorlesung
* Die SWT Vorlesung wurde an die WiInf ausgelagert. Prüfung besteht wohl aus einem Multiple Choice Test -> ist laut unserer PO nicht zulässig
* Prof. Eisenecker hat angedeutet das Modul gerne dauerhaft zu halten
* Es werden unter anderem Programmierparadigmen gelehrt, was bereits MuP2 abdeckt und es gibt Übungen in den Informatikpools - warum nicht in der WiFa, wenn es ein WiFa Modul ist?
	- Mit Eisenecker reden (Lehrinhalte, Vorkenntnisse unserer Studis (MuP 2), Raumplanung)
	- Mit Scheuermann reden (Räume, SWT-Inhalte generell)
	- Softwaretechnik wird wieder berufen
	- WiFa Softwaretechnik passt nicht zu Informatik
	- Modul ist auf Englisch --> zulässig?
	- Paul und Christofer reden mir Scheuermann

## 4 Auswertungen
* Masterkneipenabend Auswertung
	- Buchung hat nicht geklappt, funktionierte trotzdem
	- Netter Abend
* Auftaktparty
	- 160€ minus gemacht (800€ Ausgaben, 400€ Einnahmen, aufgeteilt unter Fsrä)
	- Absturz war schlechte Location
	- Unkollegialer Umgang der Germanistik vor Ort --> Einzelfall, keine generelle Germanistik-Kritik
	- Schlechte Öffentlichkeitsarbeit -> wenig Vorlauf, schlechte Werbung
	- Unter der Woche für die Hälfte des Geldes organisierbar
	- 'Security' war nervig
	- DJ war gut (für sein Genre)	
* Studienfahrt
	- viele Helfer
	- minimal weniger Essen
	- eigenartiger Geruch?
	- Forsthaus Sayda auch zukünftig denkbar
	- sehr gut angekommen
	- Workshops kamen gut an
	- Vorschlag: Anmeldetool erweitern --> Studis können kochen oder Gruppenaktivitäten organisieren
	- Forsthaus Sayda jetzt reserveren?
	- 1000€ für Forsthaus Sayda 2019 beschlosssen 8/0/0

## 5 Offener Informatikraum
* Paul und Christofer waren wieder bei Felix Wlassek
* Ruhepool wird zurzeit genutzt, was dazu führt, dass sich Erstis nicht trauen Fragen zu stellen, da Studierenden aus höheren Semestern rumnörgeln
	- Pools stark ausgelastet -> viele Lehrveranstaltungen
	- SWT nutzt Pools für Übungen	
	- nächstes Semester anders planen, dieses Semester nicht wirklich lösbar?
	- SWT nutzt Info-Pools, da zum Großteil Informatiker
	- Mehr Arbeitsräume für Studierende schaffen
	- Ruhepool abschaffen ist inakzeptabel
	- Punkt mit 3 zusammengelegt

## 6 Rechnernetze Seminar
* siehe Mail - ist StuKo der passende Weg?
* was war das nochmal?
	- Vorleistungen über StuKo kippen?
	- Leute ausgetragen, da sie 'die Vorkenntnisse nicht haben'
	- wurde über Studienbüro ausgetragen


## 7 35C3
* hat Philipp den Antrag fertig?
	- vertagt
## 8 Neues zu P-402 Zugang?
* was war das?
	- vertagt

## 9 Feierliche Exmatrikulation
* WiWi und Mathe haben eine feierliche Verabschiedung von Absolventen organisiert
* Studis haben gefragt, ob wir das auch einführen wollen
	- Meinungsbild einholen?
	- Für Interessierte anbieten?
	- Mathe fragen, wie sie es organisieren -> Paul/Christofer fragt
