---


---

**Anwesend:** Paul, Georg, Eric, Tim, Denise, Christofer, Philip, Irene, Marc (Gast)

**Sitzungsleitung:** Eric

**Protokoll:** Tim

## 1 Grillen

* über Tag verteilt ~ 60-70 Leute, gleichzeitig ~ 40-50 Leute
* Einnahmen: 150 Euro Ausgaben: noch nicht ausgerechnet.
* Grill + ein paar Kisten Getränke (Mate, Fritz Cola, Bier) noch übrig geblieben. 
* Getränke werden für zukünftige Events beansprucht
* Grill wird zurückgeschafft
* Pfand wird zurückgebracht

## 2 Kollektivfestival
* Kooperationsvertrag zw. FSR Informatik & Philosophie:
 * Vortrag im Felix-Klein-Hörsaal
 * 15.06.2018
 * Thema, Sprecher_innen, Finanzer_innen fehlen

## 4 Sommerfest
* in Zusammenarbeit mit FSR Mathematik
* vom FSR Mathe angesetzter Termin: 26.06.18
* FSR Mathe kontaktieren, dass wir uns beteiligen wollen, aber nur mit Awareness-Konzept und fragen, welche Aufgaben vom FSR Informatik übernommen werden
* erfolgreiche Abstimmung über Mithilfe zum Sommerfest in Kooperation mit FSR Mathematik **aber nur** mit einem Awareness-Konzept (6/0/1)
* gemeinsame T-Shirts/Kennung für Helfer_innen ? für nächstes Treffen

## 5 Emails
* Anwesenheitspflicht - Mail von Felix Fink 
 * FSR Informatik kann es verteilen:
 * Eric schickt die Flyer über Hauspost an alle Lehrstühle
* Das Justi hat sich zu TI geäußert
* Denise leitet es an Herrn Batsch weiter und bedankt sich bei Frau Kranich bedanken
* vertagt
* 70 Jahre Israel - wollen wir eine der Veranstaltungen unterstützen?
 * vertagt
* Marc ist auserwählt worden, sich bei der Free Software Foundation Europe e.V. zu bedanken (bzgl. Email vom 26.04.2018 14:18 - did our material arive) (zwei-Zeiler - danke, ist angekommen ;))

## 6 Wahlen
* Wurde bisher Werbung gemacht? 
 * Christofer twittert
 * in Zweiti-Veranstaltungen rennen (wer)
 * Flyer/Plakate layouten (wer)
 * vertagt

## 7 TOR-Exitserver
* Antwort von Prorektor für Bildung und Internationales Prof. Hofsäss war bzgl. des Nodeservers knapp und verneinend und deutet auf ein technisches Missverständnis hin.
* 1. Schritt: höfliche Rückantwort Email an Prof Hofsäss zur Aufklärung des vermuteten Missverständnisses durch Fabian und Eric
* 2. Schritt falls 1. Schritt unzureichend: Anfrage im Senat stellen

## 8 Corporate Design
* vertagt

## 9 Prüfungsvorleistungsmodalitäten (nach Punkt 7, Protokoll 27.3)
* vertagt
