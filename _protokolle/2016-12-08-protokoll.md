---


---

**Anwesende**: Georg, Martin, Eric, Manuel, Dominik, Chris, Denise
**Protokoll**: Dominik

# TOPs
* Weihnachtsfeier
* MuP1
* StuRa Kommunikation

## Weihnachtsfeier
* Ab 17 Uhr ist reserviert
* Ab 15 Uhr reservieren --> Anfrage
* Einkaufsliste folgt noch
* Dekorieren --> Wer Zeit hat
* 80 Becher bei Eddy sind reserviert
* Handkassenantrag noch stellen --> Dominik
* Heizplatten --> Georg
* Mathe will den Baum (evtl geschmückt)

## MuP1
* Gespräch mit Dr. Zeckzer wurde geführt
* Kritik wurde angenommen
* Vorlesung ist okay
* Antwortmail an die 3 Studenten --> Manuel

## StuRa Referent für FSR-Kommunikation
* Antwort Mail an den Referenten "Schau auf unsere Seite, komm im Januar" --> Georg oder Denise
