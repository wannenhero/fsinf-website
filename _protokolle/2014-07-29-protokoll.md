---


---

* Anwesend: Nancy, Sara, Alrik, Martin, Ben, Fabian
* Protokoll: Fabian


## OLAT-Logout-Verhalten
* wird einer weiteren Mail an Graebe kritisiert (Kasimir)

## Erstie-Infomaterial
* Soll enthalten:
	* URL der Webseite, twitter, facebook
	* Erstieinfoabend
	* Propädeutikum
	* Studienfahrt
	* Newsletter
* Als PDF zur Verteilung via Almaweb

## Abrechnung Winterparty
* Fehlbetrag wird auf die FSRä umgelegt

## Studienfahrt
* wir fahren ohne die Mathematik
* wir überweisen Ihnen den bereits ausgelegte Anzahlung
* wir fahren nächstes Jahr an den gleichen Ort (Schloss Oberau) zur gleichen Zeit (16.10.-18.10). Wieder vorbestellen - für die gleiche Anzahl (36-45) Leute --> ca 600€ Vorrauszahlung
--> 6/0/0 --> dafür

## Erstitüten
* Anfrage von Red Bull wird durch Fabian beantwortet
* beim StuRa 100 Beutel + Kalender bestellt

## Erstiinfoabend
* "Newsletter-Einschreibezettel" bereitet Alrik vor

## Webseite
* Bioinf-Master kommt mit in die FAQ (Nancy/Sara?)
* Studienverlaufsplan-Link fixen (Fabian?)
* Auf Impressum/Kontakt hinweis auf @uni-leipzig.de Whitelist
* Mod-Mail ergänzen um diese Info^^
* "Unser Twitteraccount" auf "Relevantes Gezwitscher" zurückändern (h3 vs h2)
* Kontakt-Box wieder von Startseite entfernen
* Jobs-Liste wie Newsletter auf Seite bringen


## Nächste Sitzungsverantwortliche
* Nancy
