---


---

Anwesend: Thomas, Denise, Martin, Fabian, Eric, Verena, Manu

Protokoll: Thomas

# Studienfahrt
  * Organisatorisches bis zur nächsten Sitzung klären
  * 16. bis 18.10.2015, Schloss Oberau
  * nur volljährige Teilnehmende
  * Online-Anmeldung


# Propädeutikum: Twitch Stream

* Video zum Propädeutikum
  * zwei Studierende wollen einen Stream mit einer Zusammenfassung des Propädeutikums machen
  * keine direkte Aufnahme sondern eine Wiederholung des Stoffes
  * wenn die Beispiele ähnlich sind, wird dies vorher abgeklärt
  * Videomitschnitt geplant (dieser soll eventuell auf unserer Homepage verlinkt werden)

* während des Propädeutikums soll der FSR vorgestellt werden; Informationen zu Newsletter etc.

# Einführungswoche

  
  * Newsletter soll beworben werden
  * Termine:
    * 06.10 15 Uhr Bacheloreinführungsveranstaltung
    * 07.10 13.30 Mastereinführungsveranstaltung
  * am 14.10.15 ist die alljährliche Kneipentour geplant
  * die Studendierenden sollen bitte keine Laptops am 14. mitbringen
  * einheitliche T-Shirts für Mitglieder des  FSR, um bei Veranstaltungen bessere Repräsentation zu erwirken


# FSR Verteiler

  * nur noch aktive Mitglieder verbleiben im Verteiler

# Sonstiges

  * E-Mail zum Berufungsverfahren Katja Nowak: Verena kümmert sich darum
  * Zeitmagazin, Lehmanns etc. Erstisachen: Nein
  * Weihnachtsvorlesung: von Prof. Brewka kam keine Antwort
  * Stundenplantool: Thomas schickt bitte die  Links über den Verteiler
  * Stipendienmails: seriöse Angebote können wir twittern
  * Newsletter: der nächste kommt Mitte diesen Monats (Thomas und Denise kümmern sich darum)
  * GitLab für Studienmaterial: kurz vor der Fertigstellung
  * alle: Es wird nach einem neuen schicken Logo gesucht

# Nächste Sitzung

  * 16.09.2015, 14 Uhr

