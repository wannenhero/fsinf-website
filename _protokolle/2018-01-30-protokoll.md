---


---

**Anwesend:** Josy, Bianca (Gast), Marc (Gast), Moritz (Gast), Paul, Christopher, Georg, Verena (Gast), Ahmad (später)

**Protokoll:** Georg

# Tagesordnung 30.01.2018 - 15:30 Uhr

## 1 Orga

### 1.1 Vernetzungstreffen
* Christofer berichtet: Themen: Mitgliedergewinnung, Kooptierung (Protokoll ist im Verteiler)

### 1.2 Systemakkreditierung
* Christofer: war anders als gedacht, hatte nix mit Studiengang zu tun
* andere Fachschaften/StuRa waren da und konnten mehr erzählen (Wissensweitergabe, Rechte unklar)
* Christopher erstellt Bericht im Wiki
* nächster Termin unklar, da dies nur Stichprobenartig stattfindet
* Idee: Erfahrungsaustausch in Vernetzungstreffen tragen (Josy kümmert sich)
* Verena schreibt Erinnerungsmail an Lasse

## 2 Kollektivfestival
* Termin im Juni, begrenzte Veranstalungszahl → Kooperation sinnvoll
* Wollen wir mit anderen Fachschaften kooperieren?
* Ideen Sammlung: Vortrag Ethik in der Informatik? (Gibt es passende Dozierende oder andere Menschen)
* Idee: Kontakt mit Philos/KritNawis (Josy hatte Erstkontakt), Thema spezifizieren (z.B. Big Data, Machine Learning), Person mit Fachwissen/Arbeitskreis? 
* Josy spricht mit den KritNawis über die AG und Vortrag Kollektivfestival
* Moritz (mit Christophers Hilfe) schreibt Mail an Philos

## 3 Emails
* Women* in Science: Josy bewirbt es
* Zu wenig Teilnehmende Medizinische Informatik: Homepage optimierungsfähig, Paul und Christopher treffen sich

## 4 Mail-Verteiler
* Lay-Out, Lesbarkeit im Jobverteiler ist stark eingeschränkt
* Überarbeitung des Verteilers (Marc und Josy kümmern sich um Erneuerung nach Prüfungsphase)

## 5 Awareness-Konzept
* Abstimmung (wurde am 19.12 diskutiert) → nicht beschlussfähig, vertagt

## 6 Lehrbericht
* Lasse vom Stura meldet sich nochmal, aber wir müssen eventuell demnächst einen Lehrbericht schreiben
* Lasse hat sich noch nicht gemeldet
* Wofür ist Lehrbericht da? allgemeiner Statusbericht ans Ministerium? Defizite ansprechen?
* Wie oft? unklar ob jährlich

## 7 Sitzungsleitendenplan
* Liste soll fortgesetzt werden
* Diskussion wer und wie Sitzungsleitung umgesetzt werden soll

## 8 Büromaterial
* Briefmarken
* vertagt, da nicht beschlussfähig
* Benutzung soll dokumentiert werden

## 9 Free Software Foundation Europe
* Merchandise/Infomaterial (kostenlos) bestellen und austeilen → vertagt

## 10 Kaffeeflecken im Büro
* Bitte achtet auf die Sauberkeit (insbesondere auf/im Kühlschrank) →internes [Wiki](https://git.fsinf.informatik.uni-leipzig.de/fsinf/wiki/wikis/intern/putzplan)

## 11 Nächste Tagesordnung
* bitte Punkte ausführlicher erklären, damit jede_r weiß worum es geht

## 12 Mündliche Prüfungen
* Antragstellerin nicht da, vertagt

