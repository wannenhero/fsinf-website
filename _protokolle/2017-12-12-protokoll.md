---


---

Sitzungsleitung: Paul

Protokollführend: Philipp

Anwesend: Paul, Christofer, Denise, Eric, Irene,

Gäst*innen: Marc, Sofia, Bianca


## 1 Finanzen
* Studienfahrtbrechnung Verlängert bis 22.12 (Philipp)
* Christoph, Philipp und Christofer haben jetzt Kontovollmacht



## 2 Orga
* Wünsche für eine Winterparty vorhanden: Debatte verschoben

### 2.1 Wei-Fei
* Glühwein etwas zu wenig, fürs nächste mal besser planen
* Gute Mitarbeit von allen
* Absprache mit Putzkräften verbessern
* Nächstes mal einen DJ (Musikkonzept) einsetzen
* Mehr Leute zum Aufbau für nächstes Jahr
* 60€ Gewinn gemacht + Pfand - EwiChor
* Becher werden demnächst gereinigt von Eric, Denise und Sofia

### 2.2 AK Lehramt
* Vertreter AK lehramt: vertagt, da keine Reaktion im Newsletter


### 2.3 Studienfahrt 2018
* Mehr für Oktober 
* Paul und Christofer wollen sich um Unterkunft kümmern und 
  Antrag an Unterkunft stellen

### 2.4 Sprechzeit
* Termin: Stunde vor der Sitzung (Di. 14.30-15.30), 
* Paul will Sprechzeit besetzen 
* Paul und Denise regeln das formale zu den Aushängen, Posts in Gruppen und Co

### 2.5 Rückzahlung Studienfahrt
* Auszahlung an die Studierenden die sich rechtzeitig abgemeldet haben, erfolgt demnächst
* Christofer und Eric kümmern sich um Liste der Betroffenen

### 2.6 StuRa Entsendung
* Ersatzentsendung von Marc, da Tim verhindert ist
  Abstimmung: 5-0-1.
  Vorschlag angenommen.

## 3 Emails
* Anmeldetool für C34 wird fertiggestellt
* Freiwillige Mitglieder (mit regem, konstantem Interesse an der Arbeit des FSRs) werden in Verteiler und (inoffizielle) Telegram-Gruppe aufgenommen
 
## 4 Evaluationen (Braun/Gräbe)
* Keine Neuigkeiten bzgl Gräbes Anfrage
* Keine Neuigkeiten bzgl Brauns Anfrage
  vertagt.


## 5 Discord
* Punkt gestrichen, da Interesse versandet.

## 6 Agenda
* Marc, Moritz, Gero, Paul kümmern sich um Agendaarbeit auf Website
* Abstimmung zu: Vorgehen wie auf Antragszettel, Paul wird "Head of Agendaprogramm", 
* Jeder Ak hat eine/n Verantwortliche/n, Abstimmung: 5-0-1. 
  Vorschlag angenommen. 

AK Satzungen
* Denise, Marc und Paul(Mehr fürs Controlling) haben Interesse, sich um eine Satzung zu kümmern
* Frist der Satzung ist 30.6.2018

AK Corporate Design:
* Marc, Christofer, Paul, Denise
* Frist: 30.04.2018

AK Kommunikation
* Frist 30.09.2018, Irene, Paul(Mehr fürs Controlling), Eric haben Interesse
* Im git-Repo "Agenda", gibt es im Wiki eine Seite, wo Vorschläge eingebracht werden können

##7 Next To
* Partys besprechen (Frühlingsgefühle-Party, Winterparty)

##8 Rückzahlung Kopierkasse
* Geldeinzahlung in die Kopierkasse, kann zurück gegeben werden
