---


---

anwesend: Denise, Paul, Martin, Eric, Dominik, Georg, Manuel
Protokoll: Eric

# TOPs
* Organisatorisches 
* Justitiariat (ADS)
* 33C3 (Subventionierung)
* Studienfahrt (Zeitpunkt, Buchung)
* Weihnachtsfeier
* Ersti-Einführungs-Feedback
* Gitlab
* Ummeldung der Transponder
* Entsendung ins Stura-Plenum

# Organisatorisch
* Termin
  * ab sofort
    * nächsten und übernächsten Donnerstag
  * danach
    * Turnus: Montag/ Donnerstag im Wechsel
    * jede Woche ein Termin
    * Notfalltermin für dringende Sachen: donnerstags, 12.45 bis 13.15 Uhr oder Montag 16.30 Uhr
* Sitzungsvorbereitung/ Sitzungsverantwortliche
  * Aufgaben im vorhinein (nach Kalender) verteilen
  * Sitzungsvorbereitung, Tagesleitung,  Protokoll
  * Denise erstellt Plan mit rotierenden Aufgaben
* Finanzen
  * Idee einen Haushaltsplan zu erstellen
  * würden Dominik und Manuel machen
* Webseite
  * Namen müssen aktualisiert werden

# Justitiariat (ADS)
* die vorgesehene Mail ging nicht raus
* Denise hat begonnen einen solchen Brief zu schreiben, dieser wurde nie bearbeitet
* wir sind geneigt den Brief zu versenden, eine finale Entscheidnug dazu treffen wir beim nächsten Treffen

# 33C3 (Subventionierung)
* wir halten an der Förderung fest
* Denise schaut bis zur nächsten Sitzung wegen der Finanzen, wie viele Personen mit wie viel gefördert werden können

# Studienfahrt (Zeitpunkt, Buchung)
* Termin und Unterkunft müssen geklärt werden
* Apell: wer möchte sich darum kümmern? bitte bis nächste Sitzung überlegen

# Weihnachtsfeier
* Raum P801 ist am 14.12. ab 17 Uhr frei
* Fr. Wenzel bitten, den Raum ab 17 Uhr zu reservieren, offizieller Start ab 19 Uhr
* Georg: bitte nach Kochplatte fragen
* Martin kümmert sich um einen Baum
* unverbindliche Einladung an Profs

# Ersti-Einführungs-Feedback
* verschoben

# Gitlab
* Idee: Wasserzeichen
* Hinweis bei Login (oder so) zu Urheberrecht

# TOR-Node
* Idee wird weiterhin für gut befunden

# Ummeldung der Transponder
* relevante Personen sind nicht da

# Entsendung ins Stura-Plenum
* Manu geht anstatt Eric ins Stura-Plenum
