---


---

* Anwesend: Alrik, Kasimir, Martin, Nancy, Nicole, Sebbo

## Digital-Humanities-Modul

* Beschwerden von mehreren Studierenden
* es geht um das 10-Punkte-Modul "Aktuelle Themen in der digitalen Philologie" (10-202-2336) der Abteilung "Digital Humanities"
* [Prüfungsordnung S. 39](http://db.uni-leipzig.de/bekanntmachung/dokudownload.php?dok_id=3717)
  * Vorlesung, Seminar, Praktikum
  * Modulprüfung: Projektarbeit mit mündlicher 30-minütiger Präsentation
  * Prüfungsvorleistung: ein Referat im Seminar
  * siehe auch [Modulbeschreibungen](http://db.uni-leipzig.de/bekanntmachung/dokudownload.php?dok_id=694)
* Realität
  * zweiwöchentlich montags Vorlesung (Vorlesungsreihe) mit Anwesenheitspflicht mittels Twittern ([#ehum14](https://twitter.com/search?f=realtime&q=%23ehum14)), am Anfang des Semesters Liste mit Name und Twitter-Account
    * alternativ zum Twittern: nach jeder Vorlesung eine halbe Seite Zusammenfassung abgeben
  * zweiwöchentlich dienstags Seminar: Anwesenheitspflicht via Diskussionsbeteiligung
  * zweiwöchentlich montags und dienstags Praktikum: Python-Kurs (Gruppenarbeit)
  * zusätzlich zu den in der Prüfungsordnung vorgegebenen Prüfungs(vor)leistungen werden gefordert:
    * 3-4 Python-Arbeitsblätter
    * *regelmäßige* Projektpräsentationen
    * **5-8 Seiten Abschlussarbeit von jedem Gruppenmitglied**
    * Informatik-Studierende müssen zudem 30 Minuten ein Python-Modul vorstellen und ein Arbeitsblatt dazu erstellen und außerdem den teilnehmenden GeistenswissenschaftlerInnen helfen
  * siehe auch [Syllabus](http://www.dh.uni-leipzig.de/wo/wp-content/uploads/2014/03/Syllabus-Humanities-Programming-WS-14-15.pdf) auf der [Modulwebseite](http://www.dh.uni-leipzig.de/wo/courses/digital-philology-at-the-university-of-leipzig-sommersemester-20132014/)
* Nancy redet mit Matt Munson

## ADs

* Fabian und Sebbo kümmern sich um Terminvorschläge

## Nächste Sitzung

* am 5. Februar, 15:15 Uhr
