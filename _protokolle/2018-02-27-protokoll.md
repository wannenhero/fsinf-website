---


---

**Anwesend:** Christofer, Denise, Paul, Ahmad, Georg, Eric(16:41), Phillip, Josy(16:41), Moritz(Gast)  
**Protokoll:**  Ahmad 


# Tagesordnungspunkte
* Orga
* Wahlen
* Studienfahrt
* Kollektivfestival
* E-Mails
* Zulassungsbeschränkung
* Workshop Barrierefreiheit
* MuP
* Corporate Design
* TI
* Awareness-Konzept
* Mündliche Prüfung
* Anfrage Semesterbeitragerstattung Auslandssemester

## Orga
* TOPS in Protokollen
* Raumreservierung P901 in den vorlesungsfreien Zeit
* Christofer fragt bezüglich des Postausgangs bei Frau Wenzel

## Wahlen
* Wahl am Institut mit Mathemaikter zusammen oder Ziegenledersaal
* Abstimmung Ziegenledersaal: 7/1/0
* Abstimmung FAK Raum:  1/7/0
* Wahlvorstand: Georg, Phillip, Denise, Moritz, Ahmad, Eric
* Werben für die Wahl
* Formular für Wahl wird von Phillip erstellt

## Studienfahrt
* Verschiebung Studienfahrt auf 2.11.2018 - 4.11.2018 Forsthaus Sayda
* Abstimmung: 7/0/1

## Kollektivfestival
* Kooperationsvereinbarung sollte geschlossen werden
* Konzept wird ausgearbeitet -> Finanzen in dem Punkt 
* Arbeitskreis: Christofer, Marc, Moritz, Paul

## E-Mails
* Mint Frauen E-Mail: Verteilung der Flyer, Qualität abschätzen, erstmal 25 bestellen und gucken wie gut es ankommt
* Abstimmung: 4/1/1
* Befragung der Studienplatznachfrage an Sächsischen Hochschulen -> streichen
* Hackathon der Uni-Bib -> bewerben im Newsletter, Christofer bewirbt es über Facebook und Twitter

## Zulassungsbeschränkung
* Entscheidung unklar für BA
* NC in Master nicht im Gespräch
* vertagt

## Workshop Barrierefreiheit
* Beitrag auf 200€ erhöhen
* Abstimmung: 7/0/1 
* weitere Förderung für Beitrag anfragen, falls nicht aus aus FSR Kasse
* Paul Anfrage ob Referat unterstützt wird
* Antrag wird von Eric, Paul und Phillip bearbeitet 
* vertagt

## MuP
* vertagt

## Corporate Design
* Denise hat Mediendesignerin angefragt wegen Logo
* Möglichkeit 1: Mediendesignerin -> 200€ - 300€(Maxinamlgrenze)
* Möglichkeit 2: Wettbewerb für Logo -> Sieger bekommt Prämie
* Möglichkeit 3: Ausschreibung für neues Logo (Urheberrecht beachten)
* Möglichkeiten finden für neues Logo
* Altes Logo überarbeiten?
* FSR-Name Änderung(für nächste Sitzung)

## TI
* im Stura nachfragen wie der Justizweg aussieht
* Denise schreibt Mail an Beteiligten

## Awareness-Konzept
* Paul und Georg kümmern sich Konzept und setzen sich mit dem Büro Referat für Gleichstellung- und Lebensweisenpolitik

## Mündliche Prüfung
* Planung und Kurzfristigkeit von Prüfungstermine von mündlichen Prüfungen
* Tage waren fest und wurden 2 Wochen früher bekannt gegeben
* Prüfungskollision von Prüfungen der Bioinformatikern
* vertagt

## Anfrage Semesterbeitragserstattung Auslandssemester
* Beitragsrückzahlung von Semesterbeitrag wenn man sich im Ausland befindet
* in FSRä Verteiler fragen
* rausfinden an wen man sich wenden muss
* Denise wendet sich an den Referenden der Mobilität
