---


---

anwesend: Manu, Verena, Nico, Georg, Denise, Eric, Manu (Skype)
Protokoll: Eric

# TOPs
* Studienfahrt
* Exkursion Informatica Feminale
* Abstimung letzte Sitzung
* Mails
* Mail an Prorektor
* ADS
* Wahlen
* 32c3

## Studienfahrt
* 5. bis 7. August bestenfalls, sonst 8. bis 10. August
* Angebot CVJM Strobelmühle für etwa 500€
* Buchung: 7/0/0
* Verena bucht
* 500€ Maximum für Verpflegung & Aktivitäten: 7/0/0

## Exkursion Informatica Feminale
* findet vom 8. bis 26. August an der Uni Bremen
* verschoben auf nächste Sitzung

## Abstimung letzte Sitzung (Gitlab)
* Abstimmung war formal ungültig
* Mail an Scheuermann ist nicht verschickt worden
* falls erneut abgestimmt werden soll, dann im Beisein aller zur Sitzung oder per Mail

## Mails
* Linux Vortrag soll via Twitter beworben werden: Denise

## Mail an Prorektor
* eventuell sollen Prüfungsvorleistungen reduziert werden
* Stuko meint alles solle so erstmal erhalten bleiben, da bessere Noten/ weniger Durchfall
* Konsequenz wäre evtl. Modulzusammenlegung
* Wunsch: Unterstützendes Schreiben des FSR
* Martin schreibt Entwurf

## ADS
* wir wollen Rechtssicherheit, damit sich das nicht jedes Jahr wiederholt
* Denise und Eric gehen zum Stura und holen rechtliche Infos ein
* nehmen Gesprächsangebot an
* Anwortmail soll Anfang nächster Woche raus (25.01)
* Abstimmung: 6/ 0 / 1

## Wahlen
* Wahlausschuss trifft sich morgen 15 Uhr
* Manu geht vllt hin
* neue Leute für den FSR ansprechen, checken wer von uns sich noch aufstellen lässt
* Sitzungen bewerben
* Sommergrillen als Wahlveranstaltung
* Wahlwerbung


## 32c3
* Auswertungstreffen war okay
* 10 Leute bekommen Sponsoring
* für 2016 wieder geplant
