---


---

## Anwesende
* Anwesend: Eric, Georg, Moritz, Ahmad, Christofer, Sofia, Irene, Denise, Paul
* Protokoll: Paul

## TO
1. Orga
2. Lindemann
3. Emails
4. Einführungsveranstaltungen und Propädeutikum
5. Umstrukturierung Bachelor
6. Offener Informatikraum
7. 35C3
8. Nachtrag zu Orga
9. Bioinformatik Einwahl
10. Auf Twitter KI Vorstellung teilen
11. Plagiate bei MuP
12. Newsletter Subscriptions
13. Master DH
14. Sitzungsleitung und Protokoll am 18.10

## 1 Orga

#### 1.1 PO - war Paul bei Neumann
* Paul war noch nicht da, vertagt

#### 1.2 Sitzungstermin
* Doodle nicht perfekt
* zwei wechselnde Termine?
* S: Montag und Freitag doof
* P: Mit A und B Woche (beide Termine in der selben Woche) wäre das ok
* Wechsel Donnerstag 13 Uhr/Freitag 13 Uhr?
* Zustimmung zu provisorischem Termin
* Nächste Woche Donnerstag 13 Uhr Sitzung (18.10)
* Übernächste Woche Freitag 13 Uhr Sitzung (26.10)
* doodle ausbessern, am 26.10 wird finaler Termin bestimmt
* nächste Witzungen provisorisch im Konferenzraum, für festen Sitzungstermin dann festen Raum buchen

#### 1.3 Haushalt restliches Semester
* Philipp noch nicht da -> vertagt
* Christofer schreibt Philipp nach der Sitzung
* Plan soll enthalten wieviel Geld wir haben, wieviel wir noch ausstehend ausgeben müssen und was noch ansteht/anstehen könnte 

#### 1.4 StuRa Entsendung
* Denise zur nächsten Sitzung entsandt
* Entsendung ansonsten weiterhin vakant
* Bianca fragen?
#### 1.5 Empfehlung Prof. Maletti für Theodor-Litt-Preis
*

* Wer schreibt das Empfehlungsschreiben? Und stellt alle Unterlagen zusammen Frist 30.10 (Post!)
* Maletti hat uns alle Unterlagen zukommen lassen
* Ideal wäre ein Team aus 2 Personen
* Gemeinsam sammeln, jemand macht einen Text daraus
* Wie wollen wir sammeln
* Eric findet wir schreiben etwas an den Verteiler und die Leute antworten mit ihrem Lob
* Denise findet gut dass er spontan Tutorien auf Anfrage veranstaltet hat
* Christofer würde sich dazu bereit erklären eine Rohfassung zu schreiben die gefeedbacked wird
* Denise kümmert sich schon um Christofers Empfehlung für Studierendenpreis
* Wer etwas positives über Christofer zu sagen hat schickt es Denise privat

#### 1.6 Kontakt-Daten Tabelle im Wiki
* Unter "intern" findet sich eine .md mit Kontaktdaten. Ziemlich veraltet. Diese sollte wieder gepflegt werden. (oder eingestampft?)
* Hintergrund: "Früher" war es so, dass zu diesem "internen Bereich" nur FSR-Mitglieder Zugang hatten. Es gab im alten Wiki auch Nutzer, welche nicht im FSR waren (zB Sommerparty-Orgateam). Deshalb heißt dieser Ordner überhaupt "intern".
* Denise schlägt eine Datei mit Ehemaligen-Kontaktdaten vor (freiwillige Basis).
* Wir löschen alles außer die ehemaligen und jeder und jede der oder sie will kann sich da eintragen


## 2 Lindemann
* Christofer und Paul gehen morgen zu Scheuermann
* vertagt

## 3 Emails
* Anfrage zu Cybersecurity und so
  * Emailverlauf anonymisiert als Person A: ... etc. weiterzuleiten
  * Paul schreibt die Mail
* Anfrage von kommerziellem Unternehmen
  * wir kooperieren nicht mit Unternehmen, Denise watscht die Person ab
* Emails für die Studienfahrt fehlen teilweise und müssen nachgeschickt werden
  * Christofer macht das 

## 4 Einführungsveranstaltungen und Propädeutikum
* Bachelorkneipentour?
  * 16.10 nach DS Vorlesung
  * Reservierungen fast fertig
  * Christofer, Ahmad, Moritz, Georg, Sofia, Marlo, Maximilian, (Marc), zwei Kommilitonen von Sofia, zwei oder drei Freunde von Ahmad die auch höheres Semester sind
  * 5 Ruten, mind. 2 Personen pro Rute
  * Moritz guckt nach restlichen Bars
  * dann schlauen Plan überlegen
  * Christofer, Moritz, Sofia machen das unter sich aus
  * D: Immer ein FSR-Mitglied in jeder Gruppe
  * A: Startbier?
  * Christofer: Wir kriegen noch 8 Kisten Bier vom StuRa -> nach der Sitzung holen 
  * Denise: Alkoholfreie Getränke holen
  * Christofer: 3 Kästen Limo sollten reichen
  * Vorschlag: 50€ nochmal als Maximum beschließen
  * Abstimmung: 7/0/1
* Feedback von der Vorstellung
  * Positives Feedback zur Vorstellung
  * Zu wenig Leute haben geholfen 
  * Vorschlag: Howto schreiben für nächstes Jahr

## 5 Umstrukturierung Bachelor
* Christofer hat Informationen des AK über den Mail-Verteiler geschickt
* Ist es sinnvoll darüber zu reden?
* Eric hat es nicht gelesen, deswegen und nur deswegen wird es vertagt
* Bitte lest die Mail!

## 6 Offener Informatikraum
* Christofer hat mit Fabian Schmidt über die Nutzung der Pools gesprochen auf Wunsch von Dr. Zeckzer
* Offener Informatikraum wird von der Mathedidaktik gemacht
* 2-3* die Woche im Windowspool, sonst 414 oder 412
* Wenn wir von Problemen hören sollen wir pöbeln
* D: Wie wird es den Studierenden kommuniziert und wie macht die Mathedidaktik das jetzt?
* Christofer hat keine Ahnung vom Ablauf
* Wir sollten das im Kalender eintragen und verbreiten
* ist noch in Ausarbeitung, wir kommunizieren das wenns fertig ist
* Paul und Christofer fragen morgen bei Felix nach

## 7 35C3
* Marc wollte wegen Förderung nachfragen
* Geht klar, müssen nur einen Antrag zum Haushaltsausschuss einbringen
* Philipp und Paul könnten das machen und hat ein Auge darauf dass das passiert
* vertagen

## 8 Nachtrag zur Orga
* Bitte schickt Fragenvorschläge für die FAQ
* Denise will das alles möglichst schnell machen
* Denise macht gerade Newsartikel, das Newsletterteam soll mal einen Newsletter raushauen
* Paul findet das alles toll
* Christofer shcreibt noch Anmerkungen per Mail
* Denise will es sofort platt machen
* Eric ist dagegen
* Georg ist für einen Disclaimer
* Christofer fände Georgs Lösung gut
* Denise sagt ok

## 9 Bioinformatik Einwahl
* Beschwerde eine Studierenden Person bei uns eingegangen. Bioinformatik kann sich für die allerwenigsten Module im Almaweb einschreiben. Einige Platzmäßig begrenzt. Bei manueller Einwahl kommt man zu spät und bekommt gesagt das Modul ist voll. Die Person findet das miserabel. Es soll nicht irgendwann besser werden sondern möglichst zeitnah. Vorlesungen verschwinden, Zeiten ändern sich.
* Marc: Das ist ein uniglobales Problem. Es bringt nur konkreten Menschen in Kontakt zu treten.
* Die Person meint nur Informatikmodule, sie kann sich nicht zu Softwaretechnik anmelden, und zu anderen auch nicht.
* Christofer: Eigentlich sollte man sich zu Pflichtmodulen immer anmelden können.
* Die Person geht trotzdem hin und versucht sich anzumelden.
* Dürfen im Bioinformatikmaster Bachelormodule belegt werden?
* Was sagt die Studienordnung dazu?
* Mal mit Neumann reden warum die Module nicht drin sind. Die Person könnte Dozierende direkt anfragen und mal gucken.
* Sofia und Paul und Marc gehen zu Neumann

## 10 Auf Twitter Kritische Informatik Vorstellung teilen
* Newsartikel in Arbeit, wird dann geteilt

## 11 Plagiate bei MuP
* Zeckzer will dass wir Leuten weiterleiten dass die Leute alleine arbeiten sollen
* Wir sind kein Sprachrohr von Dozierenden
* In unseren Augen keine Aktion von uns erfordlerlich

## 12 Newsletter Subscriptions
* Denise und Irene tragen die Zettel ein

## 13 Master DH
* Niemand hat sich eingeschrieben
* Anscheinend noch niemand fertig, obwohl Leute mit über 60lp angefangen haben

## 14 Sitzungsleitung und Protokoll am 18.10
* Eric macht Sitzungsleitung
* Moritz schreibt Protokoll
