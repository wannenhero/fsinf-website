---


---

* anwesend: Antonia (Gast), Josy, Fabian, Dominik, Martin, Paul, Denise
* Protokoll: Eric

# TOPS

* Unterkünfte zum Propädeutikum
* Sitzungstermine
* konstituierende Sitzung
* Mail wegen Selbsstudiumszeiten
* Kooptierung

## Unterkünfte zum Propädeutikum

* Alternative zu Couchsurfing etc
* als eigene Website gedacht
* Vorschläge:
  * "schwarzes Brett"
  * ähnlich wie Nachholfebörse der Mathe?
* Über Studentenwerk ist kurzfristige Miete schwierig
* techische Unterstützung ist gesucht, weniger ideelle
* technisch hoher Aufwand einer eigenen Implementierung
* Lösung: mailto-Link auf die Website mit Subject, to, body), Antonia kümmert sich um Zuordnung der Anfragen
* Antonia sendet welche Eck-Daten für die Zuordnung benötigt werden
* wir bewerben die Bitte bei Möglichkeit Angebote zuzusenden

## Sitzungstermine

* bis nächste Woche reguläre Sitzungen
* Was ansteht: Kneipentour, Erstigrillen vorbereiten
* alle zwei Wochen treffen, wenn kein Bedarf dann kann eine Sitzung ausfallen

## konstituierende Sitzung

* bitte alle (!) eintragen
* Tim kümmert sich, dass sich alle eintragen

## Mail wegen Selbsstudiumszeiten

* Raum: Raumbedarf wird diskutiert und in Frage gestellt
* Denise schreibt eine Mail

## Kooptierung

* Denise erstellt Vorschlag für Satzung
