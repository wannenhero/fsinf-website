---


---

**anwesend:** Martin, Georg, Chris, Eric, Dominik, Verena (Gast), Denise (ab Punkt 3)
**Protokoll:** Dominik

#TOPs
* Raumfreischaltung
* Wahlen
* Modul "Aktuelle Trends der Informatik"
* Mails
* Sitzungstermine
* Newsletter

## Räume (Bestrebungen)
* Freigabe vor 17 Uhr nicht beabsichtigt
* nicht ideal, aber mehr können wir nicht machen
* weiteres Handeln nicht erfolgversprechend
* evtl am Semesterende erneut fragen (veränderte Umstände)
* Werbung in den Vorlesungen
	
## Wahlen
* Eric erstellt Doodle für die Schichten
* DH-Studierende bezüglich Wahlaufstellung fragen/darauf aufmerksam machen erscheint sinnvoll
* Werbung machen in den Fb-Gruppen (für die Wahl, nicht für die zu Wählenden)
* Denise macht Werbung in Vorlesungen
	
## Modul "Aktuelle Trends der Informatik"
* Kryptografie ist eine einmalige Vorstellung
* wiederholende Veranstaltungen evtl in den Katalog aufnehmen (wegen Anrechenbarkeit)
	* Prof. Maletti und Prof. Rahm diesbezüglich kontaktieren --> Martin

## Mails
* Dominik kümmert sich jetzt um den Job-Verteiler
* Radio-Interview zum Thema Bundeswehranwerbung
	* Weiterleitung der Anfrage im Newsletter
	* Studierende wenden sich an uns und wir geben das dann weiter
	* vorher Kontakt mit dem Zuständigen vom MDR --> Eric
* Hacking Event & Studienkommission der Informatiker in den Newsletter
* Antrittsvorlesung quantitative Korpuslinguistik auf Twitter/Facebook bewerben
	
## Sitzungstermine
* Christofer kann Donnerstags nicht
* Termine eventuell ändern auf "nur Mittwoch" --> vertagt
	
## Newsletter
* "Marketing-Slang" soll im Newsletter vermieden werden
