---


---

**Anwesende:** Eric, Martin, Georg, Verena, Denise
**Protokoll:** Denise

**TOP** : 
- Studienfahrt
- ADS
- Text Mining
- Digital Humanities
- Sommerparty
- Wahlen
- Mails


**Studienfahrt 5.8- 7.8.16**
- Mit oder ohne Bäckerservice? --> Mit Brötchen
- Unterkunft ist Gebucht
- Georg ist Ansprechpartner--> Anrufen, Schreiben kam vor einem Monat
- 30 Personen

**ADS**
- Mail an Prof. Stadler wg Treffen

**Text Mining**
- Abwarten
- Selbststudium? Begrifflichkeit?

**Digital Humanities**
- Mail an Senat (Denise)

**Sommerparty**
- FSR Mathe anschreiben

**Wahlen 31.5-1.6.16**
- Berliner 100 Halb Zucker , halb Puderzucker
- Regelmäßige Sitzungen, Studierende einladen
- Newsletter rausschicken
- Hochschulpolitischer Abend und/oder Wahlparty
