---


---

**Anwesend:** Sofia (Gast), Irena, Paul, Eric, Ahmad, Josy, Christopher, Georg, Moritz (Gast), Tim, Bianca (Gast)

**Protokoll:** Eric

TOPs:

* Wichtige Treffen bzgl Zulassungsbeschränkung und Studienerfolg
  * Treffen mit Scheuermann und Bogdan wegen Zulassungsbeschränkung
  * Treffen mit externem Gutachter, 19. April, 14 Uhr wegen Studienerfolg
* Introduction to Digital Philologies Prüfungsleistung
* Studienfahrt
* Sitzungstermin
* Wahlen
* FSR Angrillen
* Kollektivfestival
* Emails
* Orga
* MINT Frauen und FSFE
* Newsletter
* TOR-Exitserver
* Corporate Design
* Prüfungsvorleistungsmodalitäten

# 1.1 Treffen mit Scheuermann und Bogdan wegen Zulassungsbeschränkung

* Mail ist verschickt worden
* hin gehen: Christopher, Marc, Paul

* ergänzende Ideen zu Faktoren zur Zulassungsbeschränkung:
  * Projektwoche im Vorfeld statt Propädeutikum (oder zusätzlich) 
  * "Maschine-Learning"-Verfahren um potentiellen Studienerfolg zu ermitteln (in Zusammenarbeit mit Soziologen etc…) -> möglicherweise wissenschaftliches Projekt zur Evaluation und Paramteter, die für Studienerfolg sprechen
  * Selbsteinschätznungstest, damit Leute wissen womit sie sich in Zukunft beschäftigen werden, ohne Bestehenspflicht
  * Plätze verlosen
  * Wichtung der Noten der einzelnen Abitur-Fächer
  * Ausbildung im Bereich Informatik
  * Eingangsprüfung 

* Ziel sei es, Scheinstudent_innen zu verhindern
* wir übersenden alle Vorschläge und bitten um eine Einschätzung

# 1.2 Treffen mit externem Gutachter, 19. April, 14 Uhr wegen Studienerfolg

* zum Treffen gehen: Denise, Irene, Josy
* Anregungen für Treffen:
  * Einschreibe-Praktik ist unübersichtlich (insb. bei Bio-Informatik)
  * Workload in Anfangssemestern (zu) hoch
  * transparente Gestaltung der Studiengangsbeschreibung: was lernt man (nicht)
  * Studienerfolg DH:
  *  - Romantisierung der Studienbeschreibung
  *  - Studienangebot an sich dürftig (wenige DH Module)
  *  - unübersichtlich
  * praktische Übungen weiterführen
  * offener Informatikraum

# Introduction to Digital Philologies Prüfungsleistung

* aktuelle Version der Dokumente ist nicht zugänglich
* wir unterstützen das Anliegen als FSR

# Studienfahrt

* Abstimmt zur Anreise: wir fahren mit dem Bus statt mit der Bahn: 8/0/1

# Sitzungstermin
* wir doodlen neu und werten das zum nächsten treffen aus, freitag 12 uhr, tim macht das

# Wahlen

(vertagt)

# FSR Angrillen

(vertagt)

# Kollektivfestival

(vertagt)

# Emails

(vertagt)

# Orga

(vertagt)

# MINT Frauen und FSFE

(vertagt)

# Newsletter

(vertagt)

# TOR-Exitserver

(vertagt)

# Corporate Design

(vertagt)

# Prüfungsvorleistungsmodalitäten

(vertagt)

