---


---

**Anwesende**: Paul, Dominik, Chris, Denise, Eric
**Protokoll**: Dominik

# TOPs
* Seminar Parallelverarbeitung
* Weihnachtsfeier

## Seminar Parallelverarbeitung
* Vorschlag, das Thema einzustellen
* Paul schreibt "Abschluss"-Mail an Prof. Middendorf

## Weihnachtsfeier
* Raum ab 15 Uhr reserviert
* evtl. Vorlesungen besuchen, um Werbung zu machen
* Budget für Alkohol max. 100€ --> 5/0/0
* Budget für den Rest 100€ --> 5/0/0
* Alkohol: 1€ pro Bier/Becher Glühwein
* Rest Solikasse

## 33C3
* wenige Anmeldungen, daher Umverteilung der einzelnen Förderungen
* **BESCHLUSSÄNDERUNG (vom 03.11.16):** 1000€ gleichmäßig verteilen, aber maximal 100% Erstattung pro Teilnehmer 4/0/1
