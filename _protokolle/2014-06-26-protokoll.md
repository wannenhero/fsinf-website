---


---

* Sitzungsleitung: Sarah
* Protokoll: Fabian
* Anwesende: Alrik, Nicole, Martin, Kasimir, Fabian, Sarah

##TOP 1: Öffentlichkeitsarbeit
 * Wahl RAS
 * Sommerparty
 * Frist Eignungsfeststellung und Deutschlandstipendium
 
##TOP 2: Sommerparty
 * am 01.07. um 17:00 Uhr im StuRa werden Aufgaben und Zeiten/Prioritäten/Verantwortlichkeiten verteilt

*Kasimir ist gegangen*

##TOP 3: Arbeitskreis Gesellschaftskritik
 * Antrag auf Projektförderung:
 	* Abstimmung: 0/0/5 --> abgelehnt

##TOP 4: Nächster Sitzungstermin
 * Donnerstag 10.07. 13:00 Uhr
 * es lädt ein: Fabian
