---


---

Anwesend: Bianca, Natascha, Moritz, Georg, Paul
Protokoll: Paul

#### Tagesordnung

1. Orga
2. Wahlen
3. FSR der Zukunft

## 1 Orga
* 35C3
    * Überweisungsträger sind ausgefüllt
    * Geld von den FSRä muss eingesammelt werden
    * Moritz kümmert sich drum
* Sitzung in den Semesterferien
    * Wir brauchen eine beschlussfähige Sitzung im März
    * Paul schlägt Doodle vor, und würde das auch machen

## 2 Wahlen
* Wahlleitung hat geschrieben
* Dokumente zur Wahl müssen ausgefüllt werden
* Frist 29.03
* brauchen beschlussfähige Sitzung
* Wahllokal? Wahlhelfer? Wahlvorstand? Sitzzahl?
* Christofer hat sich schon für Wahlvorstand bereiterklärt
* Bianca, Georg, Natascha würden das auch machen
* Mathe fragen ob man zusammen für Aufstellung werben will
* Paul schreibt Mail (auch wegen Klopapier und ob Platz im Raum ist)
* Lieber im Augusteum 5 wählen
* Texte auf unserer Seite zu den Kandidierenden wären gut, darauf kann man zur Auswahlhilfe verweisen
