---


---

* Anwesend: Marc (Gast), Christofer, Sofia (Gästin), Gero (Gast), Eric, Irene, Fabian (Gast), Denise, Moritz (Gast), Georg, Paul, Verena (Gästin)
* Protokollierend: Paul

## 1 Strategie nach Studiengangsevaluationen
* D: kann sich vorstellen nach Veranstaltungen direkt zu fragen und evaluieren
* C: War die Person bei Einführungsveranstaltungen
* Mehrere Diskussionen: letzte Evaluation, anderes für neue Evaluation, Reaktion auf letzte Evaluation
* versch. Vorschläge: Stammtisch, Sprechzeit etc.
* Meinungsbild: Dinge sollen verbessert werden, nächster Fragebogen soll verbessert werden
* D: kritisiert Kritik an Repräsentation, Repräsentation benötigt außerdem Menschen die entsprechende Arbeit tun
* D: für Sprechzeit, eventuell auch Stammtisch, sagt wir haben einiges getan an Angebot,dezidiert Terminkalender ziemlich stacked
* Fabian: Bei Fragebogen ausgeweitetes explizites Feedback für Website, Git, Veranstaltungen etc.
* E: Kritisiert Sprechzeit, es kommt erfahrungsgemäß niemand
* E: Will Fragebogen nach Veranstaltungsfeedback und Feedback zu unserer tatsächlichen Arbeit trennen
* Gero : Dokument was wir tun und sind wäre gut
* Denise: Ende der Diskussion, Website soll aktualisiert werden, mit Frau Braun abklären was für Fragen gestellt werden könnten
* Denise geht zu Frau Braun

## 2 Orga
* Paulinumseröffnung: Denise gibt Bescheid wieviele kommen

### 2.1 Studieneingangsphase und NC
* Christofer und Paul waren bei Neumann und Bogdan
* NC:
 * NC wird kritisch gesehen
 * Bogdan: custom NC nur möglich wenn wir selbst auf Rektorat zugehen
 * Informatik hat 130 Menschen Kapazität, 319 Neuimmatrikulierte
 * Bogdan trägt unsere Anmerkungen weiter
 * Meinung der Fachschaft ist kundgetan
* Diskussion zu NC:
 * M: sagt wir schieben uns den schwarzen Peter zu wenn wir NC einführen
 * S: Sofia sagt Informatikkenntnisse dürfen in keinem Test sein
 * weiteres per Verteiler
* Studieneingang:
 * Gespräch mit Bogdan und Neumann
 * Wir sehen Zeckzers Vorschlag kritisch
 * Neumann hat sich flächendeckende Einführung gewünscht
 * Bogdan hat Modulentwurf noch nicht gelesen
 * Handlungsbedarf: Weiterbesprechung in der StuKo, Meinung erst dafür nötig
 * Denise schreibt Anmerkung per Mail

### 2.2 Master Digital Humanities
 * Christofer: Bogdan hat Studiendokumente für Master DH rumgeschickt, sieht keine Probleme, schickt Dinge über den Verteiler

### 2.3 Prüfungsausschuss
 * Paul will vorgeschlagen werden
 * kein Widerspruch, Paul schreibt Mail

### 2.4 AK Lehramt
 * niemand gefunden, nix passiert, muss gesucht werden

### 2.5 Newsletter
 * Werbung für AK Lehramt
 * A410 als Ruhepool
 * NC rauslassen
 * Fakultätspolitik erst nach Fakrat, nur Infos über abgeschlossene Verfahren

### 2.6 Studienfahrt 2018
 * Suche nach Unterkunft steht aus
 * Georg fragt wer sich den Hut aufsetzen möchte
  * keine Person antwortet
 * Moritz möchte mit in die Telegramgruppe
 * Denise hetzt gerne Menschen Dinge zu tun, will aber selbst keine Arbeit übernehmen

## 3 Evaluation
 * Eric und Tim wurden eingeteilt, haben noch nichts übernommen

## 4 Ruhepool
 * Philipp hat etwas dazu ins git geschrieben, wir sollen weniger besuchten Pool auswählen
 * Frage nach Zeitfenster?
 * Denise: 24/7 gedacht
 * Ursprüngliche Anfrage: In einem Pool soll Ruhe herrschen
 * Frage welcher Pool:
  * Für Diskussion siehe letztes Protokoll (es ist dasselbe)
  * Windows Pool off the table sagt Fabian
  * Fabian Heusel kümmert sich um Ausschilderung
  * FSR macht Werbung
  * 410 wird ohne Einwände bestimmt

## 5 Finanzvollmachten
* Ruben hat Termin mit Sparkasse, neue Infos kommen im Lauf der Woche
* vertagt bis neue Infos kommen

## 6 Emails

### 6.1 Übernachtung 34C3
* Emails kamen im Verteiler wohl nicht an
* Eine Person will mit ihrer rzb beim 34C3 in Leipzig übernachten
* Fabian fragt ob das unsere Aufgabe ist?
* Nein
* Denise sagt der Person ab

### 6.2 Hardwarepraktikum
* Fabian sieht Handlungsspielraum
* D: neue Infos bei Neumann einholen?
* D: Geht zu Neumann, schreibt dem Betroffenen

## 7 Betriebssysteme
* C: Freiheit der Lehre, entfällt.

# 8 Agenda
* vertagt, folgende Woche neue Erkenntnisse
