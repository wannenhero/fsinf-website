---


---

Anwesende: Ahmad, Christofer, Paul, Georg, Natascha, Moritz, Bianca (Gast), Niclas (Gast)
Sitzungsleitung: Christofer
Protokollführer: Moritz


#### Tagesordnung

## 1 Orga
* Newsletter - Stand?
	- Natascha trägt Infos ins Wiki ein
	- Abstand 2 Monate?
	- Next Newsletter wie Protokolle im Git?
* Stand Website von Georg und Paul?
	- Georg und Paul wollen sich treffen
* Whiteboardmarker werden langsam alle, Tesafilm ist auch alle, Heftklammern alle, Geld für Büromaterialien beschließen?
	- Abstimmung 25€: 6/0/0
* wiki aktualisieren? (letzte Einträge gefühlt alle von 2017, sowie fehlende Protokolle)
	- Niclas, Bianca, Moritz und Christofer setzen sich hin
	- Christofer meldet sich
	- Protokolle bitte trotz kaputter Website ins Git hochladen

## 2 StuRa Entsendung
* Es gibt Interessierte für StuRa-Ersatzentsendungen
	- Paul und Alina wollen dauerhaft ersatzentsendet werden, um bei fehlen einspringen zu können
	- Abstimmung: 5/0/1

## 3 Wahlen
* Werbung für Neuaufstellungen Stand?
	- In Vorlesungen gehen (MuP2 und Berechenbarkeit)
	- Paul und Christofer kümmern sich
	- Öffnungszeiten FSR-Büro: 14-16 Uhr Moritz
* Hat jemand unser Wählerinnenverzeichnis gesehen?
	- evtl. bei Fabian Heusel, Christofer fragt
	- Nachtrag: ist aufgetaucht
	- ansonsten bei Wahlamt neues besorgen
* Änderung Wählerverzeichnis Beschließen?

## 4 Finanzen
* Wie weit sind die 35C3 Auszahlungen?
	- Überweisung vom StuRa sollte bald da sein

## 5 Vernetzungstreffen
	- Themen waren Bundesfachschaftentagungen, Wahlen
	- BuFaTa: sollten mal hinfahren
	- Interessenten: Bianca, Moritz, Natascha

## 6 Emails
* Theodor-Litt-Preis, Transfer-Preis und Wolfgang-Natonek-Preis werden bis Juli ausgeschrieben
	- Vorschläge: Maletti, Potthast
	- noch Zeit zum nominieren
* AK Lehramt am 09.05
	- Paul will hin
* Ars legendi-Preis
	- Bianca hat Erfahrungen
	- Stellungnahme schreiben
	- How-to Empfhlungsschreiben im Wiki
	- zur übernächsten Sitzung (13.05.) bringt Bianca Punkte mit
* Tim netctl
	- Moritz fragt beim URZ
	- auf Service-Seite reinstellen
	- twittern
* Probleme Leistungsanerkennung Modul SSFPL

## 7 Anwesenheit bei MedInfModulen und Vertiefungspraktikum DH
* Christofer und Paul gehen da demnächst hin (unverändert)
	- vertagt

## 8 Logo
* Antwort vom Roef kam, kurz drüber sprechen
	- Treffen mit LayouterIn
	- soll es FS Inf oder FSR Inf sein?

