---


---

anwesend: Denise, Manuel, Friedrich, Fabian, Eric, Georg

Protokoll: Manuel

- ADS Gespräch 
    - 20.04.2016
    - Teilnehmer: Denise, Eric, Martin
    - Martin als stiller Teilnehmer
    - Durchfallquote in ADS1 waren nicht dieses Jahr nicht hoch
    - Prof. Heyer müsste Studienordnung ändern, wenn er so weiter machen will. Wir wären dagegen (StuKo/LSP)
- Studienfahrt
    - Reservierung ist raus. Bestätigung sollte Mitte Mai kommen. Sollte alles klar sein
    - Sollten Infos in Newsletter schreiben. Evtl. einfach Vorlage von letztes Jahr nehmen.
    - Orga-Team
        - Georg, Danise, Manu, (Eric)
    - Eric schaltet Anmeldung um. Team soll sagen, wann freigeschaltet werden soll
- Studien-Infotag
    - Samstag sollte jemand im Büro sein
    - 10:00-15:00 Uhr
        - 12:00-15:00 Friedrich
        - 10:00-12:00 evtl. Vero
- Sommerfest
    - Klinken uns bei Mathe ein
    - Denise regelt
- Wahlen
    - Wer lässt sich wieder Aufstellen?
    - FSR-Liste Abgabe 03.05
    - UNI-Gremien-Liste Abgabe 24.04.
    - Krapfen als Lockmittel
    - Werbung kurz vorher
- Newsletter
    - Themensammeln
        - Studienfahrt
        - SKILL Flyer
        - Wahlen
- Sitzungen
    - ab jetzt wöchentliche Sitzung
    - Denise macht Doodle
- Dozentenmail
    - Friedrich schreibt sich was auf
- HEP 2020
    - Termin mit Felix ausmachen. Evtl. nächste Sitzung oder nächsten Dienstag
- Sonstiges
    - Wollen wir Alex in den Verteiler?
    -ja
