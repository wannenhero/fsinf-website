---


---

anwesend: Manuel, Christofer, Denise, Martin, Georg, Eric
Protokoll: Eric

# TOPS

* Google Hash Code
* Gruppenarbeitsräume
* 33c3
* Mails
* gitlab
* Moderation der Mails
* KritNawis
* Fahrrad-Parksituation
* Weihnachtsfeier
* Aufgabenschichtplan
* FSR-Wahlen
* Studienfahrt
* letzte Sitzung des Semsters

# Google Hash Code
* Person hat das letztes Jahr an der Uni durchgeführt
* lief in den Pools
* würde das dieses Jahr wieder machen
* Wunsch das wir einen Pool reservieren
* 23.02. ab 18 Uhr
* wegen Bewerbung fragt Christofer nach

# Gruppenarbeitsräume
* Denise hat Vorschläge für Standorte für Tische im Haus
* vorstellbar: 4. Stock neben Windows-Pool, 7. Stock (vor P702)
* Denise schreibt eine Mail an Scheuermann

# 33c3
* Denise schreibt den beiden Leuten ohne Ticket eine Erinnerung
* Eric schickt Mail-Adressen
* Auswerungstreffen findet nicht statt

# Mails
* Anfragen wegen Altklausuren
  * sind noch nicht beantwortet
  * allgemeiner Verweis auf das gitlab
  * Denise antwortet
* "mitStudieren"
  * Antwort: sollen uns gern einladen, wenns Möglich ist kommt jemand
    vorbei
  * Georg schreibt die Mail

# gitlab
* Aufruf zu Semesterende via Newsletter
  * Apell Unterlagen und Gedächtnisprotokolle hochzuladen
  * kann auch anonyisiert erfolgen
  * Eric kann Dinge hochladen
* Denise scannt Altklausuren

# Moderation der Mails 
* Wer macht das eigentlich?
* Appell: mindestens einmal (besser zweimal) wöchentlich moderieren

# KritNawis
* waren schon da
* Martin schreibt ihnen

# Fahrrad-Parksituation
* Fahrräder stehen auf Rampen, Geländern, Behindertenparkplätzen etc
* Vorschlag: Mail an Gleichstellungsbeauftragen, wann das etwas unternommen wird
* evtl. gemeinsam mit Holger Wuschke und Fakultätsrat und FSR Mathe
* Martin kümmert sich

# Weihnachtsfeier
* 40l Glühwein und 2 Kästen Bier sind alle geworden (plus Punsch)
* Chor kam gut an
* vielleicht beim nächten Mal größeren Raum

# FSR-Wahlen
* Erhöhung der Sitzzahl?
  * Newsletter mit Abfrage an Interesse am FSR
  * mindestens 4 Personen aus dem aktuellen FSR würde nochmal antreten, 2x vielleicht
  * Denise schreibt Text

# Studienfahrt
* soll im Herbst stattfinden
* bisher ist nichts passiert
* Christofer, Denise, Georg, Manu und Verena beteiligen sich
* Treffen zur Vorbereitung: 20.01. ab 11 Uhr

# letzte Sitzung des Semsters
* wird am 2. Februar sein
