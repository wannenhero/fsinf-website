---


---

**Anwesend:** Dominik, Martin, Eric, Christofer, Denise (Skype)
**Protokoll**: Chistofer 

#TOPS
* Behindertenparkplätze
* Drucker
* Wiki
* Wahlen
* Studienfahrt
* Ersti Beutel
* GitLab

## Behindertenparkplätze

* Abstimmung, dass H. Wuschke sich auch im Namen des FSR um die aktuelle Parkplatzsituation kümmert
* Abstimmung: 5/0/0

## Drucker

* Demnächst neuen Drucker kaufen
* Anforderungen: Duplex, Laser, Netzwerk Drucker mit Kabel, CUPS kompatipel
* Bis 250€, Abstimmung: 5/0/0
* Dominik sucht nach Vorschlägen

## Wiki

* Vorschlag von Eric: Das Wiki in das gitLab überführen
* Wird vertagt

## Wahlen

* Denise übernimmt die Wahlschichteinteilung
* Abstimmung, dass Denise 150 Pfannkuchen bestellt
* Abstimmung: 5/0/0

## Studienfahrt

* Christofer kümmert sich bei Carl&Carla um den Transporter für die Studienfahrt

## Ersti Beutel

* Sind mit Fotos von der Übergabe der Beutel nicht einverstanden
* Beutel bestellen nur, wenn die Fotos nicht notwendig sind
* 300 Beutel anfragen
* Martin schreibt Mail

## GitLab

* Repositorys für alle Module anlegen
* vertagt
