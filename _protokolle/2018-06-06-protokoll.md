---


---

**Anwesend:** Marc, Denise, Christoph, paul, Christofer, Irene, Georg, Lukas(Gast), Nadine(Gast), Adrian(Gast), Moritz(Gast, verspätet), Sofia(Gast,verspätet)

**Sitzungsleitung:** Denise

**Protokoll:** Christoph


## Tagesordnung

1. DH: Linguistische Informatik
2. Ramadan Fest Förderung
3. Studienfahrt
4. Lehrberichte
5. StuKo
6. DH Master
7. Informatikerinnen Stammtisch via Code Girls bewerben
8. Emails
9. TI2 und Prüfungsvorleistungen
10. Wahlen Auswertung
11. Corporate Design

# 1 DH: Linguistische Informatik
* Wahrscheinlichkeitstheorie in Ling Inf erforderlich ---> Problem in der Prüfung
* DH'ler weisen Inhaltliche Defizite im Vergleich zu Kernfach-Informatik-Studenten auf 
* Im Lehrplan der DH'ler sonst keine weiteren Mathe Module
* Sepereate Prüfung (Wahlbereich) erwünscht
* Christofer,Irene,Lukas,Adrian sprechen nochmal mit Herr Hayer um eine Lösung für Aktuelles Semester zu finden


# 2 Ramadan Fest Förderung
* 01.07 Tag des antimuslimischen Rassismus - Veranstaltung (Zuckerfest)
* Förderung in Höhe von gewünschten 30€ 6/0/0 

# 3 Studienfahrt
* Pro Teilnehmer 10€ - insgesamt 500€ Förderung (Infos vom HHA)

# 4 Lehrberichte
* wie organiseren / wer hat Interesse
* Arbeitskreis gründen
* BioInf: Sofia
* DH: Marc, Irene, Lukas
* Informatik B.Sc.: Paul, Denise, Christofer 
* Informatik M.Sc.: Denise, Christofer
* zu jedem Fach eine Stellungsnahme schreiben und dann zusammen mit der Mathematik in den Fakultätslehrbericht


# 5 StuKo
* Infos aus der StuKo zu geplanten Änderungen der Modulbeschreibung 
* betrifft Ti2, Formale Modelle, Fortgeschrittene Computergrafik und Kreativität und Technik
* Computergrafik - Prüfungsvorleistung leicht Angepasst
* Kreativität und Technik - Vorlesung und Seminar, Ziele des Moduls leicht angepasst
* Formale Modelle - Anpassung der Prüfungsvorleistung
* Ti2 - komplette Ziele überarbeitet, Protokolle und 5 Versuche 
* Christofer leitet Mail an FSINF weiter um nächste Woche (14.07.) einen gemeinsamen Endbericht zu verfassen

# 6 DH Master
* Einschreibung nur zum WS möglich 
* Christofer spricht mit Herr Neumann um Gründe zu erfahren

# 7 Informatikerinnen Stammtisch via Code Girls bewerben, Do 14.6
* https://www.facebook.com/events/216396842297133
* vertagt

# 8 Emails
* 70 Jahre Israel: Eric gibt uns Update
* vertagt

# 9 TI2 und Prüfungsvorleistungen
* Mail des Justis + Mail von Paul zu Prüfungsvorleistungen geben Anlass dies zu besprechen
* Wir wollen aufgrund der Diskussion vom 27.3 allg. über Prüfungsvorleistungen diskutieren
* vertagt

# 10 Wahlen Auswertung
* vertagt

# 11 Corporate Design 
* Meike von Mathe mal fragen
* vertagt
