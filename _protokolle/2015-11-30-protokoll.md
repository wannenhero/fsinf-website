---


---

anwesend: Friedrich, Manuel, Eric, Nico
Protokoll: Friedrich

## Topics
1. Fachschafsfahrt
2. Gitlab - Mail an Dozenten
3. Weihnachtsfeier
4. Sommerparty

## Fachschafsfahrt
* Nico hat sich erkundigt und stellt diese beim nächsten Meeting vor
* Neues Kriterium sollte ein See oder Fluss in der Nähe sein
* Orgateam sollte gebildet werden

## Gitlab
* sollte in einer größeren Runde besprochen werden

## Weihnachtsfeier
* nach wie vor 10.12.
* Orgateam hat noch nichts präsentationsfähiges
* wichtig sind auch unalkoholische Getränke zu haben

## Sommerparty
* soll in den folgenden Jahren weitergeführt werden
* Beschluss um die Organisation muss vertagt werden
