---


---

anwesend: Friedrich, Nico, Nancy, Denise, Verena, Eric, Fabian

# TOPs:

* Logo
* Erstifahrt
* Propädeutikum
* Vorstellung
* Kneipentour
* Short-URL
* Mails

# Logo

* Denise' Nachbarin würde Logo erstellen: Denise spricht mit ihr und bringt groben Entwurf mit
* Kosten: VB sind 50€ bis 150€
* Anforderung: Vektorgrafik, monochromatisch, mit Bezug zu Informatik oder abstraktes Logo

# Erstifahrt

* FSR Mathe informiert sich zu Anreisekosten
* Führung in Schloss und Brauerei (Meißen) sind möglich
* Kosten Brauereiführung 8€ pp inkl. Verkostung + Imbiss
* Besuch der Brauerei einstimmig beschlossenBesuch der Brauerei einstimmig beschlossen
* Brauereiführung soll 15 Uhr beginnen
* Schlossführung soll nicht statt finden
* Denise fragt Felix wegen Steuervergünstigungen für Gremien bei Metro bei Metro
* wir mieten den Grill für 5€
* Platzverteilung nach Windhundverfahren
* Plätze werden zu 45 geändert
* "Alkohol-Flat" wird so nicht praktiziert, Strichliste für Alkohol
* Mails werden bilingual verschickt
* Bewerbung ab Montag per Twitter, Homepage und im Propädeutikum
* Sondernewsletter zur Studienfahrt von Denise

# Vorstellung des FSR im Propädeutikum

* Nancy und Fabian gehen am Montag ins Propädeutikum und verteilen Dinge
* Details zu Ort/ Zeit sollen ins Wiki
* ggf. müssen Zettel nachgedruckt werden

# Kneipentour

* Gutscheine der MB: Fabian, Nico, Friedrich kümmern sich
* Kneipen: Fabian, Nico, Friedrich suchen 24.09. welche raus und schlagen vor, damir dann reserviert werden kann
* Spiele: Fabian sucht etwas heraus

# Short-URL

* Option eine kurze URL ist möglich, z.B. fsinf.uni-l.de
* finden alle gut, Fabian kümmert sich perspektivisch darum

# Mails
* Einführungsmail Master-Student
  * es gibt Campusführungen und Bibliotheksführungen
  * Hinweis auf Newsletter, Studienfahrt, Kneipentour
  * Nancy nimmt Kontakt auf
