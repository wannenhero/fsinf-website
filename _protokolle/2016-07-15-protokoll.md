---


---

# TOPs
* Berufungskommissionen
* Studienfahrt
* gitLab
* Mails
* Erstiwoche/ Erstitüten
* kost. Sitzung

# Berufungskommissionen
* Xuan B. wird mit 5 Stimmen in die Berufungskommission Softwaretechnik entsandt

# Studienfahrt
* 8 Anmeldungen im Moment
* bei geringer Beteiligung ist ein finanz. Minus zu erwarten
* Anreise mit Privat-PKW von Georg und Mietwagen ausreichend bei 8 Personen
* Mietwagen bei Teilauto über den StuRa, Budget: 200€ (Beschluss: 5/0/0)
* wir warten noch eine Woche, bis wir eine Entscheidung treffen, ob die Fahrt evtl. nicht stattfindet

# gitLab
* es soll ein Klausuren-Account angelegt werden
* als Mail-Addresse können man die Google-Addresse verwenden

# Mails
* auf Mail wegen Computerselbsthilfewerkstatt sollte geantwortet werden -> Denise

# Erstiwoche/ Erstitüten
* Christofer kümmert sich hauptverantwortlich um die Kneipentour
* Tendenz geht dazu, nicht in die MB zu gehen
* nach der Einführungsveranstaltung soll etwas stattfinden:
 * evtl. Uni zeigen/ Führung -> Christoph, Denise
 * (gitLab zeigen geht nicht, weil die Leute noch keinen Account haben)
 * Linux-Install-Party am Abend (Paul fragen)?
* Beutel: 120 Stück bestellen

# kost. Sitzung
* http://doodle.com/poll/xgidwsk8d7xcvxx9
* wird am 8. oder 9. August
* nächste Sitzung ist diese konst. Sitzung
