---


---

Anwesend: Denise, Manuel, Verena, Fabian, Georg, Martin

Protokoll: Denise

## Tagesordnung:
	1. Kneipentour- Auswertung
	2. Studienfahrt
	3. Email bzgl Modul bei Prof. Lindemann
	4. Exkursion Chaos Communication Congress
	5. Weihnachtsfeier
	6. Sitzungstermine
	7. Inventaränderung

## 1. Kneipentour- Auswertung
* mehr Teilnehmer als im Vorjahr (ca. 60)
* nächstes Jahr: größeres Schild
* Kneipen: "Jet" nächstes Jahr unbedingt wieder (super Konditionen!); keine Umwege; MB: Location überdenken (nächstes Jahr nicht mehr)
* Anschluss an die O-Woche der WiWis?

## 2. Studienfahrt- Auswertung
* mehr Orgaleute von Anfang an, geregeltere Arbeitsaufteilung 
* Anfahrtsplan + Karte mit in die letzte Mail
* nächstes Jahr mit einer Geisteswissenschaft 
* Location und Datum überdenken
* Glühwein ist bei kühlem Wetter gut
* Gremienworkshop kam gut an

## 3. Email bzgl Modul bei Prof. Lindemann

Mail schreiben:
4/1/1

## 4. Exkursion Chaos Communication Congress
* 27.-30.12
* Denise spricht mit Felix wegen Ticketzuschuss

Nachtrag, 26.10: Ticketzuschuss im Rahmen der 1/3-Regelung möglich

## 5. Weihnachtsfeier
* **10.12., 18 Uhr, Raum P901**, Fabian kümmert sich um Raumreservierung
* Weihnachtsmusik
* Gebäck, Glühwein, Solikasse
* Rahmenprogramm (Nico und Friedrich fragen)
* Einkaufen (Einkaufsliste rechtzeitig schrieben)
* Glühweintopf beim Stura ausleihen
* Dekoration

## 6. Sitzungstermine
* alternierend alle zwei Wochen
 * montags (A-Woche) 17 Uhr
 * mittwoch (A-Woche) 15 Uhr
* nächste Sitzung: Montag, 2.11, 17 Uhr
* Google-Kalender ist aktuell, da kann man im Zweifel nachschauen

## 7. Inventaränderung
Sebbo Karton mit Watte schenken
6/0/0
