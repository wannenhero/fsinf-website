---


---

**Anwesend**: Fabian, Eric, Verena, Martin, Friedrich, Nico, Manu, Georg, Denise

Protokoll: Denise

# Konstituierende Sitzung
* **Sprecher:**
Nico 8/0/1

* **Stellv. Sprecher:**
Fabian 8/0/1

* **Finanzerin:**
Denise 8/0/1

* **Stellv. Finanzer:**
Friedrich 8/0/1

* **Stura-Entsandte:**
Fabian und Eric 

	8/0/1      

* **Studienkommission :**
Martin
Verena
Nico 

	8/0/1


**Beginn der Amtsperiode: 1.10.2015**

### **Sonstige interne Aufgabenbereiche und Regelungen:**

* **Partybeauftragte:**
vertagt auf nächste Sitzung


* **Newsletter:**
Denise 8/0/1

* **Email-Moderation**:
Verena 8/0/1

* **Transponder:**
-Thomas → Nico
-Sebbo → Manu
-Nancy → Friedrich






#### Sonstige Themen:

* SAP: Kein Interesse an Mitgestaltung, da Anfang des Semesters viele Veranstaltungen zu planen sind

* Webseite und Klausuren-Git

**Nächste Sitzung: 3.9.15, 14 Uhr**
