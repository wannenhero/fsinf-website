---


---

Anwesend: Eric, Irene, Georg, Christofer, Paul, Bianca, Natascha, Moritz, Marc
Protokoll: Paul

## TO
1. Orga
2. 35C3 Antrag
3. Theodor Litt Preis
4. TI
5. Emails
6. Ehumanities
7. StuKo und FakRat Bericht
8. SWT Vorlesung und Offener Informatikraum
9. Tor Exit Node
10. Neues zu P-402 Zugang	
11. 35C3 Bettenbörse
12. Forglühen für Fahrradfreunde
13. Der FSR der Zukunft 

## 1 Orga

#### 1.1 Bezahlung Studienfahrt
* bitte bezahlt die Beiträge bis Ende nächster Woche!

#### 1.2 Finanzen
* Brauchen Überblick über Geld
* Moritz und Paul sollten nochmal mit Philipp sprechen

#### 1.3 WeiFei
* Schichtplan ist rumgeschickt, bitte tragt euch ein
* Bitte mehr als eine Schicht, da es viele Aufgaben gibt

## 2 35C3 Antrag
* Kooperationsvertrag mit Mathe noch nicht aufgesetzt
* sollte man nächste Woche machen, jetzt Mail schreiben
* Eric schreibt ihnen jetzt eine Email
* Antrag wurde im Plenum diskutiert
* Bekommen 3000€ aus dem Hilfsfond Fachschaften
* RAS hat Geld angeboten
* konkretes Bezahlverfahren? Müsste man bei Ruben nachfragen
* Sticker von cooler Idee abhängig machen

## 3 Theodor Litt 
* Preis wird am Montag verliehen
* Was kritisieren wir?
* Nicht persönlich, sondern Preisvergabe
* M: wir müssen eventuell auf Einzelperson eingehen
* C: Fachschaftsräte haben diverse qualifizierte Leute nominiert, und den Preis bekommt am Ende DH
* I: Die Lehre in DH ist weder hervorragend noch innovativ
* M: In dem Gremium sitzen viele Leute, wir können unsere Informationen veröffentlichen
* P: DH ist halt doof
* I: Nur Köntges hat gute Lehre gemacht
* P: Heute nach der Sitzung PM schreiben
* C: Sollten andere Person in Erfahrung bringen um die nicht zu diskreditieren
* M: Wir sollten trotzdem kritisieren was wir kennen

## 4 TI 
* Skript können wir löschen
* Praktikums- und Übungsaufgaben sind sinnvoll um Studilösungen zu verstehen
* Abstimmverfahren: Erst Skript, dann Rest abstimmen
* Lieber noch mit Gedächtnisprotokollen drohen und es erstmal drin lassen
* eventuell wird beim Institut angefragt warum es Urheberrechtsverletzungen hostet
* Da sind aber Scheuermann und Co weisungsberechtigt
* Abstimmung:
  * Nur Skript löschen: 5/2/0
  * Auch Praktikums- und Übungsaufgaben löschen: 0/4/3
* Die Skripte werden gelöscht
* Paul aktualisiert Denise Mailvorschlag und sendet ihn heute Abend

## 5 Emails


## 6 Ehumanities
* Studis haben sich an Irene gewandt weil Crane Datenschutz im Seminar schlecht handhabt
* Außerdem wurde Qualität der Lehre bemängelt
* Hat dieselben Vorträge gehalten wie letztes Jahr
* C: Würde das in der StuKo bei der DH Überarbeitung ansprechen, eventuell direkter Hinweis dass sie Datenschutzschund betreiben
* In Zukunft sollten wir Daten nicht einfach kreuz und quer teilen, selbst im Team
* Nach StuKo Termin Austausch mit Crane
* nächste Sitzung Leute aquirieren

## 7 StuKo und FakRat Bericht
* FakRat:
* Lehrbericht ist durch FakRat durch
* Es sollte was aus der StuKo besprochen werden
* im Lehramt werden Sachen geändert
* Rahms neuer Master wurde noch nicht im FakRat besprochen
* Droste ist nächstes Jahr Erasmusbeauftragter
* Mathe will neuen Masterstudiengang Mathematische Physik einfüphren
* StuKo:
* Neumann, Prohaska, Bogdan, Josy, Marc, Christofer
* Sie haben über Data Science Master gesprochen
* Der Studiengang klingt irgendwie gut
* Neue Datenbankmodule sind alte Vorlesungen neu zusammengewürfelt
* kein einziges neues Modul
* Neu ist ein importiertes Statistikmodul aus der Wirtschaft
* Rahm hat sich nicht wirklich abgesprochen
* Unklare Zielgruppe
* Es fehlen Datenverarbeitung, Sicherheitsbelange
* StuKo schiebt das erstmal auf
* nächste Woche Treffen für Bachelorstudiengänge
* StuKoProtokoll an Verteiler
* Es soll neue Statistikmodule geben, war einstimmiger Tenor

## 8 Softwaretechnik Vorlesung und Offener Informatikraum
- Denise und Moritz haben den Hut auf
- Denise will Email schreiben
- kommt in Zukunft

## 9 Tor Exit Node
- Paul und Eric kümmern sich
- nächste Woche steht der Server

## 10 Neues zu P-402 Zugang
* Irene hat noch keine Antwort
- vertagt

## 11 35C3 Bettenbörse
* Bettenbörse existiert (noch) nicht
* Ins Leben rufen und bewerben?
* Paul findet das wäre zu viel
* Wiki ist noch nicht online
* Falls es Ticket oder Schlafplatzbörse im Wiki gibt können wir das gerne bewerben

## 12 Forglühen für Fahrradfreunde
* Veranstaltung/Demonstration für Radverkehr auf der Jahnallee
* Veranstaltung macht auf Forderung nach gesondertem Radweg in der Jahnalle aufmerksam
* veranstaltet vom Radrevier
* Eric würde das per Twitter oder so mal teilen lassen
* Moritz darf das twittern

## 13 Der FSR der Zukunft
* Moritz ruft dazu auf das Wiki zu aktualisieren
* man sollte zusammentragen was veraltet ist
* Paul schlägt Agenda AG vor
* Christofer schlägt vor dass nach den nächsten Klausuren ein paar Leute zusammensetzen
* Irene findet den Vorschlag gut
