---


---

**Anwesend:** Christoph, Ahmad, Christofer, Bianca (Gast), Denise (kam später)
Marc(Gast), Sofia(Gast), Vanessa(Gast), Josy, Paul, 
Moritz(Gast), Isabell(Gast), Georg, Eric, Philipp, 
Anna von "Leipziger Schriften"(Gast)

**Protokoll:** Philipp

# Tagesordnung 23.01.2018 - 15:30 Uhr

## 1 Orga
* StuKo: kurzer Bericht von Josy und Christopher

### 1 Förderung Leipziger Schriften  
* Abstimmung Förderung in Höhe von 100€ : 6/0/2
  Philipp übernimmt das Finanzielle

### 2 Emails
* TI-Mail: FSR kann nicht helfen
	Modul-/Prüfungsbeschreibung soll präzisiert werden -> Agenda '18?
	Philipp kümmert sich um Schriftverkehr
* Mail medizinische Informatik: Terminvorschlag + Treffen, Christofer schreibt Mail,
	Eric knüpft Kontakt mit Jake

#### 2.1 Infos(Josy)
* Lehrberichte: Eric kümmert sich und gibt Antwort im Verteiler
* ADS: Josy schreibt die Mail
  - generell: Sammlung zu der ganzen Angelegenheit wird angelegt.


### 2.2 Facebook
* Bewerbung WiWi Modul: Bewerbung wird nicht erfolgen.
* Star-Trek Vorlesung über KI: Wird beworben. Denise: Twitter, Josy: Facebook

### 3 Evaluationen (Braun/Gräbe)

* Ergebnis Studienstartbefragung: Wird geklärt, gerade kein dringender Bedarf

* Neuigkeiten bezüglich möglicher Fragen in zukünftigen Studiengangsevaluationen?   
 - Ist eine AG notwenig? - Nein
 - Denise erstellt Markdown im git, jeder kann sich einbringen

* Ergebnis des Gespraechs mit Graebe?
 - SS 2018: Menschen die (strukturelle) Probleme im Plan sehen können sich direkt bei ihm melden. Wir weisen auf den Plan hin
 - Gespräch mit Graebe: Es wurde sich über die Modalitäten ausgetauscht


### 3.1 AG Oeffentlichkeitsarbeit
* Öffentlichsarbeit wird in Angriff genommen.
Vorschläge: 
 - Vor jeder Sitzung : Wo ist sie? wann? Hauptthemen?
 - monatliche zusammenfassung
 - Wahlveranstaltungen, Wahlparty, allgemein mehr Informationen vorher 
 - Repo "Öffentlichkeitsarbeit" - Ahmad kümmert sich  

### 4 Job-Verteiler
* Lay-Out, Lesbarkeit - vertagt.
