---


---

**Anwesend:** Christoph, Paul (ging früher), Eric, Christofer, Ahmad, Georg, Moritz (Gast), Marc (Gast), Bianca (Gast), Sofia (Gast) , Denise (verspätet),

**Sitzungsleitung:** Christoph

**Protokoll:** Christofer

## Tagesordnung
1. Sommerfest
2. Berufungskommission Softwaretechnik
3. DSGVO
4. Emails & Post
5. Wahlen
6. Ti2 und Prüfungsvorleistungen
7. Corporate Design


# 1 Sommerfest
* Termin steht 26.06.
* Wer geht in den Arbeitskreis? (3 pro FSR)
* FSR Mathe würde sich eine Person mit Führerschein wünschen, da wir 2 Transporter brauchen - Mittags und Abends 1x fahren
* Christofer & Denise wollen in den AK, Ahmad und Christoph unter Vorbehalt 
* Marc und Sofia könnten fahren (Sofia könnte auch über Teilauto mieten)
* Christofer schreibt Fabian vom Mathe FSR

# 2 Berufungskommission Softwaretechnik
* Christofer würde es machen und schreibt Mail an Herrn Janassary
* Abstimmung 4/0/2

# 3 DSGVO
* Website, GitLab, Event-Tool und Mailingliste sind von der DSGVO betroffen
* müssen für jeden Dienst eine Datenschutzerklärung schreiben
* Ein Verfahrensverzeichnis, das wir auf Anfrage in 10 Tagen der anfragenden Person zusenden können. Das betrifft auch nicht-digitale Prozesse.
* benötigen schnellstmöglich einen Reiter Datenschutzerklärung auf der Website und im Eventtool
* Mailverteiler müsste morgen offline genommen werden -> sonst Mail an alle, dass sie ausgetragen werden mit Link zum neu eintragen -> muss protokolliert werden
* Abstimmung Job- und Newsletterverteiler vorrübergehend lahmlegen 
	6/0/0
* Git Repo mit TODO etc.
* Eric, Georg und Marc haben interesse sich darum zu kümmern	

# 4 Emails & Post
* Mail an Prof. Scheurmann wegen interaction Leipzig e.V. 
	* Christoph schickt Tims Mail ab

* UNI findet STADT (wir haben Flyer erhalten)
	* nicht bewerben

* 70 Jahre Israel 
	* fördern wir und wenn ja wie viel?
	* Paul möchte Zweckbindung der Förderung (keine Förderung für Thomas Maul)
	* Denise eher gegen Förderung
	* Eric schlägt vor die geplante Summe für den nicht stattfindenen Barrierefreiheit Workshop zu nutzen
	* Abstimmung ob wir zweckgebunden fördern
		3/2/2
		wir fördern		
	* gestückelte Abstimmung maximale Summe
		200€
		4/2/1

* Paket vom VDI
	* nicht bewerben

* FSFE Antwort
	* Marc schreibt

# 5 Wahlen
* Wahlauszählungstermin
	* Donnerstag 18 - 21 Uhr
	* Marc, Moritz, Sofia, Denise, Christoph, (Paul und Christofer ab 19 Uhr)
* Wahlausgaben Buget von 200€
	* 6/0/0

# 6 TI2 und Prüfungsvorleistungen
* vertagt

# 7 Corporate Design
* vertagt
