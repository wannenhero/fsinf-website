---


---

Anwesend: Christoph(früher gegangen), Paul, Christofer, Eric, Denise, Josy, Georg, Verena, Ahmad (kam später und ist früher gegangen), Gero(Gast, ist früher gegangen), Marc(Gast, ist früher gegangen), Sofia(Gast) 
Protokoll: Christoph, Christofer

#TOPS
* 1 Vorbesprechung zu Diskussionsrunde zu Studieneingangsphase
* 2 Orga
* 2.1 Weihnachtsfeier
* 2.2 Änderung (& opt. Verschlüsselung) FSR-interner Passwörter
* 2.3 Moderations- und Protokollplan
* 2.4 Facebook
* 2.5 AG Forum
* 3 Emails
* 3.1 Nachklausur MuP2
* 3.2 ADS Musterloesungen
* 3.3 Studie Lebensgewohnheiten
* 4 Strategie / Agenda
* 4.1 Agenda2018/19
* 4.2 Corporate Design der Fachschaft
* 4.3 Strategie nach Studiengangsevaluationen (Kommunikation ect)
* 5 Benutzung von Twitter
* 6 Auswertungen
* 6.1 Erstigrillen
* 6.2 Auswertung: Kneipentour
* 6.3 Feedback Schlafplatzbörse Propädeutikum
* 7 Mittwochsüberschneidungen
* 8 Tueranfragen


## 1 Vorbesprechung zu Diskussionsrunde zu Studieneingangsphase
* siehe Mail von Neumann
* Christofer wurde entsendet und erhält weitere Informationen 
* am 14.11.2017 — 11:15 - 13:00 Uhr Diskussionsrunde
* wahrscheinlich hauptsächliches Thema: Programmierausbildung 
* Fuer Christofer
 * "für jedes Modul sollte es eher ein Tutorium geben" —> Gelder besser aufteilen

## 2 Orga
### 2.1 Weihnachtsfeier
* Wie wäre es mit einer Kostümparty? —> Nein 
* Vorbereitung 1 1/2 Wochen vor Feier
* Beteilungsinteressierte Personen melden sich bei Denise oder Irene

### 2.2 Änderung (& opt. Verschlüsselung) FSR-interner Passwörter
* Keine Einwände mehr gegen Verschlüsselung
* Alle sollen ihre PGP Keys zeitnah an Eric senden 

### 2.3 Moderations- und Protokollplan 
* Paul würde Tim kontaktieren oder selbst einen Plan erstellen

### 2.4 Facebook
* Die alten Facebook Gruppen werden mit der FSR-Seite verknüpft
* Admins der Gruppen wenden sich an Denise
* Christoph wird hinzugefügt 

### 2.5 AG Forum
* Verwirrung ueber bisherige Beschluesse
* Wird im Rahmen von Strategie/Agenda Debatte neu aufgegriffen

### 2.6 Studienkommision 
* Josy, Ahmad, Christofer
* Prüfungskommsion ? —>  Vertagt 
* Mail an Bogdan für die Studienkommision (Josy)

### 2.7 Stura
* Ersatzendsendung durch Gero heute dem 07.11.2017
* Abstimmung  Mh/0/0
* ständige Ersatzentsendung durch Gero? -> Vertagt

### 2.8 Verteiler
* Vertagt

### 2.9 Zugang 901
* es brauchen FSR-Mitglieder Zugang zum Raum
* Denise fragt bei Herrn Janassary nach

## 3 Emails
### 3.1 Nachklausur MuP2
* Josy formuliert Mail an die Studierenden
* nach Einschätzung von Prof. Scheuermann war die Klausur gleich schwer 

### 3.2 ADS Musterloesungen
* Urheberrechtverletzung nicht klar
* Alle Abteilungen haben eine Sammelmail von Prof. Scheuermann erhalten bei der Erstellung des Gits
* Die Art und Weise der Anfrage war so nicht in Ordnung
* Ahmad ist vor der Abstimmung gegangen
* Abstimmung ob Dokumente entfernt werden
	Mh/0/0
* Paul und Denise verfassen Antwortmail

### 3.3 Studie Lebensgewohnheiten
* vertagt

### 3.4 Anmerkung zum 06.11.2017
* ist geklärt
* Denise schreibt Mail

### 3.5 34C3
* Abstimmung Förderung Fachfremde für Studierende der Uni Leipzig
 3/3/1
 Vorschlag nicht angenommen

## 4 Strategie / Agenda
### 4.1 Agenda2018/19
* vertragt
### 4.2 Corporate Design der Fachschaft
* vertagt
### 4.3 Strategie nach Studiengangsevaluationen (Kommunikation ect)
* vertagt

## 5 Benutzung von Twitter
* vertagt

## 6 Auswertungen
### 6.1 Erstigrillen
* vertagt
### 6.2 Auswertung: Kneipentour
* vertagt
### 6.3 Feedback Schlafplatzbörse Propädeutikum
* vertagt

## 7 Mittwochsüberschneidungen
* vertagt

## 8 Tueranfragen
* vertagt
