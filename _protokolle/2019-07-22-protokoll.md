---
---

## Tagesordnung 22.07

Protokoll: Paul
Anwesend: Niclas, Benedikt, Christofer, Natascha, Alina, Paul

## 1 Finanzen
  * Onlinebanking
    * wird wahrscheinlich dieses Semester nichts mehr
    * Paul fände es gut wenn es noch mit den aktuellen Finanzpersonen passiert, bevor es wieder Wechselchaos gibt
    * theoretisch bräuchte es nur Termin mit der Sparkasse, Paul fragt nochmal Philip
  * Stand Auszahlung von Förderungen
    * 35C3 ist raus
    * Geld an Jan Kaßel etc. raus?

## 2 ADS Gespräch
  * kommenden Montag vorgeschlagen
  * Alina würde gerne zu dem Gespräch gehen
  * Alina würde Themenliste sammeln und nochmal versenden
  * Paul würde mitkommen
  * Dokument muss noch gelöscht werden

## 3 Emails

#### 3.1 WHK Tutorien (Mail vom 08.07)
  * Workshops für Studis die Tutorien betreuen
  * **Newsletter**
  * Twittern macht Christofer

#### 3.2 Bitte um Aushang ZLS (Mail vom 09.07)
  * interessant für MINT Lehramtsstudis
  * Twittern macht Christofer

#### 3.3 Career Service (Mail vom 11.07)
  * Careerservice lädt für 28.10. 13 Uhr zu Gesprächsrunde mit StuRa und FSRä ein
  * Themen bis 30.09 einreichbar
  * **Konstisitzung**

#### 3.4 Förderung Theaterprojekt (Mail vom 10.07)
  * Förderung für studentisches Theaterprojekt beantragt
  * nicht beschlussfähig -> vertagt

#### 3.5 Semesterauftaktparty Germanistik (Mail vom 10.07)
  * Germanistik hat wieder wegen Semesterauftaktparty angefragt
  * letztes mal ungünstige Location, doofes Türpersonal
  * Absprache mit Freigetränken schwierig
  * Motivierte Leute könnten das stemmen mit schneller Organisation, sonst schwierig
  * Natascha hat Lust
  * Nochmal in Gruppe und Verteiler nach Motivierten fragen
  * Alina schriebt FSR Germanistik, kann aber nichts organisieren
  * Christofer fragt in Telegram und Verteiler

#### 3.6 Aktualisierung Textbeiträge (Mail vom 16.07)
  * Textbeiträge müssen bis heute überarbeitet werden
  * Elena und andere sollten eingebunden werden
  * Paul setzt sich gleich ins FSR Büro und macht das

#### 3.7 Vergabesitzung Deutschlandstipendien (Mail vom 15.07)
  * Georg würde dahin
  * es gibt schon drei Terminvorschläge, Georg kann sich an Maletti wenden
  * Georg darf das machen

## 4 Sommerfest Auswertung
  * keine Ahnung wie der Stand ist
  * in kommender Sitzung oder Emailverteiler auswerten

## 5 Erstiwoche
  * Konstisitzung?
  * auf Konstisitzung vertagt

## 6 Treffen der sächs. Infofachschaften
  * Es gibt eine Telegrammgruppe
  * Zwei mögliche Termine zum Doodlen wurden ausgewählt
  * 10.08 oder 11.08 oder 03.08 oder 04.08
