---


---

Anwesende: Eric, Tim, Ahmad, Denise, Josy, Irene, Georg, Paul, Christopher, Marc (Gast), Verena (Gast), Bianca (Gast), Moritz (Gast)
Sitzungsleitung: Eric
Protokoll: Tim

## 1 Abstimmung erneut über 34C3 - Sollen auch Studierende anderer Studiengänge durch den FSR Informatik bzgl. des 34C3-Tickets gefördert werden? (Abstimmung vom 14.11.2017)
* waren damals aufgrund nicht beschlussfähig
* Nur Studierende der Fakultät FMI für 34C3 fördern?
* Abstimmung: (4/2/2)
* Beschluss: es werden ausschließlich alle Studierenden der Fakultät FMI für 34C3-Ticketrabatt gefördert

## 2 Prof. Bogdan: Übungsaufgaben Technische Informatik
* Denise: FB-Nachricht bzgl. der Existenz des Hinweises von Bogdan, dass die Übungen wichtig und klausurrelevant sind
* Josy schreibt es in die Erstigruppe

## 3 Tim entschuldigt sich dafür, dass er im Dezember 2017 und Januar 2018 wenig Zeit für FSR Informatik hat(te)
* Josy: fühl dich nicht schlecht, in letzten Monat Sitzung ziemlich durchlöchert

## 4 Studienfahrt 2018
* Paul: Kalkulation 50 Person letzte Studienfahrt (siehe Studienfahrt Repo 2018 Buchungskalkulation) 1993 € für Unterkunft und Anreise
* Paul: für Studienfahrt 2018 20 Euro mehr als 2017. Das heißt Ca. 50 Euro pro Person bei 50 Personen für 2 Nächte und Hin- und Rückfahrt
* Denise: Gebt Stura schonmal Antrag und mehr Vergleiche einbringen
* Josy und Eric halten es für zu teuer für die Studierende. Wie wahrscheinlich ist es, durch Vergleiche die Kosten zu drücken?
* Marc: Buskosten kann evtl. reduziert werden.
* Christopher: 50 Euro ist Höchstgrenze. Vergleiche holen wir ein.
* Paul: 30 Euro zu unrealistisch.
* Wir beschließen eine Studienfahrt 2018 zum Forsthaus, die Beantragung Studienbeförderung beantragen und wenn Antrag beim Stura durch ist, die Unterkunft buchen, 800 Euro FSR Eigenbeteiligung, 50 Euro Maximaleigenanteil für Studierende
* Abstimmung: (8/0/0)

## 5 AK Lehramt
* wird ad acta gelegt, bis Neuigkeiten eintreffen

## 6 Emails (Digitale Bürgerbeteiligung, Nachhilfesuche, Förderung Leipziger Schriften, Maus ...)
* wurden besprochen

## 7 Newsletter
* Themen, die reinsollen:
** Hinweis von Gräbe, dass SoSe Module fertig geplant sind und Studis bei Problemen (insbes. Überschneidungen) direkt an Herrn Gräbe schicken (siehe Punkt 11 unten)
** 34C3 Förderung
** Systemakkreditierung, Mensch wird noch gesucht

## 8 Tor-Knoten
* Vorschlag: Tor Exit-Node über URZ hosten
* Christopher: Haben auch schon andere Unis. Schreiben wurde erstellt vor einem Jahr.
* Eric: Posteingangsbestätigung gab es. URZ sollte erinnert werden. Wer hat Lust eine Email/einen Brief zu schreiben?
* Eric und Marc kümmern sich darum.

## 9 Stura-Entsendung
* zwei freie Plätze (ehemalig von Felix und Tim)
* heute: Moritz ersatzentsandt (MH/0/0)
* Christopher: nächste Sitzung können wir darüber abstimmen, ob wir Marc und Moritz dauerhaft entsenden

## 10 Zusammenarbeit mit dem FSR Geschichte
* Paul: FSR will am Donnerstag Termin besprechen, Anfang des Sommersemesters wird angepeilt

## 11 Gräbe: Modulplan für SoSe sind fertig.
* Eric: Blick darauf werfen und überprüfen, ob sich wieder Module überschneiden und dann rechtzeitig nachhaken
* Paul: Hinweis dass Modulplan fertig ist und Überprüfung notwendig ist (bzgl. Überschneidung) sollte direkt an die Studis weitergeleitet werden
* Eric: Sollte aber gesammelt weitergegeben werden, sonst bekommt Herr Gräbe viel zu viele Emails auf einmal
* Dieses Thema sollte dringend in den #NEWSLETTER. Paul schreibt einen Text, der darein kommt und evtl. auch wo anders hin! Eric und Tim fragen morgen Herrn Gräbe, ob die Studis die Emails direkt an ihn senden oder der FSR Informatik als Zwischenverteiler fungiert und die Überschneidungen gebündelt und strukturiert an Herrn Gräbe weiterleitet

## 12 Anmeldetool 34C3
* ist fertig

## 13 Parties
* veraltet, es gibt hierfür nun einen Arbeitskreis
* Paul stößt es an, denn Marc, Georg und Christopher möchten mitmachen 
