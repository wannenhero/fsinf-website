---


---

* anwesend: Christofer, Irene, Denise, Christoph, Paul, Georg, Philipp, Ahmad
* Protokoll: Christoph


# TOPS

* Mail an Prof. Scheuermann
* Studienfahrt
* Erstigrillen
* Erstibeutel
* Kneinpentouren
* Die ersten Vorlesungen
* Sitzungstermin
* Logo
* Newsletter
* Deutschland Stipendium


## Mail an Prof. Scheuermann

* vertagt


## Studienfahrt

* Bollerwagen und Beamer am Start
* Georg fährt Transporter
* Ahmad und Paul kümmern sich um Turnhalle - Programm muss noch geplant werden
* Georg kleine Wanderung
* Christofer und Christoph große Wanderung mit Bollerwagen 
* Handkassenantrag muss noch gestellt werden


## Erstigrillen

* 05.10.2017
* in Oase 18:00 - 21:00 Uhr
* Kosten: 10€ Grillpauschale von Oase (Abstimmung: 8/0/0)
* Christoph ruft an und bestätigt Annahme des Angebots


## Erstibeutel

* Christofer schreibt Mail 


## Kneipentouren

* 12.10.2017 - 18:45 Uhr
* 3 Gruppen á ~30 Leuten
* Irene(optional), Christofer, Paul, Denise, Christoph, Philipp, Ahmad 
* Ahmad geht in DS-Vorlesung und gibt Informationen bekannt
* Digital Humanities muss noch angesagt werden
* Irene plant DH Kneipenabend vor Modul Einschreibung


## Die ersten Vorlesungen

* in DS Vorlesung FSR vorstellen 
* Denise plant eine Präsentation  


## Sitzungstermine

* Doodle in 2 Wochen auswerten
* nächste Sitzung: 26.09.2017  - 11:00 Uhr


## Logo

* Paul - FSINF in Automatengraphen


## Newsletter

* Studienfahrt, Kneipentour



## Deutschland Stipendium

* Denise schreibt Mail an Prof. Bogdan

