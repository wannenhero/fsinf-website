---


---

## Tagesordnung
##### 1 Orga
##### 2 Lindemann
##### 3 Emails
##### 4 35C3
##### 5 Einführungsveranstaltungen
##### 6 Umstrukturierung Bachelor
##### 7 Offener Informatikraum
##### 8 Offener DH Raum
##### 9 Bioinformatik Einwahl

Anwesende:Natasha, Moritz, Paul, Ahmad, Denise, Irene (kam später), Marc (Gast), Nora (Gast), Sofia (Gast)
Protokoll: Phillip, später Moritz
Leitung: Eric

## 1 Orga

### 1.1 Haushalt
- 2230€ nach Studienfahrt übrig
- 1000€ für 35C3 (5/0/3)
- Verweis Protokoll 24.10, Beschluss über 50€ für Regal -> umsetzen (5/0/2)
- Mit dem Sofa MUSS etwas geschehen, Paul holt 3 Angebote rein - vertagt
- kein Vortrag
- dekadente WeiFei vertagt
- Mate-Depot abgelehnt
- Chor Unterstützung 200€ (8/0/0) -> angenommen



### 1.2 Rest
* PO - Paul war bei Neumann
 - entweder über StuKo, gegenwärtige Form abschaffen
 - oder das Wort "Bindung" streichen
-> Marc, Christofer, Josi tragen das in die Stuko

* Sitzungstermin
 - generelle bereitschaft, Sitzungstermin auf 19.00 zu setzen, vorhanden
 - Denise fügt Möglichkeit bei Doodle hinzu

* StuRa Entsendung
 - Denise geht, unter Wahrung ihrer Vorstellungen (Blutdruck, abhauen wenn scheiße) (8/0/0) -> angenommen

* FAQ und Service
  - Christofer hat einiges hinzugefügt, Rest folgt. Vertagt


* Empfehlung Prof. Maletti für Theodor-Litt-Preis
  - keine Ergänzungen zu dem was Christofer geschrieben hat. Frist 30.10 (Post!)


## 2 Lindemann
* Paul hat berichtet vom Treffen mit Scheuermann
  - lt Scheuermann noch keine guten Bewerber vorhanden
  - Scheuermann hat Lindemann Hilfe nahe gelegt bei weiteren Problemen mit der Besetzung der Stelle


## 3 Emails
* Studienfahrt Hund mitnehmen
 -> abgelehnt
* TI Sachen im git entfernen
 - Gespräch suchen, Gründe und Lösungen heraus(finden/arbeiten), unseren Standpunkt nicht verlassen
 - Paul sucht das Gespräch, Christofer wenn er Lust hat

* Förderung Denise/Irene/Sonja für "gotocph"
 - Abstimmung: Förderung 100€/Person, ABER: tägl. Berichte auf Twitter, abschließender Vortrag für alle, Werbung vom FSR dass sowas allg. möglich ist (6/0/2)


## 4 35C3
* Philipp schreibt einen Antrag auf Förderung über 700€, Förderung als Fachschaft, Leute für Informatik (Institut), aus Hilfstopf
 - Aussagekräftigen Antragstext beifügen, Paul will helfen
 

## 5 Einführungsveranstaltungen
* Bachelorkneipentour Auswertung
  --> vertagt
* Masterkneipenabend Auswertung
  --> vertagt
* Studienfahrt - wir müssen Termine zum Bar bezahlen kommunizieren - wie viele haben schon gezahlt? Plätze sind bald voll -> Erinnerungsmail?

## 6 Umstrukturierung Bachelor
* Christofer hat Informationen des AK über den Mail-Verteiler geschickt, ist aber heute nicht da. Bitte trotzdem drüber reden
  --> vertagt

## 7 Offener Informatikraum
* Paul und Christofer waren bei Felix Wlassak
* Wahrscheinlich Montag Dienstag Freitag P401 und Mittwoch Donnerstag A414 von 11 - 17 uhr 1-2SHKs
* Beginn 22.10 generell für alle Info 1. Semester Module
* Zeckzer macht noch Werbung - sollten wir lieber auch machen
 -Tage sollen getwitternt werden
 -Denise kümmert sich darum, Informationen sichtbar zu machen
 -kommt auch in den Newsletter

## 8 Offener DH Raum
* Wurde mal angesprochen, sollte mal in die Wege geleitet werden.
 -eigener Raum für Selbstudium speziell für DHler?
 -P402 als möglicher Raum?
 -Primärzweck: zusammensetzen für Gruppenarbeiten
 -Raum soll wenn, dann für alle frei sein
 -Räume in Bibliothek -> sind oft voll
 -Raum P701 oft leer, ist bisher für Selbststudium offen
 -Irene kümmert sich


## 9 Bioinformatik Einwahl
* Sofia, Paul und Marc wollten zu Neumann - was neues?
