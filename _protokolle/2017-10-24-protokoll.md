---


---

Abstimmungen sind nach Pro/Contra/Enthaltung aufgeschlüsselt  
Anwesend: Verena (Gast), Irene, Denise, Fabian (Gast), Georg, Felix (Gast), Philipp, Christoph, Christofer, Paul, Eric, Sofia (Gast)  
Protokoll: Paul  

## TOPS
#### Finanzen
 * 2429€ übrig
 * neues Büromaterial (20€)
  * Briefumschläge, Klebestift, Mehrfachstecker
 * Weihnachtsfeier (200€)
  * letztes Jahr 10-20€ verlust
  * 400€ wahrsch. zu viel kalkuliert
  * da Getränkeverkauf wahrsch. kein minus
  * für Chor 70€ (wie letztes jahr)
  * Lampenzeug?
    * mit Beamer zeug laufen lassen ++licht
    * Lampen mitbringen?
    * Baum
    * Es ex. 1-2 Lichterketten
    * Mehr Lichterketten für Tischdeko usw.
  * Dekobums für 50€
  * Christbaumständer kaufen 20€ aufwerts
  * Denise hat 190€ stehen -> aufrunden auf 200€
 * FSR Raum (50€)
  * Bürostuhl erledigt sich von selbst
  * Wandregalsituation für 50€
 * Workshop für Barrierefreie Webseiten (100€)
  * Geschätzt 15 Teilnehmer
 * Studienfahrtbeiträge (270€)
  * Sollen FSRant*innen entsprechende Beiträge bezahlt bekommen
  * Beim StuRa klären wie es organisatorisch aussähe
  * Für Studienfahrt (270€)
  * einzeln abstimmen
 * Kontovollmacht (0€)
  * Kontovollmacht ändern würde 40€ kosten
  * Bis Januar warten um Geld zu sparen
 * Abstimmung im Block über obige 370€ (ohne Studienfahrtbeiträge)
  * Abstimmung 8/0/0
 * Abstimmung über max. 270€ für Studienfahrtbeiträge
  * Abstimmung 6/0/2
 * Damit sind 640€ aus dem Budget verplant
 * 2429€ - 640€ = ca. 1800 € uebrig
  * Studienfahrt eventüll mehr Kosten da nicht ausgebucht
  * Worstcase 1200 € übrig
 * 34C3
  * entweder begrenzte Liste und 20 Leute bekommen 50€ oder wir machen unbegrenzte List und verteilen auf alle
  * alternativ mind. 30% foerdern, wenn weniger sich anmelden mehr
  * organisatorisches für Zukunft
  * Topf von 1200€ + Rest des Budgets soll fuer Foerderung beschlossen werden
  * Abstimmung 7/0/0 (Christofer abwesend)

#### Studentenverbindungen Antrag im StuRa

 * Generelle Zustimmung zum Antrag, nur Verallgemeinerung der Verbindungen stoesst wenigen auf

#### Emails

###### Email von Neumann zu Entsendung zu Besprechung zu Studieneingangsphase

 * Christofer würde gerne teilnehmen
 * Christofer sagt Bescheid und nimmt Teil
 * sammeln von Vorschlägen zum Thema im Git

###### Email zu ProKooperation

 * Mailverlauf abgeklärt
 * zum nächsten Semester auf jeden Fall drum kümmern
 * in nächster Sitzung unter Agenda-Punkt besprechen

#### Nächste Sitzung

 * Paul nächste Sitzungsleitung
 * Christoph nächstes Protokoll
 * Weiterer Austausch ob am 31.10 Sitzung stattfinden soll noetig

###### Vertagtes
 * Die Punkte 3.1-3.6 sowie die gesamten Punkte 4 und 5 wurden vertagt. Das Thema Mittwochsüberschneidungen ist immernoch aufgeschoben.
