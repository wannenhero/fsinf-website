---


---

* Anwesend: Alrik, Kasimir, Fabian, Martin, Nancy, Nicole, Sebbo

## Entbindung der ärztlichen Schweigepflicht

Uns erreichte die Information, dass Studierende der Institute WiWi und BioPharm, wenn sie sich zu einer Prüfung krankmelden möchten, ihren Arzt/Ärztin mittels eines [Formulars](http://www.wifa.uni-leipzig.de/fileadmin/user_upload/pruefungsmgmt/2_bachelor/Mitteilungen_Formulare/Pruefungsunfaehigkeit_Attest.pdf) von der Schweigepflicht entbinden und dem Prüfungsausschuss ihre Symptome mitteilen lassen müssen. Das Justiziariat habe dieses Vorgehen geprüft und für rechtens befunden.

Wir werden in unserer Prüfungskommission ggf. dafür sorgen, dass es bei uns nicht so weit kommt. Schon jetzt darf das Prüfungsamt im Zweifelsfall ein Attest vom Amtsarzt einfordern. Dieses Mittel gegen Missbrauch von Krankschreibungen reicht unseres Erachtens vollkommen aus.

Der StuRa weiß auch schon Bescheid.

## Weihnachtspicknick

* waren ca. 30 Leute, positives Feedback
* nächstes Mal gern wärmeres Licht
* Werbekanäle (Webseite, Facebook, Twitter) sind anscheinend noch ausbaufähig

## Altklausurensammlung

* eine zentrale Verwaltung ist seit Ewigkeiten überfällig
* tendenziell kümmert Fabian sich darum

## Weihnachtsparty 18.12.2014

* wird super
* wer will, macht Schichten

## xceeth

* Wir sind gespannt auf Januar

## Finanzen

* gerade relativ viel Geld auf dem FSR-Konto
* Rechnung von Open Houses e.V. für die Anzahlung Studienfahrt 2015 (Schloss Oberau) müsste bald eintreffen, Reservierung ist telefonisch schon bestätigt
* Abrechnung der Weihnachtsparty (18.12.) werden wir erst im neuen Jahr schaffen
* Auslagenerstattung Schaden und Technikverlust Sommerparty machen wir, sobald die Weihnachtsparty abgerechnet ist und eventuelle Förderungen der Weihnachtsparty eingetroffen sind
* Wir müssen einen Antrag auf Kassenrest stellen, um das Geld auf dem Konto ins neue Haushaltsjahr übernehmen zu können. Abstimmung: 7/0/0

## Magnethalterung für unsere Tür

* zum brandschutztechnisch korrekten Anbringen von Plakaten an der Tür unseres FSR-Raums
* günstigstes Angebot: im Zweierpack für 19 Euro bei amazon
* wir können den FSR Mathe fragen, ob sie uns eines abkaufen möchten
* Abstimmung: 7/0/0
