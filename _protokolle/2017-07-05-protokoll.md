---


---

**anwesend:** Christofer, Denise, Dominik, Paul S., Martin, Josy (Gast), Paul R.(Gast), Tim (Gast), Ahmad (Gast)
**Protokoll:** Dominik

# TOPs
* Gespräch mit Scheuermann
* Unterkünfte Propädeutikum
* Konstisitzung
* Erstikram
* Sommerfest Mathe
* Diskussionskultur

## Gespräch Scheuermann
* Josy war mit
* 2 Sachen
    - Selbststudium Feedback
        + Raum öfter zu (Frau Wenzel könnte sich drum kümmern)
        + Scheuermann legt bei Gräbe ein Wort für uns ein (1-2 Räume ab 15:00 Uhr)
        + Displays für Belegungsplan der Räume vorgeschlagen
        + Tischgruppe an 401/402 vorgeschlagen
        + 
    - FSR-Raum
        + schwierige Raumsituation, aber Bedarf erkannt
        + Lindemann hat zu viele Gelder
        + wahrscheinlich erst in den nächsten Jahren
        + kommt auf uns zu, wenn was frei ist
        + Sitzreihe alá Hörsaal für unsere Wand
        + Denise fühlt sich hier wohl

## Buddy Programm
* Mail
    - Text für die Seite schreiben, wie in der Mail teilweise vorgegeben
    - Newsartikel schreibt Dominik
* Syrischer Student
    - *vertagt*

## Konstisitzung
* erste Augustwoche (KW31) am besten (nur Paul nicht da, aber via Skype dazu)
* Tim erstellt neues Doodle für genauen Tag

## Sommerfest Mathe
* Morgen 15 Uhr geht keiner zum FSR Mathe
* Frage nach nächster Sitzung

## Erstikram
* Kneipentouren
    - separat für Bachelor- und Masterstudenten
    - mehr Werbung
    - mehrere Gruppen pro Tour, da letztes Jahr zu viele
    - *Bachelor*
    - Pausengestaltung zw MuP (16:45) und Tour
    - Suchen Kneipen, die rechtzeitig offen haben
    - Chris ist verantwortlich (mit Paul R.)
    - *Master*
    - Chris und Tim
    - für Master vllt keine Tour, sondern nur einen Tisch reservieren
    - Werbung bei den BioInformatikern
* Erstigrillen in der Destille (FSR Chemie)
    - "Tag der Einführung"
    - Chris fragt
    - Denise ist bereit
* Erstiguide
    - Denise
    - evtl drucken lassen

## Kryptographiemodul
* Trends der Informatik erweitern wegen Anrechenbarkeit
* Paul geht zum Prüfungsamt und fragt nach

## Diskussionskultur
* Denise überlegt sich andere Sachen für neue Amtsperiode
* "Rote Karte"
* Sprecher ist organisatorischer Chef (Manager)
* Redeball oder -stab
* Alle gehen in sich un denken drüber nach

