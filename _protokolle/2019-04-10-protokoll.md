---


---

**Anwesend**:
 * Niclas (Gast)
 * Marvin (Gast)
 * Alina (Gast)
 * Paul (Sitzungsleitung)
 * Georg 
 * Moritz
 * Christofer (Protokoll)
 * Natascha 

## 1 Orga
* Sitzungen im SoSe
	Montag 13 Uhr und Mittwoch 17 Uhr testweise im Wechsel 
	nächster Termin 15.04. 13 Uhr

## 2 Wahlen
* Werbung für Neuaufstellungen
* Plakat mit Hinweis zu Doppelstudium, Lehramt und Neuaufstellung
	Frist ist 07. Mai ?
	Paul schreibt FSR Mathe eine Mail zur Koordination

## 3 Finanzen
* 35C3 Auszahlungen
	Moritz braucht nochmal die Kooperationsverträge, danach können die Beträge ausgezahlt werden
	Moritz holt die Verträge aus dem Stura und redet mit Philipp

## 4 Emails
* Förderung von Einat Wilf und Efraim Zuroff
	Paul schreibt Mail

## 5 Anwesenheit bei MedInfModulen
* Bei Lehrimport gilt die Ordnung der Fakultät, die das Modul anbietet (Gleichberechtigung)
* Was bei Modulen einer anderen Fakultät, die nur für eine bestimmte Fakultät angeboten werden
* Paul und Christofer gehen zu Neumann

## 6 Newsletter
* Natascha redet mit Irene
* vertagt
