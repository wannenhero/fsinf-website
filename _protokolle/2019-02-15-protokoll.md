---


---

# Anwesende: 
* Christofer (Protokoll)
* Moritz (Sitzungsleitung)
* Irene 
* Sofia (Gast)
* Adrian (Gast)
* Denise
* Paul 

# TOPs:
* Orga
* Lehrberichtgespräch mit Hoffsäß Auswertung
* Emails
* Bericht aus anderen Gremien
* SWT Email
* Ehumanities - Crane Lehre
* Wahlen

## 1 Orga
* Abschied von Sofia und Denise
* Drucker im Pool funktionieren nicht und der Sound funktioniert teilweise nicht (keine Hardware gefunden)
	* kann am PDF Reader liegen
	* Christofer redet mit den Zentralen Diensten, aber erst im März
* FSR-Seite nicht erreichbar
	* Christofer schreibt Eric

## 2 Lehrberichtgespräch mit Hoffsäß Auswertung
* Fazit: DH läuft noch nicht so, aber Änderungen werden eingearbeitet

## 3 Emails
* 35C3 Registrierung
	* Auflistung Erstellung
	* Geld bei Philo, Mathe und Stura abfragen
	* Paul bringt neuen Förderentwurf für 2019 in den Stura ein, um rechtzeitig Werbung zu machen, damit Leute rechtzeitig Tickets kaufen können mit der Förderung im Hinterkopf
* Berufungsverfahren Anfrage
	* Antwortmail, dass wir keine subjektive Meinung äußern wollen, aber auf das Evaluationsbüro verweisen verweisen

## 4 Bericht aus anderen Gremien
* Stura Beitragserhöhung wurde abgelehnt (41/42 notwendigen Stimmen)
	* man hat in der Diskussion gemerkt, dass viele unvorbereitet waren und sich den Haushaltsplan nichtmal angeschaut haben
	* wird weiterhin im Stura diskutiert 
	* notwendig, da Referate vollbesetzt sind und Mitarbeitertariflöhne gestiegen sind
* Stura Barrierefreiheit bei Veranstaltungen
	* Referat für Inklusion: bei Veranstaltungen transparent gestalten, welche Einschränkungen es gibt
* Zivil-Transparenzklausel
	* wird demnächst im Stura besprochen	
* Fakultätsrat
	* Data Science Master ist im Januar im FakRat beschlossen wurden
	* Denomination seiner Professur für "Technische Informatik" in "Neuroinspirierte Informationsverarbeitung" 
	* Softwaresysteme Professur wird im Sommer nicht vertreten
	* IT-Sicherheitsmodule für den Master wurden bewilligt
	* Evaluationsordnung - Lehre und Studium und Handbuch Qualitätsmanagement - benehmen wurde hergestellt
* Berufungskommission Softwaresysteme
	* Liste ist verabschiedet - wahrscheinlich Berufung zum Wintersemester

## 5 SWT Email
* Moritz schickt Email Entwurf an den Verteiler
* in der Mail wegen P402 nochmal nachhaken
* dann an Scheuermann

## 6 Ehumanities - Crane Lehre
* Bogdan auf die Lehre hinweisen
* Christofer fertigt Entwurf an

## 7 Wahlen
* Wahlleitung hat geschrieben die Dokumente zur Wahl müssen ausgefüllt werden
* Frist 29.03 für Wahllokal, Wahlvorstände und Sitzzahl
* Wahllokal? 
	* Ziegenledersaal reservieren oder im FakRat-Raum mit Mathe zusammen
	* FakRat erscheint sinnvoll
* Wahlvorstand? 
	* Christofer kann Wahlvorstand machen
	* Moritz und Paul können nicht Wahlvorstand machen
	* im Verteiler fragen, wer noch Wahlvorstand macht (ca. 4 Personen?)
	* 
* Sitzzahl?
	* Paul: Sitzzahl auf Maximum erhöhen
	* Paul fragt Steven, ob ein Unbesetzter Sitz ein Problem darstellt
	* 6 Personen hören auf
	* 3-4 neue zurzeit
	* Diskussion wird hinterher im Verteiler geführt
* Wahlhelfer?
	* auch im Verteiler diskutieren, aber eigentlich erst ab Mitte April möglich
* Briefwahl bewerben
* Wahlwerbung für neue Mitglieder -> Moritz hat den Hut auf
	* Paul: Texte zu den Personen auf, Facebook und Twitter
