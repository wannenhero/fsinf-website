---


---

Anwesend: Georg, Nina(Gästin), Sofia, Jan, Moritz, Bianca, Paul, Erich

Protokoll Paul

# TO
1. Emails 1
2. Orga
3. Emails 2
4. Softwaretechnikvorlesung
5. Offener Informatikraum
6. StuKo Bericht
7. Rechnernetze Seminar
8. 35C3 Antrag
9. Neues zu P-402 Zugang?
10. Feierliche Exmatrikulation
11. Magic Turnier
12. Der FSR der Zukunft 

### 1. Emails 1
- Antrag von Nina
  - Zwei Referent_innen aus Israel eingeladen, daher teuer
  - In Summe 5000€ Ausgaben erwartet
  - Sie sind 3 Personen die sich dafür zusammengetan haben
  - loser Zusammenschluss von Interessierten
  - Trägerverein ist der Krew (hat was mit ifz zu tun)
  - 200€ würden gerne beantragt werden, sie nehmen aber was sie kriegen
  - Themen der Vorträge sind:
    - Ephraim Zuroff: 
      - Erfahrung mit Mühen der Bürokratie bei Nazijagd und Aufklärungsarbeit
      - vermittelt werden soll wie Rechtsmittel genutzt werden können
    - Einat Wilf
      - Konzept des Zionismus, daraus entwickelter Freiheitsbegriff
      - stellt aus ihrer Erfahrung damit Bezug zu Realpolitik her
  - heute nicht beschlussfähig -> vertagt, Nina schreibt nochmal eine kurze Zusammenfassungsmail

### 2. Orga
- Raumbuchung
  - A531 ist für immer gebucht und offizieller Sitzungsraum
- Sofa
  - neue Sofamail hat negative Resonanz bekommen 
  - Reiniger kriegt man bei dm kostenlos
  - man muss das Reinigungsmittel dazu kaufen, kostet ca. 20€
  - kann nächste Woche beschlossen werden als Test
  - falls es nix wird waren es nur 20€
  - Paul und Moritz würden Teil des Reinigungssquads werden
- WeiFei
  - 18.12, Dienstag
  - Raumreservierung ist durch
  - ab 17 uhr FKH gebucht, eventuell können wir früher rein
  - die Orga läuft weiter, es gibt Telegramgruppe
  - Eric hat seinen Glühweinkocher ins Büro gestellt 
- FAQ beantworten auf Serviceseite
  - die Antworten fehlen noch
  - es soll sich jemand drum kümmern
  - vertagt
- Sitzungsdisziplin
  - bitte früh genug in Kalender etc. eintragen
  - Vorschlag Paul: TO nicht mehr rumschicken sondern nur im git ändern und dann mal Bescheid geben wenn sie aktuell ist

### 3. Emails 2
- Anfrage aus dem HoPo Referat
  - Paul (Referent für Hochschulpolitik) weiß nicht was da gewesen sein soll
- Mail wegen Nachhilfe
  - Sofia antwortet dass wir keine Vermittlung sind und empfiehlt das schwarze Brett

### 4. Softwaretechnik Vorlesung
- es soll Multiple Choice Klausur geschrieben werden
- da sollte sich an Eisenecker gewandt werden
- Paul und Sofia machen das
- vertagt, Gespräch findet noch statt

### 5. Offener Informatikraum
- vertagt, Gespräch findet noch statt

### 6. StuKo
- vertagt, niemand der StuKo-Menschen ist da

### 7. Rechnernetze Seminar
- Lindemann verlangt die Teilnahme an seinen Kernmodulen und hat Leute austragen lassen
- Modulverraussetzung kann von ihm verlangt werden
- muss über die StuKo gekippt werden weil sie praktisch unnötig ist
- nächste Woche besprechen wenn StuKo Menschen da sind

### 8. 35C3 Antrag
- 2000€ beantragt
- Haushaltsausschuss ist nächsten Dienstag
- FSR Mathe um Coförderung anfragen, Paul macht das

### 9. Neues zu P-402 Zugang
- es finden Veranstaltungen in den Pools statt
- der Raum sollte ein Gruppenarbeitsraum sein
- an wen muss die Anfrage? 
- vertagen bis jemand Ahnung hat

### 10. Feierliche Exmatrikulation
- Mathe hat sich an WiWis gehangen
- man müsste bei WiWis nachfragen
- Paul findet es nicht gut weil Jahrgänge sich sehr stretchen
- Alle hier anwesenden können mal mit Kommilitonen schwätzen um die Stimmung zu testen
- Sofia findet es gut nicht einfach nur Bum Studium Ende
- cirE findet dass sowas das nicht ersetzen kann und es müsste halt jemand organisieren (er_sie will es auch nicht ersetzen)
- vertagt

### 11. Magic Turnier
- das ist Moritz Baby
- entstanden im Wahllokal
- Magicturnier im FKH veranstalten
- scherzhafter Preis
- einfach als Turnier und nebenbei spielen und so
- dazu vielleicht Getränke und Brezeln verkaufen
- Wer Lust hat kann sich an Moritz wenden
- cirE fragt ob Moritz schon mit Menschen in Kontakt steht
- Moritz weiß nicht wer mit organisieren würd
- Orga ist aber auch flach (eigentlich nur Raumbuchung)
- Paul: schlägt Workshop für Anfänger vorher vor
- Dafür könnte man versuchen simple Decks zu organisieren (oder zu kaufen)
- Moritz hat den Hut

### 12 Der FSR der Zukunft
- Es gibt Menschen die wahrscheinlich aufhören 
- cirE findet das Thema etwas früh 
- Moritz findet es genau rechtzeitig weil langfristig besser ist
- Paul findet früher auch besser
- Jan ist neu und in seinem Semester ist das nicht so bekannt mit dem FSR
- Präsenz könnte mehr da sein 
- Mundpropaganda ist wohl besser, Leute darauf aufmerksam zu machen wenn Probleme sind
- Moritz: Direktes auf Leute zugehen hilft wohl mehr
- Moritz schlägt vor die Sitzungen inklusiver gestalten und mehr zu erklären
- Jan: vielleicht am Anfang von jedem Thema aktuellen Stand wiederholen
- für die Zukunft immer mal wieder besprechen
