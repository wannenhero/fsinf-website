---


---

Anwesende: Tim, Paul, Ahmad, Josy, Irene, Christofer, Denise, Philipp,
Christoph, Eric

Protokoll: Denise

##Konstituierende Sitzung

### Sprecher
- Sprecher: Denise 10/0/0
Keine Kontovollmacht

- Stellv. Sprecher:
Wahl, zwei Runden

Tim

inkl. Kontovollmacht 10/0/0

### Finanzen

- Finanzer: Philipp 10/0/0

- Stellv. Finanzer: Christoph 10/0/0

- Weitere Kontovollmacht: Josy 10/0/0

### Stura-Entsandte:

Paul
Tim

9/1/0

### Studienkommission (drei Personen):

Josy

Ahmad

Christofer

10/0/0


Beginn Amtsperiode: 1.10.2017


##Sonstige interne Aufgabenbereiche und Regelungen

- Party: Ahmad
- Newsletter: Irene
- Email-Moderation: Christofer
- Git: Tim und Josy wenden sich an Fabian
- Transponder:
Martin --> Josy
Verena --> Christoph
Manuel --> Ahmad
Dominik --> Tim

Paul und Philipp: Zu Janassary
Irene: Klärt mit Janassary Zugang

Sitzungsdoodle: Tim
