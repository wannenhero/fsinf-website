---


---

* Anwesend: Alrik, Fabian, Kasimir, Martin, Nancy, Sebbo

## ADs-Übungen und Anwesenheitspflicht

* Gespräch des FSR mit Florian Holz und Prof. Dr. Heyer
  * <s>Florian Holz: Es sei ein Fehler gewesen, bei den ersten beiden Serien zu spät abgegebene Übungen anzunehmen.</s> Florian Holz: Es sei ein Fehler gewesen nicht wiederholt genug kommuniziert zu haben, daß zu spät abgegebene Übungen nicht gewertet werden. **Man könne auch außerhalb des auf der Webseite angegebenen engen Briefkasten-Zeitfensters Übungen einwerfen bzw. bei der Sekretärin abgeben.**
  * Kompromiss einer persönlichen Härtefallreglung: **Der FSR stellt eine Liste von Namen der betroffenen Personen, welche unter 50 Prozent liegen, zusammen. Deren Doppelserie 3+4 werde er dann noch korrigieren.**
  * Auf der Modulwebseite steht immer noch etwas von Anwesenheitspflicht, das sei aber nicht so gemeint, es gehe nur eine moralische Verpflichtung.
  * Florian Holz: Misslungenes Vorrechnen einer Aufgabe an der Tafel führe *in seinen Übungen* 
  nicht zu Punktabzug. (Dafür gibt es in der Studienordnung auch keine Grundlage.)
* Möglicherweise wurden laut [Punkteliste](http://aspra29.informatik.uni-leipzig.de/lehre/ads1-ws14/ads1-ws14_punkteliste.pdf) schon drei betroffene Doppelserien korrigiert und angerechnet. Wir verfassen trotzdem einen Artikel und weisen betroffene Studierende darauf hin, das nocheinmal zu überprüfen.

<div class="panel panel-info">
 <div class="panel-heading">Protokolländerung vom 11.02.2015</div>
  <div class="panel-body">
    <p>Herr Holz bittet per E-Mail vom 09.02.2015 um folgende Änderung im Protokoll:
    <li> Florian Holz: Es sei ein Fehler gewesen, bei den ersten beiden Serien zu spät abgegebene Übungen anzunehmen.<br>
    geändert zu:<br>
	<li> Florian Holz: Es sei ein Fehler gewesen nicht wiederholt genug kommuniziert zu haben, daß zu spät abgegebene Übungen nicht gewertet werden.</p>
  </div>
</div>

## ZKK 2015 in Aachen

* Möchte jemand mitfahren?

## Nächste Sitzung

* konstituiernde Sitzung, 20. Februar, 13:00 Uhr
* danach 14:00 Uhr reguläre Sitzung (in alter Besetzung)
