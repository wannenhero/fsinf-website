---


---

Anwesende: Paul, Georg, Nicky, Moritz, 
Protokollführer: Philipp 
Leiter: Natasha 

#### Tagesordnung
1. Orga
2. Wahlen
3. Finanzen
4. Emails
5. Anwesenheit bei MedInfModulen und Vertiefungspraktikum DH
6. Logo
7. Treffen UBL

## 1 Orga
* Newsletter - Stand?
* Stand Website von Georg und Paul?
	* Nichts weiter passiert, Georg kümmert sich. Wer Bock hat, kann joinen
	

## 2 Wahlen
* Werbung für Neuaufstellungen Stand?
	* Irgendwas mit Fabian von Mathe, Paul hat den Schuh

## 3 Finanzen
* Wie weit sind die 35C3 Auszahlungen?
	* Nicht weiter als letzte Woche, Moritz und Philipp kümmern sich dringendst
* Kassenprüfung Stand?
	* abgehakt, evtl. Wikieintrag zu den Notizen "Wie nächstes mal besser machen?" Philipp+Moritz

## 4 Emails
	* Ars-Legendi-Preis: Graebes Modul kommt nicht so gut an


## 5 Anwesenheit bei MedInfModulen und Vertiefungspraktikum DH
	* Neumann ist diese Woche im Urlaub, ging noch nicht
	* Paul und Christofer haben es auf dem Schirm --> vertagt

## 6 Logo
* Man könnte ziemlich günstig mit StuRa-Layoutern Logo neuer machen lassen, wollen wir sowas?
	* Abstimmung: Abstimmung über 100€ Budget, Paul macht Termin mit Layouterin aus, jeder kann kommen (max 3 Leute pls), Designvorschläge können gebracht werden, 
	wenn Designerin eine geile Idee hat nehmen wir die, wir bekommen eine Skizze: (6/0/0)

## 7 Treffen mit UBL
	* Mail muss an LuSt-Referat weitergeleitet werden
	* Paul schickt Text von irgendwem an irgendwen 
