---


---

* Sitzungsleitung: Martin
* Protokoll: Kasimir
* Anwesende: Ben, Fabian, Kasimir, Martin, Nancy, Sarah, Sebbo
* beschlussfähig mit 7 von 9

## Beschluss Protokoll 22.05.2014

* 6/0/1

## Fsinf-Newsletter

* primär: Twitter
* Twitter-Posts erscheinen automatisch auch bei Facebook
* Newsletter aggregiert Twitter-Posts
* Newsletter werden in separater Rubrik auf der Webseite archiviert
* extra Newsletter für Demo + Wahlen zwei Tage vorher

## Wahlen

* am 24. und 25. Juni 2014
* Senat, Erw Senat, FakRat
* Werbung auf Webseite
* Neues Augusteum, Raum A 520

## Aktionswoche (16.-20.6.) + Demo gegen Kürzungen

* Werbung online
* Werbung in den Vorlesungen
  * Sarah: ADs 2
  * Nancy: BioInf, Zellularautomaten
  * Judith: LinAlg (montags 13:15-14:45 H2), Berechenbarkeit (freitags 11:15 Felix-Klein-Hörsaal)
* Senatsbeschluss: am Nachmittag des 25. Juni soll keine Prüfungen und kein prüfungsrelevanter Stoff behandelt werden, keine Anwesenheitspflicht bei Laborpraktika
* eventuell Werbung mit Swimmingpool auf dem Innenhof, Sarah macht ein Doodle

## Nächste Sitzung

* am 26. Juni
* Sitzungsverantwortliche: Nancy
