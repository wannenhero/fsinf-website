---


---

* Anwesende: Marc(Gast), Christofer, Tim, Christoph, Denise, Paul, Eric, Georg, Moritz(Gast)

# Tagesordnung 13.02.2018 - 15:30 Uhr

## 1 Orga
* Klausuren im obersten Ordner des Repos (Übersichtlichkeit) ?
* Antragsteller lieferte Beispiel anhand von AuS 
* unsere Kapazitäten reichen nicht aus um das in jedem Repo zu ordnen
* Tim schreibt eine Nachricht an den Antragsteller in welcher dies mitgeteilt wird   

### 1.1 NC
* 22.02. Abstimmung über NC Einführung Ba.-Studiengänge
* Telefonat mit Rektorat (Eric,Marc,Denise) -- falls NC in Planung, Termin mit Rektorat vereinbaren 
* Vorschläge sammeln, Prioritäten beim Auswahlverfahren.. Position per Mail an Scheuermann und Bogdan, Neumann(Zahlen) senden (Denise)

##### 1.1.1 Vorschläge zur Priorisierung beim Auswahlverfahren
* Bundesfreiwilligendienst
* Ausbildung im Fachbereich
* Test um Schnitt zu verbessern 

## 2 34C3
* Information per Mail an Personen ("fehlendes Dokument nachreichen") Christofer
* Erik soll bitte auch sein fehlendes Dokument nachreichen 
* Nur vollständige Anmeldungen akzeptabel(alle Dokumente, teilweise Schwärzung möglich) *5/0/1*
* Frist: 22.02.2018

## 3 Kollektivfestival
* die Beiteiligten wissen bescheid (Paul,Marc)
* 19 Uhr

## 4 Email
* ToninTOn Förderung 1/2/3 --> nein 

## 5 MuP
* Diskussion mit Dr. Zeckzer im Büro 
* Einführungsmodul erwünscht, Josy merkte Probleme an
* besprochenes vertagt

## 6 Mündliche Prüfungen
* Sofia --> Vertagt

## 7 Studienfahrt
* Paul und Christofer kümmern sich nach den Prüfungen um Anreisemöglichkeit
* Antrag eingereicht

## 8 Free Software Foundation Europe
* Merche bestellen 4/0/2 

## 9 Awareness-Konzept
* vertagt 

## 10 Qualitätsmanagement
* Lasse vom Stura hat einen Guide geschrieben --> bitte lesen
* Christofer hat einen Punkt im Wiki erstellt, wo alles gesammelt werden kann
* Gremium Workshop 28.03.2018

## 11 Medizinische Informatik
* Paul und Christofer haben sich mit Prof. Winter getroffen
* Infoveranstaltung bewerben 5/0/1

## 13 Büromaterial
* Mail an Fr. Wenzel (Stempel als Alternative zu Briefmarken?) Christofer

## 14 Selbstverständnis Büronutzung
* auf Rücksicht pledieren !
* 1. FSR Nutzen(z.B. Finanzen) > 2. chillout > 3. privater Nutzen(z.B für Klausuren lernen) 
* Abstimmung dazu 4/1/1

## 15 Sitzungsleitenden- und -protokollierendenplan
* Wer will drauf, wer will nicht drauf? --> Melden bei Paul bis 20.02.2018

## 16 Sitzungsraum
* Tim setzt sich mit Frau Wenzel in Verbindung

## 17. Freizeit
* FSR Bowling 
* Deniese sendet Doodle
