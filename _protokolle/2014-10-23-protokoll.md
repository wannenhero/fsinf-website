---


---

* Anwesend: Ben, Nicole, Kasimir, Fabian, Alrik, Nancy

## Studienfahrt

* ein paar Leute sind nicht erschienen, schließlich waren wir 26 Leute
* Nebenkosten sind geringer ausgefallen als letztes Jahr
* stressfreier als letztes Jahr
* positives Feedback

## Schaden Transporter Sommerparty

* bei der Sommerparty entstand am Sixt-Transporter eine Delle in der Schiebetür, Schadenshöhe 1340 €, Selbstbeteiligung liegt bei 750 Euro
* Begutachtung der Fotos durch die anwesenden FSR-Mitglieder: es sieht aus, als sei ein spitzes Teil dagegen gestoßen, möglicherweise eine Bühnenplatte
* schriftlich vorliegender Antrag von Sebbo, der gefahren ist: Er hat nicht gemerkt, wie das passiert ist, und bittet den FSR um Kostenübernahme
* Konsens der FSR-Mitglieder: Ein Schaden ist ein generelles Risiko, was sich nie ganz vermeiden lässt
* **Beschluss/Maßnahme: zukünftig Tarife mit geringerer Selbstbeteiligung wählen**
* Abstimmung Übernahme des Schadens: 6/0/0

## Weihnachtsfeier

* für Informatik-Studis
* Nancy reserviert einen Raum, bevorzugt Raum P901
* 10.12.2014, 17:00
* Werbung: nächste Sitzung

## Aktueller Stand Rahmenrichtlinien

* Fabian erzählt aus der StuKo: Forderungen (insbesondere die Verringerung der Prüfungsanzahl) ist in der Informatik nach wie vor nicht 1:1 umsetzbar

## Neue Sitzungszeit

* Alrik schickt ein Doodle herum

## Kommission Deutschlandstipendium

* wir entsenden weiterhin Martin: 6/0/0
