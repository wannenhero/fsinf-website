---


---

﻿**Anwesend:** Christofer, Paul, Phillip, Irene, Georg, Moritz (Gast), Bianca (Gast)

**Sitzungsleitung:** Christofer

**Protokoll:** Moritz


## 1 Orga
- 35C3 Voucher
  --> Moritz kümmert sich
- Bäckerkisten webringen
  --> Moritz und Bianca direkt nach der Sitzung
- Finanzen: Philipp möchte kurz über den Ratenantrag reden
  -Neuer Antrag muss gestellt werden
  -Email wurde rumgeschickt
  -Ausgaben müssen beschlossen sein
    -->200 € für Sofa?
    -->Höhe der Ausgaben für die Studienfahrt?
  -->Nachfrist beantragen falls notwendig

## 2 TI2 und Prüfungsvorleistungen
- Bericht vom StuKo Treffen
- Protokoll des Treffens im Verteiler
	--> Protokolle als Prüfungen? Überarbeitung der Protokolle möglich
- Praktikum nach einem Fehlversuch vorbei? 
--> Schieben in anderen Praktika auch möglich
--> keine direkte Kritik an momentaner Fassung, in Zukunft Überarbeitung anpeilen

## 3 Prüfungsausschuss
- vorgezogen, von Paul eingebracht
  - Fall TI2/HWP geht vor den Prüfungsausschuss
  - Als Studienvertretung für Studierenden sprechen 
--> Mail kommt von Paul

## 4 Lehrberichte
- Herr Neumann hat uns in Auftrag von Frau Braun nochmal darum gebeten, diese bald fertigzustellen
- Lehrbericht Lehramt Informatik kommt irgendwann -> rechtzeitig eine Person suchen die Lehramt studiert
   --> möglichst Lehrämtler aus den letzten paar Jahren
-Wie ist der Zwischenstand?
   --> Informatik (Bachelor und Master) wird die Tage durch den Verteiler geschickt
  --> DH kommt vorraussichtlich nächste Woche

## 5 Emails
- Klausur Semantic Web
  - Anfrage aus Paderborn
  - Können nicht weiterhelfen
- Saxonia Woman Award
  - Preis für Informatikerinnen
  - Bewerbung bis Samstag (zu spät?)
	--> Christofer tweetet
- jeder der an Abschlussarbeit arbeitet/diese abgegeben hat kann sich bewerben

- Informatica Feminale
  - Sommeruni für Studentinnen
  - Anmeldung bis 24. Juli

- Barrierefrei Studieren
  - will Verlinkung der Website
  - Informationsgewinn für Studis?
  - Meinungsbild dagegen
- Leipzig Summer School - Sozialwissenschaftliche Methodenwerkstatt für Schul- und Unterrichtsforschung
  - Wenig Sinn, da Lehrämtler nicht gut erreichbar

- 70 Jahre Israel: Eric ist dran - dauert aber noch
  --> vertagt
## 6 Sommerfest
- Auswertung
  --> vertagt
## 7 DH Bachelor Aufbau
- wir und die Professoren möchten eine Überarbeitung des DH Bachelors
- dafür sollten wir Vorschläge sammeln für die StuKo
  --> vertagt
## 8 Informatik Bachelor Einführungsphasen Aufbau
- die Fakultät möchte die Informatik Einführungsphase verbessern
   - dafür sollten wir Vorschläge sammeln für die StuKo
  --> vertagt
## 9 Ordnung und Sauberkeit im FSR-Raum
- Irene merkt eklige Zustände an
- neuer FSR-Raum
  --> vertagt
## 10 Corporate Design
- Meike von Mathe mal fragen
- steht in einem Protokoll schon drin - wurde das gemacht oder ist das ein Fehler?
  --> vertagt
