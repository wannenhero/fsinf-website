---


---

**Anwesende:** Georg, Eric, Verena, Martin, Denise

**Protokoll:** Denise

TOPs
1. Studienfahrt
2. Emails
3. Weihnachtsfeier Auswertung
4. GitLab
5. Sommerparty
6. Drucker
7. Nächste Sitzung


### Studienfahrt
- Stand der Dinge?
- Denise kontaktiert Nico

### Emails


- DS Email:
--> Verena schreibt Mail mit Einladung zur nächsten Sitzung

- CCC
Es gibt noch zwei freie Plätze
--> Eric schreibt Mail an Studierende

- ProKooperation: Newsartikel muss geschrieben werden
--> Eric


### Weihnachtsfeier-Auswertung

- Musik, Deko (insb. Baum) waren sehr schön
- Menge an Glühwein hat gepasst (ohne Bier wäre schwierig gewesen)
- Becher rechtzeitig ausleihen
- Nächstes Jahr zeitig im Oktober Raum reservieren
- Konflikt wegen Raumbelegung ist gelöst

### GitLab
- Sollen Dozenten Zugang zum GitLab haben?
Abstimmung: Sollen Dozenten Zugang zum Git haben können?
0/3/2
--> Abgelehnt

- Standart-Antwort für bestimmte Mail-Anfragen festlegen
(auf unseren Newsartikel inkl. Erklärungen verweisen)

### Sommerparty
- Wer ist motiviert?
- Denise schreibt Mail an Orgaliste für Planungstreffen im neuen Jahr
- Georg würde dran teil nehmen. 
- Mail auch an Geschichte FSR

### Drucker
Wer ist für Infrastruktur zuständig?

- Zentrale Dienste kontaktieren
--> PDF drucken geht nicht (nur über externen PDF-Reader)
--> Müssen gewartet werden (Druckergebnis nicht zufriedenstellend)

- Denise kümmert sich


### Nächste Sitzung
**4.1.16, 17 Uhr**
Denise aktualisiert Google Kalender
