---


---

**Anwesende:** Georg, Eric, Verena, Fabian, Friedrich, Nico, Fabian, Martin, Denise

**Protokoll:** Friedrich

# Sondersitzung zum Gitlab

## Beschluss
**Titel:** Wir ermöglichen allen Angehörigen des Institutes für Informatik (inkl. Lehrpersonal) den Zugriff auf das Gitlab.

**Inhalt:** Derzeit haben die Studenten des Institutes für Informatik Zugang zu den Modulmaterialien im Gitlab-Systems des Fachschaftsrates.
Dieser Zugang soll auf alle Anghörigen des Institutes ausgeweitet werden und explizit Dozenten mit einschließen.  
Weiterhin sollen die Nutzer auf der Startseite darauf hingewiesen werden, dass diese sich an den Fachschaftsrat wenden können, sofern sie sich unsicher sind, welche Dokumente hochgeladen werden dürfen.

*Die Wahl findet offen statt.*

**Ergebnis:** 6/2/1 (Angenommen)
