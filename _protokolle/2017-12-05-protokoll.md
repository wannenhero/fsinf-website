---


---

* Anwesend: 
Christofer, Eric, Paul, Philipp, Josy, Sofia (Gästin), Moritz (Gast), Marc (Gast)
* Sitzungsleitung: Paul 
* Protokollierend: Christofer

## 1 Finanzvollmachten
* Es können 3 Bevollmächtigte noch diesen Monat zur Sparkasse gehen
* Christofer bekommt die 3. Vollmacht
* Philipp macht einen Termin mit der Sparkasse

## 2 Orga

### 2.1 WeiFei Schichtplan
* Schichtplan muss vervollständigt werden
* Hängt im FSR-Raum aus

### 2.2 AK Lehramt
* Christofer hat jemanden gefragt, aber es gibt noch keine Neuigkeiten
* vertagt

### 2.3 Studienfahrt 2018
* Es liegen Vorschläge vor, gab aber noch keine Diskussion
* vertagt

### 2.4 Sprechzeit
* Paul will die Sprechzeit übernehmen
* Sollte eine gewisse Regelmäßigkeit haben
* Paul überlegt sich einen Termin
* vertagt

### 2.5 Sitzung am 19.12
* findet statt, außer es fällt kurzfristig auf, dass zu wenig Personen Zeit haben

## 3 Emails
* Mail zu Kneipenabend mit Profs
* die Idee ist gut, wird im Hinterkopf behalten

## 4 Evaluationen

### 4.1 Studieneingangsevaluation
* Denise wollte mit Frau Braun sprechen, aber ist nicht da
* vertagt

### 4.2 SWT-Praktikum Evaluation
* Eric und Tim haben sich noch nicht mit Prof. Gräbe getroffen
* sollte am besten vor Weihnachten geschehen
* vertagt

## 5 Agenda
* Der AK Agenda reicht einen Antrag für die Agenda 2018 ein
* Agenda Projekte sollen vom FSR beschlossen werden und in einzelne AKs ausgelagert werden
* Die AKs treffen sich regelmäßig, um zukünftige Projekte zu besprechen und halten Rücksprache mit dem FSR
* Die Themen der AKs sollen auf der Website dargestellt werden
* Die Themen betreffen z.B. Satzung, Corporate Design oder Kommunikation
* Die Mehrheit der Anwesenden ist dafür
* Genaueres wird nächste Sitzung besprochen

## 6 Discord Service
* Marc: Es gibt einen Discord Server von Studierenden unseres Instituts
* Beinhaltet Selbsthilfegruppen für das Studium 
* Die Person soll zur Sitzung vorbeikommen
* vertagt
