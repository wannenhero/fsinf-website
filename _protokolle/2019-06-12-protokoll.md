---

---


## Protokoll 12.06.19

## 1 Orga
* Stand Website von Georg und Paul?
* nicht so kaputt wie gedacht
* Plan: Eric und/oder Fabian bzgl bauen der Webseite fragen

## 2 App
* Stellungnahme in Stura prasentiert
* Antrag von Stura zuruckgezogen
* Stellungsnahme auf Twitter stellen?
* ggf erweitern. offen fur Studentenprojekte
* Veranstaltungsidee: Barrierefreie Technologie

## 3 Wahlen Auswertung
* vertagt

## 4 Sommerfest
* Awarnessteam suche
* Aushang auf Fest
* in den Schichtplan eintragen 
* Rest am Montag mit Orga-Personen

* Off topic: FSR Hilfe Gruppe
* welche themen fur wen zuganglich

## 5 Finanzen
* Wie weit sind die 35C3 Auszahlungen?
* Finanzantrag Moritz Zugtickets zur KIF
* vertagt

## 6 Emails
* Abrechnung Efraim Zuroff
* Gremienworkshop
* Freitag und Samstag 21. 14:00-19:00 und 22.06 9:00-18:00 vom StuRa
* Rechte, Pflichten von FSR
* Anmeldungen erwunscht - an Paul wenden
* Interesse: Paul, Natascha, Niclas, Alina 
* Studienerfolgstag 21.06
* Bewerben auf Twitter Ende der Woche, Anfang nachster Woche
* Fragen zum Doppelstudium mit Philosophie
* Kontakt zwischen bereits Studierenden in dieser Kombination aufbauen

## 7 Bericht Treffen Bogdan
* Zu Tutoriengeldern haben wir Dinge erfahren, mehr in der Email an den FSR DaFZ
* keine Tutorien fur Deutsch als Fremdsprache und Zweitsprache aus finanziellen Grunden
* Zu Gräbe wurde auch ein bisschen was abgesprochen

## 8 ars legendi Preis
* ars Lendi Preis (Neue Infos?)
* Bogdan: scheinbar motivationsarme Lehre
* Treffen mit Gräbe steht aus 

## 9 Erstiwoche
* was wollen wir anbieten?
* ggfs. nach Möglichkeit schauen das übers Studienbüro zu bewerben
* Campus-Tour speziell fur Informatik
* Kneipentour
* Spieleabend
* Informationsabend/veranstaltungen fur (auslandische) Studierende
* Infoveranstaltung speziell fur DH Studierende
* Infogruppen organization. Facebook? Moodle? Telegram?