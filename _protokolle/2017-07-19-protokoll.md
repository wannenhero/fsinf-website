---


---

* anwesend: Ahmad, Josy, Martin, Paul R., Georg
* Protokoll: Eric

# TOPS

* Unterkünfte zum Propädeutikum
* konstituierende Sitzung
* Tool für Stundenplan (SWT)
* Update Infrastruktur
* Protokolle
* Kryptographie (verschoben)
* Gesprächskultur (verschoben)

## Unterkünfte zum Propädeutikum

* Georg stellt Text aus dem Gitlab auf die Website

## konstituierende Sitzung

* wird am 01. August (vormittags stattfinden)
* sollte im kommenden Jahr vielleicht schon vor der vorlesungsfreien Zeit stattfinden

## Tool für Stundenplan (SWT)

* Eric meldet, dass es Bedarf an einem solchen Tool gibt

## Update Infrastruktur

* es wird (neue) Tools geben für
  * Anmeldung zu Veranstalungen (Studifahrt, Congress, etc)
  * Drucken und Mails schreiben
* es soll eine neue "Startseite" für das Gitlab her

## Protokolle

* Bitte, nochmal nach Protokollen zu schauen und diese hochzuladen

## Studierender aus Syrien

* wir bitten via Facebook/ Twitter darum, dass sich Leute für den Support bei buddy@math.uni-leipzig.de melden
* Josy kümmert sich um Facebook
