---


---

**Anwesend:** Denise, Christofer, Paul, Ahmad, Georg, (Gast) Sofia, (Gast) Marc (Gast) Moritz

**Sitzungsleitung:** Ahmad

**Protokoll:** Christofer

# TOPS
1. Orga
2. Lehrberichte
3. Emails
4. Hakenkreuze
5. Sommerfest
6. DH Bachelor Aufbau & Informatik Bachelor Aufbau
7. Ordnung
8. Konstisitzung und Sitzungen während der Sommerpause
9. Corporate-Design

## 1 Orga
* Erstiveranstaltungen / Kneipentour
	* Denise möchte vorher wissen, ob es eine reine Raucherkneipe ist
	* Masterkneipenabend
		* Christofer kümmert sich mit Sofia 
	* Erstikneipentour 
		* Moritz und Sofia organisieren, Christofer und Marc würden helfen (keine Orga)
	* Einführungsvorstellung
		* Ahmad bereitet Folien vor
		* Sofia stellt sich bei den Bioinformatikern mit vor 	
* Erstigruppen 
	* Christofer erstellt
* Erstigrillen 
	* gute Idee, aber sollte zentral sein
	* zu wenig Personen zum organisieren

## 2 Lehrberichte
* DH Lehrbericht 
	* es ist nichts passiert - Probleme bei der Terminfindung
	* Marc wendet sich für Hilfe an Christofer
	* Lukas und Marc Treffen sich noch - Irene kann zeitlich nicht
* Informatik Bachelor und Master Lehrbericht ist fertig
* Bioinformatik ist quasi fertig
	* einiges aus dem Master Info Lehrbericht übernommen
	* schwierig, da Betrachtungsdauer die letzten 2 Jahre sind -> Regelstudienzeit 2 Jahre
	* Sofia schickt ihn die Tage über den Verteiler
* Feedback für die 3 fertigen Lehrberichte bis Freitag 18 Uhr
* Lehramt Info Lehrbereicht
	* Christofer, Ahmad und Denise fragen paar Lehrämter

## 3 Emails
* Klausur Semantic Web -> Antwort steht noch aus
	* Christofer antwortet
* Medizinische Informatik Treffen bewerben
	* Christofer twittert
* Erstibeutel
	* wollen keine Werbung/Schrott	

## 4 Hakenkreuze
* bisher kein Statement der Uni zu den Schmiererreien in der 4. Etage
* Denise möchte das Thema in den Stura einbringen
	* Zuständigkeit und Ablauf bei solchen Problemen nicht klar
	* Grundsätzlich wird keine Anzeige erstattet
* Marc würde mit HoPo-Referenten und Antifaschismus Beauftragen reden
* Denise wünscht sich einen klaren Ablauf bei so einem Vorfall
* Denise schickt ein angepasstes Statement rum

## 5 Sommerfest
* Auswertung
	* beim Abbau fehlten Leute
	* Denise sieht das Problem bei Open-End Veranstaltungen
	* extra Schicht Abbau wäre nötig gewesen
	* gab sonst keine Probleme
	* mehr Plakate für das Awareness-Team
		* sollte es für die kommenden Veranstaltungen weiterhin geben
		* Paul hat einen Eintrag ins Wiki geschrieben -> FSR HowTos unter Wie organisiere ich ein Awarenesskonzept?

## 6 DH Bachelor Aufbau & Informatik Bachelor Einführungsphasen Aufbau
* wir und die Professoren möchten eine Überarbeitung des DH Bachelors und der Informatik Bachelor Einführungsphase
* dafür sollten wir Vorschläge sammeln für die StuKo
* Marc:  erst Treffen dann in der StuKo konkret besprechen
* Christofer schlägt AK vor -> Konsens finden -> im FSR besprechen ->  mit Proffessoren treffen -> Stuko
* AK Mitglieder: Christofer, Marc und Paul
	* weitere Personen sollen sich melden

## 7 Ordnung und Sauberkeit im FSR-Raum
* Irene merkt eklige Zustände an
	* Denise sieht das Thema als erledigt
	* Paul sieht das anders -> vertagen und mit allen drüber reden
	* Marc sieht das nochmal anders -> Marc möchte das Thema nicht vertagen, sondern erst wieder besprechen wenn das Problem exisitiert
* neuer FSR-Raum
	* man könnte mal wieder nachfragen -> steter Tropfen hüllt den Stein
		* Denise schlägt nächstes Semester vor, da Prof. Scheuermann und Dr. Janassary letztes Jahr erst die Raumpläne überprüft haben	
* Antrag das der Punkt beendet wird
	3/1/1 

## 8 Konstisitzung & Sitzungen während der Sommerpause
* wann ist die Konstisitzung und was ist mit dem Doodle?
	* Paul schickt das Doodle für die Konstisitzung rum 
	* nächste Woche Freitag sollten sich alle Eintragen
* was ist mit Sitzungen während der Sommerpause
	* Marc schlägt vor 2 wöchentlich, wenn mehr als 3 Personen Zeit haben
		* Kommunikation über den Mail-Verteiler

## 9 Corporate Design
* Meike vonm FSR-Mathe fragen?
	* Marc fand das Sommerfestplakat gut
	* Paul will den Punkt von der TO nehmen, da er das Logo eigentlich ganz gut findet
	* Marc schlägt Umfrage über den Verteiler vor
	* Denise will aufgrund vergangener Beschlüsse direkt Meike fragen
		* Denise fragt Meike
