---


---

* Anwesend: Sarah, Martin, Nicole, Alrik, Ben, Kasimir
* Protokoll: Kasimir

## Sommerparty

* sehr gut gelaufen
* Es ging etwas länger als geplant, gegen 1:00 Uhr kam deshalb kurz die Polizei vorbei um uns an die Zimmerlautstärke zu erinnern, in dem Durcheinander wurden offenbar zwei Plattenspieler und ein Mikrofon entwendet. Das Fehlen wurde am nächsten Morgen bemerkt.
* **Beschluss/Maßnahme: auf Technik noch besser aufpassen, teure Technik mit Kabelschlössern befestigen, nicht mehr genutzte Technik sofort wegräumen**
* Abstimmung Übernahme der Kosten: 6/0/0

## Studifahrt

* 17.-19.10.2014, Oberau, 36-45 Leute
* FSR Mathe hat die Anzahlung bezahlt, Reservierung läuft auf uns
* Werbung: Anfrage an das Studentensekretariat wegen Ersti-Post ist gestellt
* Team: Nicole, Al, Exe?; Ben, Kasi, Martin wahrscheinlich
* Reservierung für 2015 müsste bald gemacht werden
* Abstimmung: 2015 wieder in Oberau: 6/0/0

## OLAT-PA Ding

* Prof. Dr. Graebe wurde per Mail informiert

## Anfrage nach Zufriedenheit mit dem Prüfungsamt

* Herr Neumann hat anscheinend immer genug zu tun
* keine einheitliche Notenverwaltung, Transcript of Records dauert lange
* ansonsten alles super

## Altklausuren

* erstmal im Website-Repository sammeln
* für Zugang dafür: public key an Fabian schicken
* perspektivisch einen schicken Index dafür basteln

## Webseite

* "zuletzt aktualisiert am.." ist sinnlos und kann entfernt werden

## Nächste Sitzung

* Sitzungsverantwortlicher: Alrik
* wahrscheinlich am 31. Juli
