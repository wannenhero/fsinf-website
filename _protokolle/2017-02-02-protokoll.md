---


---

**Anwesende Mitglieder:** Georg, Denise, Christofer, Martin **Protokoll**: Christofer
**Sonstige** Josi

# TOPs
* Finanzplan
* Gruppenarbeitsplätze
* Studienfahrt
* Studiengangsmodalitäten
* Propädeutikum & MuP
* GitLab
* E-Mails
* E-Mail Verteiler
* Kooptierung

## Finanzplan
* Studienfahrt
* CCC
* Büromaterial
* Unterstützungskontingent für Konferenzen
* Weihnachtsfeier
* Sommerparty
* Externe Vorträge

## Gruppenarbeitsplätze
* A531 wenn Raum nicht reserviert ist für Studierende nutzen, allerdings Vorrang für Lehrende und Mitarbeiter
* Karten eventuell freischalten -> mit Dr. Bayer reden
* Prof. Scheuermann fragen, inwiefern die Seminarräume im Paulinum für Studierende nutzbar sind
* Raumbelegung an der Tür? -> problematisch, da Aktuallisierung des Plans
* Denise schreibt Mail

## Studienfahrt
* Finanzantrag ist gestellt
* Platz für 50 Leute
* Aufstocken auf 60 Leute
* Aktivitäten stehen noch aus
* Eigenanteil FSR: Maximal 550€ (Abstimmung: 4/0/0)

## Studiengangsmodalitäten
* geklärt 

## Propädeutikum & MuP
* Vorschlag Propädeutikum in Java -> leichterer Einstieg?
* Evaluierung abwarten -> mit Scheuermann reden 

## GitLab
* Friedrich schreibt einen Guide

## E-Mails
* mitStudieren haben geantwortet
* Termin 21.02. 11 Uhr

## E-Mail Verteiler
* Vorschlag mit 2 Verteilern zu Arbeiten
    * Intern/Privat
    * Interessierte
##Kooptierung
* vertagt	
