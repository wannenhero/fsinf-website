---


---

anwesend: Denise, Eric, Georg, Manu, Alex (Gast), Stefan (Gast), Nico

Protokoll: Eric

# TOPs
* Unite 2016
* allgemeine Förderrichtlinien
* Sommerfest
* Studienfahrt
* Newsletter
* GitLab-Dozentenmail
* ADS 2
* Wahlen
* Stura-Plenum
* Sitzungstermin
* Hochschulentwicklungsplan
* Teambuilding
* Finanzplan

## Unite 2016
* Stefan und Alex sind als Gäste da
* Schwerpunkt Computergrafik (BSV)
* Kongress zu Grafik-Engine (Film, Media, Computerspiele, wiss. Simulationen)
* Schon Einarbeitung in Freizeit
* Gespräch mit Scheuermann, meit es sei eine gute Idee
* Ziel der Konferenz: Vernetzung, Vorträge
* Einführungstag: wäre als extra Anmelung für 150€, kommt aber nicht mehr in Betracht
* als Fortbildung
* würden Bericht erstatten oder kurzen Vortag/ Workshop halten
* würden auch andere Finanzierung (Scheuermann) anfragen

### Diskussion im FSR

* Abstimmung der Optionen (weitestreichende zuerst)
* Option Ticket, Fahrt, Unterkunft: 0/HM/1
* Option Ficket, Fahrt: 3/2/0
* damit beschlossen und weitere Abstimmungen obsolet
* unter der Bedingung: die beiden Veranstalten einen Workshop
* Denise sagt den beiden Bescheid

* Konsistenz im Vergleich zu 32C3 wäre: nur Ticket
* andererseits: zum C3 nur Ticket bezahlt wegen Bequemlichkeit der Abrechnung

## allgemeine Förderrichtlinien
* wir wollen solche Richtlinien
* Bezug zur Informatik
* Darlegung von Interesse/ Motivation
* Grundsätzliche Bereichtschaft, Wissen anderen Studierenden zu vermitteln
* OpenSource, Privacy etc sind uns wichtig, aber wir entscheiden im Einzelfall
* Inititive muss Leute müssen ausgehen, Kostenvoranschlag ist wichtig
* Richtlinien können auf die Homepage und wird per Newsletter beworben
* Antragstellung spätestens 2 Monate vor Veranstalungsbeginn
* Absichtserklärung: regelmäßig Geld zur Verfügung stellen (Details sind zu klären)

## Sommerfest
* FSR Mathe bekommt Ort zum Wunschtermin nicht
* wollen weiter mit uns zusammen arbeiten
* wir werden den Termin bald erfahren

## Studienfahrt
* Reservierungsbestätigung wurde versandt
* Denise hat Termin mit HHA vereinbart (24. Mai)

## Newsletter
* TOPs: Wahlen, Familienfrühstück, Studienfahrt, Kollektiv-Festival, GitLab
* Denise macht inhaltliche Arbeit
* Eric baut pdf und schickt es herum

## GitLab-Dozentenmail
* Mail fertig formuliert: Eric schickt sie an Scheuermann (mit kurzer Bitte um Weiterleitung)

## ADS 2
* lange Debatte
* TeX als Abgabe wird akzeptiert
* Stadler ist mit Prüfung durch Justitiariat einverstanden
* ggf. Änderung der Modulbeschreibung
* bisheriger Modus aus didaktischen Gründen
* Mail muss überarbeitet werden
* würden diese vorher nochmal an Stadler schicken

## Wahlen
* Denise schickt doodle für Wahlschichten herum
* Seminar als Voraussetzung als Wahlvorstand? Lukas fragen
* Denise bestellt Pfannkuchen
* Pauschal 120€ Budget: 5/0/0

## Stura-Plenum
* mehr Rückmeldung und Vorbereitung ist gewünscht

## Sitzungstermin
* jede Woche Freitag, 15 Uhr

## Hochschulentwicklungsplan
* alle haben Interesse
* Vorschlag 10.05. (19 Uhr) mit Felix

## Teambuilding
* wir machen mal was zusammen
* z.B. Grillen
* nach den Wahlen mit neuen Mitgliedern

## Finanzplan
* bräuchten wir mal und streben wir für das nächste Haushaltsjahr an (evtl. Denise und Eric)
