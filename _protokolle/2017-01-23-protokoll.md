---


---

**Anwesende Mitglieder:** Manuel, Eric, Georg, Matin, Denise (Skype) **Protokoll**: Manuel Scheub
**Sonstige** Ahmad, Josi, Verena


# TOPs

* FsInf Webseite
* Kooptierte Mitglieder
* Studienfahrt
* Sitzungstermin im März
* Gruppenarbeitsplätze
* Veranstaltungen im Sommersemster
* Finanzplan
* Wahlen
* StuRa
* Studiengangsmodalitäten
* Propädeutikum
* MuP
* Emails
* Sontiges

## FsInf Webseite
* Subscribe Feld für Job-Verteiler auf Homepage einfügen. 
* Hat auf der Startseite nichts zu suchen, da der Inhalt nicht vom FSR gehandelt wird.
* Findet sich keine der es machen will.

## Kooptierte Mitglieder
* wollen wir Josi, Ahmad und Verena kooptieren?
* Berechtigungsgrudlage, was und warum koopiert heißt.

## Studienfahrt
* Kostenvoranschlag bekommen
* Finanzer nicht da. Vertagt

## Veranstaltungen im Sommersemster
* SAP's zu knapp -> wird nichts
* Grillen/Party -> externe Besprechung (Josi, Verena, Manu)

## Wahlen
* Sitzanzahl erhöhen?
* Bis April muss die Zahl an Wahlamt übergeben werden
* Will jemand in BioInf und Digital Humanities Werbung machen um sich für die Wahl aufstellen zu lassen bzw. Nachwuchs. (Denizze, Ahmad, Verena macht BioInf)

## Sitzungstermine im Feb/März
* Ab 16.02. alle 2 wochen Donnerstags 14.00 Uhr
* Denizze trägts in den Kalender ein

## Gruppenarbeitsplätze
* Warten auf Antwort
* Vertagt

## Finanzplan
* Finanzer nicht anwesend. Vertagt

## Studiengangsmodalitäten
* Vertagt

## Propädeutikum
* Vertagt

## MuP
* Vertagt

## Emails
* Vorschlag für 'Ethik in der Informatik'. Verweis an die KritNaWis. (Martin)

## Sonstiges
* Nichts

Sitzungsende 17:40
