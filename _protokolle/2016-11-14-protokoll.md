---


---

**Anwesend:** Denise, Manuel, Georg, Christofer, Eric, Verena, Dominik
**Protokoll:** Dominik

# TOPs
* Kryptographie-Modul, Gespräch mit Prof. Scheuermann
* Brief ans Justitiariat wg ADS
* MuP1 Beschwerde-Mail
* Git-Mail von Felix
* Vernetzungstreffen
* Vorstellung KritNaWi
* Finanzpunkt
* Job-Verteiler

# Kryptographie-Modul, Gespräch mit Prof. Scheuermann
* E-Mail an Prof. Scheuermann --> Christofer und Dominik

# Brief ans Justitiariat
* Korrektur und Ergänzung
* Mail geht raus, CC Prof. Stadler

# MuP1 Beschwerde-Mail
* Veranstaltung wird evaluiert --> Manuel
* Rückmail von Denise
* Mail nach der Evaluation --> noch keiner

# Git-Mail von Felix
* Antwort-Mail --> Denise

# Vernetzungstreffen
* 2 Leute gehen mit ins Beyerhaus
* Chris schreibt Antwortmail

# Vorstellung KritNaWi
* Antwort-Mail --> Manuel

# Finanzpunkt
* Rückzahlung an Leute, die sich rechtzeitig von der Studienfahrt abgemeldet haben 6/0/0
* Neuanschaffungen:
	* 3D-Drucker
	* Büro-Equipment
	* Shredder
		* Eric kümmert sich um Angebote
		* 100€ Budget 6/0/0
	* Druckerreparatur
	* Baum und Schmuck für Weihnachtsfeier
		* 70€ Budget 6/0/0 
	* Monitor, Peripherie und Lautsprecher
		* 400€ Budget 6/0/0

# Job-Verteiler
* Campus-Jäger spammt den Jobverteiler --> Mail an die, bzw. Block

# Umzug in größeren Raum
* Mail ans Dezernat --> Denise
