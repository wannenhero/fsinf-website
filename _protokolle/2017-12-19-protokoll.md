---


---

Anwesend: Marc (Gast), Paul, Sofia (Gast), Irene, Moritz (Gast), Verena (Gast), Denise
Protokoll: Eric

# TOPs

* Orga
* AK Lehramt
* Studienfahrt 2018
* Rückzahlung Studienfahrt 2017
* Partys 2018
* Emails
* Evaluationen (Braun/Gräbe)
* Agenda

# Anmeldetool für 34c3

* fast fertig

# AK Lehramt

* nichts neues, vertagt

# Stura-Entsendung

* Felix ist als Referent für Lehramt gewählt worden, also ist wieder ein Platz frei
* Marc hat interesse, wir entscheiden wenn wir wieder beschlusfähig sind

# Studienfahrt 2018

* Unterkunft ist reserviert
* es müsste noch einmal kalkuliert werden, ob das preislich okay ist
* Recherche im Finanzordner ist möglich
* Paul und Christofer haben den Hut auf
* Bestätigung soll im Januar erfolgen

# Rückzahlung Studienfahrt 2017

* Rückzahlung sind 'bestimmt' erfolgt
* Philipp und Christoph wissen sicher mehr
* Bestätigung der Rückzahlung erfolgt bei nächster Sitzung

# Partys 2018

* Ideen: Frühlingsgefühle-Party, Winterparty

* Party im Frühjahr 2018 ist wohl unrealistisch
* Wunsch einzelner, Weihnachtsfeier um Winterparty zu ergänzen/ zu ersetzen
  oder eine Sommerparty, oder… egal, überhaupt irgendeine Party
* Recherche bei anderen FSRä, wie viel Aufwand das tatsächlich bedeutet
* Menschen haben Interesse sich darum zu kümmern (Ideen sammeln und vorbereiten): Marc, Paul, Marc, Sofia (Arbeitskreis)

# Ersti-Broschüre

* an einigen Stellen fehlen ganz konkrete Angaben (Zeit, Zielgruppe)
* offene Lernräume sollten erwähnt werden
* Ersti-Hilfestellungen offensiver auf die Webseite stellen?

* Treffen mit Antonia/ Studienbüro soll stattfinden
* Sofia, Irene und weitere Person(en) treffen sich (mglw. Person, die "klassische" Informatik studiert)
* Sofia schreibt Antonia, dass wir ein Treffen Mitte Januar anpeilen

# Problem bzgl. Hardwarepraktikum

* nichts neues

# Evaluationen
* Neuigkeiten bezüglich Gräbes Anfrage?
* Neuigkeiten bezüglich möglicher Fragen in zukünftigen
Studiengangsevaluationen?

* Denise hat mit Fr. Braun gesprochen
  * gern können wir weitere Fragen einreichen, sie holt Rückmeldung bei Stabsstelle
  * nächste Evaluation wohl erst im Sommersemester
  * Personen müssen gefunden werden, welche sich Fragen überlegen
* Treffen mit Hrn. Gräbe hat nicht geklappt

# Agenda
* wer Lust hat sich in einen AK einzubringen, meldet sich und bringt sich ein

# FSR Geschichte
* Idee: Austausch mit anderen FSRä (etwa Geschichte) um gegenseitig Arbeitsweise kennenzulernen
* Paul bringt das ins Rollen

# Awareness-Konzept

* kollentiver Wunsch bei zukünftigen Veranstaltungen ein Awareness-"Team" zu haben (oder Personen)
* wir stimmen das auf der nächsten Sitzung ab

# Sonstiges
* Denise wird ab Anfang 2018 auf unbestimmte Zeit nicht zur Sitzung kommen (und keine Mails lesen)
