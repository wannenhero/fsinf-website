---


---

* Anwesend: Alrik, Nicole, Nancy, Fabian, Sebbo
* Protokoll: Sebbo


## Xceeth

* Umfrage abwarten (siehe Mail)

## Erstitüten

* 100 Stück sind gepackt und stehem im FSR raum bereit um verteilt zu werden

## Studienfahrt

* ca 20 angemeldet, die ersten haben bereits das Geld überwiesen
* maximal 25 Plätze noch frei
* es soll nochmal einen Newsletter speziell dafür geben (Alrik sagt Sarah Bescheid)
* wer von uns mitfährt, bitte auch das Geld überweisen

## Ersti-Infoveranstaltungen

* kommende Woche Dienstag, Mittwoch und Donnerstag jeweils 15 Uhr im Felix-Klein-Hörsaal
* Erstitüten verteilen, Newsletter-Zettel austeilen, FSR vorstellen, Sebbo schickt Präsentation rum

## Kneipentour am 15.10.

* Waldi und Beyerhaus (wollen am Montag davor nochmal erinnert werden) gehen klar
* Noch ins Jet? Würde zeitlich eng werden.
* Moritzbastei wird auch geklärt

## Prof. Dr. Lindemann

* macht wieder Computational Advertisung statt wie der Wechsel wäre Mobile Adv.
* am Dienstag in der StuKo ansprechen: Martin, Fabian, Nancy, Sarah (Stellvertreterin)
