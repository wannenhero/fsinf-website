---


---

**Anwesend**:
 * Niclas (Gast)
 * Marvin (Gast)
 * Bianca (Gast)
 * Paul 
 * Georg (Sitzungsleitung)
 * Moritz
 * Christofer (Protokoll)
 * Ahmad
 * Natascha

## 1 Orga
* Newsletter - bis zum 29.04 bei der Sitzung an dem Tag nochmal besprechen
* "Neue" Website - aktuell werden Änderungen nicht angezeigt, da der Site Builder kaputt ist.
	Website ist kaputt - gerade statische Website über Fabian Heusel
	gerade Lösung mit Docpad - einige Node.js dependencies
	Paul schlägt Jekyll oder alternative vor
	Georg würde sich mit darum kümmern, aber könnte ein paar Wochen dauern
	Georg hat die "Mütze" auf und Paul hilft ihm
* neue Domain und Email-Adresse?
	fsinf@fsinf sehr seltsam
	catch all auf die fsinf Domain für beliebige Mails ? (sehr viel Spam)
	Ahamd würde es nicht ändern für reine ästhetik
	eine Möglichkeit wäre fsinf.uni-l.de oder beliebig andere
	Paul überlegt sich was und stellt es demnächst vor
	alte Mail würde beibehalten, aber nicht mehr kommuniziert werden
* Sitzungsraum buchen für die Termine
	Christofer bucht die A531

## 2 Wahlen
* Klopapier mit FSR Mathe vor der Wahl 
* Paul schreibt nochmal dem FSR Mathe (gemeinsame Wahlwerbung etc.)

## Feierliche Zeugnisübergabe
* Ahmad würde die Kommunikation mit FSR Mathe und FSR Wiwi übernehmen

## 3 Finanzen
* 35C3 Auszahlungen
	Philipp muss noch unterschreiben
* "Citizen Science, Build Your Sensor", es kam die Abrechnung 
	Moritz und Philipp überweisen das Geld
* Kassenprüfung (→Mail)
	Philipp, Moritz und Paul treffen sich mit Malte Kobus (Kassenprüfer)

## 4 Emails
* Anfrage von Nina Heinke zur Förderung von Einat und Zuroff
	Paul hat sich schon gekümmert
* 26.04.2019 ab 18.00 Uhr FSR Vernetzungstreffen 
	Paul und Marvin haben interesse
	nächste Woche nochmal nachfragen

## 5 Anwesenheit bei MedInfModulen
* Paul und Christofer wollten mit Herrn Neumann reden
* vertagt 

## 6 Übungszettelabgabe Lineare Algebra
* Paul hat mit C. Eder gesprochen
* die Zettel müssen bis Mittwoch 15 Uhr ins Fach geworfen werden, aber werden auch Mittwoch rausgegeben

## 7 Treffen mit UBL
* Es geht um Situation/Ausstattung, Zukunftsideen, Probleme (zukunftsgerichtete Weiterentwicklung)
* 24.04 19 Uhr - niemand wirklich interesse
* Denise hat bereits Ideen gesammelt, die an Christopher Hermes (Referent für Lehre und Studium) weiterleiten

## 8 Vertiefungsmodul DH
* DH kann kein Berufspraktikum als Vertiefungsmodul belegen
* Paul und Christofer fragen bei Neumann nach
* wird demnächst in die StuKo getragen
