---


---

**Anwesend:** Denise, Christofer, Paul, Georg, (Gast) Moritz, (Gast) Marc 

**Sitzungsleitung:** Denise

**Protokoll:** Christofer

# TOPS
1. TI2 und Prüfungsvorleistungen
2. Wahlen Auswertung
3. Kollektivfestival
4. Master Einschreibung
5. DH: Linguistische Informatik
6. Facebook Anfrage
7. Emails
8. Corporate Design

# 1 TI2 und Prüfungsvorleistungen
* sind für mehr Wiederholungstermine im HWP
* PVL soll konkreter beschrieben werden, ist uns aber noch nicht genau genug
* Denise ist für Regelung mit 60% pro Versuch - kulantere Abprüfung
* Antestieren am Anfang des Versuchs ist notwendig, da die Laborarbeit mit Strom erfolgt, sollte aber kulant erfolgen 
* Paul ist für ein Note für das HWP, da HWP als PVL wenig mit der Prüfung zu tun hat
* Denise ist gegen Benotung von Praktika, schwer durchzuführen da 2 Personen mit Bachelor Abschluss benötigt werden und Protokollbewertungen auch schwer durchführbar sind (z.B. die Überarbeitung der Protokolle)
* Marc schlägt Bewertung der Protokolle bei 2. Abgabe vor (1. Abgabe gibt Stichpunktartig Hinweise), da wissenschaftliche Protokolle anzufertigen wichtig ist
* Gespräch mit Prof. Bogdan suchen, um über die Situation Hardware-Praktikum und Vl zu reden
* wir sind gegen die aktuelle Änderung der PVL von Ti2
* Christofer schickt Entwurf der StuKo rum und vereinbart Termin mit Prof. Bogdan

# 2 Wahlen Auswertung
* sind für die Hilfe bei der Wahl beim FSR Germanisitik zum Grillen eingeladen (wahrscheinlich 21.06)
* Konstisitzung - Doodle mit Wochen zum eintragen -> Paul erstellt
* zu wenig Berliner - je nach Anzahl Erstis mehr Berliner (Erstwähler gehen länger wählen)
* mehr Wahlbeteiligung - vielleicht größeres Wahllokal ?
* nächstes Jahr Auszählung in der Fakultät
* zu wenig Wahlhelfer während der Wahl und bei der Auszählung (immer 3 während der Wahl)
* Listen aufteilen - schneller abarbeiten?
* nächstes Jahr Werbung für Briefwahl

# 3 Kollektivfestival
* Reminder: Vortrag Ethik in der Informatik Freitag 15:30 im Felix-Klein-Hörsaal
* Ahmad macht Werbung bei Berechenbarkeit

# 4 Master Einschreibung DH
* Fakultät hat vor die Einschreibung zum Sommersemester zu ergänzen, das muss aber noch durch alle Gremien
* unklar ob dies zum nächsten SoSe erfolgt
* Im September Reminder ans Studienbüro

# 5 DH: Linguistische Informatik 
* Christofer, Irene und Lukas Treffen sich mit Prof. Heyer am 21.06.

# 6 Anfragen von Leo via Facebook
* https://adamag.de/programmieren-unter-zwang-gewerkschaft-slack-startup
* alle schauen es sich nochmal an
* vertagt

# 7 Emails
* 70 Jahre Israel: Eric ist dran - dauert aber noch
	Denise möchte, dass wir den Organisatoren gerne eine Email schreiben, da sie keinen Grund für Verzögerung sieht und gerne Transparenz möchte
	Denise spricht sich mit Eric ab

# 8 Corporate Design
* Denise schlägt vor Meike vom Mathe FSR zu fragen
