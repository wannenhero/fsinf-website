---


---

##Anwesende: 
* Nicole
* Fabian
* Alrik
* Benjamin
* Sebastian
* Nancy
* Sarah
* Martin
* Kasimir

## TOPs
1. Wahlen
2. Transponder
3. Winterparty
4. Server/Webseite

### 1. Wahlen
#### Sprecher_in + Stellv. Sprecher_in
##### Kandidaten
* Alrik
* Nicole (Stellvertretung)

##### Abstimmung
* Alrik: 9/0/0
* Nicole (Stellvertretung): 8/0/1

#### Finanzverantwortliche_r + Stellv. Finanzverantwortl.
##### Kandidaten
* Benjamin
* Kasimir (Vertretung)

##### Abstimmung
* Benjamin: 8/0/1
* Kasimir (Vertretung): 8/0/1

#### Weitere Kontovollmacht
##### Kandidaten
* Alrik
* Nicole

##### Abstimmung
* Alrik: 7/0/2
* Nicole: 7/0/2

#### StuKo (3 Plätze + 1 Stellvertretung)
##### Kandidaten
* Fabian
* Nancy
* Martin
* Sarah (Stellvertretung)

##### Abstimmung
* im Block: 8/0/1

#### StuRa (2 Plätze + evtl. Stellvertretung)
##### Kandidaten
* Kasimir
* Fabian
* Benjamin (Stellvertretung)

##### Abstimmung
* im Block: 6/1/2


#### Sonstige "Ämter"
##### Erstifahrt Orga
* Nicole
* Alrik
* Nancy
* Martin
* Kasimir

##### Newsletter
* Sarah + (Kasimir)
* Kasimir kümmert sich um Ankündigungen via twitter + "Webseiten-News-Sparte" + facebook

##### Partybeauftragte_r
* Sebastian 8/0/1

### 2. Transponder
Manuel, Daniel und Katharina müssen Ihre Transponder zum Stichtag 01.04.14 abgeben:
#### Matching an Janassary melden
* Daniel --> Sarah
* Manuel --> Nancy
* Katharina --> Martin


### 3. Winterparty

### 4. Server/Webseite
#### Server
* virtualisiert in der Hyper-V-Cloud von ZD Informatik
* 2 virtuelle Cores, 2GB Ram
* WDE mit Pre-Boot-Environment zum Eingeben des Decryption-Keys via SSH

#### Webseite
* erste Gehversuche mit Static-Site-Generator "Docpad"
