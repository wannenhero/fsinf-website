---


---

Anwesend: Denise, Manuel, Verena, Fabian, Eric, Georg, Martin


### Kneipentour
* Friedrich kümmert sich um Details des Abends
* Fabian hat sich um die MB gekümmert.
* Georg macht Schild mit „Kneipentour“. 

### Studienfahrt
* Einkaufsliste muss fertig gestellt werden
* Felix Ramberg kommt mit. Bekommt offiziellen Platz
* Friedrich plant Schnitzeljagd
* Kasimir leiht uns Lautsprecher aus.
* Es gäbe noch 5 weitere Nachrücker, gute Resonanz
* Nötige Ausgaben können nicht privat gestemmt werden
--> Abstimmung: Denise (Finanzverantwortliche) und Georg (Kontobevollmächtigter des letzten Semesters und noch aktuell im Unterschriftsverzeichnis) gehen Donnerstag oder Freitag zur Sparkasse (nach Rücksprache mit dem Stura) und heben 700€ vom FSR-Konto ab

Ergebnis der Abstimmung: 6/0/1

Nachtrag vom 15.10: Rücksprache mit dem Stura ist erfolgt. 
Ausnahmesituation, da Finanzeinweisung und Termin bei Sparkasse für neuen stellv. Finanzverantwortlichen und die neuen Unterschriftenbevollmächtigten noch aussteht.

### GitLab
* Fakultätsfremde Studierende --> Solange ein Account für den Solaris-pool vorhanden, kein Problem. Verweisen an Fabian Schmidt, um Account zu bekommen.
* Extra Mail-Adresse für Fragen, anstatt an den Verteiler? 

### Unregelmäßigkeiten in Modulen und Beschwerden von Studierenden
* Verfassen Email an Prof. Dr. Scheuermann in Bezug auf betreffende Module.

### Referent für FSR-Kommunikation
* Möchte unsere Sitzung besuchen. Sobald neuer Termin gefunden ist, melden wir uns bei ihm.

### Newsletter
* Newsletter: Deutliche Verbesserungen bemerkbar, nächsten Newsletter in txt/md abschicken.
* Newsletter-Archiv auf FSR-Seite einrichten.

