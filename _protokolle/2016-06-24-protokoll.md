---


---

Anwesend: Verena, Manu, Eric, Martin, Denise
Protokoll: Denise

###TOPs
Berufungskommission
Studienfahrt
Sommerfest
Gitlab
Logo
CryptoParty
Ventilator

1. Berufungskommission
Software Architektur: Martin
5/0/0

Computational Humanities: Eric
5/0/0

Didaktik in der Informatik: Verena, Manu
5/0/0 Verena
5/0/0 Manu

Zwei Studierende noch zu entsenden



2. Studienfahrt
Eric schickt alte Mailfassungen an Denise und bearbeitet Formular
- Manu dabei, Verena evtl nachkommen, Christopher und Dominik geben nochmal Rückmeldung
- Hochseilgarten
- HoPro Workshop

--> Verena und Manu wollen mit planen


3. Sommerfest


Mit Germanistik Weihnachtsfeier: Nur keine Gedichte (Verena)


4. GitLab
Eric schickt Mail raus

5. Logo

Muss auf tshirts druckbar sein


6. CryptoParty
Warten auf Rückmeldung
Verena bleibt auf dem Laufenden

7. Ventilator
FSR kauft Standventilator

5/0/0
