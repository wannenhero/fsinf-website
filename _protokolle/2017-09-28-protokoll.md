---


---

Anwesende: Tim, Eric, Verena, Ahmad, Denise, Philipp, Christofer, Paul
Protokoll: Denise

### Tagesordnung
1. FSR Wohlfühlatmosphäre + Netzwerkkommunikation

2. FSR Sitzungstermin

3. Erstibeutel

4. Kaffee- & FSR-Raum Facility Management

5. FAQ Webseite

6. Studienfahrt Alkohol

7. Änderung (& opt. Verschlüsselung) FSR-interner Passwörter

8. FSR Logo

9. Studiengangsevaluation

#### Oase
Oase kümmert sich um Getränke, wir ums Essen.

Beginn: 18 Uhr

Jemand trifft sich mit den Studis an der Uni und nimmt die Tram.
(Christofer (trifft sich 17.30 Uhr)

Paul und Denise gehen einkaufen

Planen mit ca. 100 Leute

Idee fürs Essen:
Thüringer 100
Steaks 50
Grillkäse und Gemüse
Brötchen 150
Käse
Tofu
Gemüse
Ketchup

Nochmal Oase kontaktieren wegen Kohle, Tellern und Besteck und wann wir
da sein sollen.

Handkasse beantragen

Denise, Paul,
Tim vllt.
Ahmad vllt
Rest fragen


#### Wohlfühlatmosphäre

Tim: Telegram, Logo
Grenze erreicht (50+ Nachrichten)

Sitzungskommunikation über Telegram suboptimal.


##### Abgrenzung Email und Telegram
Telegram für kurzfristige Sachen.

Emails: Wie schaffen wir es, rechtzeitig und auf jede Email zu
antworten? Tim wünscht sich 100% Reaktionsquote. Zustimmung im Plenum.
Diskussion über Lösungsansätze. Verschiedene Ideen, um zuverlässiger zu
sein Emailverantwortlichkeit rotierend?
--> Nochmals nächste Sitzung ansprechen.


#### Sitzungsdoodle

Jeder schaut noch mal drüber. Tendenziell Dienstag 16 Uhr.


#### Erstibeutel

Morgen 11 Uhr, Bier.

Eric kann ab 13 Uhr
Ahmad ab 13 Uhr
Tim


#### Kaffee- & FSR-Raum Facility Management

Ahmad hat seinen eigenen. Müssen evtl nochmal die Betreffenden drüber reden.


#### Webseite Artikel Shops

Abstimmung nächste Woche. Bis dahin verschwindet Liste.


#### Studienfahrt Alkohol

10€- Debatte.

Mischkalkulation

Selbstorganisierte Bar (zu Timeslots Bier kaufen)?

Vorschlag: Jeder zahlt bei Fahrtbeginn, was er trinkt. Dann wird
Strichliste geführt. Am Ende gibts evtl Geld raus oder man muss nachzahlen.


Flatrate 0/4/2

Strichliste 3/0/3

Guthaben 5/1/0 Paul ist verantwortlich

#### Passwörter
Eric ändert die Passwörter und legt es in gpg-verschlüsseltes Repo

#### FSR Logo

Paul hat Vorschlag. Automat mit Gebäudeumriss. Stößt auf Zustimmung.
Nächste Sitzung Weiterentwicklung.

#### Nächste Sitzung
Doodle abwarten.
