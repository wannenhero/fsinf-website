---


---

Anwesende: Sofia, Eric, Irene, Moritz, Bianca, Georg, Paul, Marc, Ahmad, Denise
Protokoll: Paul
Sitzungsleitung: Georg

## 1 Awarenesskonzept
- steht in meinem Notizbuch und in Mail an FSR-Mathe
- Christofer organisiert Aufkleber für T-Shirts beim Sommerfest

## 2 TI-2 und Prüfungsvorleistungen
- es gibt Gespräch zu Prüfungsvorleistungen und DH
- Weitere Anmerkungen über den Verteiler
- Treffen nächste Woche Mittwoch

## 3 Finanzen
- es gibt neue Regelung für Raten
- bis Ende Juni müssen für die Rate Beschlüsse da sein damit unser Konto leergeräumt wird
- es muss Beschluss für Geld für die Raten geben
- Christofer erinnert Philipp dass der ne Mail für die Beschlussauflistung schreibt

## 4 Kollektivfestival Auswertung
- knapp 40 Leute
- sehr guter Vortrag, interessante Diskussion
- Paul will wieder Vortrag organisieren
- Publikum Philsoph*innen und Informatiker*innen
- FSR Philo ist für weitere Kooperationen offen

## 5 Digital Humanities und Linguistische Informatik
- Protokoll im Verteiler
- Heier sieht Handlungsbedarf im Studiengang DH
- Vorschläge von ihm und uns
- ADS für DH?
- Prof. Burkhardt will Statistikmodul für DH Master, vielleicht stattdessen im Bachelor?
- MuP und ADS zusammenlegen?
- Crane ist nicht mehr lange da, hat nurnoch halbe Stelle
- Punkte sollen in StuKo angebracht werden
- Linguistische Informatik:
 - keine einzelne DH-Klausur
 - intensiv mathematische Inhalte sind nicht Klausurrelevant
- hat Christofer was vergessen?
- S: Warum neues Statistikmodul wenn DHler in existierende Statistikmodul reinkönnten? (z.B. von IMISE)
- sonstige Anmerkungen per Mail (u.A. von Denise)

## 6 Emails

- Leipziger Straßenmagazin
 - Man könnte sie an Sarah Sterz weiterleiten, bei uns keine entsprechende Person
 - E: weiterleiten an Brewka für Demystifikation?
 - C. antwortet die nächsten Tage
- Leipziger Schriften als E-book bewerben?
 - G: mal mit auf sozialen Medien teilen
 - E.: an einen Fehler nicht unbedingt den zweiten anschließen
  - fand Inhalte nicht überzeugend
  - ist Leidenschaftslos
 - M: gegen neue Teilung
 - D: möchte die Diskussion unterbinden
 - Abstimmung 3/0/3 -> wird geteilt

- TV-Club 
 - fragt nach Dingen für Erstibeutel
 - P: Gegen jegliche Assoziation mit TV-Club
 - keine weitere Reaktion

- 70 Jahre Israel
 - Vorbereitungskreis kalkuliert noch
 - Weiß von unserem Unterstützungsvorhaben
 - D: offizielle Email schreiben damit es nicht mehr privat über E. läuft
 - S: Timelimit setzen?
 - D: Projektabrechnung hat nach Ende 3 Monate Frist vom StuRa für Abrechnung, können uns da anschließen
 - D: schreibt Mail mit Infos und fragt nach Klarheit

## 7 Anfrage von LEO via Facebook 
- D: Person möchte Vortrag über Arbeitsbedingungen in der Programmierbranche veranstalten
- E: Es gab Zeitungsartikel 
- Vertagt weil nur Christofer Ahnung hat2

## 8 Ordnung im FSR-Raum
- I: erklärt sich dazu bereit neues Sofa mitzuorganisieren
- Raum sieht aus wie Rumpelkammer 
- Diskussion vertagt

## 9 Hakenkreuzsachen auf dem Klo
- man müsste heute drüber reden
- D: verfasst heute Newsartikel, schickt ihn rum
- P: Schmiererei noch nicht voll weg
- D: schreibt Janassary 
- E: Kennt jemand einen diskriminierten Nils? Möglicherweise noch andere Mobbingauswirkungen
- I: Kennt keinen Nils
- M: Politisch motiviert

## 10 Corporate Design
- vertagt
