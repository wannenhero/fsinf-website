---


---

**Anwesend:** Paul, Tim, Ahmad, Marc (Gast)

**Sitzungsleitung:** Irene (Sitzungsleitung)

**Protokoll:** Graubi (Protokoll)

## 1 Orga
* Sitzungsdisziplin
 (--> Wird in Konsti-Sitzung besprochen)
* Ämter
 Kurze Amtsübersicht für Neuzugänge, schon mal Gedanken machen, was man von Ämtern wie Öffentlichkeitsarbeit hält
 Marc will gerne ins Plenum/Stuko gewählt werden bei Konstisitzung (unabhängig davon ob er da ist)

## 2 Lehrberichte
* Wie ist der Zwischenstand?
 Fertig ist: Info Bachelor + Master; BioInf Master; DH Bachelor
* Lehramt Info Lehrbericht - Ausstehend
* DH Ergänzungen?
 Paul formuliert eine treffendere Beschreibung zum Punkt "Digital Philology"
 Berichte sollen am 13.07. alle weggeschickt werden
 
## 3 CCC
* Marc schreibt Statement zum Vorfall und schickt Vorschlag in den Verteiler

## 4 Newsletter
* DSGVO, verschoben - brauchen Erics Statement, Irene will demnächst Newsletter raushauen

## 5 Praktische Übungen WiSe 18/19
* aktueller Stand?
 praktische Übungen über 3h in den Pools wird es nicht mehr geben, "Offener Info-Raum" wird eingerichtet 

## 6 Emails
* Nichts.

## 7 Corporate Design
* Denise fragt Meike - was passiert --> Vertagt, Denise abwesend
