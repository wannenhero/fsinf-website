---


---

**Anwesende: Josy, Christofer, Moritz, Denise, Sofia, Marc**

Protokoll: Denise

## Tagesordnung
1. Orga
2. Wahlen
3. Studienfahrt
4. Emails
5. Zulassungsbeschränkung
6. MINT-Frauen Leitfaden
7. TI
8. Mündliche Prüfungen
9. Corporate Design
10. Newsletter
11. Tor-Exitserver
12. Nächster Sitzungstermin

## 1 Orga
### Briefmarken
Haben Antwort von Frau Wenzel wegen Postausgang im 5. Stock. Müssen Dr. Janassary fragen, ob es eine Vereinbarung für die Kosten gibt.

---> Denise fragt Janassary

### Sitzungen in neuem Semester
---> Sofia erstellt Doodle

### Sitzungseinladung
TO-Agenda online stellen vor Sitzung?
Bot und entsprechendes Configfile
---> Marc und Josy

### FSR Raum: Pfand
Kommt weg

### Stura
Moritz braucht entsprechendes Dokument, welches ihn dauerhaft entsendet. Kümmert sich.

### AG-Satzung: Update
Marc hat schon mit Vorlage angefangen, muss sich mit Sächs. HochschG. auseinander setzen und mit entsprechenden Personen sprechen.

## 2 Wahlen
### Wahlwerbung und Veranstaltungen
Es bestehen Ideen zu Wahlplakaten. ---> Vertagt, um in größerer Runde zu besprechen

Wahlveranstaltung: ---> Mitte Mai im FKH, Bier, Brezeln, lockeres Ambiente

## 3 Studienfahrt
Buchung bestätigt, 1. Anzahlung ist erfolgt.
Bus muss noch gebucht werden. Sobald Paul wieder da ist, wird das erledigt
 Denise möchte die Küche übernehmen (evtl gemeinsam mit Eric).

## 4 Emails
### 70 Jahre Israel
Wollen wir eine der Veranstaltungen unterstützen? Email dazu vom 28.2, Felix Fink --> Vertagt wg Beschlussfähigkeit

### Teach First: Jugendliche in sozialen Brennpunkt
Sehen Teach First kritisch und wollen dies in keiner Form unterstützen.

### Informationsveranstaltung IMISE für M.Sc. / B.Sc.
Bewerben via Twitter und Newsletter

## 5 Studiengangmodalitäten
### Studienerfolg: Treffen mit externem Gutachter/ Berater am 19. April
Wer geht hin? - oder direkt mit dem ganzen FSR?
Denise hat mit Prof. Scheuermann kurz gesprochen, ideal wären 2-3 Personen in kleinem Rahmen. Wichtig sind Vertreter beider Bachelor-Studiengänge zu entsenden.
DH (1 Person): Marc || Irene
Info B.Sc. (2 Personen): Josy | Christofer | Denise 

---> Christofer schreibt Mail an Scheuermann mit Terminzusage
---> TODO: Nächste Woche besprechen, was wir in dem Gespräch besprechen


### Zulassungsbeschränkung
Zulassungsbeschränkung wurde für Bachelor Informatik und DH beschlossen. Möchten treffen mit Dekanat vereinbaren und Vorschläge zur Gestaltung einbringen.

Bisherige in vorherigen Sitzungen erarbeitete Vorschläge:
  Freiwilligendienst
  Ehrenamt
  Kind
  Auswahlverfahren, Test?
  Zweiter Bildungsweg
  Ausbildung in der Richtung
  Hochschulzulassung über Ausbildung

## 6 Material für Studierende
Haben verschiedenes Material, welches sich zB bei der Wahlveranstaltung auslegen lässt.

### MINT Frauen Leitfaden
Exemplare liegen nun im FSR-Raum. Sollten sich alle mal in Ruhe anschauen.

### Free Software Foundation (FSF)
FSF twittern, dass wir Merch (Sticker, Postkarten, Flyer) haben?

## 7 HWP und TI2
Denise hat nochmal mit Stura-GF Sebastian Adam gesprochen. Nach unserer Entscheidung, keinen Handlungsspielraum für den Studierenden zu sehen, ist der Fall nun gänzlich beim Stura und Justitiariat. Sebastian versteht unsere Position, möchte aber die Prüfung nicht zurückziehen. Er hat auf sein Telefonat mit dem Justi verwiesen, in dem die Sache nicht so kritisch rüber kam wie wir es einschätzen. Eine genaue konkrete Antwort des Justi ist in 3-5 Monaten zu erwarten. Im Allgemeinen scheinen MINT-Studierende und Vertreter Prüfungsvorleistungen eher zu befürworten als in den Geisteswissenschaften. Prüfungsvorleistungen dienen als Schutzmechanismus für die Studierenden, damit sie Prüfungen bestehen.

Denise ist der Meinung, dass wenn der Studierende die Prüfungsleistung trotz unvollständigem HWP anerkannt bekommt, dass dies Vorleistungen bzw die Pflicht hierzu ad absurdum führe.

Das Plenum diskutiert über das HWP und Prüfungsvorleistungen. Es bestehen verschiedene Ideen, Lösungsansätze und Veränderungswünsche.
---> Outcome der Diskussion: Wir möchten allgemein das HWP sowie Prüfungsvorleistungen nochmals im Plenum besprechen. (TODO nächste Sitzung)

## 8 Mündliche Prüfungen
Ungereimtheiten in Prüfungsterminbekanntgaben in versch. Modulen, insbesodnere kurzfristige Bekanntgaben und Änderungen. Konkret folgende Module:

- Genomik und Sequenzierung
- Statistik, insbesondere Clash mit ADS für Bioinformatiker, die ADS machen mussten
- Künstliche Neuronale Netze

PO gibt eine "soll-Regelung" von 4 Wochen und 2 Wochen als "Muss-Regelung". Letztere wurde gerade so eingehalten.

---> Sofia und Christofer kontaktieren Prof. Bogdan für ein Gespräch

## 9 Corporate Design
Neuer Name?
Welches Ausschreibungsverfahren für Logo?
---> Vertagt

## 10 Newsletter
- MONAliesA Stellenausschreibung bewerben?
- Studienfahrttermin- und Ort bereits bewerben?
- Medizinische Informatik Infoveranstaltung
- Wahlveranstaltung und Wahlen

---> An Irene weitergeben; vertagt

## 11 TOR-Exitserver
Aktueller Stand?
---> Vertagt

## 12 Nächster Sitzungstermin
Dienstag, 3.4, 15:30 Uhr, P-901
