---


---

Anwesend: Marvin (Gast), Moritz, Philipp, Paul, Georg, Christofer, Natascha
Sitzungsleitung: Christofer
Protokoll: Natascha

#### Tagesordnung
1. Orga
2. Wahlen
3. Finanzen
4. Emails
5. SWT

## 1. Orga:

  * Sitzungstermine SoSe
    * Doodeln für neuen Termin: erstellt durch Moritz
    * Nächste Sitzung: 05.04 Fr 11.00

## 2. Wahlen:

  * 12 vs 11 Sitze?
    * Alle sollen reinkommen, für alle 1 Platz mehr
    * Abstimmung für Erhöhung der Sitzzahl auf 12: 5/0/1

  * Wahlen Räumlichkeit
    * Augusteum vs Ziegenledersaal?
    * Abstimmumg für Ziegenledersaal: 6/0/0

  * Wahlverantwortliche
    * Wahlen am 4/5. Juni
    * Aufstellen bis zum 07. Mai
    * Wahlverantwortlicher: Christopher, Natascha
    * Wahlvorstand: Natascha, Georg, Bianca
    * Wahlausschuss: Moritz
    * Blatter liegen zum ausfüllen im Büro und werden nach der nächsten Sitzung von Paul abgegeben

  * Mit FSR Mathe Koordinieren
    * Gemeinsame Wahlplakate sind anzustreben
    * Mail an Mathe FSR: von Georg

  * Werbung
    * Paul will mehr als nur Sprüche machen
    * Kleine Motivations/Vorstellungstexte
    * Aktiv in Vorlesung gehen und bewerben
    * Offene Sitzung? * müsste thematisch interessant sein
      * zB. Artikel 13, Uploadfilter
      * falls ähnliches aufkommt gerne während des Semesters

  * Plakate
    * bis 7.5 Leute mit Zweitstudium können sich entscheiden an welcher Fakultät sie wählen wollen
    * bis 20.5 Beantragung der Briefwahl

## 3. Finanzen

  * Mail für die nächste Rate schreiben : Philipp

  * 35C3
    * noch keine Rückmeldung von Mathe und Philo
    * grade bei 107€ pro Person
    * wird von Philipp und Moritz fertiggemacht

  * Förderantrag AG für kritische Informatik (Mail vom 20.03)
    * Abstimmung Geld für Einzelteile
    * Förderung des Luftdatenworkshops (250€): 5/0/1

## 4. Emails

  * Unibib*Sache (FSR Jura und Referat LuSt)
    * Keine Drucker/Kopierer dafür Scanner:
    * Grund: Vertrag mit Canon ist abgelaufen und besser für die Umwelt
    * gibt eine Aktion dagegen
    * keine akute Betroffenheit der Informatikstudierenden
    * andere Leute kümmern sich drum

  * Nachhilfe
    * wir sind keine Vermittlung, Verweis auf Jobverteiler, in Pools/ Facebookgruppe fragen, oder Aushang an offenen Pinnwänden
    * Mail geschrieben von Christopher

  * MI Connect
    * Twittern, Posten auf Facebook der Veranstaltung: Christofer

## 5. SWT

  * Wie ist der Stand?
    * Scheuermann hatte Prüfung zugeschickt: MultipleCoiseKlausur
    * Modulprobleme wurden an ihn nicht direkt, sondern über StuKo weitergetragen
        * Christopher spricht nochmal direkt mit Scheuermann
