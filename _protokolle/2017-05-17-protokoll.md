---


---

* anwesend: Martin, Dominik, Verena (Gast), Christofer, Denise, Eric, Paul 
* Protokoll: Eric

# TOPS

* Wahl
* Mail DH
* Gitlab/ Wiki
* Drucker
* Studienfahrt
* Trends in der Inf.
* Mail Stura

## Wahl
* Denise hat Schichtplan erstellt

## Mail DH
* Treffen mit Person für Rückfragen: Christofer und weitere Person
* Christofer: Fragt Z., ob es Anfänger-Kurse noch gibt und was deren Alleinstellungsmerkmal ist
* Hackathons und "sonstige IT-Events": [allgemeine Debatte zum Umgang mit Werbung]

## Gitlab/ Wiki
[verschoben]

## Drucker
[verschoben]

## Studienfahrt
[verschoben]

## Trends in der Inf.
[verschoben]

## Mail Stura
[verschoben]

