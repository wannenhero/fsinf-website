---


---

* Anwesend: Nancy, Sarah, Martin, Fabian, Nicole, Alrik, Kasimir
* Protokoll: Alrik


## Xceeth
* Kenntnisnahme über Sachverhalt an alle FSRä (7/0/0)

## Erstiinfomails
* geht dieses Jahr nicht, nächstes Jahr über AlmaWeb vielleicht


## Erstitüten
* Stück: 100
* Inhalt: 
	* Erstiinfo Kasimir druckt
	* Krosti
	* Flyer, "How to Study"
	* StuRa Flaschenöffner

## Finanzen
* Anzahlung Studifahrt ist schon raus
* wir bekommen noch diverse Restbeträge

## Kneipentour

* Mittwoch, den 15.10.14, nach der MuP-Vorlesung
* Sebbo reserviert MB
* Kasimir Hörsaal-Reservierung + Mail an Prof.
* Kasimir Dance Dance Revolution
  * Kauf eines USB-Verlängerungskabels für maximal 12 Euro: 7/0/0
* Fabian + Doreen für Spiele unterwegs
* Kneipen: Cafe Waldi, Bayerhaus, eventuell Jet, Moritzbastei (Orga: Sarah und Nancy)

## Erstipropaedeutikum

* erste Übungen Nicole und Alrik (Newsletter-Zettel + Infos)

## Studifahrt

* Wer kommt vorläufig mit: Georg(Fahrer), Martin(Co-Fahrer), Daniel(Bespaßung), Alrik (bezahlt Transporter), Nicole, Nancy, Antonia(Essen), Fabian Exe (Bespaßung), 
* Anmeldung auf Webseite
* AnmeldeStart ab Propaedeutikum
* Buchung neue Fahrt erst nach Abrechnung dieses Jahr


## Nächste Sitzung:

* 2.10.
* Verantwortlich: Nancy
