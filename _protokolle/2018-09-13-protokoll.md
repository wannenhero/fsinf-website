---


---

**Anwesend:** Eric, Denise, Christofer, Moritz, Ahmad, Sofia , Georg, Josy, Natascha

**Sitzungsleitung:** Moritz

**Protokoll:** Christofer

## Tagesordnung
1. Orga
2. Studienfahrt
3. HWP
4. Semesterauftaktparty
5. Einführungsveranstaltungen & Proädeutikum
6. 35C3
7. Emails

# 1 Orga
* PO - Paul nicht da -> vertagt

# 2 Abstimmung Studienfahrt
* Denise hat die Kopien vom StuRa abgeholt für die Kalkulation
* Transporter -> Betriebsfahrerlaubnis muss der Vorgesetzte unterschreiben
* Marc, Moritz und Ahmad dürften fahren
* bräuchten aber Erlaubnis vom Vorgesetzten 
* Moritz fragt am URZ nach
* Marc fragen, ob er es machen würde
* Ahmad will eher nicht
* Abstimmung Teilnehmerbeiträge der Mitarbeitenden wird zu 50% übernommen
	4/0/2 -> wird angenommen
* Teilnehmerbeitrag wurde im Januar abgestimmt -> Christofer und Paul kalkulieren und geben Ergebnis über Mailverteiler bekannt
* nächste Woche im Propädeutikum bewerben

# 3 HWP Evaluation
* Paul nicht da -> vertagt

# 4 Semesterauftaktparty
* Auftaktparty mit Germanistik
* Germanisitk hat TV-Club oder Neues Schauspiel vorgeschlagen
* die Interessierten (siehe letztes Protokoll) sollten sich mal mit dem FSR Germanistik treffen
* 15.10.-22.10. wäre sinnvoll, da keine Überschneidung mit Studienfahrt
* Moritz schreibt Mail

# 5 Einführungsveranstaltungen und Propädeutikum
* Propädeutikum
	Ahmad wollte in den Übungen bewerben, aber finden nicht statt
	Ahmad und Christofer gehen in die VL
	Moritz-Bastei Abend mit den Leuten -> Ahmad fragt in der MB nach
* Moritz bewirbt nochmal die Erstigruppen	
* Masterkneipenabend:  Donnerstag 11.10 20.00 Uhr für 30 Personen reserviert
* Bachelorkneipentour
	Sofia, Moritz und Christofer treffen sich
	Ersti-Woche oder erste Vl Woche wäre gut
	ca. 8 Personen wären gut zum laufen

# 6 35C3
* Marc wollte nachfragen, wegen Förderung -> vertagt

# 7 Emails
* AK Lehramt
	wir haben keine Mitglieder im FSR die Lehramt studieren
	Christofer antwortet
	Bewerbung bei Einführungsveranstaltung 
* Erstitüten
	kein Sponsoring
	Eric schreibt Stura, dass wir 300 Beutel brauchen
* Karriere-Ratgeber EFellows
	kein Interesse
* UniNow
	kein Interesse
* Auslobung Wolfgang-Natonek-Preis und Theodor-Litt-Preis
	Frist ist der 30.10.	
	Theodor-Litt-Preis
		Denise Vorschlag  
			Holger Wuschke - mit FSR Mathe absprechen
		Josy Vorschlag
			Prof. Maletti
		Sofia Vorschlag
			Zasha Weinberg Bioinformatik
	Wolfgang-Natonek-Preis
		Denise Vorschlag
			Christofer	
	weitere Überlegungen möglich bis zum 30.10.
	Denise würde als (stellvertende) Sprecherin ein Schreiben für Christofer aufsetzen			
 	
