---


---

Anwesend: Georg, Verena, Eric, Thomas, Denise, Nancy, Martin, Fabian, Sebastian
zu Gast: Dorothea (ProKooperation)
Protokoll: Eric

# ProKooperation
* Dorothea vom Projekt ProKooperation ist anwesend
* Informatik ist Pilotprojekt
* aktueller Stand: Umfrage hat stattgefunden, Gepräche wegen Anrechnung von Leistungen haben stattgefunden, Gespräche mit Lehrenden laufen
* Bewerbung und Informationsverteilung würde über den Fachschaftsrat laufen
* soll es im Herbst wieder geben: allerdings noch keine Details zu Veranstaltungen klar
* Einschreibung soll über Parallelstudium funktionieren
* ProKooperation soll ins FAQ
* Informationen kommen demnächst nochmal alle gebündelt zu uns
* Ziel wäre es, dass Menschen aus dem Fachschaftsrat kompetente Ansprechpersonen sind
* Kooperation bezieht sich lediglich auf den Master Studiengang
* Flyer für Master Studierende sind geplant, Kontakt zu Master Erstsemestern wird versucht bei der Einführungsveranstaltung herzustellen
* Folie zu ProKooperation soll in die Einführungspräsentation
* in Planung: kooperative Master-Arbeiten und Promotionen

# Probleme beim Hardwarepraktikum
* informelles Gespräch mit Prof. Bogdan hat stattgefunden
* StuKo wird demnächst über Modulhandbuch sprechen
* statt 6 Versuche müssen 5 erfolgreich sein, die letzten 5 sollten zählen: der FSR schätzt diese Auslegung als nicht ungerechtfertigt ein
* da das Praktikum bald vorbei ist, ist Eile ist geboten
* Praktikumsbetreuer sollen im Praktikum für Klarheit sorgen
* bestimmte Mail an Prof. Bogdan soll geschrieben werden (Denise macht Vorschlag im Pad)
* Inhalte für Mail: 5 aus 6 Versuche müssen bestanden werden, Studierende entscheiden welche, Kommunikation der (neuen) Modalitäten über Olat und Praktikumsanleiter
* einstimmige Entscheidung, eine solche Mail zu formulieren

# Sommerparty
* ist alles klar

# Tor-Node
* Entscheidung fällt nächste Woche (Nancy schlägt Deadline vor)

# Studienfahrt
* weiterer Versuch der Kontaktaufnahme mit FSR Mathe soll bald erfolgen

# nächstes Treffen
am 9. Juli 14 Uhr (Raum P901)
