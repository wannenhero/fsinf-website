---


---

**Anwesende**: Georg, Christofer, Manu, Eric, Denise, Martin, Paul, Verena (Gast)
**Protokoll:** Christofer

# Tagesordnung
* KritNawi
* Anwesenheitpflicht Seminar Parallelverarbeitung
* MuP
* ADS
* Neuer Raum
* Newsletter
* Weihnachtsfeier Chor
* TOR Node
* Finanzen
* Kryptographie Modul
* Sonstiges

## 1. KritNawi
* Ist eine AG und sind teilzugehörig zur kritischen Einführungswoche
* Besteht aus Naturwissenschaftlern die sich im kritischen Kontext mit Naturwissenschaften auseinander setzen bzw. zusätzliche Themen zum Vorlesungsangebot anbieten wollen.
* Vorschlag der KritNawis gemeinsam Veranstaltungen zu organisieren z.B. Big Data. 
* Sie melden sich nochmal bei uns.

## 2. Anwesenheitpflicht Seminar Parallelverarbeitung
* Im Seminar geht eine Liste rum zum unterschreiben -> Anwesenheitpflicht
* Allerdings ist ein Ausweichtermin möglich.
* Paul spricht mit Prof. Middendorf.

## 3. MuP
* Vorlesung wirkt ausführlich und ausreichend genug um die Aufgaben zu lösen
* Manu und Christofer sprechen mit Dr. Zeckzer, wegen der Beschwerden

## 4. ADS
* Studiendokumente werden aktualisiert und Eric schickt den Brief ab.

## 5. Neuer Raum
* Denise spricht mit Prof. Scheuermann.

## 6. Newsletter
* Abstimmung ob Weihnachtsfeier im Newsletter beworben wird. 
* 5/1/1
* Georg schreibt einen Text.

## 7. Weihnachtsfeier Chor
* Denise fragt nach.

## 8. TOR Node
* Entwurf ist im git.

## 9. Finanzen 
* Denise hat einen Teil der Deko für 30€ gekauft. 
* Paul guckt, ob es studentische, soziale Projekte gibt die Geld brauchen.

## 10. Kryptographie Modul
* Paul fragt Prof. Droste nach Interesse.

## 11. Sonstiges
* Verena leiht die DDR Platten aus.
