---


---

**Anwesende:** Martin, Georg, Denise, Manu, Verena
**Protokoll:** Georg

**TOP** 
* Wahlen
* E-Mail-Verteiler
* Mails
* Newsletter
* Organisatorisches
* Sonstiges

# WAHLEN
* Wahlvorstandsschulung: Alle Wahlvorstände müssen zu einem der verbleibenden Termine gehen, Denise hat E-Mail weitergeleitet
* Denise erstellt Doodle zur Besetzung des Wahllokals

# E-Mail-Verteiler
* Wer soll alles auf der Liste sein? klare Regelung gewünscht
* Kriterien:
	- Wille zur dauerhaften Beteiligung
	und/oder
	- Mitglied in Gremien
	
	Alte Mitglieder:
	- Halbes Jahr Karenz, werden bei dauerhafter Beteiligung nicht entfernt
	- Vor der Entfernung Mitteilung darüber

* Sollen diese gelten? 5/0/0 -> Angenommen

# Mails
* zu viele Jobangebote, Hinweis auf Extra-Mailingliste
* Programmierwettbewerb TU München: leider sehr kurzfristig, aber grundsätzliches Interesse derartige Veranstaltungen zu unterstützen
* Stellungnahme Lehrbericht & Pilotprojekt "Studienerfolg" → Bitte um indiv. Auseinandersetzung
* Beitrag Programmheft Einführungswoche → bis 04.06.

# Newsletter
* Bitte um Ergänzung des nächsten Newsletters im Git

# Organisatorisches
* Kalender: Sprechzeiten und Sitzungstermine eintragen!

# Sonstiges
* Sommerfest mit Mathe 15.06. im Friedenspark

