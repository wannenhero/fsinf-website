---


---

anwesend: Martin, Nico, Friedrich, Eric, Denise, Georg, Manuel

Protokoll: Eric

# TOPs:
* Facebook
* Fr. Radl/ Trennung von Veranstalungen
* Altklausuren
* ADS
* Lindemann
* Exkursion zum 32C3
* Anschaffungen
* Newsletter
* Weihnachtsparty
* Mailverteiler
* Nutzungsbedingungen gitlab
* Physik-Grill
* eingegangene Mails

## Facebook
* Werbung in Erstsemestergruppe ist unerwünscht
* Friedricht schreibt Textentwurf

## Fr. Radl/ Trennung von Veranstalungen
* Vorschlag und Wunsch Vorlesungen für lin. Alg. und Analysis zwischen Lehramt und Informatik trennen
* allerdings: gleiche Lehrinhalte bei beiden Studiengängen
* nach intensiver Debatte befürworten wir dieses Vorgehen nicht
* Denise schreibt eine Mail

## Altklausuren
* Altklausuren von Fr. Radl gehen klar, Gittel wird angefragt
* Altklausuren bei Scheuermann erfragen

## Nutzungsbedingungen gitlab
* Mail an Person die Homepage wegen TI ging raus (Urheberrechtsverletzung, Umgehen eines Passworts), Aufruf zur Unterlassung
* sind wir als Seitenbetreiber verantwortlich für Inhalte?
* rechtliche Distanzierung, formaler Akt
* nur für persönliche, private Verwendung
* Martin schreibt Vorschlag für Nutzungsbedingungen

## ADS
* wie leiten den Mailverkehr an Hrn. Scheuermann mit kurzer Anmerkung weiter: 6/0/1
* Eric schreibt Mail

## Lindemann
* Vertiefungsmodul CA
* Leute haben sich auf Anraten von Modul abgemeldet, so ist keine Handlungsgrundlage mehr gegeben
* ohne Kläger keine Richter
* Anmeldefrist zum Modul ist vorbei - Sache gelaufen
* Fehler: Leute nicht aufgefordert zu haben sich nicht abzumelden

## Exkursion zum 32C3
* Bewerbung via Newsletter
* Finanzen gehen klar: übliche Drittel-Förderung von StuRa/ FSR für Ticket
* Nachbereitungstreffen für Teilnehmende soll (verpflichtend) stattfinden
* Nachbereitungstreffen im kleine Rahmen, keine große Veranstalungen
* im Newsletter wird auf Nachbereitungstreffen hingewiesen
* externe Interessierte werden kurzfristig per Twitter eingeladen
* Termin: 11. Januar, 19 Uhr in der Uni, Raum P901 (Raum reservieren, Fabian?)
* Förderung mit 2/3 des Ticketpreises (90€) von maximal 20 Leuten
* Beschluss der Exkursion: 7/0/0

## Anschaffungen
* Anschaffung von Wifi-Routern steht im Raum (Eric recherchiert)

## Newsletter
* bitte um Feedback
* Eric baut *.pdf

## Weihnachtsparty
* Raum muss reserviert werden (P 901, Fabian)
* Rahmenprogramm wird gesucht: kurzer interaktiver Teil ist gut, Programmkommission soll gebildet werden
* Idee: Sofas vom Stura abholen
* Denise fragt im FSR-Verteiler nach Weihnachtsbaumständer

## Mailverteiler
* vertagt

## Physik-Grill
* muss zurück zur Physik
* Nico und Friedrich schaffen Grill zurück: Manu gibt Telefonnummer weiter

## eingegangene Mails
* Mail 2. Okt. 2015: Christoph - wir schicken ihm Termine oder verweisen auf den Kalender - er soll kommen wenn er Lust hat
* neue Jobvermttlung auf Homepage
* Abstimmung "Erziehung nach Ausschwitz" 4/2/1 -> angenommen (Eric kümmert sich)
* C++: Friedrich antwortet, dass wir keine Kurse am Institut haben (26. Okt.)
* Altklausuren MuP1: Denise antwortet (27. Okt.)

## Sonstiges
* Eric erinnert Fabian eine Funktion zu bauen um die Newsletter auf der Homepage zu archivieren
