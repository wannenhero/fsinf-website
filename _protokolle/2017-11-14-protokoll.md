---


---

* anwesend: Denise, Paul, Eric, Georg, Christofer, Marc (Gast), Max (Gast), Moritz (Gast), Gero (Gast), Verena (Gast), Sofia (Gast)
* Protokoll: Eric


# Raumzugang
* wir haben (bald) alle Zugang zur P901

# Auswertung Erstigrillen 
 * Echt weit draußen, aber trotzdem viel Zulauf.
 * Oase und wir waren wegen der Menschenmasse etwas überfordert. 
 * Verbesserung falls Wiederholung: Auf jeden Fall Schichtpläne Kasse und Grill.
 * Dokumentieren was wir ausgegeben, verbraucht etc. haben für zukünftige Veranstaltungen und Grillereien in dieser Größenordnung.
 * Abrechung und Dokumentation soll ins Wiki (Christofer macht Eintrag)
 * nächstes Mal vielliecht auf dem Campus oder näher an der Stadt dran

# Auswertung Kneipentour
* 3 Touren (1x Osten, 2x Südvorstadt)
* nicht gut:
  * Kommunikation mit einer der Kneipen am Anbend wäre zu verbessern
  * zu voll, vielleicht andere Situation für Abschluss-Treffen
* Kneipen sollten mindestens rauchfreien Bereich haben
* es sollte ein sinnvolles alkoholfreies Angebot in Kneipen geben
* (alkoholfreie) altenative zum Bier verteilen vor der Tour

# Feedback Schlafplatzbörse Propädeutikum
* es hat wenig Bedarf bestanden
* Angebote und Gesuche nächstes Jahr wieder händisch zugeordnet
* mehr Bewerbung im Vorfeld
* Buddy-Person sollte nächstes Jahr zu dieser Zeit vielleicht besser nicht im Urlaub sein

# Auswertung Studienfahrt
* genaue Infos: im Studienfahrts-Repo
* nächestes Mal:
  * mindestens eine Personen muss nach Unterkunft für 2018 schauen
  * vor Buchung muss ein Antrag beim Stura gestellt werden
* vertagt auf nächste Woche

# Moderations- und Protokollplan
* Es wurde beschlossen, dass Paul auf Tim wegen eines Plans zu geht.
* Paul sagt zu, dass wir bis zur nächsten Sitzung einen Plan haben

# Prüfungskommission
* wir müssen vermutlich 1 Person entsenden
* Christofer fragt bei Prof. Bogdan nochmal nmach Anzahl der zu entsendenden Personen
* vertagt

# AK Lehramt
* Person gesucht für AK Lehramt des Stura
* vertragt

# Weihnachtsfeier
* Denise gibt Verantwortung ab
* Denise hat How-To (Merkliste) geschrieben (Wiki)
* Gero, Sofia, Christofer kümmern sich
* Denise bietet Unterstützung an

# Förderung 34C3
Muss beworben werden, Event auf Webseite erstellt werden usw.
* Event erstellen
* Unterstützung muss beworben werden
* Abstimmung:
  * Förderung ohne Mindestförderung: MH
  * Förderung mit Mindestförderung: 1
  * Enthaltung: 0
  
Beschluss: es werden alle Studierenden der Fakultät (auf Wunsch) mit der gleichen Höhe gefördert (also: Wir verteilen den zur Verfügung stehenden Betrag unter allen Angemeldeten)
* Eric implentiert nötige Funktionen bis Dezember

# Evaluation
* vertagt

# Emails
* vertagt

# Ruhepool
* vertagt

# Benutzung von Twitter
* vertagt

# Mittwochsüberschneidungen
* vertagt


# Strategie / "Agenda"
* Paul schildert die Idee: strukturierte Arbeit, Agenda-Setting statt nur Agieren
* Idee: Satzung, Beschlusssammlung, Treffen zur Klärung von seit langen begonnenen Debatten, Strukturkommission, Einarbeitungsphasen für neue Mitglieder, Klausurwochenende
* Satzung als Agenda-Ziel, also zeitlich danach
* evaluieren, ob Interesse daran besteht (AG könnte sich gründen): Marc

* Abstimmg (4/0/1), dass sich AG gründet (alle menschen die Teilnehmen wollen schreiben eine Mail an den Verteiler)
 
### 5.2 Corporate Design der Fachschaft
* vertragt
 * Wie praesentieren wir uns in Zukunft als Fachschaft? Brauchen wir einen neuen Namen um die digitalen Humanitäten auch mehr einzubinden? Brauchen wir ein neues Logo? 

# Treffen zur Evaluation der Studieneingangsphase

* Treffen mit Neuman, Bogdan, Stadler etc hat stattgefunden
* Christofer berichtet ausführlich: Protokoll ging über den Verteiler
* Einschränkung des Wahlbereichs ist aus unserer Sicht überhaupt nicht gut
* wir bringen uns mit Vorschägen/ Ideen in die StuKo ein
* wir erarbteiten bis zur nächsten Sitzung Vorschläge und stimmen dann über diese ab

# Entsendung StuRa

* Abstimmung: "Wir entsenden Gero ins Stura-Plenum"
* Ergebnis: 2/ 1/ 2; wir entsenden Gero nicht

