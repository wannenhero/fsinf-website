---


---

* Anwesende FSR-Mitglieder: Kasimir, Martin, Nancy, Sarah
  * nicht beschlussfähig, aber wir erwarten noch Stimmen der abwesenden Personen
* Gäste: Eric, Georg, Thomas, Verena

## Tanz der Moleküle

* FSR-Party am 22. April 2015 in der Distillery
* Kooperationsvertrag mit dem FSR Chemie, wir zahlen maximal 90 Euro
* Abstimmung: 4/0/0

## Wahlen

* am 9. und 10. Juni 2015
* StuRa will, dass der FSR zwei AnsprechpartnerInnen bestimmt
  * Eric und Nancy
* Wahlvorstände
  * Georg
  * Eric
  * Kasimir
  * Martin
  * Thomas
  * Verena
  * **wir können noch weitere bestimmen**
* Änderung der Sitzzahl nötig?
  * Das möge der neue FSR beschließen
  * **Inwieweit hat die Sitzzahl einen Einfluss auf die Beschlussfähigkeit bzw. Abstimmungen?**
* Abstimmung: 4/0/0
