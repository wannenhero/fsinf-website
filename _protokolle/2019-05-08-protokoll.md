---
---

Anwesend: Natascha, Victor, Bianca, Alina, Niel
Protokoll: Paul

## 1 Orga
* Newsletter - Stand?
    * Newsletter ist raus
    * Howto müsste mal überarbeitet werden (Natascha würde das mal machen)
* Stand Website?
    * Es gab ein Treffen und es wird (wahrscheinlich bis Juni)
    * schön wäre bis vor den Wahlen, um Wahlwerbung hochzuladen
* wiki aktualisieren?
    * Christofer melde sich wenn er genug Kapazitäten hat -> vertagt

## 2 Studiengangsevaluation Digital Humanities
* Wie war das Treffen am 06.05?
* Niemand hier der dort war, eventuell war auch niemand dort

## 3 Wahlen
* Schichtplan?
    * Bianca erstellt Doodle für Wahlzeiten
* Wahlwerbung?
    * Vorschlag Texte von Aufgestellten, Wahlplakate mit Wahlaufruf
    * Natascha kümmert sich darum Texte einzusammeln
    * Vorschlag: Montag nochmal ansprechen

## 4 Finanzen
* Wie weit sind die 35C3 Auszahlungen?
    * vertagt, keine Finanzperson da

## 5 Sommerfest
* 2-3 Leute fürs Orgateam mit Mathe
    * vertagt weil wenig Menschen da sind

## 6 Emails
* Finanzierung von Tutorien an unserm Institut - Mail DaFZ
    * Neumann fragen
    * Berechenbarkeit findet statt
    * kein LinA Tutorium
    * Logik Tutorium gibt es bald
    * Alina und Natascha fragen bei Neumann
* Theodor-Litt-Preis, Transfer-Preis und Wolfgang-Natonek-Preis werden bis Juli ausgeschrieben
    * in den nächsten Wochen klar machen wen wir nominieren wollen
* ars Legendi
    * nächste Sitzung nochmal ansprechen (vertagt)
* Adas Women Werbung
    * erledigt

## 7 Anwesenheit bei MedInfModulen und Vertiefungspraktikum DH
* Christofer und Paul waren bei Neumann -> Mail an Winter für Gespräch
* Prakitkum als Vertiefungsmodul für DH Bachelor in der StuKo ansprechen - Vorschlag von Neumann statt Vertiefungsmodul die SQ nehmen -> StuKo

## 8 Logo
* Paul muss noch eine Email ans Referat für Öffentlichkeit schreiben und macht das auch bald
