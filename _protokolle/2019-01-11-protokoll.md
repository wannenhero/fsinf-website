---


---

**Anwesend**:
 * Bianca (Gast),
 * Marvin (Gast) 
 * Paul
 * Graubi
 * Denise
 * Georg 
 * Moritz
 * Josy (Protokoll)
 * Natascha (Sitzungsleitung)
## TOPs:

* Orga
* Auswertungsgespräch Lehrbericht mit Herrn Prorektor Hofsäß
* Evaluationsordnung
* 35C3 
* Theodor Litt Preis
* StuKo Ergebnisse
* Emails
* Softwaretechnik Vorlesung und Offener Informatikraum
* Neues zu P-402 Zugang?
* Ehumanities
* Der FSR der Zukunft
* Weihnachtsfeier


## 1 Orga
* Finanzen
    * Abrechnung noch offen- Graubi erledigt das
    * Kongressförderung: Kooperationsvereinbarung an Philosophie und Mathe ist raus, Eingang naechste Woche erwartet
    * Unterkunft für Studienfahrt muss gebucht werden (Forsthaus Seider) 
        * Erstes freies Wochenende Anfang November
        * Paul kümmert sich um Buchung und Finanzantrag
* Seit 23.11 kein Protokoll hochgeladen
    * Paul lädt die hoch
* Bericht aus dem Plenum (Stura)
    * Abwahlantrag gegen Finanzer des Sturas (Ruben) wegen groben Verfehlungen/ Misstrauen
        * abgelehnt
* Stura Entsendung
    * ab 01.02. neue Person notwendig, weil Denise ausscheidet
    * Marvin (neuer Gast, erstes mal anwesend) äußert Interesse 
        * Abstimmung Ja: 6, Nein: 0, Enthaltung: 1 --> Marvin ist entsendet
* Denise verlässt FSR zum 31.03.
    * übergibt Transponder an Marc
    * Stimmabgabe an Marc ab 16.02.? 
        Ja: 7, Nein: 0, Enthaltung: 0 --> Marc übernimmt Denises Stimmrecht ab 16.02. (Annahme ausstehend)

## 2 Auswertungsgespräch Lehrbericht mit Herrn Prorektor Hofsäß
* endgültige Teilnehmenden beschließen - laut Neumann 2-3 Personen (wir + FSR Mathe)
    * Paul übernimmt einen Sitz
* Termin: 05.02.19 im Zeitraum 10:00-12:00 Uhr


## 3 Evaluationsordnung 
* Treffen mit Stabsstelle für Qualitätssicherung
* Mittwoch, 16.01.2019  19 Uhr im Seminarraum 103 im NSG.
* wer geht hin?
    * Paul
    * Christofer als FakRat-MItglied

## 4 35C3 
* Wann überweisen?
    * Frist: 20.01.2019
    * Überweisung: 
* Nochmal Werbung?
    * Wird nochmal digital verbreitet

## 5 Theodor Litt Preis
* was mit der Anfrage des Fördervereins?
    * noch keine Konkreten Ergebnisse
* Paul und Denise berichteten

## 6 StuKo Ergebnisse
* DH Treffen mit Crane und Burghardt
    * nächste Woche 
* StuKo Bericht Data Science
    * siehe Stuko-Protokoll im Verteiler
* Bericht MuP und ADS
    * siehe Stuko-Protokoll im Verteiler
    * Off-Topic: Bianca wird nach Abstimmung (6/0/1) in den Verteiler und die Telegram-Gruppe aufgenommen

## 7 Emails
* "Klopapier" - Idee des FSR Mathe - ein gemeinsamer Fakultäts-Newsletter auf der Toilette - Beispiel von anderne Unis liegt im FSR Büro
    * Grobe Zustimmung
    * Moritz setzt sich mit FSR Mathe auseinander
* Bogdan: TI Dokumente im Git - Interessenskonflikt - Eskalation möglich
    * Paul waere gerne beim Gespräch dabei
    * "Wir sind keine Git-Polizei"
    * Wurde bereits einmal gelöscht und kurz darauf unabhängig von einem Studierenden erneut gepusht
    * Paul und 2 weitere gehen zum Gespräch
* Ada's Women
    * Ellen Körbes Zugticket(s) und ein Getränk fördern?
    * Ja: 5, Nein: 0, Enthaltung: 0 --> Angenommen
* Mirco: Litt Preis und Online Self Assessment
    * Paul kümmert sich   

## 8 Softwaretechnik Vorlesung und Offener Informatikraum
* Treffen ist erst am 16.1
* Moritz und Denise werden berichten


## 9 Neues zu P-402 Zugang?
* evtl. Statusbericht von Irene
    * nicht anwesend
    * wird von Denise und Moritz geklärt

## 10 Ehumanities
* Gespräch mit den Dozierenden Suchen?
    * vertagt auf nächste Woche

## 11 Weihnachtsfeier
* Auswertung
    * wir brauchen zukünftig dringend mehr Leute für den Rück- und Abbau
        * lief nicht gut

## 12 Der FSR der Zukunft
* einige Abgänge
    * neue Mitglieder werben
    * Verhältnis besetzter Sitze wichtig? --> Moritz erkundigt sich
* wie machen wir Werbung?
    * (...)
* Extra Transponder beantragen für sehr engagierte Personen?
    * Josy spricht mal mit Fabian vom FSR-Mathe
