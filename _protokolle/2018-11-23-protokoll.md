---


---

**Anwesend:** Marc (Gast), Natascha, Bianca (Gast), Sofia (Gast), Christofer, Moritz, Paul, Eric, Denise, Irene (verspätet), Josy (verspätet)

**Protokoll:** Eric

TOPs:
* Orga
* 35C3 Antrag
* Theodor-Litt-Preis
* Emails
* StuKo und FakRat Bericht
* Öffentlichkeitsarbeit nach Kopenhagen
* Softwaretechnik Vorlesung und Offener Informatikraum
* Rechnernetze Seminar
* FSR-Ball
* Tor Exit Node
* Neues zu P-402 Zugang?
* Feierliche Exmatrikulation
* Weihnachtsfeier
* Der FSR der Zukunft (Werbung etc.)
* TI


# Orga

## FAQ

* Denise und Christofer haben begonnen FAQ zu erstellen
* Appell, Fragen aus dem FAQ zu beantworten: bisher im Repo "Kommunikation"

## Sitzungsdisziplin

* Appell: Sitzungsleitung bereitet Sitzung und TO tatsächlich auch vor
* Tagesordnung soll auf Twitter veröffentlicht werden
* Denise trägt Situngstermine in den Google-Kalender ein
* TO soll aus vorläufiges Protokoll auf die Webseite: der Link kann dann getwittert werden

## Studienfahrt

* Erinnerung: einige Menschen müssen noch Geld bezahlen

## Finanzen

* der Finanzplan ist nicht aktuell, so bringt das wenig
* Moritz wäre gern dabei wenn das nächste Mal ein solcher Plan erstellt wird
* Paul und Moritz fragen Graubi nett, ob er nicht gern öfters zur Sitzung anwesend sein möchte

# 35C3 Antrag

* Paul war beim FSR Mathe: sie finden das gut und stellen in Aussicht Geld in den Topf zu werfen
* wir müssen wohl eine Kooperationsvereinbarung aufsetzen
* Marc und Paul waren im HHA des Stura: es gab Einwände, so dass beim nächsten Stura-Plenum darüber gesprochen werden wird
* Appell: man kann der Sitzung auch mit guten Argumenten beiwohnen
* Förderung wird durch Stura-Gelder auf alle Studierenden der Uni ausgeweitet, nicht nur auf Angehörige der FMI
* Eric recherchiert zu alter Stura-Förderung

# Theodor-Litt-Preis

* Verleihung des Preises findet am dies academicus statt
* Marc, Christofer und ggf. weitere Person kümmern sich um Weiteres

# Emails

## FSR-Vernetungstreffen

* 30. November, 19 Uhr im FKH
* Christofer geht wahrscheinlich hin, Moritz und Marc auch

## Förderung Israel-Veranstaltung

* wir stimmen über die Förderung der beiden Veranstaltungen für 200€ wie folgt ab: 5/1/2

## Anfrage Bits-und-Bäume

* wir fragen noch einmal zur exakten Förderhöhe nach

# StuKo und FakRat Bericht

(vertagt)

# Öffentlichkeitsarbeit nach Kopenhagen

* Bericht und Reflexion der Konferenz in Kopenhagen zur Weihnachtsfeier des fsinf und der AG ADA Woman
* kurzer Input und Diskussion sind geplant

# Softwaretechnik Vorlesung und Offener Informatikraum

* Terminfindung ist gescheitert
* Moritz und Denise führen das Gespräch

# Rechnernetze Seminar

* Hr. Lindemann verlangt Zulassungsvoraussetzung
* wir beantragen in der Stuko das die Zulassungsvoraussetzung abgeschafft wird

# FSR-Ball

(Werbeblock für den FSR-Ball)

# Tor Exit Node

* unser Hopo-Referent war bei Prorektor Hofsäß für ein Gespräch
* Empfehlung uns mit dem KMW-FSR zu vernetzen
* auch Kooperation mit Stura/ Referaten möglich
* Senat als Mittel auch nicht vergessen

# Neues zu P-402 Zugang?

* Irene hat mit Hr. Köntges gesprochen
* zur nächsten Sitzung gibt es bestenfalls nochmal eine Rückmeldung

# Feierliche Exmatrikulation

* FSR Wiwi macht so etwas und weiß wie das geht
* Überlegung der Kooperation mit dem FSR Mathe
* Denise erinnert im nächsten Juli via Mail an diese Idee

# Weihnachtsfeier

* Menschen wollten im letzten Jahr gern tanzen und feiern
* Idee `triac` als Act einzuladen
* Natascha kümmert sich um Technik aus dem Stura

# Der FSR der Zukunft (Werbung etc.)

(vertagt)

# TI

(vertagt)
