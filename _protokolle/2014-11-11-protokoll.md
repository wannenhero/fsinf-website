---


---

* Anwesend: Alrik, Ben, Fabian, Kasimir, Martin, Nancy, Nicole, Sarah, Sebbo (9/9)
* Protokoll: Kasimir

## Frauen- und transfeindliche Sprüche vor der Kneipentour

* Hinweis auf diskriminierende (frauen- und transfeindliche) "Witze" während eines PowerPoint-Karaoke-Auftritts auf der Hörsaalveranstaltung vor der Kneipentour erhalten
* eine oder mehrere Personen haben aufgrund dessen (und aufgrund eines fehlenden Eingreifens seitens des FSR) wohl die Veranstaltung verlassen
* Ziele für einen angemessenen Umgang mit solchen Situationen
  * Sensibilisierung
  * Mut, in so einem Fall auch einzugreifen
  * Wissen, dass im Falle eines Eingreifens Rückendeckung durch den FSR da ist
* Maßnahmen
  * vor einer Veranstaltung Gedanken über solche Fälle machen
  * vorher ggf. sagen, dass die Leute keinen diskriminierenden Müll erzählen sollen
  * Nachbereitung zur Kneipentour: Artikel
  * online Hinweis auf Gleichstellungsbeauftragte, StuRa etc.
  * möglicherweise FSR-Amt der Vertrauensperson, die als Ansprechpartner bei Diskriminierung u.a. fungiert

## FSR-Wahlen 3. und 4. Dezember

* Wir sammeln Kandidaturen für die FSR-Wahl
* Wieder 150 Pfannkuchen bestellen (2x 75), ungefähr 85 Euro: Absimmung 9/0/0

## Evalutation-Mail

* alle tragen Kritik/Lob ins Wiki ein
* Fabian schreibt E-Mail aus den Punkten im Wiki

## Studienfahrt 2015

* Nancy übernimmt Verantwortung für die Planung

## Weihnachtsparty

* Antrag von Sebbo
* 18.12.2014, E35, Wittenberger Straße
* FSRä Informatik, Farao, Kunstpädagogik/Musikwissenschaften
* vertagt wegen Frage nach verbindlicher relativer Beteiligung der anderen FSRä und Bezahlung von ihren Beiträgen zu den diesjährigen Partys

## Wahlwerbung

* gleiche Wahlwerbung für Webseite/Aushang und Wahllokal
* keine Fotos, kein Semester
* Veröffentlichung der zugelassenen Wahlvorschläge abwarten und gucken, ob wir alle Personen erreichen können
* Motivationstext
* Thema vertagt

## Neuer Sitzungstermin

* donnerstags 15:15 Uhr, nächster Termin ist der 27. November 2014
