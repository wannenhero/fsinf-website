---


---

**Anwesende:** Marc, Paul, Eric, Ahmad, Natasha, Moritz, Irene, Denise
**Protokoll:** Denise

## TOPS
1. Orga
2. Lehrberichte
3. Studienfahrt
4. Erstiveranstaltungen
5. Emails
6. CCC
7. Newsletter
8. Corporate Design
9. TI2

## 1 Orga
* Sitzungsdisziplin
* PO
Ringbindung in der FAQ des Studienbüros, nicht aber in der PO für Abschlussarbeiten
Wer macht das Studienbüro darauf aufmerksam? --> Paul

## 2 Lehrberichte
* Muss noch was geklärt werden?
Nein, sind raus.

## 3 Studienfahrt
* Paul weiß Bescheid
Christofer und Paul haben sich kürzlich getroffen.
Transporter: Bei Fahrgemeinschaft anfragen --> Denise
Marc geht in den Stura und besorgt die Zahlen vom letzten Jahr, damit wir wissen, wie viel wir fürs Essen veranschlagen.

* Vorläufiger Beschluss (Muss nochmal bestätigt werden):
Teilnehmerbeiträge der Mitarbeitenden wird zu 50% übernommen
    7/0/1 --> angenommen


## 4 Erstiveranstalungen
* Einführungsveranstaltungen ? 
Propädeutikum Ahmad macht das Propädeutikum
Einführungsveranstaltung
MuP oder DS Vorlesung

* Kneipenabende
   * Bachelor - Zwischenstand? Moritz und Sofia, Christofer Marc helfen
   * Master - noch nichts passiert - Christofer wird sich demnächst mit Sofia absprechen

## 5 Emails
* Erstiparty Germanistik
8/0/0
Interesse: Moritz, Paul, Ahmad, Natascha
Interesse vor Ort: E., Irene, Marc

* Die Universität Leipzig im Nationalsozialismus - Aufarbeitung, Gedenken, Politik"
    * Es gibt eine Email vom SDS, es geht um Projektförderung in Höhe von 50€
    * Die Veranstaltung ist am 13.10.2018
    Wer ist dagegen?
    8/0/0

## 6 CCC
* Marc schlägt vor, dafür weitere Töpfe zu beantragen
Eric gibt zu bedenken, dass Stura Förderung auch gewisse Restriktionen mit sich bringen könnte
Teilgeschwärzte Dokumente?

## 7 Newsletter
* DSGVO?
Eric hat mit Datenschutzbeauftragten der Uni gesprochen. Alles ist ok über Mailman.
Zukünftige Eintragungen? Leute einzeln einladen? Berufen uns auf Datenschutzbeauftragten

## 8 Corporate Design
* Denise fragt Meike - was passiert?

## 9 TI2
* 5/5 Versuch: Herr Schmid bat um Programm per Email und die Evaluation musste ausgefüllt werden, damit man den Versuch machen durfte (Nötigung). Herr Schmid ist nicht Modulverantwortlicher
--> Paul kümmert sich (Fr. Braun und Hr Neummann)

## Nächste Sitzung
* Per Mail
