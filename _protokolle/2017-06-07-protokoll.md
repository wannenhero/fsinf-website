---


---

* anwesend: Eric, Denise, Georg, Dominik, Martin, Christofer 
* Protokoll: Martin

# TOPS

* Drucker
* FSR-Grillen
* Stura-Sitzung
* Studienfahrt
* Wahl
* Dozentenmail
* Git
* Programmheft für Erstis
* Gleichstellung

## Drucker
* Vorschläge der Modelle im Git ergänzen
* Farbdrucker ist nicht zwingend nötig
* Wichtig: Kompatibilität mit Linux

## FSR-Grillen
* Termin: 16.06.2017
* Wetter abwarten, davon abhängig ist der Ort
* Denise gibt den Ort an

## Stura-Sitzung
* FSR entsendet gerade niemand
* vielleicht findet sich im nächsten Semester jemand

## Studienfahrt
* Transporter noch nicht gebucht, macht Georg
* Stura genehmigt wahrscheinlich nur 650,- Euro
* Anhebung der geplanten Teilnahmegebühr von 25,- auf 30,- Euro (Abstimmung: 6/0/0)
* Anmeldetool soll demnächst fertig sein
-> bastelt Eric (Dauer ca. 1 Monat)
-> Denise formatiert Texte

## Dozentenmail
* soll Martin verschicken

## Git
* Fabian fragen, ob er es weiterhin betreuen will

## Programmheft für Erstis
* Christofer erstellt Inhalte bis Juni

## Gleichstellung
* Holger hätte gern Gleichstellungsbüro mit Stillraum
* falls er einen bekommt, würden wir eventuell die Räume tauschen
* Holger soll den Raum besorgen, wir entscheiden dann über einen eventuellen Tausch

