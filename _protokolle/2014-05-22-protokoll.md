---


---

* Sitzungsleitung: Nancy
* Protokoll: Alrik
* Anwesende: Fabian, Alrik, Nancy, Martin, Sarah, Sebbo, Nicole
* Beschlussfähigkeit 7/9

## Tagesordnung
1. Vorlagen Abschlussarbeiten:
2. Finanzen Sommerparty
3. StuKo: Mail für Antrag zu Moduländerungen mindestens ein Semester zuvor
4. Verantwortlicher für Jobs/Fsinf-Liste
5. Berufungskommissionen:
6. Mails
7. Finanzen allgemein
8. Briefkastenschlüssel
9. Beschluss letztes Protokoll
10. Nächster Sitzungsverantwortliche

## 1. Vorlagen Abschlussarbeiten
* Nancy hat verschiedenen Vorlagen, läd sie im DokuWiki (FAQ) hoch

## 2. Finanzen Sommerparty
* stehen im DokuWiki
* wir zahlen ca. 250 Euro
* Donnerstag, 3.7.2014 (nach dem Fusion-Festival)
* Finanzabstimmung 6/0/1

## 3. StuKo
* Mail für Antrag zu Moduländerungen mindestens ein Semester zuvor
* Nancy schreibt Prof. Bogdan

## 4. Verantwortlicher für Jobs/Fsinf-Liste
* Sarah macht Moderation und kümmert sich um Vertretung

## 5. Berufungskommissionen
* Schwarmintelligenz und komplexe Systeme: Nicole, Daniel
* Text Mining: Sarah, Martin
* Computational Humanities: Nancy, Alrik
   
## 6. Mails
* informatica feminale - kommt in nächsten Newsletter

## 7. Finanzen allgemein
* alte Finanzunterlagen in Büro 007 abholen
* per Hauspost bereits erhalten

## 8. Briefkastenschlüssel
- wer hat einen?
- von Anwesenden haben: Nicole und Sebbo (und Kasimir)
- wir hatten 4 Stück

## 9. Beschluss letztes Protokoll
- Ändern: Sitzungsverantwortliche
- Beschluss: (6/0/1)
   
## 10. Nächster Sitzung:
- in 3 Wochen (12.6.)
- Sitzungsverantwortliche: Martin
