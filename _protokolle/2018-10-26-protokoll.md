---


---

## Tagesordnung

Anwesenheit: Eric, Josy, Moritz, Paul, Georg, Irene Gäste: Lukas, Nora
Protokoll: Georg

## 1 Umstrukturierung Bachelor (speziell DH)
* Christofer hat Informationen des AK über den Mail-Verteiler geschickt
* Marc, Paul, Christopher, Lukas wissen bescheid
* Probleme: hohe Abbrecherquote, kein Vertiefungsmodel Theor. Informatik, HWP, starker Fokus auf textuelle Forschung/fehlende Interdisziplinarität, faktische Vorraussetzungen werden nicht gelehrt (→Statistik)
* Vorschlag: Einführung in die Informatik/DH zu Beginn, engl.-sprachige Übungsgruppen, ADS- und MuP-Verbesserungen, mehr Wahlmodule (bis jetzt nur eins?), Statistikmodul, eigenes OOP DH, überarbeiteter Studienverlaufsplan → Details im per Mail rumgegangen Protokoll
* →wird in die StuKo eingebracht

## 2 Orga
* Finanzen (WeiFei, Sofa)
	* WeiFei: Verantwortliche und Termin gesucht. AK WeiFei mit Irene und Moritz
	* Sofa: Polsterreinigung ab 180€ oder neues? Irene recherchiert nach Gebrauchtangeboten
* TI Sachen im git: einige Materialien (Aufgabenstellungen, Skripte) der letzten Semester sollen dedupliziert werden um Verwirrungen zu vermeiden → vertagt
* Sitzungstermin: frühe Termine sind problematisch wegen Zeitdruck, wechselnde Termine führen zu Verwirrung → wir bleiben Freitag 13 Uhr, Abstimmung 6/0/0
	* Paul kümmert sich um Sitzungsleitendenplan
* FAQ und Service: Denise hat Vorschläge im Git, Paul schreibt nochmal Erinnerungsmail an Verteiler
* Post 
   * Careerer Service
   * academica
   * neues FiFF Magazin
   * Veranstaltung des Gleichstellungsbüros
* Empfehlung Prof. Maletti für Theodor-Litt-Preis
	* Was macht das Empfehlungsschreiben? Es muss raus! 30.10 muss es spätestens da sein. (POST!)
        * Materialien sind vollständig müssen nur noch ausgedruckt werden. Empfehlungsschreiben sollte nochmal angepasst werden und formatiert werden → Christopher/Denise machen das fertig, Abgabe geschieht Montag/Dienstag direkt!
* Denise: Newsletterarchiv über Mailman abrufbar. Kann man das öffentlich stellen? Dann könnte man das irgendwo verlinken und Leute könnten den Newsletter downloaden. Wer kennt sich mit Mailman genug aus? → Eric kümmert sich

## 3 Emails
* Eric und Georg tragen sich für etwas ein

## 4 Auswertungen
* Bachelorkneipentour Auswertung: War nett, hat alles geklappt, Jet hat an Flair verloren (Einlasskontrollen), Nichtraucherkneipen wären gut, Rücksicht auf nicht-alkohol-trinkende Menschen wäre auch gut
* Masterkneipenabend Auswertung → vertagt

## 6 Offener Informatikraum
* Fand doch im Ruhepool statt -> ist nicht gut → Paul geht zu Felix Wlassak

## 7 Rechnernetze Seminar
* siehe Mail - ist StuKo der passende Weg? → vertagt

## 8 35C3
* hat Philipp den Antrag fertig? → vertagt

## 9 Neues zu P-402 Zugang?
* ? →

## 10 Transporter für Studienfahrt
* fragen bei StuRa an für Teilauto; Abstimmung: 6/0/0
