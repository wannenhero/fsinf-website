---


---

Anwesende:
Eric, Paul, Irene, Verena, Phillip, Christofer, Tim, Georg, Josy, Denise, Marc (Gast), Moritz (Gast), Gero (Gast)

Sitzungsleitung: Phillip
Protokoll: Tim

## 1 Orga
### 1.1 Studieneingangsphase
* Christofer: war letzte Woche beim Treffen dabei mit Herrn Neumann, Herrn Zeckzer, Herrn Maletti etc. bzgl Studieneinstieg. 
* Herr Zeckzer schlägt 5CP mehr für MUP 1 vor (zusätzlich sollen Programmierkenntnisse vermittelt werden); Entwurf schickte er ab
* FSR soll Ideen sammeln und Alternativvorschläge bringen:
	* Wer formuliert eine Email, wer passt es an, wen schreibt man an?
* Paul: Handlungsarmut führt evtl. zum NC im nächsten Jahr, wir sollten was machen
* Christofer: zusammen mit einem anderen FSR Mitglied mit Herrn Neumann treffen z.B. am Donnerstag, vorher soll das gesamte Meinungsbild des FSR abgebildet werden.
* Paul: würde gerne mitkommen am Donnerstag mit C.
* Christofer: Es gibt keinen Informatikstudierenden im Fakultätsrat. 
* Paul: möchte ebenfalls ein Meinungsbild bzgl. NC vom gesamten FSR
* alle FSR-Mitglieder sprechen sich gegen den NC aus
* Paul: Möchte gerne Gespräch suchen mit Fakultätsrat (z.B. mit Max König, Fabian)
* Christofer: Vorher mit Neumann reden, letzten Punkt (von Paul) verbinden. Herr Neumann kritisiert zudem, dass Informatik an der Uni Leipzig beschrieben wird als ein Studiengang, bei dem keine Programmierkenntnisse erfordet sind und dass nicht die Realität abspiegelt
* Irene: stimmt zu
* Verena: sieht gerade darin den Unterschied zwischen einer FH und einer Universität. 
* Resultat: Gespräch bzgl. Studieneingangsphase + NC führen Christofer und Paul mit Herrn Neumann. Bitte an alle FSR-Mitglieder, die Emails zu lesen und die Vorschläge von Paul abzusegnen oder zu befürworten

### 1.2 Prüfungskommission
* vertagt

### 1.3 AK Lehramt
* Christofer: Bewerben auf Facebook, Mitglieder für den AK Lehramt vom Stura zu finden. 
* Josy: bewirbt Suche über Facebook, Twitter 

### 1.4 Newsletter (mit einigen Off-Topic Punkten)
* Irene: Vorschläge für den nächsten Newsleter? Z.B. Weihnachtsfeier, 34C3 Bewerben (falls gefördert wird)
* Paul: Wann ist Weihnachtsfeier?
* Denise: 11.12.2017
* Christofer: 34C3 wird gefördert. Muss auch noch kund getan werden. SVG für Weihnachtsfeier macht Philip
* Denise: Vorschlag bzgl. FSR Seite aktualisieren: Tagesordnungspunkte, die momentan relevant, anzeigen lassen
* Paul: Vorschläge für Agenda an mich schicken oder nächste Sitzung
* Gero: möchte gerne mit am Newsletter mitarbeiten
* Denise: Kann Fabian Bescheid geben, damit Gero dafür freigeschaltet werden kann
* Christofer: Agenda Repo aufmachen
* Denise: Eric, Fabian und Tim können nur Repos erstellen. Möchte das gerne ändern
* Eric: Seit 12 Monaten ist keiner auf ihn deswegen zugekommen. Hält es für überhaupt nicht sinnvoll, wenn alle Zugang haben
* Irene: hält fest: Weihnachtsfeier, 34C3 Finanzierung kommen rein in Newsletter bis spätestens nächsten Freitag (1.12.)

### 1.5 Förderung 34C3
* Christofer: Förderung muss noch beworben werden
* Josy: muss auf Agenda 2018 notiert werden

### 1.6 Entsendung in den Stura von Gero
* Gero: ist begeistert vom Stura, möchte sich weiterhin darin engagieren. Unklar von letzter FSR Sitzung, wie Entsendung funktioniert. Heute mehr abstimmungsberechtigte Mitglieder als letzte Woche, daher bittet er um eine erneute Abstimmung. 
* Josy: Wie trägst du Politik mit in den Stura?
* Paul: Für heute sind bereits drei Mitglieder entsendet.
* Gero: Wir haben bereits über Ersatzentsendung gesprochen. Möchte Meinung von Tim, Felix.
* Denise: möchte auch nochmal über Geros politische Einstellung Bescheid wissen
((Diskurs über politische Einstellung))
* Paul: Wir können ruhig darüber abstimmen, Gero als Ersatzentsendung zu wählen.
* Tim: Felix und Paul gehen stets zum Stura, Tim möchte jede 2. Sitzung hingehen, ansonsten Josy
* Abstimmung über Vertretungsplatz, falls Tim und Josy nicht können: (3/3/3) - abgelehnt

## 2 Emails Finanzvollmachten
* Phillip: Einmalig 40 Euro Gebühren bezahlen? Abstimmung: (7/0/1) - abgelehnt
* Christofer und Phillip sprechen mit Personal vom Sparkasse, ob das wirklich notwendig ist

## 3 Evaluation
* Christofer: Herr Gräbe kam ins FSR Büro. Interesse, um Evaluationsbogen zu reden (Art)? Finden wir ihn auch nicht sinnvoll?
* Eric, Tim gehen auf Herrn Gräbe zu.

## 4 Ruhepool

* Christofer: 2 Kommilitonen sind auf ihn zugekommen, PC Pools sind zu laut. Idee: Ruhepool
* allgemeine Zustimmung
* Zentrale Dienste aufsuchen
* Verena: Sollte im Newsletter thematisiert werden
* Welcher Raum?
* Verena: Nicht Windowspool, wir haben nur einen
* Eric: Windowspool bietet sich an - weit abgelegen
* Paul: Nicht Windowspool, lieber Linuxpool 
* Josy: Windowspool
* Christopher: Windowspool oder kleiner Linuxpool
* Abstimmung darüber, ob wir überhaupt einen Ruhepool beschließen? (8/0/0) - angenommen
* vertagt, Phillip redet mit den Zentralen Diensten


## 5 Studienfahrtbeitrag Rückzahlung für die FSR Mitglieder? 
* wird die Tage überwiesen

## 6 Benutzung von Twitter (evtl über Mailverteiler regeln)
* Paul: Tweets sollen stets mit Kürzeln versehen werden und Inhaltlicher Überpürung
* Eric: Twittertext sollte auch angepasst werden ("Hier twittern Fabian (rauslöschen) + neue Kürzel")

### 7 Strategie nach Studiengangsevaluationen (Kommunikation etc.)
* vertagt

### 8 Email bzgl. Hardwarepraktikum
* vertagt, rausfinden bzgl. des genaueren Kontextes

## 9 Kommunikations- und Betriebssystem
* Josy: Email an Herrn Lindemann und mit CC Studienbüro oder Modulbeschreibung ändern (Prozesse, Threads, C++ werden nicht gelehrt)
* Eric, Paul suchen u.a. das Gespräch mit Herrn Lindemann

## 10 Kurzbericht Vernetzungstreffen
* Christofer und Paul waren anwesend. FSRä tauschten sich aus. Paul + Christofer berichteten über Weihnachtsfeier, Gitlab und der Agenda
* 16.01.2017 ist das nächste Treffen
