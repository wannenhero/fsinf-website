---


---

* anwesend: Christofer, Dominik, Denise, Christoph, Paul, Tim, Verena (Gast)
* Protokoll: Christofer

# TOPS
* Mail an Prof. Gräbe
* Mail an Prof. Scheuermann
* Studienfahrt
* Propädeutikum
* Erstiwoche
* Erstibeutel
* Kneinpentouren
* Die ersten Vorlesungen
* Facebook
* Sitzungstermin

## Mail an Prof. Gräbe
* Tim überarbeitet die Mail nochmal

## Mail an Prof. Scheuermann
* Nachfrage, wegen der Lernzeiten und der Seminarräume

## Studienfahrt
* Mitfahren: Christofer, Denise, Christoph, Paul, Philip (Georg), (Verena)
* Ahmad, Irene und Josy nochmal nachfragen
* Küche: Philip und Paul
* Große Wandertour: Christoph und Christofer
* Kleine Wandertour: Denise
* Linux Workshop? : vielleicht Denise ansstatt der Wanderotur
* Denise fährt mit eigenem Auto
* Eric muss das Tool fertigstellen
* Transporter: falls Georg nicht kann, eventuell Christoph
* Bus: 1-2 Personen sollten mit den Studenten fahren
* Kasse: in der nächsten Sitzung besprechen
* Paul fragt beim Stura nach einem Hochschulworkshop
* Geld vom Stura ist beantragt und genehmigt
* Denise erstellt Orga- und Programmplan, Mails etc.


## Propädeutikum
* Denise erstellt Zettel für Newsletter und Jobnewsletter
* Christofer und Ahmad stellen den FSR vor

## Erstiwoche
* Grillen am Tag der Einführungsveranstaltung
* In der Distille klappt nicht
* Paul fragt bei Oase in Grünau und TV Club nach
* Christofer, Denise, Paul und Tim stellen den FSR bei den Einführungsveranstaltungen vor

## Erstibeutel
* Übergabe übernehmen die, die bei den Veranstaltungen da sind

## Kneipentouren
* Master Kneipenabend 11.10. für 30 Leute im Beyerhaus
* Bachelor Kneipentour wahrscheinlich in Gruppen
* Benachrichtigung über Twitter, Facebook und Newsletter ab den 20.09

# Die ersten Vorlesungen
* vertagt

## Facebook
* FSR Seite als Admin und die FSR Mitglieder als Moderatoren einsetzen
* Tim übernimmt die Facebook Adminstration

## Sitzungstermine
* Tim erstellt ein erstes Doodle für regelmäßige Termine
* nächste Sitzung 13.09. 12 Uhr
