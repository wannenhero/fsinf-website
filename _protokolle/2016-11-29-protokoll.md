---


---

anwesend: Georg, Fabian, Denise, Christofer, Eric
Protokoll: Eric

# TOPs
* 33c3
* Weihnachtsfeier
* Anschaffungen

# 33c3

* Denise bewirbt das noch einmal auf Facebook

# Weihnachtsfeier

* Einkaufsliste:
  * Glühwein, Traubensaft (Idee: Liefern lassen)
  * Spekulatius, Lebkuchen: Christofer kauft das ein
* Chor:
  * bestenfalls ~15min Zeitfenster
  * wir möchten Geld bezahlen (Beschluss: 4/0/1 für 75€)
  * geht wohl klar
* Antrag auf Handkasse: Dominik/ Christofer
* Denise macht Kinderpunsch (inkl. Einkauf des Benötigten)
* Spenden
  * für Glühwein
  * keine Spenden für Snacks und Alkoholfreies
* wir kaufen nicht aller billigsten Glühwein

* Denise holt Weihnachtsbaum und Ständer
* Denise kauft Lichterkette
* Licht müsste dedämpft werden
* Eric kümmert sich um Becher (70 Stück)

* Denise recherchiert wegen Mengen an Getränken aus dem letzten Jahr

noch offen:
* Heizplatte (Georg im Zweifel)
* Töpfe/ Kelle

# Anschaffungen

* Denise Geld legt aus und bestellt
* Lieferung zu Fabian

* Folgendes wird angeschafft:
  * Aktenvernichter: https://www.amazon.de/dp/B00GT4BH2Q
  * Tastatur: https://www.amazon.de/dp/B00E6KGKXG
  * Maus: https://www.amazon.de/dp/B00HLXY15Y
  * Monitor: https://www.amazon.de/dp/B00M8F9W2W
  * Lautsprecher: https://www.amazon.de/dp/B00AY9ZZTI
  * Tacker: https://www.amazon.de/dp/B001J8IB6W/

* Beschluss, dass eben diese Dinge gekauft werden: 5/0/0
