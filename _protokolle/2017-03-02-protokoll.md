---


---

**Anwesende Mitglieder:** Martin, Eric, Dominik, Denise (1. Hälfte der Sitzung), Christofer, Paul
**Protokoll:** Dominik
**Sonstige:** Josi, Frau Braun, Verena, 

# TOPs
* Evaluation
* Erhöhung der Mitgliederzahl
* Dr. phil. Digital Humanities
* GitLab-Guide & Logo
* Tor-Node
* Berufungskommission
* Gespräch mit Droste (Kryptografie und ERASMUS)
* Sommerfest
* Sponsoring Radio Mephisto


## Evaluation
* aggressivere Social-Media Präsenz
* Ideen:
	* Mentorenprogramm (Grüppchen mit Mentor, themenorientierte Treffen, Führungen, Verknüpfung mit FSR)
	* Tutorien unter den Studierenden

## Erhöhung der Mitgliederzahl
* Sitzplatzerhöhung auf 11 Plätze
* Abstimmung 6/0/0

## Dr. phil. Digital Humanities
* Weiterleiten an Promovierendenrat
* keine grundsätzliche Abneigung
* Martin überlegt sich was

## GitLab-Guide & Logo
* vertagt

## Tor-Node
* Briefvorschlag
* Ready to send (Git)
* Abschicken --> Eric

## Berufungskommission
* Entsendung von Chris
* Abstimmung 5/0/0
* Dominik schreibt Mail

## Gespräch mit Droste (Kryptografie und ERASMUS)
* hat keine Zeit, aber Idee ist gut
* Diem hat immernoch keine Zeit
* evtl HTWK pushen
* ProKoop anfragen --> Paul
* auf Erasmus verweisen zu dem Thema (Kaliningrad biete etwas in die Richtung an)

## Sommerfest
* Chris fragt beim FSR Mathe an

## Sponsoring Radio Mephisto
* Nein
* lieber in Vorträge/Veranstaltungen investieren
