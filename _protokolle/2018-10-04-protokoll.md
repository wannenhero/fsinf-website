---


---

## Anwesende
Georg, Paul, Eric, Natascha, Moritz, Ahmad, Denise, Marc, Christofer (ab Punkt 4)

Protokoll: Denise
## TO
##### 1 Orga
##### 2 nicht öffentlich
##### 3 Abstimmung Studienfahrt
##### 4 Einführungsveranstaltungen und Propädeutikum
##### 5 Semesterauftaktparty
##### 6 HWP Evaluation
##### 7 Stuko
##### 8 Emails
##### 9 35C3
##### 10 35C3 Förderung
##### 11 "Kritische Informatik" AG in Gründung

## 1 Orga
* Paul geht zu Neumann wegen der Ringbindung
* Haushalt --> Vertagt, weil Philipp abwesend
* Sitzungstermin. Wer erstellt Doodle? --> Denise.
* Nächster Sitzungstermin? 11.10.18, 15 Uhr, Donnerstag
* Erstibeutel, wer holt morgen mit ab? Ahmad, Paul, Natascha, Denise, Eric vllt. Alternative: Limo. 50€ für alkoholfreie Getränke. Abstimmung: 7/0/0
* Stura-Entsendung: Paul ist jetzt über StuRa-Referat entsandt. Wer möchte stattdessen? Denise möchte unverbindlich ein Plenum besuchen und es sich überlegen. Ansonsten Werbung machen. Ersatzentsendung Denise 16.10. 6/0/1 beschlossen

## 2 nicht öffentlich

## 3 Studienfahrt
* Moritz, Marc und Denise gehen einkaufen.
* Teilnehmerbeitrag 35€
* Stimmen die Busdaten? Wo fährt der Bus los? Paul ruft an. Bus muss zwei Wochen vorher bezahlt werden. ---> Philipp informieren
* Werbung: Denise empfiehlt Werbeslides.
* Studienfahrtorga trifft sich zeitnah.

## 4 Einführungsveranstaltungen
* Christofer anwesend
* Auswertung Kneipenabend in MB: Vllt woanders? Diskussion über Locations. Pro Contra MB, Beyerhaus, Puschkin. 40 Leute
* Masterkneipenabend, 11.10, 20 Uhr Beyerhaus: Christofer und Sofia
* Bachelorkneipentour 16.10., 19 Uhr nach DS (Überschneidung mit Plenum): Moritz: Sind mitten in den Planungen. 5 Gruppen. Christofer, Moritz, Ahmad, Georg, Natascha, Sofia?, Maximilian?, Irene
* Einführungsveranstaltung: FSR vorstellen, Studienfahrt, Semilesung, Kneipentouren
* Auch zu Lehramt gehen

## 5 Semesterauftaktparty
* 8.11, Absturz, 21 Uhr.
* Moritz, Natscha und Paul waren beim Treffen
* Awareness-Team Interessierte wenden sich an Paul.
* Marc würde helfen.
* Unsere finanzielle Beteiligung (kommt dann in den Kooperationsvertrag).
    Rahmen: 200€, Beschluss: 8/0/0

## 6 HWP Evaluation
* Paul hat mit den Durchführenden gesprochen. Es wurde zugesichert, dass mehr Kommunikation darüber stattfindet, dass die Evaluation keine Voraussetzung für das Testat ist. Des Weiteren soll gleich zu Anfang kommuniziert werden, wann evaluiert wird.

## 7 Stuko
* Master DH Immatrikulation zum Sommersemester von StuKo beschlossen, geht jetzt durch weitere Gremien
* Evaluierungsplan für die nächsten ~10 Jahre durch: Jede Abteilung im 1.5 jährigen Rhythmus (Wahlmdodule) und Pflichtmodule haben eigenen regelmäßigen Turnus.
* Marc, Paul, Christofer, Lukas: Umstrukturierung B.Sc. DH ausgearbeitet. Geht durch den Verteiler.

## 8 Rechenzentrum
* Marc ist im CIO-Ausschuss (Kontrollgremium) gewählt.
* Wenn es etwas gibt bzgl. Rechenzentrum, kann man sich an Marc wenden.
* Universität will Highend-Rechenzetrum aufbauen. Auch Studierende sollen auch Zugriff auf Serverspace bekommen.
* Wir möchten gerne, dass der CIO-Ausschuss anbringt, dass wir ein TOR-Node einrichten möchten.

## 9 Emails
* Kiff ist während Studienfahrt
* Filmvorführung CCC teilen
* Jobverteiler

## 10 35C3
* Kriegen Förderung, wenn wir Antrag schreiben

## 11 AG Kritische Informatik
* Gründet sich gerade
* Möchte sich auch in den Einführungsveranstaltungen vorstellen --> Müssen sich an Neumann wenden
* 24.10. Kritische Einführungswochen
* Paul schickt Informationen an Verteiler
