---
---

Anwesende: Christofer, Georg, Paul, Irene, Moritz, Natascha, Elena, Bianca
Protokoll: Moritz
Sitzungsleitung: Christofer


## 1 Orga
* Stand Website von Georg und Paul?
	- hat sich noch nichts getan
	- Paul kümmert sich um Vorstellungstexte im Internet

## 2 Wahlen
* Schichtplan?
	- vertagt
* Wahlwerbung?
	- eine Person muss noch erreicht werden
	- Dienstag plakatieren
	- Paul und Moritz treffen sich am Montag

## 3 Sommerfest
* Orgateam hat sich getroffen
* Marvin, Moritz und Christofer berichten
	- Planung verteilt
	- Sommerfest ist am 19.06. im Friedenspark
	- Awareness-Team müsste nochmal kommunizieren
	- Schichtplan kommt bald rum
	- BITTE auch für späte Schichten eintragen
	- DJs von 21-23 und 23-01 Uhr, rumfragen wer Interesse hat
	- Hinweise auf Übungszettel bringen -> Christofer
	- Fahrer können sich melden bei Interesse
	- Christofer fragt nochmal wegen Geld

## 4 Finanzen
* Wie weit sind die 35C3 Auszahlungen?
	-nächste/übernächste Sitzung Fehler ansehen
- brauchen neuen Haushaltsplan


## 5 Emails
* ProChoice Förderung -> abstimmen
	- wollen 100€ für Rechnung für den Druck Mobilisierungsmaterialien
	- auch auf soz. Netzwerken teilen
	- Abstimmung 5/0/1 angenommen
* Förderung: neue Couch in der 4ten 30€
	- Förderung ist für Transport einer Couch
	- Maximalbetrag 30€
	- Abstimmung: 5/0/1 angenommen
* Alina Anfrage SIPS 50€ Reisekosten
	- Bedingung: Feedback muss zurückgetragen werden
	- Abstimmung 6/0/0 angenommen
* MHG Zuckerfest Förderung 30€
	- Geld ist für Materialien
	- Abstimmung: 6/0/0 angenommen
* Teaching You Anfrage Link auf der Website
	- Geht gerade nicht, Website funktioniert nicht
	- wird in (zukünftige) Website übernommen

## 6 Stuko
* P402 wir sollten mit Crane reden oder Bogdan macht das für uns
	- Christofer schreibt Email
* Praktikumsmodul DH -> Christofer hats vergessen und schreibt Bogdan
	- Christofer schickt StuKo Protokoll rum
* Evaluationsergebnis Bericht
* ADS + MuP Bericht
	- MuP 1+2 werden zu 10 Punkte Modul im 1. Semester
	- ADS 1 und 2 rücken ins 2. bzw. 3. Semester
* Data Science
* Medizin Informatik

## 7 ars legendi Preis
* ars Legendi Preis (Neue Infos?)
* betrifft gesamtes Lehrangebot nicht nur einzelne Module
* Ein Treffen mit Gräbe und ein Treffen mit Bogdan wären wichtig
	- Treffen mit Bogdan passiert zusammen mit Tutorien
	- Bianca und Paul melden sich bei Gräbe

## 8 Logo
* Stand?
	- vertagt

## 9 Anwesenheit bei MedInfModulen
* Marvin und Christofer haben Mail geschrieben
	- vertagt

## 10 Theodor-Litt und Wolfgang Natonek Preis
- Nomierungen? -> nach Wahl vertagt
