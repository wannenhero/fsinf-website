---


---

Anwesend: Josy, Marc, Irene, Moritz, Eric, Christofer, Philipp, Christoph, Ahmad, Bianca (Gast)

Protokoll: Irene

# Tagesordnung 16.01.2018 - 15:30 Uhr

## 1 Orga

### 1.1 Stura-Entsendung
* Abstimmung über eine dauerhafte entsendung von Marc und Moritz:  
Marc: 7/0/1   
Moritz: 7/0/1

### 1.2 Werbung
* Ada Lovelace "Heldin der Programmierung"      
Werbung für die Veranstaltung wird über unsere Social Media Kanäle geschaltet.

## 2 Emails
* TI-Mail   
Josy holt weitere Infos ein. Vertagt.

## 3 Evaluationen (Braun/Gräbe)
* Ergebnis Studienstartbefragung   
Wird vertagt.

* Neuigkeiten bezüglich Gräbes Anfrage?    
Eric bemüht sich weiterhin um einen Termin.

* Neuigkeiten bezüglich möglicher Fragen in zukünftigen Studiengangsevaluationen?   
Wird vertagt. Ist eine AG notwenig?

## 4 FSR Geschichte
* Terminvorschläge für koop. Sitzung (sind im Verteiler)    
12.04., 19:15 Uhr

## 5 Förderung Leipziger Schriften   
* Email von Anna vom 11.01.2018
Anna wird zu Vorstellung des Projekts eingeladen. Philipp kümmert sich darum.

* Förderung ja/nein? Wenn ja, wie viel? 100 Euro oder weniger?   
Wird vertagt.

## 6 Sitzungen in der Vorlesungsfreien Zeit
Abstimmung das ab 30.01. die Sitzungen im 2 Wochen Rythmus stattfinden:   
7/0/0
