---


---

**Anwesende:** Georg, Eric, Verena, Fabian, Friedrich, Nico, Fabian vom Mathe-FSR als Gast

**Protokoll:** Georg

TOPs
1. DS
2. Studifahrt
3. ADS
4. GitLab-Zugang für Dozenten
5. 32C3
6. E-Mails


### DS
- aus unserer Sicht keine Änderungen notwendig
- Verena schreibt Mail

### Studifahrt
- Abstimmung über Ort in nächster Sitzung. Die bisherigen Vorschläge sind im GitLab
- Termin: Nach den Prüfungen, Anfang August

### ADS
- Fabian fragt nach aktuellem Stand bei Herrn Neumann nach

### GitLab-Zugang für Dozenten
- nach Diskussion: erneute Abstimmung notwendig
- Transparenz nützt im Endeffekt allen
- Abstimmung: Jegliche an der Lehre beteiligte bekommen einen Zugang: 5/1/1 → angenommen

### 32C3
- Nachtreffen: Termin am 11.01.1016, Eric kümmert sich um inhaltliche Struktur

### E-Mails
- nichts relevantes

