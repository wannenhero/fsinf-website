---


---

**Anwesend:** Eric, Tim, Denise, Christofer, Gast(Elisabeth), Gast(Moritz), Gast(Sofia), Philipp (verspätet)

**Sitzungsleitung:** Tim

**Protokoll:** Christofer

## Tagesordnung
1. Sommerfest
2. Emails
3. Ti2 und Prüfungsvorleistungen
4. Wahlen
5. 35C3
6. Berufungskommission Softwaretechnik
7. Corporate Design


# 1 Sommerfest
* Sichtbarkeit der FSR-Mitglieder auf der Veranstaltung
* schwarzes T-Shirt mit farbigen Tape am Oberkörper - mit 3 Streifen
* Info über Mail-Verteiler 

# 2 Emails
* MI - Verteilerliste bewerben 
    * Newsletter, Newsartikel und Christofer twittert)

* interaction Leipzig e.V. Kooperationsanfrage für Programmierkurs für Geflüchtete 
	* Zentrale Dienste/Dekanat anfragen für die Poolnutzung - Tim schreibt die Mail (interaction leipzig e.V. in CC)

* Seminar Modul angewandte Informatik (Blockchain Hackathon) 
	* beginnt am 24.05.
	* Christofer twittert

* FSR Vernetzungsausflug - anstatt Vernetzungstreffen am 03.06 Ausflug
	Christofer, Moritz und Denise haben interesse, vielleicht auch Eric
	Rückmeldung bis zum 25.05

* FSR DAFZ Anfrage ob wir die Lesebühne bewerben
	* am 20.06 - Frist bis zum 04.06 bei FSR DaFZ melden
	* generell News Artikel zum Kollektivfestival
	* Christofer twittert

* 70 Jahre Israel - wollen wir eine der Veranstaltungen unterstützen?
	* nicht beschlussfähig 
	* vertagt 

* Neuerungen im Urheberrechtsgesetz
	* Christofer twittert

# 3 TI2 und Prüfungsvorleistungen
* Paul und Marc waren bei Herrn Neumann PO für Lehramt ist anders als für Bachelor und Master
* Institut will PO überarbeiten - aber wie?
* vertragt 

# 4 Wahlen
* Wahlauszählungstermin
	* Doodle vom Stura
	* Josy, Paul und Christofer dürfen auch helfen nur nicht bei FakRat
* Design eines Plakats
	* Namen und kleinen Text zu jeder Person, die aufgestellt ist
	* Moritz versucht sich dran 

# 5 35C3
* Idee: Bewerbung für Voucher, sodass Studierende besser an Tickets kommen
* Sofia recherchiert und schreibt Mail mit Denise

# 6 Berufungskommission Softwaretechnik
* Dauerhafteentsendung für Xuan
* Christofer hat interesse, vielleicht noch jemand?
* Abstimmung wird verschoben, da nicht beschlussfähig
* Mail an Herrn Janassary, wenn jemand gefunden wurde

# 7 Corporate Design
* vertagt
