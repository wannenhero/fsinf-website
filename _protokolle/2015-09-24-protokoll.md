---


---

Anwesende: Friedrich, Nico, Nancy, Manu, Eric, Thomas, Fabian, Georg, Sebbo


### Studienfahrt:
Einkaufen: Freitag, Fabian, Verena, Friedrich im Transporter
Getränke auf Kommission 

### Kneipentour:
In Planung

### Berufungskomission für Deutschlandstipendium
Martin wird entsendet nachdem Rücksprache mit Prof. Bogdan gehalten wurde, da zwei FSRä sich beworben haben

### Prokooperation-Flyer
Wenden uns an Studienbüro wegen der Weitergabe bei der Einführungsveranstaltung

### GitLab
Ordnerstruktur soweit angelegt

### Weihnachtsvorlesung
Aufgrund organisatorischer Ungereimtheiten auf Eis gelegt

### Weihnachtsfeier
Wieder im 9. Stock

### Stura
* KSS Studienfahrt
* AG Brettspiele 

### Sonstiges 
* Sebbo behält seinen Transponder, Manu gekommt einen neuen. Sebbo bringt seine Sachen aus dem FSR-Raum weg.

* Erstibeutel können ab Montag abgeholt werden → Fabian, Eric, Manu, Thomas und Denise 15 Uhr




