---


---

* anwesend: Christofer, Denise, Christoph, Paul, Philipp, Ahmad, Josy, Tim, Irene, Erik ,Felix(Gast)
* Protokoll: Christoph, Tim

# TOPS

### nach innen
* Kaffee- & FSR-Raum Facility Management
* FSR Wohlfühlatmosphäre + Ordnung im FSR Raum
* FSR Logo
* Status: aktuelle Entsendungen in die Gremien
* Status: Transponder
* Plenumsentsendung
* Klärung: Verlegung Logik (SoSe), Optionsvariante SWT
* Aufgabenrotation: Vorbereitung/ Moderation, Protokoll

### Webseite/ GitLab/ Social Media
* (neue) Selbststudienzeiten bekannt machen
* Änderung (& opt. Verschlüsselung) FSR-interner Passwörter
* Protokolle veröffentlichen
* FAQ (Shopliste, Abschnitt "Technik" und GitLab)

### Termine
* regelmäßige FSR-Sitzungen
* Weihnachtsfeier

### Auswertung
* Erstigrillen
* Feedback Schlafplatzbörse Propädeutikum
* Studiengangsevaluation

### Planung
* Kneipentour
* Sponsoring 34C4

### Anfragen
* Sprache und Studienerfolg
* Überschneidungen bei Veranstaltungen med. Informatik

## Entsendung in die Gremien
* Tim, Paul, Felix(Gast), Josy(Vertretung)
* Felix, 7-0-1
* Auflistung anfertigen wer in welchem Gremium entsendet wird --> über Mail Verteiler

## Transponder
* Paul & Josy in Arbeit
* Erik sorgt dafür, dass die Transponder der ehemaligen FSRLer für den FSR-Raum deaktivieren lassen

## Sitzungstermin
* 17.10.2017 15:30 Uhr
* in Zukunft eventuell 30min eher (15:30 Uhr anstatt 16:00 Uhr)
* P-801

## Weihnachtsfeier
* 6.12.2017 in FKH 
* in nächster Sitzung weiteres klären

## Kneipentour
* vom Hörsaal abholen, am Audimax sammeln 
* Irene, Ahmad, Paul, Christofer, Philipp, Christoph
* Freigetränk Master 4-2-3

## FAQ
* Hardwarekauf und Lizenzerwerb neutraler formulieren
* keine direkten Links und Shophinweise
* Paul und Tim aktualisieren FAQ

## Newsletter
* Anmerkungen per Mail an Denise 

## Kaffee- & FSR-Raum Facility Management
* insgesamt sollte mehr auf Sauberkeit geachtet werden 
* Kühlschrank Reinigung: 8-0-1
* Kühlschrank monatlich putzen: 3-4-2
* Kühlschrank aller zwei monate putzen: 5-0-4
* Putzplan, Tim
* Putzmittel zeitnah anschaffen 9-0-0
* Pfand wegschaffen, Philipp
* wegen Kaffee intern absprechen

## Protokollfortsetzung durch Tim

## Verlegung Logik (SoSe), Optionsvariante SWT
* Paul wird bei Prof. Gräbe nachfragen, wie lange und wann die SWT Praktikumsphasen losgehen. Außerdem Infotext schreiben für FSR Seite.

## Moderation der Emailverteiler
* Josi: Email-Verteiler - Wer moderiert?
* Christofer: Tim soll sich eintragen, wenn er Interesse hat, mitzumoderieren. ansonsten Christofer
* Josi: Effizientere Emails-beantwortung?
* Tim: Sensibilisierung bereits in der letzten FSR-Sitzung, dass jedes FSR Mitglied sich verantwortlich sein sollte, Emails eigenständig zu beantworten, ohne auf ein OK durch ein anderes FSR Mitglied zu warten.

## Aufgabenrotation: Vorbereitung/ Moderation, Protokoll (vertagt)
* vertagt

## FSR Logo (vertagt)
* vertagt

## (neue) Selbststudienzeiten
* müssen bekannt gemacht werden
* Denise und Irene bearbeiten Newsletter, da sollen auch die Zeiten rein.
* Tim pflegt es ins FAQ ein

## Verschlossene Seminarräume
* Irene: Seminarraum manchmal verschlossen im Paulinum, 701. Was tun?
* Christofer: Mit Prof. Scheuermann reden, oder ähnliches, um herauszufinden, wer Zugang hat.
* Tim: Wer kümmert sich drum?
* Christofer: Tim und Christofer gehen der Sache nach
* (jetzt ist gerade auch Studienzeit und die Tür lässt sich nicht öffnen)

## Protokolle veröffentlichen
* Tim: veröffentlicht die vom 27. September und von der Konstisitzung

## Erstigrillen (vertagt)
* Christofer: ins Wiki Fazit festhalten (wie viel ausgegeben, wie liefs ab etc.)
* genaueres erst einmal vertagt

## Feedback Schlafplatzbörse Propädeutikum
* Paul: Alle Emails sind an Buddy gegangen, sie war im Urlaub. Frau Güttler in nächste FSR Sitzung einladen?
* Ahmad: nicht optimal, da Sprechstunde vom Studienbüro. Dann lieber Email
* Eric versucht nachzufragen.

## Studiengangsevaluation (vertagt)
* Christofer: Jeder nochmal nachlesen und vertagen.
* Paul: nächste FSR Sitzung am Anfang halten.

## Sponsoring 34C3
* Eric: mit Ticket unterstützen, aber wie viele und in welcher Höhe? In fünf Tagen beginnt der Vorverkauf. Zeitnahe erfahren in welcher Größenordnung fördern. Im letzten Jahr - 50% des Tickets gesponsert.
* Josi: den 2. Vorverkaufstermin nehmen, damit wir nächste Woche darüber mit den Finanzern diskutieren und abstimmen können. 
* Christofer setzt sich schon mit Philip und Chrostoph zusammen.
* Eric: Vorschlag - Studierende können sich jetzt schon dafür anmelden, es wird aber erst später bekannt gegeben, wie hoch sie gefördert werden.
* Bemerkung: (der obige Plan bleibt davon unberührt)

## Anfrage von "Sprache und Studienerfolg" (vertagt)

## Überschneidungen bei Veranstaltungen med. Informatik
* aktuell kein Handlungsbedarf mehr - vertagt
