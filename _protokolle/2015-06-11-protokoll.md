---


---

Anwesend: Georg, Verena, Eric, Martin, Thomas, Denise  
Protokoll: Denise

## TOP 1: Brief ans Rektorat
* Anfrage zu einer Stellungnahme bezüglich Presse- und Meinungsfreiheit, auch im Hinblick auf die Rolle und das Leitbild der Universität.
* Wollen uns für ein freies Netz engagieren

## TOP 2: Stundenplan-Tool
* Höhere Semester haben im Rahmen von Projektgruppen Tools zur Stundenplanerstellung geschrieben
* Überlegung, eines der Tools auf unserer Homepage einzubinden
  * Thomas und Eric werten die Tools aus. Rückmeldung nächste Sitzung.

## TOP 3: Studienfahrt
* 16.-18.10.15, Schloss Oberau
* Wie viele Plätze werden wir haben?
  * ~40 Leute, 35€ Beitrag
* Werbung in FB- Erstsemestergruppe, damit auch mehr Erstsemester dabei sind - Mathe-FSR fragen zwecks Beteiligung?
*  Wer fährt von uns mit:
  Thomas, Verena, Martin vllt, Georg, Denise, Eric vllt
* Weiteres nächste Sitzung (Programm etc.)

## TOP 4: Wahlevaluation
Es besteht Unmut über einige Abläufe:  

* Im Ziegenledersaal war Campusfestmaterial gelagert und so wurde der Wahlvorgang teils
beeinträchtigt
* Wahlregelungen waren nicht rechtzeitig bekannt oder undeutlich formuliert
Konsequenz: Personalmangel bei der Wahlurnenbesetzung - Chaotische Wahlvorstandseinweisung
* Auszähltermin wurde ohne Vorwarnung kurzfristig verlegt
  * Werden ein Schreiben an den Wahlausschuss formulieren - Ausweispflicht bei Wahl (ist Perso/ Führerschein zulässig?)
  

## TOP 5: Mathe-FSR
* Wollen mehr mit uns machen. Eric wurde angesprochen
  * Werden sie kontaktieren
  
  
## TOP 6: Stura-Plenum
* Next-Bike: Semesterbeitrag würde um 1€ erhöht - 30min Fahrtzeit. Hintereinander buchbar
  * Datenschutzrechtliche Fragen müssen geklärt werden: Wie erfolgt die Buchen? Welche Daten bekommt Next-Bike?
  * Fabian und Eric erstellen einen Fragenkatalog. Berichterstattung nächste Sitzung.

## TOP 7: Termine und Sonstiges
* Erstsemestereinführungsheft: Ist das Verschicken von Erstsemesterinfos per Mail nun möglich?
* Sommerparty 2.7:
Aufbau- Abbauhelfer werden gesucht Sebbo fragen wegen Kooperationsverträgen
* Überlegung der Einrichtung einer festen Bürozeit → Denise würde es machen, z.B. Do 11-12 Uhr
