---


---

**Anwesende**: Georg, Paul, Christofer, Manu, Eric, Denise
**Protokoll:** Denise

# Tagesordnung
-  Kryptomodul
- Prüfungskommission
- Studienfahrt
- 33C3
- Erstieinführungsfeedback
- Transponder
- Tor Node
- Sparkasse
- Plenum
- Sitzungstermine in Kalender und Sprechzeit
- Moderation Emails
- ADS



## 1. Kryptomodul
Fehlendes Kryptomodul. Bedarf und Interesse sollte aber gegeben sein. Eric fragt jemanden. Alle weiteren Modalitäten ergeben sich, wenn wir näheres wissen.

## 2. Prüfungskommission
Martin oder Nico hatten sich bereit erklärt in die Prüfungskommission zu gehen.
Es erfolgt eine Abstimmung mit dem Ergebnis:

6 Stimmen für Nico.
0 Stimmen für Martin.
0 ungültige Stimmen.
0 Enthaltungen.

Der FSR entsendet Nico in die Prüfungskommission.

## 3. Studienfahrt
Der Zeitraum Herbst erscheint uns am sinnvollsten.
AK Studienfahrt: Georg, Christofer, Denise

## 4. 33C3
Dieses Jahr stehen noch zwei Raten aus, dementsprechend haben wir eine Finanzsituation, die die Förderung auch ohne Stura ermöglicht.

Finanzbeschluss des FSR Informatik: Um Studierende zum 33C3 zu fördern wird ein Topf in Höhe von 1000€ festgelegt.
Maximal 20 Studierende werden gefördert (50% des Tickets). Sollten weniger Studierende gefördert werden, behält sich der FSR vor, den übrigen Betrag auf die Geförderten umzuverteilen, also mehr als 50% des Tickets zu übernehmen.

6/0/0

Es soll wieder ein Feedbacktreffen im Januar geben.

Denise kümmert sich um folgende Texte:
Du bist dabei Mail
Du bist auf der Warteliste
Du bist doch dabei
Infotext

## 5. Erstieinführungsfeedback
**Führung** kam ganz gut an (ca. 40 Leute).
MB war überfüllt. Kam sehr gut an. Hatten gute Gespräche mit den Erstsemestern.

**Kneipentour:** Puschkin war bisschen eng. 80+ Leute. 
Beyerhaus war gut gefüllt, haben nicht alle unsere Plätze gebraucht. 
Jet. Kam sehr gut an. Jet hatte vergessen, dass wir kommen. Nächstes Jahr nochmal anrufen oder ein paar Tage vorher hingehen. Christofer hatte sie im Vorfeld nicht mehr erreicht.

Nächstes Jahr Party organisieren (Räume bei der Feinkost, oder Destille).

Infos bzgl Kneipen ins Wiki stellen.

## 6. Transponder
Manu bekommt Verena's Transponder. Manu gibt seinen Transponder ab und Paul beantragt Zugang.

## 7. Tor Node
Keine Neuigkeiten

## 8. Spk
Unterschriften abgeben oder warten bis neuer Stura Finanzbeauftragter?

Nachtrag 5.11.16: Manu hat sich beim Stura erkundigt. Unterschriften abgeben.

## 9. Plenum 
Eric bittet aus dem Plenum entlassen zu werden. Manu erklärt sich bereit ins Plenum zu gehen.

Beschluss: Der FSR Informatik entsendet Manu ins Plenum.
5/0/1


## 10. Sitzungstermine in Kalender und Sprechzeit
Eric kann in den ungeraden Wochen nicht. Sprechzeit wird erst einmal nicht gelegt.

## 11. Moderation Emails
Dominik kontaktiert Verena.

## 12. ADS
Mail an Stadler verfassen.
Schreiben an Justitiariat fertig haben, bevor wir Stadler kontaktieren.
Mail: Denise
Just: Georg
