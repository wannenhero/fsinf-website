---
---

Anwesend: Natascha, Bianca, Moritz, Ahmad, Elena, Niel, Benedikt, Alina, Georg, Niclas, Paul, Amin, Andre (StuK)
Sitzungsleitung: Ahmad
Protokoll: Paul

#### Tagesordnung
1. Orga
2. Vernetzung Mitteldeutscher Unibund
3. KIF 47.0
4. Wahlen Auswertung
5. Sommerfest
6. Finanzen
7. Emails
8. Bericht Treffen Bogdan
9. Ars Lendy Preis
10. Erstiwoche
11. Moodle

## 1 Orga

#### 1.1 Website
* Paul hat keine Zeit, fragt noch nach wie wir die Website bauen können
* Natascha würde sich mit einklinken

#### 1.2 FSR Raum
  * Hygiene vom Kühlschrank sehr mäßig
  * Teller wegbringen dürfte nicht so schwer sein
  * Salat stand mal sehr lange im Kühlschrank
  * Pfand kann nicht so lange da rum stehen
  * Differenzieren zwischen Pfand und Essenssachen
  * Tüte für Pfand wäre gut -> Bianca bringt Tüte mit
  * Im Kühlschrank sehr alte Dinge von alten Veranstaltungen
  * Geschirr muss einfach in den Geschirrspüler
  * Eigener Dreck sollte weggemacht werden
  * Reste von Veranstaltungen sollten mal entfernt werden
  * Individualität baut nicht genug Druck auf
  * Alina würde den Kühlschrank reinigen und sich beschweren wenns eklig ist
  * Vorschlag: Veranstaltungsreste verschenken wenn sie nicht absehbar verbraucht werden
  * z.B. Kooperation mit Foodsharing, bei Verteilergruppen reinstellen, über FSR-Channel mitteilen etc.
  * Vorschlag: Foodsharingverantwortliche Person bestimmen?
     * Alina und Elena würden sich damit auseinandersetzen


## 2 Vernetzung Mitteldeutscher Unibund
  * es können Module in Jena, Halle, (HTWK) belegen
  * wenige Menschen wissen das
  * Moritz hat FSRä Halle und Jena angeschrieben
  * Rückmeldung steht aus, eventuell mal Vernetzungstreffen -> kommt nochmal zurück in die Sitzung
  * Ziel?
    * Vernetzung mit den anderen FSRä, Bewerbung des Angebots in den jeweiligen Fachschaften
  * Wie kann sowas beworben werden?
     * Hinweis bei Studienverlaufsplänen oder Ähnliches
     * gucken wer die fmi-Seite verwaltet
     * Newsletter
     * Moritz würde sich mal informieren wer die Seite verwaltet
     * bei Erstiwoche bewerben
        * eventuell in Neumanns Erstiprogramm hinzufügen
     * evtl. Infoabend in Erstiwoche wo Erfahrungen davon vermittelt werden
     * Redundanz kann vermieden werden wenn wir tatsächlichen Infoabend mit Erfahrungsberichten machen
     * Infoabend exklusiv zu dem Thema schwierig, sollte später bei Erstiwoche nochmal besprochen werden

## 3 Bericht von der KIF 47.0
  * Moritz war da
  * viele Informatikfachschaften vertreten
  * es wurden Resolutionen gemacht
    * online nachlesbar
  * ziemlich nett und freundlich, viele Umarmungen
  * KIF 47.5 wird nicht in Leipzig stattfinden, da neue FSRä keinen Bock mehr haben
  * da wäre es eventuell gut zu unterstützen, in 4 Monaten muss die organisiert sein
  * Moritz hat sich vernetzt mit anderen Menschen, vorallem Raum Mitteldeutschland
  * KIF 47.5 ist möglicherweise während unserer Studienfahrt, möglicherweise eine Woche früher

## 4 Wahlen Auswertung
  * Wahlbeteiligung war hoch, das fand Paul gut
  * Vorstellungstexte gut?
    * Benedikt fand Vorstellungstexte gut
    * Alina war zwischendrin skeptisch, hatte endgültig gutes Gefühl mit Texten und Sprüchen auf Plakaten, sehr gute Sichtbarkeit
    * Bianca fand Sichtbarkeit gut, besser als Vorjahr, allerdings sehr kurzfristig
    * Moritz sagt Texte hätten direkt eingesammelt werden sollen, also schneller
    * Paul sagt vielleicht ist es schwierig das früher
    * Texte/Plakate waren in der Vorwoche ausgehangen, Aufruf für Texte war 2 Wochen vorher ausgegeben)
  * 200 Krapfen (100 pro Tag haben gut gepasst, am zweiten Tag ca. 20 übrig) dazu Neapolitaner (vegan) und glutenfreier Kuchen (von Natascha) kamen gut an

## 5 Sommerfest
  * in Schichtplan eintragen, viel fehlt noch
  * Paul fehlt noch, wartet noch auf Awarenesstreffen
  * Moritz hat 4 Schichten, kann eventuell noch eine mehr machen
  * Bewerbung Bändchen gegen Hilfe
    * Zustimmung, Moritz twittert einen kleinen Text (fragt vorher nochmal in der Sommerfestgruppe)
    * auch nochmal in die Facebookveranstaltung
  * es wird noch dringend Awarenesshilfe gesucht, bisher sind wir 7 Leute, heute abend 17:30 Treffen am FSR Büro
  * Ort im Friedenspark muss unbedingt dann gepostet werden, spätestens am Tag des Sommerfests aber so früh wie möglich

## 6 Finanzen

#### 6.1 35C3 Fehleranalyse
  * Wir haben den Kongress im Dezember gefördert
  * Überweisen sollten heute oder vor heute rausgehen
  * 2 Probleme
  * Kommunikation mit StuRa-Finanzen sehr schwierig, es wurde nicht direkt gesagt was wir schicken sollen
  * dabei gingen Wochen zwischen Antworten ins Land
  * Moritz hatte persönliche Probleme und hat teilweise nur verzögert daran arbeiten können
  * nächstes Jahr Bezahlbestätigung im Anmeldetool, zusätzlich zu Ticket und Imma
  * Sofort nach dem Kongress angehen
  * Finanzer und stellv. Finanzer sollten das gemeinsam machen
  * Aufgaben wie Tickets runterladen und zusammenhängen sehr schwierig
  * Tool müsste verbessert werden
  * Ahmad: es sollte Dokumentiert werden was gebraucht
  * Paul hat auch Erinnerungen im Januar verbummelt
  * Zahlungsbestätigungen sind schwierig wenn Menschen in Gruppen Tickets kaufen -> dafür brauchen wir Lösung
  * Man könnte sagen Tickets sollen schon umbenannt hochgeladen werden oder Tool benennt es selbst um
  * Finanzanträge für dieses Jahr sollten frühzeitig rauskommen, möglichst bald gestellt werden
  * Moritz und Graubi schließen sich dafür kurz

#### 6.2 Antrag KIF Förderung
  * Christofer betritt den Raum
  * Moritz hat vor Wochen 29,80€ beantragt für seine Zugfahrt nach und von Dresden
    * Abstimmung 5/0/1 angenommen

## 7 Emails

#### 7.1 Abrechnung Efraim Zuroff
  * müsste von Graubi geklärt werden, genauso wie alle anderen offenen Förderungen
  * Alina schreibt auch eine Email an MHG

#### 7.2 Gremienworkshop
  * Freitag und Samstag Gremienworkshop, Teilnahme da wäre sehr nützlich

#### 7.3 Studienerfolgstag 20.06
  * Studienerfolgsprojekte stellen sich von 11-14 Uhr im Hörsaalgebäude vor
  * Twittern wäre gut -> Paul macht das

#### 7.4 Fragen zum Doppelstudium mit Philosophie
  * Alina hat der Person geschrieben dass es eine Person gibt die Ahnung hat. Falls keine Rückmeldung kommt ist es abgehakt

## 8 ars lendi Preis
  * Bianca und Paul schreiben Prof. Gräbe für Gespräch
  * Ist Preis für interdisziplinäre Lehre
  * Elena findet dass ein Preis nicht unbedingt für Lehre sein sollte die mit gemischten Gefühlen aufgenommen wird
  * geht es um interdisziplinäre Module oder Interdisziplinarität in ganzer Lehre? Mit Gräbe mal klären

## 9 Erstiwoche
  * Andre vom StuK: StuK will mehr Angebot für Erstsemester machen
  * will mit unserem FSR zusammenarbeiten, z.B. in Erstiwoche oder bei Kneipentour
  * Alina fände Kooperation nicht schlecht
  * Ahmad fand Kneipentour mit vielen Locations besser
  * eventuell fürs Erstigrillen sehr gut
  * Kneipentour letztes Jahr sehr getaktet
  * Moritz: Kneipentour hatte mehrere Probleme die einzelne sehr stören (Raucherkneipen, Alkohollastigkeit, Orgastress, wenig Zeit in einzelner Kneipe)
  * Alina: Idee: Dazu Spieleabend machen um diverser anzusprechen
  * Wollen wir mit dem StuK zusammenarbeiten?
  * Ahmad: Wir machen relativ wenig Einführungsveranstaltungsorga. Spieleabend und Grillabend können Kneipentour gut ergänzen
  * Alina: Grundlegend gerade viel Motivation vorhanden
  * aus dem letzten Protokoll: Campustour, Kneipentour, Spieleabend, Infoveranstaltung für ausländische Studis, Infoveransaltung für DH, Gruppen organisieren
  * Alina: Ideen offen lassen, seperate Einführungen für DH, Ausländische Studis, etc. im eigenen Kontext wäre gut
  * Wie ist Austausch mit StuK gewünscht? StuK wünscht sich mehr Austausch und Veränderung, da sind FSRä sehr interessant
    * StuK kann auch Räumlichkeiten für Spieleabende stellen, hat auch sehr viel Technik
  * Bianca: Kneipenquiz könnte sich anbieten
  * Amin fände interkulturelle Veranstaltungen gut
  * Wie ist Miete, Preise, etc. im StuK?
  * keine Raummiete, normale StuK-Preise, das StuK versucht dabei Webung zu machen/ Leute zu aquirieren
  * Bianca: Es geht nicht um entweder oder, z.B. könnte man erstmal einfach das Erstigrillen hier machen
  * Andre lädt nochmal zum Clubrat ein, alle 2 Wochen Montag Sitzung
  * Alina: Spieleabend könnte man mal wieder organisieren, man bräuchte dafür aber mehr als eine Person
  * Moritz würde zu dem Clubrat gehen
  * Alina: Spieleabend ist keine Institution, es gab wenig Beteiligung aus dem FSR und wenig Feedback. Aus Spieleabendgruppe kam auch für neue Spieleabende wenig Feedback. Mehr Feedback allgemein wäre da toll.
  * Ahmad: In Konstisitzung könnte Hut verteilt werden für Veranstaltungen
  * Andre: muss bald los, eine Art Stammtisch wäre schön, oder nach Sitzung vorbeikommen, erzählt nochmal normalen Zeitplan des StuK's, Kontaktadresse gibts auch


## 10 Moodle
  * Wir kriegen einen Moodlekurs wenn wir das wollen, da könnte Erstivernetzung gut funktionieren
  * Vorschlag Moodlekurs mit mehreren Foren aufmachen
  * Wissen kann dort über Semester hinaus weiter getragen werden
  * Auf Adobe Connect könnte zurückgegriffen werden für Onlinesitzungen etc.
  * Georg findets prinzipiell gut, hat aber keine Ahnung von Moodle, Austauschmöglichkeit ist gut
  * Natascha findets ziemlich gut, müsste man sich mal einlesen
  * Paul findets sehr gut, wir wollten ja mal Mattermost machen, würde sich auch beteiligen
  * Natascha, Paul, Georg wollen trainieren
  * erstmal testen und dann gucken
  * Werbung bei Dozierenden kann sehr gut funktionieren
  * Für Erstiwerbung sehr gut
