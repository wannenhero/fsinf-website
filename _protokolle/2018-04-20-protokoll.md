---


---

**Anwesende:** Tim, Christofer, Paul, Ahmad, Irene, Sofia, Phillipp, Georg, Conrad (Gast), Jonathan (Gast), Denise (später)

**Protokoll:** Georg

## Tagesordnung
1. Kollektivfestival
2. Grillen
3. Treffen bzgl Zulassungsbeschränkung und Studienerfolg
4. Sitzungstermin
5. Introduction to Digital Philologies Prüfungsleistung
6. Studienfahrt
7. E-Mails
8. Orga
9. Newsletter
10. TOR-Exitnode
11. Corporate Design
12. Prüfungsvorleistungen

## Kollektivfestival
* Paul, Marc, Moritz, Christopher
* Vortrag: Ethik in der Informatik (Sarah Sterz, WHK Uni des Saarlandes)
* Kooperationsvertrag muss eingegangen werden
* Kosten belaufen sich auf Hin- und Rückfahrt aus dem Saarland (mit BahnCard 50)
* Beschluss Honorar 100€: 7/0/0 → Angnommen

## Grillen
* Conrad hat Initiativanfrage gestellt
* Mit Wahlveranstaltung verbinden?
* Sommerfest + Wahlveranstaltung kommen → möglichst zeitnah
* Was gibt es zu organisieren: Ort (Richard-Wagner-Hain), Kasse, Grill, Grillschichten, Grillgut selbst mitbringen?, Getränke?
* Beschluss bis 200€ für Getränke auszugeben: 6/0/1
* Conrad, Jonathan (setzt sich Hut auf), Phillipp, Christopher organisieren/unterstützen
* Datum: Mittwoch, 02.05.2018

## Treffen bzgl Zulassungsbeschränkung und Studienerfolg
Protokoll zum Treffen ging über den Verteiler  
Themen:
* vorhandene Kapazitäten
* Zulassungsbeschränkung kommt, muss gestaltet werden (Selfassesment (dauert noch), Infobroschüre)
* Abbruchgründe/Scheinstudierende
* weitere Masterstudiengänge
* Ziel sind mehr Kapazitäten und Masterabschlüsse 

Bemerkungen: 
* offener Informatikraum gewüscht (Finanzmittelproblem)
* Digital Humanities: mehr spezialisierte Module durch neue Berufungen
* Workload/Selbstverantwortung für Schulabgänger ungewohnt

* Weitere Mitarbeit und Ideen sind erwünscht, als nächstes wird das Thema in den entsprechenden Gremien diskutiert
* als kurzfristige Lösung gibt es immer noch die Losungsmöglichkeit
* Wir bleiben am Thema dran! (→nächste Sitzung)

Treffen mit externem Gutachter:
* Auswertung vertagt, Denise schickt Protokoll rum

## Sitzungstermin
* wie immer schwierig...
* Freitag ist auch immer schwierig, aber Donnerstag sieht doch ganz gut aus!
* Abstimmung Donnerstag 15 Uhr Termin: 8/0/0
* TO wird zukünftig zusammen mit Sitzungstermin veröffentlicht (soweit bekannt)

## Wahlen
* ab sofort Aufstellung möglich
* mehr FAK-Rat-Kandidierende wären gut, damit bei Rücktritten keine studentischen Stimmen fehlen
* Wahlveranstaltung zur Vorstellung
* Werbung in Vorlesungen, Social Media, persönlich, etc.
* Belohnung (Krapfen)
* Schichtplan → Denise

## Introduction to Digital Philologies Prüfungsleistung
* Klausur steht in Prüfungsordnung, welche noch nicht öffentlich zugänglich ist
* Denise antwortet Prof. Bogdan

## Studienfahrt
* Flixbus ist gebucht
* vertagt

## E-Mails
* vertagt

## Orga
* Postausgang: Porto wird übernommen. Wichtig: Uni-Absender (FSR Informatik, Universität Leipzig)

* KIF: Konferenz Infomatikfachschaften
	* Termin 09.-13.05 in Bremen
	* Kosten können übernommen werden
	* Bei Interesse: Anmeldeschluss ist Sonntag 22.04.

## Newsletter
* Irene kümmert sich

## TOR-Exitnode
* vertagt

## Corporate Design
* vertagt

## Prüfungsvorleistungen
* Prüfungsvorleistungsmodalitäten (nach Punkt 7, Protokoll 27.3)
* vertagt

