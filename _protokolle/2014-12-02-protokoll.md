---


---

* Anwesend: Fabian, Kasimir, Martin, Nancy, Sebbo
* Protokoll: Kasimir

## Wahlen

* Schichtplan wurde gebastelt

## Weihnachtsparty

* Plakate maximal 100 €
* Bedingung: Verteilungsschlüssel Gewinn/Verlust prozentual, also 43,75% Info, 43,75% FaRAO, 12.5% MuWi/KuPäd
* Abstimmung: 4/0/1

## Lehrbericht 2012/13

* Wir müssen unseren Beitrag bis 4.12.2014 einreichen.
