---


---

* Sitzungsleitung: Alrik
* Protokoll: Fabian
* Anwesende: Fabian, Alrik, Nancy, Martin, Kasimir
* beschlussfähig mit 5 von 9

## Tagesordnung
1. Technische Informatik SoSe 2014
2. Mobile Advertising SoSe 2014
3. FSR-Server
4. Verantwortlicher für Jobs/Fsinf-Liste
5. Klausur ADS I
6. Beschluss letztes Protokoll
7. Nächster Sitzungsverantwotliche

## 1. Technische Informatik
*  Hausarbeit stand nicht in der Modulbeschreibung, wurde jedoch als Prüfungsleistung(?) verlangt
*  Nach Ansprache der Problematik ist die Hausarbeit nun fakultativ

## 2. Mobile Advertising
* Vertiefungsmodul Mobile Advertising (jetzt Mobile Peer-To-Peer Systeme) wird nicht angeboten, obwohl es im Modulkatalog steht
* wird wohl verschoben - das wird Interessenten bei Bedarf auch attestiert (evtl. für BAföG-Empfänger interessant)
* FSR will via StuKo vorschlagen, dass mit einer Vorlauffrist von mindestens 1 Semester die angebotenen Module feststehen müssen

## 3. FSR-Server
* Fabian schreibt Infomail zu Webseiten-Interna
* Seite
  * FAQ in die Seite integrieren (Dabei aus dem Dokuwiki raushauen)
  * twitter-feed/link (Kasi ist fleißiger twitter-Meister)
  * google calendar
  * protokolle brauchen kein last modified
  * (Studenten-)Service-Seite
    * Tex-Vorlagen
    * Klausuren als unterpunkt(?)
    * VPN as a service für conference-wifi, von strongswan auf softether wechseln?

## 4. Verantwortlicher für Jobs/Fsinf-Liste
* fsi-Liste muss einen qualvollen Tod sterben
* wünschenswert: jeder kümmert sich ein bisschen
* kommissarisch besetzt durch Alrik

## 5. Klausur ADS I
* Herr Heyer lehnt eine Veröffentlichung der Altklausur ab.

## 6. Beschluss letztes Protokoll
* beschlossen mit 5/0/0

## 7. Nächster Sitzungsverantwotliche
* Nancy freut sich schon tierisch drauf
