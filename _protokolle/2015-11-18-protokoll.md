---


---

**Anwesende:** Georg, Eric, Fabian, Friedrich, Nico, Denise und Hans-Christoph Thiele (Referent für FSR-Kommunikation) als Gast

**Protokoll:** Fabian


## TOPs:

1. FSR-Kommunikation
2. Sommerparty
3. GitLab + Webseite + Mail-Tool
4. Rundmail bzgl. GitLab
5. Weihnachtsfeier
6. Studienfahrt
7. Newsletter
8. Anschaffungen

## 1. FSR-Kommunikation

  - Hans-Christoph Thiele ist als Referent für FSR-Kommunikation zu Gast
  - Checkliste ausgehändigt zu der Rückkopplung erfolgen soll
  - wir sollen uns mit dem FSR Mathe bzgl. der Lehramtsstudierenden kurzschließen
  - FSR-Vernetzungstreffen sollen ab ca. Januar wieder stattfinden


## 2. Sommerparty

  - FSR Geschichte fragt an, ob wir in Kooperation eine Sommerparty organisieren wollen (Mail bzgl. Status von Fabian)
    - Wir formulieren eine Mail an die bisherige Orgaliste um anzufragen, wer noch Lust hat eine weitere Party zu organisieren (Georg + Fabian)
  - FSR Physik fragt an, ob wir ein Sommerfest von ihnen unterstützen wollen
    - Wir wären über ein Protokoll des ersten Orgatreffens und konrekte Vorschläge wie eine Kooperation aussehen kann erfreut (Denise)


## 3. GitLab + Webseite + Mail-Tool

  - Shorturl --> Fabian kümmert sich (Chris Bescheid sagen)
  - Mail-Tool wird befürwortet


## 4. Rundmail bzgl. GitLab

  - Friedrich und Nico schreiben Mail an Herrn Scheuermann, die dieser an die Dozenten weiterleitet.


## 5. Weihnachtsfeier

  - Orgateam besteht aus: Friedrich, Georg und Fabian
  - Abstimmung: Kosten der Weihnachtsfeier werden vom FSR getragen. Es wird eine Soli Kasse für nicht-alkoholische Getränke geben. Ergebnis: 6/0/0 (angenommen)


## 6. Studienfahrt

  - Nico sucht und bucht Locations
  - Fahrt im Sommer 4/0/2 --> Erstifahrt durch O-Woche o.ä. ersetzen
  - evtl. auch Zelten möglich (wichtig: möglichkeit der "Massen"-Selbstversorgung)
  - Erreichbarkeit via Nahverkehr wichtig
  - idealerweise an einem See


## 7. Newsletter

  - Wird demnächst im GitLab verwaltet und entworfen


## 8. Anschaffungen

  - Raspberry Pi 2 + Netzteil + Gehäuse + HDMI zu VGA-Converter + 3 MicroSD-Karten
  - Aktivboxen (z.B. [bei Thomann](http://www.thomann.de/de/samson_mediaone_4a_bt.htm))
