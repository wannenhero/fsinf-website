---


---

* konstituierende Sitzung für die Amtszeit April-September 2015

## Anwesende:
* Nancy
* Martin
* Thomas
* Denise
* Eric
* Georg
* Fabian
* Kasimir (Gast)
* Verena
* Sebbo


## TOPs:

1. Wahlen
2. Transponder

## 1. Wahlen
### Finanzverantwortliche_r + Stellv. Finanzverantwortl.
#### Wahlvorstand
* Kasimir
* Nancy

#### Kandidaten
* Denise
* Thomas (Vertretung)

#### Abstimmung
* Denise 6/2/0
* Thomas (Vertretung) 7/1/0

### Sprecher_in + Stellv. Sprecher_in + weitere Kontovollmacht
#### Wahlvorstand
* Kasimir
* Martin

#### Kandidaten
* Nancy
* Georg (Vertretung)

#### Abstimmung
* Nancy 8/0/0
* Georg (Vertretung) 8/0/0


### StuRa (2 Plätze + evtl. Stellvertretung)
#### Kandidaten
* Fabian
* Eric
* Denise (Vertretung)

#### Abstimmung im Block
* 8/1/0


### StuKo (3 Plätze + 1 Stellvertretung)
#### Kandidaten
* Verena
* Martin
* Nancy
* Eric (Vertreung)

#### Abstimmung
* 8/1/0


### Sonstige "Ämter"
#### Party-Beauftragte_r
* Sebbo
* Georg

#### Newsletter
* Thomas

#### Twitter
* Fabian
* Eric

#### E-Mail Moderation
* Verena

## 2. Transponder

* Martin → Georg
* Kasimir → Denise
* Daniel → Thomas
* Alrik → Eric
* Nicole → Martin
* Benjamin → Verena

* Matching an Dr. Janassary melden
