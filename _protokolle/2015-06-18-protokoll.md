---


---

Anwesend: Eric, Georg, Denise, Verena, Fabian, Nancy     
Protokoll: Denise

# Tagesordnung
    1. Sommerparty
    2. Brief ans Rektorat bzgl. Meinungs- und Pressefreiheit
    3. Mathe-FSR: Kooperation + Studienfahrt
    4. TI und Hardware-Praktikum → Vertagt auf nächste Sitzung
    5. Weihnachtsvorlesung
    6. STURA-Update
    7. Bürozeit
    8. Neue Lehrveranstaltungsevaluationsordnung
    9. Sonstiges

## 1. Sommerparty

* Schichtplan zum Eintragen
* Abstimmung: Der FSR übernimmt die Kosten und versucht diese möglichst durch Kooperationsverträge zu decken
  Ergebnis: 5/1/0 (angenommen)
* Kooperationsverträge werden diese Woche noch abgeschlossen und bei der nächsten Sitzung           
  besprochen


## 2. Brief ans Rektorat bzgl. Meinungs- und Pressefreiheit

* Brief in Bearbeitung


## 3. Mathe-FSR: Kooperation + Studienfahrt

* Dokumentenfolie für Tür wird verschenkt
* Denise war in der Sitzung des Mathe-FSR 
→ wollen mit uns mehr kooperieren
* Würden 3 Mitglieder für ein gemeinsames Studienfahrtorgateam entsenden
* Abstimmung: Studienfahrtkooperation mit Mathe-FSR
   Ergebnis: 6/0/0 (beschlossen)

* Abstimmung: Staffelpreise bei Studienfahrt
  Ergebnis:  0/1/5 (abgelehnt)

* Antrag auf Zuschuss im Stura gestellt 
→ Beitrag: etwa 35€ Pro Person
→ Dank MDV-Vollticket kann Gruppenticket ab Oschatz gelöst werden (~5€ pro Person)

* Nancy kann nicht mitfahren.
 → Mitgliederentsendung Orgateam: Georg, Verena


## 4. TI und Hardware-Praktikum → Vertagt auf nächste Sitzung

## 5. Weihnachtsvorlesung

* Zwei Vorträge oder eine gemeinsame Vorlesung (~60min)
* Denise schreibt Mails an Professoren


## 6. STURA- Update

* RAS-Wahl wird nachgeholt (16./17.7)
* Nextbike:
→ Abstimmung unter allen Studierenden über Almaweb (wann?)
→ Ausleihen über Studentenausweis


## 7. Bürozeit

* Verena/ Denise 
→ Ab nächstem Semester


## 8. Neue Lehrveranstaltungsevaluationsordnung

* Verena schreibt Email an Prof. Bogdan zur Klärung von Fragen


## 9. Sonstiges
* Ausstehende Mails beantworten
* Nächste Sitzung Mittwoch, 1.Juli, 15.30 Uhr
