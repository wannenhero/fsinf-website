---


---

Anwesend: Moritz (Sitzungsleitung), Natascha (Protokoll),Paul, Bianca, Deniese (per Computer)

### 1 Orga :
 - Studienfahrt bezahlt: min 2 Leute fehlen
 - Finanazen: Überblich benötigt, nur Abrechnung gemacht -> Überblick wird angefertigt
 - WeiFei: Technik, bei Natascha, würde es gene abgeben
   - Triak buchen? Beschuss (über Geld) erst nächste Woche möglich. 
   - Deniese will zu- oder absagen. 
   - Option Sitzung/Meetup für nächste Woche, auf anderen Tag verlegen (um Beschussfähigkeit zu gewährleisten). Moritz kümmert sich drum.

### 2 Surafinanzen: 
 - Finanzenproblem  	
   - Information:
     - Entweder Studienbetrag erhöhen oder Ausgaben kürzen
       - für nächstes Jahr gibt es keine Rücklagen mehr
       - Gehälter von Festangestellten vom Stura sind gestiegen
       - Studienbeitragerhöhung von 8,50 für Sem. aufsteigen auf 12 Euro
       - muss noch dieses Sem. beschlossen werden
       - Ausgabenkürunung nicht ersichtlich (alle Bereiche sehr wichtig)
       - entstandene Rücklagen durch unbesetzte Stellen, ungenutzes Projektengeld, FSR Töpfe nicht ausgeschöpft


### 3 35C3: 
 - Kooperationsvertäge müssen beschlossen werden -> Paul kümmert sich drum
   - Rechnung an alle Förderer -> nach dem Anmeldung für dei Förtederung, geld einsammeln, dann ausschütten
	
### 4 Theodor- Litt- Preis
 - Fehler aufgetreten, Rückfragen zum Stura anstatt zu uns gesendet
 - Jemand wollte sich der Kritik annehmen -> Gespräch mit dem FSr

### 5 Mails
 - Überweisung von Teilautovon Studienfahrt muss noch überwiesen werden
 - Auswetung der Lehrberichte  5.2 
   - Deniese Frage. Sind Lehrberichte öffentlich? : vielleicht in FakRatProtokoll 
     - Frau Braun fragen -> Moritz fragt nach

### 6 SWT VL 
 - keine Infoationen zu -> vertagt

### 6 Offener InfRaum
 - Deniese hat den Hut auf, kommt nicht dazu 
 - P402 zugang, Irene nicht da, Vertagt

### 8 eHumanities
 - Leute sollten aquiriert werden -> vertagt

### 9 Umfrage Ziwielklausel
 - nächste 2 Wochen Umfrage für Studierende über Tool
 - Podiumsdiskussion am 12.12 19 Uhr HS11
 - Bewerbung: auf Tiwitter, Facebook und co. 
 - Beschluss für den Stura wird gefasst, damit zum Senat, um feste Statistik zu haben

### 10 FSR der Zukunft
 - wir sollten uns eher um den EFS der gegenwart kümmern
 - -> vertagt
