---


---

anwesend
* Mitglieder: Martin, Fabian, Eric, Georg, Sebastian, Denise, Nancy, Thomas
* Gäste: Friedrich, Nico

## TOPs:
* StuKo
* Wahlen/ Newsletter
* Berufungskommission
 

## StuKo
* Prof. Lindemann möchte Praktika streichen
* Entscheidung wird zur nächsten StuKo gefällt

## Wahlen
* Der FSR möchte mit Kuchen zur Wahlbeteiligung ermutigen
* Abstimmung: Ausgabe Kuchenkosten von max 30 Euro. Ergebnis: (7/1/0)
* Wählerinnen-VZ liegt bei Hr. Schmidt aus
* 12. Mai ist Frist für Aufstellung

## Berufungskommision für Nachfolge Prof. Hayer
* Denise und Verena kommunizerien mit einander, wer von beiden in die Kommision gehen

##Aufgaben
* Sebbo bucht Raum für zukünftige FSR-Sitzung
* es wird versucht sich um eine Kopierkarte für 5.OG zu bekommen (Fabian)
