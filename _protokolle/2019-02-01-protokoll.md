---


---

## Anwesende: 
* Josy (Protokoll)
* Irene (Sitzungsleitung)
* Marvin (Gast)
* Bianca(Gast)
* Sofia(Gast)
* Natascha
* Moritz
* Marc
* Georg


## 1 Orga

* nix

## 2 Emails


* FSR Mathe Kooperationsideen
    * Weihnachtsfeier-/ Weihnachtsvorlesungskooperative?
        * Idee: Ausweichen auf Paulinum?
        * Gegenvorstoss: Eigene Vorlesung, zusammengelete Feier?
            * AG Gruendung aus Moritz, Bianca, Natascha, Marvin

* StuRa Qualitätsmanagement-Handbuch
    * jemand Interesse an Beitiligung?
        * Kurz und Knapp:  
        Nein
* Autorisierung Artikel zu Digital Humanities
    * LVZ Artikel bekommt grünes Licht
    * Wunsch über TL-Preis zu Sprechen soll geäussert werden

## 3 Ehumanities
* Gespräch mit den Dozierenden Suchen?
    * Crane's Modul
        * nicht anwesend ('Skype-Lehre')
        * Abweichung Lehre von Modulhandbuch
            * nur Vorträge
            * keine Fremdvorträge
        * Datenschutzbestimmungen nicht eingehalten
        * Marc erkundigt sich bzgl rechtlicher Grundlagen

## 4 LP WT
* Zu viel Arbeitsaufwand für 5 LP? 
    * Anfrage von Bianca
* Bianca und weitere gehen auf Dozentin zu


## 5 Der FSR der Zukunft
* einige Abgänge
* wie machen wir Werbung?
    * Wahlaufstellungswerbung soll in den Pools ausgehaengt werden, ggf noch Mensa und Bib
    * Social Media

## Weiteres
* SWT: Bewertungsmaßstab liegt im Moodle vor
* allgemeine Kritik an Modulinhalten
* warten Antwort auf Email an Scheuermann ab
