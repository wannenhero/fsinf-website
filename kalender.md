---
title: Kalender
permalink: /kalender/
lang: de
ref: kalender
---

<div class="hidden-xs responsive-iframe-container">
<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;height=600&amp;wkst=2&amp;hl=de&amp;bgcolor=%23FFFFFF&amp;src=fsinf.leipzig%40gmail.com&amp;color=%2323164E&amp;ctz=Europe%2FBerlin" style=" border-width:0 " height="575" width="100%" frameborder="0" scrolling="no"></iframe>
</div>
<!--<div class="visible-xs">
<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=fsinf.leipzig%40gmail.com&amp;color=%2323164E&amp;ctz=Europe%2FBerlin" style=" border-width:0 " width="100%" height="600" frameborder="0" scrolling="no"></iframe>
</div>
<div>
Abonniere den iCal-Feed unter: <a href="https://fsinf.informatik.uni-leipzig.de/calendar/">https://fsinf.informatik.uni-leipzig.de/calendar/</a>
</div>-->
