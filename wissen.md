---
title: Wissen
permalink: /wissen/
lang: de
ref: wissen
---

Hier haben wir für dich nützliche Informationen über das Studium zusammengetragen. Darüber findet  Die Informationen, die wir dir hier zur Verfügung stellen, entsprechen unserem eigenen Wissensstand nach mehreren Semestern Studium und bestem Wissen und Gewissen. Sie stellen **keine rechtsverbindlichen Auskünfte** dar, sondern sollen dir das Studienleben ergänzen und erleichtern. Sie können – insbesondere bei Problemfällen – keinesfalls eine Studienberatung durch das Studienbüro ersetzen.

Im Zweifelsfall verlass dich nicht auf unsere Informationen, sondern lies die Ordnungen und/oder frage im Studienbüro nach!

## Inhaltsverzeichnis
1. [ **FAQ** ](#FAQ)
1. [ **Dein Studium**](#Studium)
	1. [Universität](#1.1)
	2. [Räumlichkeiten der Informatik](#1.2)
	3. [Studiengänge an unserem Institut](#1.3)
	4. [Regelstudienzeit](#1.4)
	5. [Module und Workload](#1.3)
	6. [Ergänzungsfach](#1.6)
	7. [Studien- und Prüfungsordnung](#1.7)
	8. [Studienbüro](#1.8)
	9. [Moduleinschreibung](#1.9)
	10. [Prüfungsvorleistungen](#1.10)
	11. [Prüfungsan- und abmeldung](#1.11)
	12. [Prüfungen und Modulnote](#1.12)
	13. [Prüfungsergebnisse und Nachprüfungen](#1.13)
	14. [Studienorganisation](#1.14)
	15. [Anwesenheitspflicht?](#1.15)
	16. [Hilfe im Studium](#1.16)
2. [**Technik**](#2)
	1. [GitLab des FSR](#2.1)
	2. [WLAN](#2.2)
	3. [Uni E-Mail](#2.3)
	4. [Hardwarekauf und Lizenzen](#2.4)
3. [**Arbeitsplätze an der Universität**](#3)
	1. [URZ Arbeitsplätze und Drucker im Augusteum](#3.1)
	2. [Kopieren](#3.2)
	3. [Fakultätsinterne Arbeitsplätze](#3.3)
	4. [Bibliothek](#3.4)
4. [**Demokratie an der Uni**](#4)
	1. [Verfasste Student_innenschaft](#4.1)
	2. [StuRa](#4.2)
	3. [Dein Fachschaftsrat](#4.3)
	4. [Fakultätsrat](#4.4)
5. [**Sonstiges**](#5)
	1. [Geld haben und verdienen](#5.1)
	2. [Skills](#5.2)
	3. [Für Nicht-Deutsch-MuttersprachlerInnen](#5.3)
	4. [Praktika und Auslandserfahrungen](#5.4)
	5. [Freizeit](#5.5)

## 1. FAQ <a name="FAQ"></a>

## Inhaltsverzeichnis
1. [Allgemeines](#faqAllgemein)  
2. [FSR](#faqFSR)  
    2.1 [Was ist der FSR?](#faq2.1)  
    2.2 [Ich habe einen Vorschlag für eine Veranstaltung](#faq2.2)  
	2.3 [Wie kann ich bei euch mitmachen?](#faq2.3)  
	2.4 [Warum habt ihr kein Foto von euch auf eurer Seite?](#faq2.4)  
3. [GitLab](#faqGitLab)  
	3.1 [Was ist Git?](#faq3.1)  
	3.2 [Wie bekomme ich Zugang zum GitLab?](#faq3.2)  
	3.3 [Ich habe meinen Zugang vergessen](#faq3.3)  
4. [Studium](#faqStudium)  
	4.1 [Was ist der Unterschied zwischen Digital Humanities und Informatik?](#faq4.1)  
	4.2 [Wie finde ich die Seiten der Abteilungen?](#faq4.4)  
	4.3 [Kann ich Modul XY als Ergänzungsfach belegen?](#faq4.5)  

## 1. Allgemeines <a name="faqAllgemein"></a>
Dieses FAQ soll ich Fragen zum Studium beantworten. Weitere Informationen gibt es noch im [FAQ der Fakultät](http://studium.fmi.uni-leipzig.de/faq/)
## 2. FSR <a name="faqFSR"></a>
### 2.1 Was ist der FSR? <a name="faq2.1"></a>
Der Fachschaftsrat (FSR) Informatik ist die gesetzlich vorgesehene Interessenvertretung der Informatik-Studis an der Uni Leipzig auf Instituts- und Fakultätsebene. Gewählt wird jährlich, im Mai-Juni. Die Amtszeit dauert von Anfang Oktober des einen bis Ende September des nächsten Jahres. Ihr könnt euch natürlich auch einbringen, ohne gewählt zu sein: Kommt einfach zu einer Sitzung vorbei.

Der Fachschaftsrat entsendet Vertreter_innen in mehrere Gremien, darunter in das Plenum des StudentInnenRates, in den Prüfungsausschuss, in die Studienkommission und in die Berufungskommissionen. Mehr Informationen gibt es auf der Service-Seite.

### 2.2 Ich habe einen Vorschlag für eine Veranstaltung <a name="faq2.2"></a>
Falls du eine Idee oder einen Wunsch für eine Veranstaltung hast, dann komm doch einfach zu einer Sitzung vorbei. Die Termine stehen im Kalender. Alternativ kannst du uns auch eine Mail schreiben.

### 2.3 Wie kann ich bei euch mitmachen? <a name="faq2.3"></a>
Komm einfach zu einer Sitzung vorbei. Es gibt viele Dinge zu erledigen und jeder kann sich einbringen.

### 2.4 Warum habt ihr kein Foto von euch auf eurer Seite? <a name="faq2.4"></a>
Uns sind Persönlichkeitsrechte sehr wichtig. (bitte ausführen)

## 3. GitLab <a name="faqGitLab"></a>

### 3.1 Was ist Git? <a name="faq3.1"></a>
Git ist ein Versionskontrollsystem. Mehr Informationen findest du in unserer [Git-Anleitung]( {{"/images/static/Gitlab-Anleitung.pdf" | relative_url }}).

### 3.2 Wie bekomme ich Zugang zum GitLab? <a name="faq3.2"></a>
Einloggen können sich Informatik-Studierende und -Mitarbeiter_innen mit ihrem LDAP-Login der Informatik (Das ist der, der auch in den Linux-Pools gilt.) Accounts vergibt [Fabian Schmidt](https://www.informatik.uni-leipzig.de/ifi/oeffentlichkeit/zd/mitarbeiter-zd/).

### 3.3 Ich habe meinen Zugang vergessen <a name="faq3.3"></a>
Bei Problemen mit deinem Account kannst du dich einfach bei [Fabian Schmidt](https://www.informatik.uni-leipzig.de/ifi/oeffentlichkeit/zd/mitarbeiter-zd/) melden.

## 4. Studium <a name="faqStudium"></a>

### 4.1 Ein Modul ist voll, aber ich muss es belegen - was soll ich machen? <a name="faq4.2"></a>
Sollte es sich um ein Pflichtmodul halten, so wird in den ersten Vorlesungen dafür gesorgt, dass genug Plätze existieren. Bei allen anderen Modulen bietet es sich an mit der Dozierenden Person zu reden. Manchmal werden Plätze auch noch aufgestockt oder frei.

### 4.2 Wie finde ich die Seiten der Abteilungen? <a name="faq4.4"></a>
Hier eine Auflistung
* [Algebraische und Logische Grundlagen der Informatik](http://www.informatik.uni-leipzig.de/alg/)
* [Automaten und Sprachen](http://www.informatik.uni-leipzig.de/theo/theo.html)
* [Automatische Sprachverarbeitung](http://asv.informatik.uni-leipzig.de/de/)
* [Betriebliche Informationssysteme](http://bis.informatik.uni-leipzig.de/de/Aktuelles)
* [Bild- und Signalverarbeitung](https://www.informatik.uni-leipzig.de/bsv/homepage/de)
* [Bioinformatik](http://www.bioinf.uni-leipzig.de/)
* [Computional Humanities](https://ch.uni-leipzig.de/)
* [Datenbanken](https://dbs.uni-leipzig.de/de)
* [Didaktik der Informatik](https://www.informatik.uni-leipzig.de/ddi/)
* [Digital Humanities](http://www.dh.uni-leipzig.de/wo/)
* [Intelligente Systeme](https://www.informatik.uni-leipzig.de/ifi/professuren/theoretischeinf/intelligente-systeme/home/)
* [Medizinische Informatik](https://www.imise.uni-leipzig.de/)
* [Rechnernetze und Verteilte Systeme](https://rvs.informatik.uni-leipzig.de/de/startseite/)
* [Schwarmintelligenz und Komplexe Systeme](http://pacosy.informatik.uni-leipzig.de/)
* [Softwaresysteme](https://sosy.informatik.uni-leipzig.de/)
* [Technische Informatik](https://ti.informatik.uni-leipzig.de/)
* [Text Mining & Retrieval](www.temir.org)
* [Versicherungsinformatik](http://home.uni-leipzig.de/versicherungsinformatik/lehrstuhl_vi/studium/richt_hausarb.shtml)

### 4.3 Kann ich das Modul XY als Ergänzungsfach belegen? <a name="faq4.5"></a>
Du kannst im Bachelor und Master Informatik als Ergänzungsfach jedes beliebige Module aus allen Studiengängen der Universität Leipzig wählen. Allerdings haben einige Fakultäten zu Beginn des Semesters besondere Einschreibemodalitäten für die Teilnahme an ihren Veranstaltungen. Darum musst du dich selbst kümmern. Es kann auch sein, dass du für ein fakultäts externes Modul andersweitige Vorleistungen erbracht haben musst z. B. (Sportmodule). Einige Module z. B. Psychologie erlauben zwar an sich die Einschreibung, wenn Plätze frei sind, aber dies ist meistens nicht der Fall.
Am besten daher früh mit dem Studienbüro/Prüfungsamt der jeweiligen Fakultät kommunizieren.


## 1. Dein Studium <a name="Studium"></a>
### 1.1 Universität <a name="1.1"></a>

Die Universität Leipzig besteht aus 14 Fakultäten, die wiederum in Institute untergliedert sind. Falls du Bachelor Informatik, Master Informatik, Bachelor Digital Humanities, Master Digital Humanities, Master Bioinformatik, Lehramt Informatik oder noch Magister/Diplom Informatik studierst, dann ist dein Institut das Institut für Informatik, welches Teil der Fakultät für Mathematik und Informatik ist.

### 1.2 Räumlichkeiten der Informatik <a name="1.2"></a>

Das Institut für Informatik befindet sich auf dem Campus Augustusplatz. Die meisten Räume findest du im vierten und fünften Obergeschoss des Augusteums sowie im Paulinum. Der Felix-Klein-Hörsaal hat die Raumnummer P-501 und befindet sich im 5. Obergeschoss des Paulinums, also über der Kirche. Vorlesungen, Seminare und Übungen finden meist im Hörsaalgebäude und im Seminargebäude auf dem Campus Augustusplatz statt.

### 1.3 Studiengänge an unserem Institut  <a name="1.3"></a>
Das Institut für Informatik beherbergt mehrere Studiengänge:

* Bachelor Informatik
* Bachelor Digital Humanities
* Master Informatik
* Master Digital Humanities
* Master Bioinformatik
* Lehramt Informatik

Informationen und Beschreibungen zu den Studiengängen findest du auf der [Webseite des Instituts](http://studium.fmi.uni-leipzig.de/studium/)

### 1.4 Regelstudienzeit  <a name="1.4"></a>

Die Regelstudienzeit ist die Zeit, die du für dein Studium brauchst, wenn du gemäß dem empfohlenen Studienablaufplan studierst. Das bedeutet, dass du Module in einem Umfang von 30 Leistungspunkten (kurz: LP, auch: credit points oder ECTS-Punkte) pro Semester absolvierst.

Ab einer Überschreitung der Regelstudienzeit um 4 Semester werden Langzeitstudiengebühren fällig. Näheres erfährst du [hier](https://www.uni-leipzig.de/studium/studienorganisation/studiengebuehren/langzeitstudiengebuehren.html).

Bei besonderen Umständen - Teilzeitstudium, Studieren mit Kind, langer Krankheit und anderen besonderen sozialen Lagen - sind Ausnahmeregelungen vorgesehen. Wende dich an den [StuRa](https://stura.uni-leipzig.de) oder das [StudentInnensekretariat](https://www.studentenwerk-leipzig.de/beratung-soziales/studenten-service-zentrum).

### 1.5 Module und Workload  <a name="1.5"></a>

Bachelor- und Masterstudiengänge bestehen aus Modulen. Die Module umfassen 5 oder 10 Leistungspunkte, wobei ein Leistungspunkt einer (sehr hoch angesetzten und in der Praxis kaum machbaren) Arbeitszeit von 30 Stunden entsprechen soll. Module bestehen gewöhnlich aus einer oder mehreren Vorlesungen, Seminaren, Übungen und/oder Praktika und enden mit einer oder mehreren schriftlichen oder mündlichen Prüfungen. Auch die Abschlussarbeiten (Bachelorarbeit und Masterarbeit) werden als Module aufgeführt.

Mit Ausnahme des Masters Informatik haben alle Studiengänge unseres Instituts **Pflichtmodule**. Du kannst die Module auch in einer anderen Reihenfolge als im Studienverlaufsplan absolvieren, allerdings bauen manche Module aufeinander auf. Früher oder später musst du für deinen Abschluss alle erforderlichen Pflichtmodule bestanden haben.

Neben den Pflichtmodulen gibt es **Wahlpflichtmodule**. Du musst in einem bestimmten Umfang **Kernmodule** (jeweils 5 LP), **Vertiefungsmodule** (jeweils 10 LP) und **Schlüsselqualifikationsmodule** (5 oder 10 LP) absolvieren. Informationen über alle Module findest du [in den Modulbeschreibungen, im Studienverlaufsplan und im Studienablaufplan](https://www.informatik.uni-leipzig.de/ifi/studium/studiengnge.html).

Für Schlüsselqualifikations-Module (SQ) gilt folgendes:

* Im Bachelor müssen 10 LP in [fakultätsexternen SQ-Modulen](https://www.uni-leipzig.de/studium/studienorganisation/moduleinschreibung/sq-bereich.html) absolviert werden. Dazu findet eine Onlineeinschreibung statt (siehe Abschnitt "Einschreibung"). Es werden Module aus verschiedenen Fachrichtungen angeboten. Du kannst entweder ein 10-LP-Modul oder zwei 5-LP-Module belegen.
* Im Master muss ein fakultäts_internes_ SQ-Modul gewählt werden. Dies kann jedes beliebige Modul aus der Mathematik, Informatik oder medizinischen Informatik sein, dass du noch nicht belegt hast. Auch hier können entweder ein 10-LP-Modul oder zwei 5-LP-Module absolviert werden.

### 1.6 Ergänzungsfach <a name="1.6"></a>

Im Master und Bachelor Informatik musst du Module mit insgesamt 20 LP als Ergänzungsfach belegen. Dafür kannst du beliebige Module aus allen Studiengängen der Universität Leipzig wählen.

Einige Fakultäten haben zu Beginn des Semesters besondere Einschreibemodalitäten für die Teilnahme an ihren Veranstaltungen. Darum musst du dich selbst kümmern.

Es gibt auch empfohlene Ergänzungsfach-Kombinationen, darunter [Medizininformatik](https://www.imise.uni-leipzig.de/Lehre/MedInf/index.jsp) und Biologie. In diesem Fall kannst du vier bestimmte Module à 5 LP aus dem Bereich medizinische Informatik bzw. Biologie belegen.

### 1.7 Studien- und Prüfungsordnung <a name="1.7"></a>

Gesetzliche Grundlage deines Studiums und daher lesenswert sind die jeweilige Studienordnung (kurz SO) und Prüfungsordnung (kurz PO). Diese Dokumente findest du [auf der Website der Universität](https://www.uni-leipzig.de/universitaet/profil/entwicklungen/amtliche-bekanntmachungen.html?kat_id=125) und auch auf der [Webseite des Instituts](https://www.informatik.uni-leipzig.de/ifi/studium/studiengnge.html).

Während die Prüfungsordnung festlegt, welche Leistungen (in Form von Prüfungen) du erbringen musst, regelt die Studienordnung Inhalte und Aufbau des Studiums.

Diese Ordnungen sind verpflichtend für beide Seiten. Das bedeutet, dass du als Student_in verpflichtet bist, gemäß SO/PO zu studieren, wenn du deinen Abschluss machen möchtest. Das bedeutet aber auch, dass keine zusätzlichen, in den Ordnungen nicht aufgeführten Prüfungen oder Leistungen von dir verlangt werden dürfen. Insbesondere gibt es an der Universität Leipzig grundsätzlich keine Anwesenheitspflicht, nur in sehr speziellen Fällen wie bspw. Laborpraktika. Sollte dennoch ein_e Professor_in oder eine Lehrkraft zu deinem Nachteil von der PO/SO abweichen, sprich ihn_sie darauf an, sprich mit dem Studienbüro und/oder wende dich an uns.

### 1.8 Studienbüro <a name="1.8"></a>

Kompetente und verbindliche Antworten in Studien- und Prüfungsbelangen erhälst du bei Herrn Neumann und Frau Güttler im [Studienbüro der Fakultät](http://studium.fmi.uni-leipzig.de), ehemals Prüfungsamt. Dort landen Bescheinigungen über all deine bestandenen (und nicht bestandenen) Prüfungen – meist automatisch, aber bei Prüfungen an anderen Instituten empfehlen wir, nochmal nachzufragen.

### 1.9 Moduleinschreibung <a name="1.9"></a>

Zum WS15/16 erfolgte die Umstellung auf  [Almaweb](https://almaweb.uni-leipzig.de). Dort erfolgt die Einschreibung für die meisten Module. Hinweise zur Moduleinschreibung findest du zu Beginn jeden Semesters [hier](http://studium.fmi.uni-leipzig.de/moduleinschreibung.html).
Zu praktisch allen Modulen gibt es Informationen auf der Webseite der zuständigen Abteilung und im Modulhandbuch deines Studiengangs.

Die Modulanmeldung ist zugleich auch die Anmeldung zur Prüfung. Genauere Infos und Anworten auf deine Fagen bekommst du beim [Studienbüro](http://studium.fmi.uni-leipzig.de/) oder beim jeweiligen Dozenten.

Bitte informiert euch ein paar Wochen vor Semesterbeginn über eventuelle Anmeldungen – beispielsweise zu Praktika oder Prüfungsvorleistungen während des Semesters. Manchmal werden euch solche Informationen auch bei den ersten Terminen mitgeteilt.

Die Einschreibung in fakultätsexterne Schlüsselqualifikationen (SQ-Module) geschieht via Losverfahren über das [TOOL](https://tool.uni-leipzig.de).

Zur Einschreibung in Ergänzungsfach-Module informiert euch bitte an den jeweiligen Instituten. Insbesondere in den Geistes- und Sozialwissenschaften gibt es feste Einschreibezeiträume _vor_ Beginn der Vorlesungszeit. Hier erfolgt die Einschreibung über das [TOOL](https://tool.uni-leipzig.de). (Stand Oktober 2018)

### 1.10 Prüfungsvorleistungen <a name="1.10"></a>

In einigen Modulen gibt es Prüfungsvorleistungen, die du bestehen musst, um zur Prüfung zugelassen zu werden.

Informationen zu den Modulen und dem Übungsmaterial findest du auf der vom Dozenten genutzten Plattform. In der ersten Veranstaltung wird dies mitgeteilt. In der Regel liegt das Veranstaltungsmaterial im Almaweb, Moodle, OLAT oder auf der Abteilungswebseite. (Stand Oktober 2018).

Die häufigsten Prüfungsvorleistungen sind:

* Zwischenklausuren (Vorklausuren),
* Übungsscheine (meist regelmäßige Übungsaufgaben, bei denen ihr oft mindestens 50% der Punkte erreichen müsst),
* Testate (z.B. bei Praktika) und
* Vorträge/Präsentationen mit oder ohne schriftliche Ausarbeitung.

Ob Module Prüfungsvorleistungen haben (und welche), steht in der [Prüfungsordnung](https://www.informatik.uni-leipzig.de/ifi/studium/studiengnge.html). Du erfährst es auch beim ersten Termin des Moduls.

### 1.11 Prüfungsan- und abmeldung <a name="1.11"></a>

Die Prüfungsanmeldung erfolgt in Informatik-Modulen automatisch mit der Moduleinschreibung im Almaweb. Eine Abmeldung kann im Almaweb bis zu einer bestimmten Frist (wird vom Studienbüro veröffentlicht und steht im Almaweb) erfolgen. Bis 3 Werktage vor einer Prüfung ist eine Abmeldung beim Studienbüro möglich. Allerdings bedarf es hier trifftiger Gründe, welche in einem formlosen Antrag dargelegt werden müssen.

Prüfungstermine werden in der Regel über das Almaweb und über den Modulverantwortlichen in der Veranstaltung mitgeteilt. Mündliche Prüfungstermine erfolgen nach Absprache, meist in einem vom Modulverantwortlichen festgelegten Prüfungszeitraum.

Sobald du zur Prüfung andemeldet bist, musst du auch dort erscheinen. Ein Nichterscheinen hat Note 5,0 (nicht bestanden) zur Folge. Im Falle einer Krankheit muss spätestens am 3. Werktag ein ärztliches Attest, zum Beispiel eine gelbe Arbeitsunfähigkeitsbescheinigung, dem Studienbüro vorliegen.

Beachte, dass du prinzipiell die Möglichkeit hast, dich rechtzeitig vorab beim Studienbüro von einer Prüfung oder Nachprüfung abzumelden und die Prüfung zu einem späteren Zeitpunkt abzulegen. Ratsam ist es jedoch, Prüfungen zum vorgesehenen Zeitpunkt abzulegen. Wer Prüfungen verschiebt, hat im nächsten Semester eine umso höhere Lernbelastung.

### 1.12 Prüfungen und Modulnote <a name="1.12"></a>

Die meisten Module schließen mit einer Note ab, bei wenigen ist lediglich ein Bestehen erforderlich. Prüfungen werden oft schriftlich, manchmal auch mündlich abgelegt, bei Seminaren meist in Form eines Vortrags und einer Ausarbeitung.

### 1.13 Prüfungsergebnisse und Nachprüfungen <a name="1.13"></a>

Die Prüfungsergebnisse werden gewöhnlich auf der jeweiligen Webseite der Module oder (seltener) auf Aushängen im Institut veröffentlicht. Bestehst du eine Prüfung nicht, so bist du zumeist automatisch als "Wiederholer_in" zum nächsten Nachprüfungstermin angemeldet. Falls du auch die Wiederholungsprüfung nicht bestehst, wird es ungemütlich - dann solltest du innerhalb von einer Woche einen Antrag auf eine zweite Wiederholungsprüfung beim zuständigen Prüfungsamt/Studienbüro abgeben. Der entsprechende Prüfungsausschuss entscheidet über dein Anliegen. Bestehst du auch die zweite Wiederholungsprüfung nicht, gilt das Modul als endgültig nicht bestanden. Falls es sich um ein Wahlmodul handelt, kannst du stattdessen ein anderes Modul belegen und das Nichtbestehen ausgleichen. Handelt es sich jedoch um ein Pflichtmodul, wirst du aus dem Studiengang exmatrikuliert.

### 1.14 Studienorganisation <a name="1.14"></a>

Alle wichtigen Infos zum Thema Studienorganisation (Rückmeldung, Studienbescheinigungen, Beurlaubung, Doppelstudium etc.) erhaltet ihr auf der Website des [Studentensekretariates](https://www.uni-leipzig.de/studium/studienorganisation.html).

### 1.15 Anwesenheitspflicht? <a name="1.15"></a>

Anders als in der Schule gibt es in Sachsen **keine** grundsätzliche Anwesenheitspflicht. Ausnahmen bilden beispielsweise Laborpraktika. Ausschlaggebend ist allein, dass du den verlangten Prüfungsstoff sicher beherrschst und die Prüfungsleistung(en) bestehst. Wie du dir den Lernstoff aneignest, ist dir theoretisch selbst überlassen. Anwesenheitslisten oder "Sitzscheine" sind deshalb unzulässig. Es darf niemand wegen Abwesenheit benachteiligt werden.

Auch wenn keine Pflicht zur Anwesenheit besteht, ist sie oft zum besseren Verständnis der Veranstaltungsinhalte ratsam. Viele Profs geben außerdem in den Vorlesungen nützliche Hinweise zu Prüfungsschwerpunkten.

Weitere Informationen zur Rechtslage gibt's [beim StuRa](https://www.stura.uni-leipzig.de).

### 1.16 Hilfe im Studium <a name="1.16"></a>

* Studentische Angelegenheiten und Fragen: [FSR Informatik](https://fsinf.informatik.uni-leipzig.de/pages/impressum/) und [StuRa](https://stura.uni-leipzig.de/)
<!-- StuRa- und StuWe-Beratungskram verlinken -->
* [Studieren mit Kind - Beratung des Studentenwerks](https://www.studentenwerk-leipzig.de/beratung/studieren-mit-kind)
* [Sozialberatung des Studentenwerks](https://www.studentenwerk-leipzig.de/beratung/sozialberatung)
* [Psychosoziale Beratung des Studentenwerks](https://www.studentenwerk-leipzig.de/beratung/psychosoziale-beratung)
* [Psychologische Beratungsstelle des Zentrums für Lehrerbildung und Schulforschung](http://www.zls.uni-leipzig.de/psychologischeberatungsstelle.html)
* [Studentisches Sorgentelefon Nightline](http://leipzig.nightlines.eu/)
<!--Überschrift AntidiskriminierungAntidiskriminierung, evtl Link auf antisexismusbroschuere-->
* [Fördermöglichkeiten speziell für Studenten mit Behinderung](http://www.barrierefrei-studieren.de/)

## 2. Technik <a name="2"></a>
### 2.1 GitLab <a name="2.1"></a>
Hier findest du eine [Anleitung]({{"/images/static/Gitlab-Anleitung.pdf" | relative_url}}) für das GitLab des FSR Informatik.

#### 2.1.1 Erreichbarkeit
Das Git ist über deinen Browser auf https://git.fsinf.informatik.uni-leipzig.de/ erreichbar.
Einloggen kannst du dich mit dem selben Account, mit dem du dich in den PC-Pools anmeldest. Nähere Informationen zum Informatik-Pool Account findest du unter [fakultätsinterne Arbeitsplätze](#3.3).

#### 2.1.2 Struktur
Alle Module haben ein eigenes Repository.
Wenn ein Modul fehlt, bei dem du gerne Material hinzufügen möchtest, dann kannst du auch diesbezüglich eine [Mail](mailto:{{ site.email }}) schreiben.
Sämtliches Material wird nach Semester geordnet. Die Ordner der einzelnen Semester müssen “WiSeXX” oder “SoSeXX” heißen, wobei “XX” durch die entsprechende Jahreszahl, des Jahres, ersetzt werden muss, in dem das Semester angefangen hat.
Als Unterordner kann es die Ordner “Uebungen”, “Skript” und “Klausuren" geben. Diese Bezeichnungen sprechen für sich selbst.

### 2.2 WLAN <a name="2.2"></a>

Die lange Anleitung zur WLAN-Einrichtung findet ihr auf den Seiten des [Universitätsrechenzentrums](https://www.urz.uni-leipzig.de/dienste/netze-zugang/wlan/), einschließlich [ausführlicher Anleitungen](https://www.urz.uni-leipzig.de/hilfe/anleitungen-a-z/wlan-einrichtung/) zu vielen Betriebssystemen.
Kurz gefasst: Access Point eduroam, DHCP, WPA & WPA2 Enterprise, TTLS, PAP, anonymous, mai12xyz@studserv.uni-leipzig.de + euer Studserv-Passwort und ihr braucht [dieses Zertifikat](https://pki.pca.dfn.de/unilei-ca/pub/cacert/rootcert.crt).

Du nutzt Linux und keinen Network Manager und willst weiterhin netctl verwenden, um dich mit eduroam zu verbinden?

Das Profil unten läuft, wenn man die groß geschriebenen Konstanten INTERFACE, USER und PASSWORD entsprechend ändert.
Das T-Telesec GlobalRoot Class 2 Zertifikat sollte standardmäßig schon vorinstalliert sein.


Description='A wireless connection to the eduroam network provided by
the Leipzig University.'
Interface=INTERFACE
Connection=wireless
Security='wpa-configsection'
IP=dhcp
ESSID=eduroam
WPAConfigSection=(
    'ssid="eduroam"'
    'key_mgmt=WPA-EAP'
    'eap=TTLS'
    'phase2="auth=PAP"'
    'ca_cert="/etc/ssl/certs/T-TeleSec_GlobalRoot_Class_2.pem"'
    'altsubject_match="DNS:radius.uni-leipzig.de"'
    'identity="USER@uni-leipzig.de"'
    'anonymous_identity="eduroam@uni-leipzig.de"'
    'password="PASSWORD"'
)

### 2.3 Uni E-Mail / studserv <a name="2.3"></a>

Die Uni Leipzig stellt euch für die Zeit eures Studiums eine eigene Mailadresse mit 300 MB Speicher zur Verfügung. Die eMail-Adresse lautet mai12xyz@studserv.uni-leipzig.de.
Auch wenn ihr diese Adresse nicht aktiv nutzt, werden dorthin z.B. offizielle Nachrichten der Universitätsverwaltung, der Unibibliothek oder des StuRa geschickt.

Die Webmail-Oberfläche befindet sich unter [mail.uni-leipzig.de](https://mail.uni-leipzig.de/portal/imp/login.php?server=studserv). Die Login-Daten sind die üblichen (mai12xyz + Passwort). Dort habt ihr die Möglichkeit, euch einen personalisierten Mail-Alias (z.B. Vorname.Nachname@studserv.uni-leipzig.de) einzurichten. Außerdem kann eine Weiterleitung an eine andere eMail-Adresse konfiguriert werden.

Es ist auch möglich sich einen Alias einzurichten. zB mustermensch@studserv.uni-leipzig.de .

Hinweise zur Nutzung des studserv via POP, IMAP und SMTP gibt es [ebenfalls beim URZ](https://www.urz.uni-leipzig.de/studserv.html).

### 2.4 Hardwarekauf und Lizenzen <a name="2.4"></a>
Uns erreichen regelmäßig Anfragen zum Hardwarekauf und zu Lizenzerwerb. Wir können dazu ein paar Ratschläge geben.

#### Gebrauchte Hardware
Gebrauchte Hardware zu kaufen ist nicht nur umweltbewusst, sondern genügt in den meisten Fällen den Ansprüchen fürs Studium bei weitem. Dass bei Privatkäufen ein gewisses Risiko besteht, ist verständlich. Für alle, die auf Nummer sicher gehen wollen, gibt es zahlreiche seriöse Anbieter im Internet, die generalüberholte Geräte inkl. einjähriger Garantie vertreiben.

#### Neue Rechner und Hardware
Solltest Du dennoch einen neuen Rechner oder Hardware für Dein Studium anschaffen, informierst Du dich am besten über Sonderangebote für Studierende. Die meisten Hersteller haben spezielle Campusprogramme und stellen oftmals sogar Sondereditionen und -baureihen her. In jedem Fall lohnt es sich, beim Erwerb auf eine Betriebssystemslizenz und zusätzliche Software ab Werk zu verzichten. Näheres erfährst Du dazu im nächsten Absatz.

#### Betriebssystem und Lizenzen
Wir empfehlen Dir, Dich während des Studiums mit freien Betriebssystemen, zum Beispiel [Linux-Derivaten](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen) , auseinander zu setzen und diese zu benutzen.

Solltest Du dennoch Microsoft-Lizenzen, zum Beispiel Windows 10, benötigen, kannst du diese kostenfrei über das [Microsoft-Imagine Programm](https://e5.onthehub.com/WebStore/Welcome.aspx?ws=dadbecac-699b-e011-969d-0030487d8897&vsro=8)  erhalten. Wende dich dazu bitte an die Zentralen Dienste im 5. Stock im Augusteum, da diese einen Zugang für dich beantragen können. Wichtig ist, dass eine kommerzielle Nutzung der Software unzulässig ist.

Das Office-Paket erhälst du über [Campus Sachsen](https://campussachsen.tu-dresden.de/) . Du musst dich lediglich alle 180 Tage dort einloggen, damit deine Lizenz nicht verfällt.

#### Sonstiges
Empfehlenswert sind Businessmodelle, da diese in der Regel eine bessere Qualität und manchmal auch eine längere Garantie aufweisen.

Anschaffungen fürs Studium sind unter Umständen steuerlich absetzbar.

## 3. Arbeitsplätze an der Universität <a name="3"></a>

Als Studierende der Fakultät für Mathematik und Informatik habt ihr das Glück am sehr schönen Hauptcampus zu studieren. Viele Arbeitsplätze sorgen für eine angenehme Arbeitsatmosphäre.

### 3.1 Computerarbeitsplätze und Drucker des Universitätsrechenzentrums (URZ) im Augusteum <a name="3.1"></a>

Sicher hast du die offenen Arbeitsplätze im Erdgeschoss des Augusteums schon gesehen.
Es gibt neben diesen noch weitere [Computerarbeitsplätze des URZ](https://www.urz.uni-leipzig.de/dienste/pc-arbeitsplaetze/) im 2. Stock, welche ausschließlich von den Aufzügen auf der Seite des Paulinums aus erreichbar sind. Dort stehen nicht nur Windows-Rechner, sondern auch Macs.
Das URZ betreibt dort auch Drucker.
Es gibt die Möglichkeit zum (preiswerten) Schwarzweiß- oder  Farb(laser)druck, und außerdem zum (kostenlosen) Einscannen von Schriftdokumenten am Dokumentenscanner.
Das Druckguthaben befindet sich auf einem eigenen Druckerkonto auf eurer Unicard. Um Geld auf dieses Konto zu buchen, geht ihr zu einem entsprechenden Umbuchungsautomaten (z.B. im 2. Stock) und bucht dort Geld von eurem Mensakonto auf euer Druckerkonto.
Weitere Infos zu den Druckpools des URZ sowie Preislisten und Hinweise zur Bezahlung findet ihr bei den [Hinweisen zum Drucken](https://www.urz.uni-leipzig.de/dienste/pc-arbeitsplaetze/drucken-im-pc-pool/) des URZ.


### 3.2 Kopieren <a name="3.2"></a>

Die Firma Canon betreibt unter dem Namen "Studentenkopierdienst" einige Kopiergeräte an der Uni. Scannen und Drucken könnt ihr dort auch, falls nicht gerade ein Papierstau das Gerät blockiert.

Die Kopierbörse ist ein weiteres eigenes Konto auf eurer Unicard. Es kann an  entsprechenden Automaten, zum Beispiel in der Campus-Bibiliothek, mit Bargeld aufgeladen werden. Weitere Standorte der Automaten findet ihr [hier](https://www.uni-leipzig.de/studium/studienorganisation/unicard.html#c33694).
Die Kopierer stehen an vielen zentralen Stellen in der Uni. Eine Liste aller Kopierer-Standorte ist [auf diesem Flyer des "Studentenkopierdienstes"](https://www.ub.uni-leipzig.de/fileadmin/Resources/Public/Docs/Upload_Service/canon_kopierer_flyer.pdf) zu finden.



### 3.3 Fakultätsinterne Arbeitsräume und Drucker im Augusteum <a name="3.3"></a>

Die Fakultät für Mathematik und Informatik bietet dir verschiedene Arbeitsplätze.
Zugang erhälst du über deine Unicard, welche du zunächst bei bei Hr. Heusel (A-527) freischalten lassen musst. Ebenso erhälst du ein gesondertes Benutzerkonto (Almaweb-Benutzername mit anderem Passwort) für die PCs.

- A-429: Gruppenarbeitsraum mit Tafel

- A-410, A-412, A-414 und P-401: PC-Pools des Instituts für Informatik.
Ausstattung: Drucker, Beamer und Whiteboard. Je 1GB Speicher für jeden Pool-Nutzer.
Nähere Infos zu den Rechnern findest du [hier](https://www.informatik.uni-leipzig.de/ifi/oeffentlichkeit/zd/cip-pools/pools.html ).
Das Drucken in diesen Pools erfolgt über ein gesondertes Druckkonto, welches bei uns aufgeladen werden kann. Vereinbart hierzu einfach einen Termin per Mail oder kommt in unsere Sprechzeit.
Kosten pro Druck: **1ct**. Papier ist selbst mitzubringen.


- A-538: Gruppenarbeitsraum mit ein paar Rechnern.


- A-310, A-312: PC-Pools des mathematischen Instituts. Ausstattung ähnlich wie Informatik- Pools. Unicard- Zugang erfolgt über Hr. Heusel (A-527) und PC-Login-Zugang über mathematisches Institut.



### 3.4 Bibliothek <a name="3.4"></a>

* [Website der Universitätsbibliothek (UB)](https://www.ub.uni-leipzig.de)
* [Die UB bei Twitter](https://twitter.com/#!/ubleipzig)
* [Standorte und Öffnungszeiten](https://www.ub.uni-leipzig.de/ubl/standorte.html)
* [Schulungen und Führungen](https://www.ub.uni-leipzig.de/service/schulungen-und-fuehrungen.html)
* [Online-Tutorials](https://www.ub.uni-leipzig.de/service/online-tutorials.html)

#### 3.4.1 Arbeitsplätze in der Universitätsbibliothek <a name="3.4.1"></a>

Die Bibliothek ist nicht nur ein Ort an dem Lehrbücher zu finden sind, deren Lektüre wir dir natürlich wärmstens ans Herz legen, sondern bietet auch verschiedene Arbeitsplätze.
Normalerweise findet man immer einen Platz, nur in der Prüfungszeit könnte es ein wenig knapp werden.

In der Campusbibliothek gibt es sowohl im ersten Ober- als auch Untergeschoss Gruppenarbeitsräume, die sich hervorragend dazu eignen gemeinsam zu arbeiten. Diese kannst du auf der [Raumbuchungsseite](https://www.ub.uni-leipzig.de/standorte/campus-bibliothek/raumbuchung/) der UB reservieren.
Außerdem hat die Campusbibliothek rund um die Uhr geöffnet. Ab 22 Uhr könnt Ihr diese über den Nachteingang mit eurem Student_innenausweis betreten.

#### 3.4.2 Benutzung der Bibliothek <a name="3.4.2"></a>
Nahezu alle Lehrbücher, die ihr jemals fürs Studium braucht, findet ihr in der Bibliothek. Als Bibliotheksausweis dient eure Unicard. Logindaten für das [UB-Nutzer_innenkonto](https://katalog.ub.uni-leipzig.de/vufind/MyResearch/Home) sind eure Nutzer_innennummer (auf der Rückseite der Unicard) und euer Geburtsdatum in der Form DDMMYYYY.

Der Informatik-Freihandbereich befindet sich im Untergeschoss der Campusbibliothek. Vereinzelt sind Informatik-Bücher auch in anderen Zweigstellen (Physik, Medizin, Albertina, usw.) zu finden.

Startpunkt einer Büchersuche ist der [Katalog](https://katalog.ub.uni-leipzig.de) der Bibliothek.
Die Nutzung gestaltet sich denkbar einfach:

* Den Titel und/oder Autor_innen in die Suchleiste eintippen, Treffer werden angezeigt.
  * Die Farbmarkierung bedeutet: Grau = nicht verfügbar, Gelb = ausgeliehen (man kann sich vormerken lassen), Grün = verfügbar
* Klickt ihr auf einen Eintrag, seht ihr unter "Exemplare" den Standort und die Signatur des Buches, und ob es ausleihbar ist.
* In der entsprechenden Zweigstelle lässt sich das Buch anhand der Signatur leicht finden.
* Die Labels am Buchrücken haben folgende Bedeutung:
  * weißes Label = ausleihbar. Kann am Automaten oder beim Personal gegen Vorlage der Unicard ausgeliehen werden.
  * grünes oder grün-weißes Label = "Präsenz"-Bestand. Verbleibt in der UB und kann dort gelesen, aber nicht ausgeliehen werden.
* Lesen oder ggf. ausleihen. Die Ausleihzeit beträgt vier Wochen und kann online zwei Mal um jeweils maximal vier Wochen verlängert werden.
  * Tipp: Immer möglichst spät verlängern, denn die 4 Wochen zählen ab sofort und _nicht_ ab dem ursprünglichen Rückgabedatum.
* Die Rückgabe erfolgt wahlweise in der Zweigstelle, wo ihr das Buch entliehen habt, oder 24/7 in der Campusbibliothek an den Rückgabeautomaten.


## 4. Demokratie an der Uni <a name="4"></a>

Wurde Ende September 2012 mit den Stimmen von FDP, CDU und NPD im sächsischen Landtag faktisch abgeschafft. Seitdem können Studierende ab dem zweiten Semester aus der verfassten Studierendenschaft austreten. Wir empfehlen euch, dies nicht zu tun.

### 4.1 Die verfasste Student_innenschaft <a name="4.1"></a>

Nach dem sächsischen Hochschulgesetz bilden die Student_innen einer Hochschule die Student_innenschaft. Die Student_innenschaft hat das Recht auf Selbstverwaltung.

Die Student_innenschaft hat folgende Aufgaben:

* die Wahrnehmung der hochschulpolitischen, hochschulinternen, sozialen und kulturellen Belange der Student_innen,
* die Unterstützung der wirtschaftlichen und sozialen Selbsthilfe der Student_innen,
* die Förderung des freiwilligen Student_innensports, unbeschadet der Zuständigkeit der Hochschule,
* die Pflege der überregionalen und internationalen Student_innenbeziehungen,
* die Förderung der politischen Bildung und des staatsbürgerlichen Verantwortungsbewusstseins der Student_innen.

Organe der StudentInnenschaft sind der StudentInnenRat (StuRa) sowie die Fachschaftsräte. Der StuRa vertrtitt die Student_innen im Rahmen der oben genannten Aufgaben (siehe auch sächsisches Hochschulgesetz §§74-80).

### 4.2 StudentInnenRat (StuRa) <a name="4.2"></a>

Der StuRa ist eure zentrale Anlaufstelle bei Problemen, die nicht studiengangsspezifisch sind, insbesondere bei Fragen zur Hochschulpolitik, zum BAföG, zu umweltpolitischen und vielen weiteren Themen. Das zentrale Gremium des StuRa der Uni Leipzig ist das Plenum, das in der Regel alle zwei Wochen dienstags um 19:00 Uhr stattfindet.

Weitere Informationen gibts unter [stura.uni-leipzig.de](https://stura.uni-leipzig.de).

### 4.3 Dein Fachschaftsrat <a name="4.3"></a>

Der Fachschaftsrat (FSR) Informatik ist die gesetzlich vorgesehene Interessenvertretung der Informatik-Studis an der Uni Leipzig auf Instituts- und Fakultätsebene. Gewählt wird jährlich im Mai. Die Amtszeit dauert von Anfang Oktoberdes einen bis Ende September des nächsten Jahres. Ihr könnt euch natürlich auch einbringen, ohne gewählt zu sein: Kommt einfach zu einer Sitzung vorbei.

Der Fachschaftsrat entsendet Vertreter_innen in mehrere Gremien, darunter in das Plenum des StudentInnenRates, in den Prüfungsausschuss, in die Studienkommission und in die Berufungskommisionen.

#### 4.3.1 Prüfungsausschuss

Der Prüfungsausschuss ist ein Gremium auf Fakultätsebene, das heißt jede Fakultät hat einen Prüfungsausschuss. Er ist für alle zur Fakultät gehörenden Institute zuständig. Sein Aufgabenbereich umfasst alles, was mit Prüfungen und Abschlussarbeiten zu tun hat. Es ist zum Beispiel seine Aufgabe, über Anträge auf Fristverlängerung bei der Abschlussarbeit zu entscheiden, Einsprüche gegen die Benotung von Abschlussarbeiten zu prüfen, Rücktritte von Prüfungen zu gewähren bzw. abzulehnen, Prüfer_innenlisten zu erstellen und die Prüfungsstatistik zu führen, anhand derer er Vorschläge für Stipendien aussprechen kann. Außerdem befasst er sich mit Betrugsfällen und ist für die Einhaltung der Studien- und Prüfungsordnungen zuständig.

Der FSR empfiehlt dem Fakultätsrat ein Mitglied für den Prüfungsausschuss.

#### 4.3.2 Studienkommission

Der Fakultätsrat bestellt für jeden Studiengang eine Studienkommission, der paritätisch Lehrende der Fakultät und Student_innen angehören.

> Die Studienkommission erfüllt beratend Aufgaben, die für die sinnvolle
> Organisation und ordnungsgemäße Durchführung des Lehr- und
> Studienbetriebes und die Gewährleistung eines ordnungsgemäßen Studiums
> bedeutsam sind; insbesondere unterbreitet sie Vorschläge für die
> Studienordnung und den Studienablauf. Sie besitzt bezüglich ihrer
> Aufgaben Antragsrecht im Fakultätsrat (s.u.). Der Dekan und der
> Fakultätsrat stützen sich bei der Erfüllung ihrer Aufgaben auf die
> Studienkommission.
>
> Die Studienkommission erarbeitet als Beitrag zum Lehrbericht der
> Fakultät einen Jahresbericht über ihren Studiengang; in ihm sind die
> Befragungen zur Qualität der Lehrveranstaltungen darzustellen.
>
> Die Studienkommission soll im Zusammenwirken mit den studentischen
> Fachschaftsräten regelmäßig Befragungen der Student_innen zur Qualität
> der Lehrveranstaltungen durchführen. Der Lehrkörper muss vorher von
> den Befragungen unterrichtet werden. Die Befragungen erfolgen unter
> Beachtung von §106 Abs. 3 auf der Grundlage von Kriterien und mit
> Hilfe von Methoden, die von einer je zur Hälfte aus Lehrenden und
> Student_innen besetzten Arbeitsgruppe erarbeitet und vom Fakultätsrat
> oder vom Senat beschlossen werden. Die Ergebnisse der Befragungen sind
> den Teilnehmer_innen der Lehrveranstaltung bekannt zu geben.

(aus dem sächsischen Hochschulgesetz, §88)

Die Studienkommission Informatik besteht aus drei studentischen Mitgliedern und drei Lehrenden. Als einziges wirklich paritätisch besetztes Gremium ist die Studienkommission eine der Instanzen, bei der wir Studierenden das meiste Mitspracherecht haben.

#### 4.4 Fakultätsrat <a name="4.4"></a>

Der Fakultätsrat ist zuständig in allen Lehre, Forschung und Kunst betreffenden Angelegenheiten der Fakultät, für die nicht der Dekan oder die Leitung der den Fakultäten zugeordneten Institute und Betriebseinheiten einschließlich der veterinärmedizinischen Kliniken zuständig ist.

Der Fakultätsrat ist insbesondere zuständig für

* Vorschläge für Studien- und Prüfungsordnungen,
* Beschluss über die Promotions- und Habilitationsordnungen, die Studienordnung für das Graduiertenstudium und die Einsetzung von Promotions- und Habilitationskommissionen,
* Berufungsvorschläge,
* die Planung des Studienangebots, die Koordination der Studiengänge und die Sicherung des Lehrangebots gemäß § 11,
* den Beschluss über die jährlichen Lehr- und Forschungsberichte,
* die Gewährleistung der Studienfachberatung der Studierenden,
* Vorschläge zur Gründung, Änderung oder Auflösung von Instituten und veterinärmedizinischen Kliniken,
* die Organisation des Forschungsbetriebs, die Förderung und Abstimmung von Forschungsvorhaben, die Bildung von Forschungsschwerpunkten und die Förderung von künstlerischen Projekten,
* die Förderung der interdisziplinären Zusammenarbeit von Hochschullehrern in Lehre und Forschung,
* den Beschluss über den Plan für die strukturelle Entwicklung der Fakultät auf der Basis der Gesamtplanung des Rektoratskollegiums.

(aus dem sächsischen Hochschulgesetz, §85)

An der Fakultät für Mathematik und Informatik sitzen drei studentische Mitglieder im Fakultätsrat.

## 5 Sonstiges <a name="5"></a>
### 5.1 Geld haben und verdienen <a name="5.1"></a>
* **Wir haben eine [Mailingliste für Jobangebote](https://fsinf.informatik.uni-leipzig.de/mailman/listinfo/jobs)**
* Euren BAföG-Antrag nimmt das [Amt für Ausbildungsförderung des Studentenwerks](https://www.studentenwerk-leipzig.de/bafoeg) entgegen
* Der StuRa kann euch [Bafög-Beratung](https://stura.uni-leipzig.de/bafoeg-beratung) geben
* Wer "dem Grunde nach" keinen Anspruch auf BAföG hat, kann eventuell [Wohngeld](https://www.leipzig.de/buergerservice-und-verwaltung/aemter-und-behoerdengaenge/behoerden-und-dienstleistungen/dienststelle/wohngeld-504/) abgreifen
* Das Studentenwerk führt eine [Jobbörse](https://www.studentenwerk-leipzig.de/jobs)
* Auch die Uni führt ein [Jobportal](https://www.jobportal.uni-leipzig.de)
* An der Uni Leipzig stehen [Deutschlandstipendien](https://www.uni-leipzig.de/studium/angebot/foerderung/finanzierungsmoeglichkeiten/deutschlandstipendium.html) zur Verfügung
* Einen Überblick über zahlreiche weitere Stipendienprogramme bieten euch Webseiten wie [mystipendium.de](https://www.mystipendium.de)
* Informationen über ESF-Promotionsstipendien gibt's beim [PromovierendenRat](https://www.prorat.uni-leipzig.de)

### 5.2 Skills <a name="5.2"></a>

* Der [Career Service](https://www.uni-leipzig.de/studium/career-service.html) (ehemals Career Center) bietet einige interessante Angebote.
* Das [Schreibportal der Uni Leipzig](https://home.uni-leipzig.de/schreibportal/) hilft euch beim Verfassen guter Texte
* Das [Schreibzentrum Leipzig](http://schreibzentrum-leipzig.de) bietet gelegentlich Workshops für wissenschaftliches Schreiben an
* Die Uni bietet [Qualifizierungsworkshops](https://www.uni-leipzig.de/tutorenqualifizierung) für angehende Tutor/innen bzw. Übungsgruppenleiter/innen an
* Studierende können drei [Fahrradselbsthilfewerkstätten](https://www.studentenwerk-leipzig.de/service/mobil-leipzig#radfahrer) in Leipzig nutzen

### 5.3 Für Nicht-Deutsch-MuttersprachlerInnen <a name="5.3"></a>

* Das [Studienkolleg Sachsen](https://www.stksachs.uni-leipzig.de) bietet u.a. [studienbegleitenden Deutschunterricht](https://www.stksachs.uni-leipzig.de/studienbegleitung.html) an

### 5.4 Praktika und Auslandserfahrung <a name="5.4"></a>

* Weltweite Förderung von Semesteraufenthalten und Abschlussarbeiten [PROMOS](https://www.uni-leipzig.de/de/studium/auslandsaufenthalt/studium-im-ausland/finanzierung/promos-stipendium.html)
* Angebote für Praktika im Ausland findet ihr mitunter bei [AIESEC Leipzig](https://aiesec.de/leipzig/)
* Weltweite Forschungspraktika für Bachelorstudierende: [DAAD - Rise weltweit](https://ssl.daad.de/rise-weltweit/de/)
* Auslandsaufenthalte und Praktika sind auch über [Erasmus+](https://www.uni-leipzig.de/studium/auslandsaufenthalt/studium-im-ausland/europa/erasmus.html) möglich

### 5.5 Freizeit <a name="5.5"></a>
* Der Stura beherbergt zahlreiche [AGs](https://stura.uni-leipzig.de/finanzen)
* Am Zentrum für [Hochschulsport](https://hochschulsport.uni-leipzig.de/angebote/aktueller_zeitraum/index.html) und am Zentrum für [Gesundheitssport](https://www.gesundheit.uni-leipzig.de/) kann man Kurse belegen
