---
title: Sitzungsprotokolle
permalink: /protokolle/
lang: de
ref: protokolle
---

{% assign protokolle-desc = site.protokolle | sort: 'date' | reverse %}
{% for protokoll in protokolle-desc %}
* [Protokoll vom {{ protokoll.date | date: "%d.%m.%Y" }}]({{ protokoll.url | relative_url }}){% endfor %}
