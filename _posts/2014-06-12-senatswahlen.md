---
title: Wahlen des Fakultätsrats, Senats und Erweiterten Senats am 24. und 25. Juni 2014



---

Am 24. und 25. Juni werden die studentischen Mitglieder des Fakultätsrats der Fakultät für Mathematik und Informatik sowie des Senats und des Erweiterten Senats der Universität gewählt. Ihr wählt zwischen 9:00 und 16:00 Uhr im Neuen Augusteum, Raum A 520. Die [zugelassenen Wahlvorschläge](http://www.zv.uni-leipzig.de/fileadmin/user_upload/UniStadt/akademische_angelegenheiten/pdf/Wahl2014/Bekannt_ZWV.pdf) könnt ihr ebenso im Internet einsehen wie die offizielle [Wahlausschreibung](http://www.zv.uni-leipzig.de/fileadmin/user_upload/UniStadt/akademische_angelegenheiten/pdf/Wahl2014/Wausschreibung_140506.pdf).

Ergänzung: Auf der StuRa-Webseite stellen sich die Kandidierenden für den [Senat](http://stura.uni-leipzig.de/kandidatinnen-fuer-den-senat) und für den [Erweiterten Senat](http://stura.uni-leipzig.de/kandidatinnen-zum-erweiterten-senat) vor.
