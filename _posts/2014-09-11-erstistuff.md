---
title: Studienfahrt und Erstiveranstaltungen im Oktober



---

Im Oktober fahren wir wieder mit euch für ein Wochenende auf **Studienfahrt**. Eingeladen sich nicht nur Erstis, sondern alle Studierenden und Promovierenden der Informatik. Dieses Jahr geht's vom 17.-19. Oktober auf das Schloss Oberau bei Meißen. [ANMELDUNG](http://studienfahrt.informatiko.net/)

### Weitere Termine im Oktober 2014

* **ab 22. September**: [C-Propädeutikum](http://www.informatik.uni-leipzig.de/ifi/studium/propaedeutikum.html) für Bachelor-Erstsemester
* [Informationsveranstaltungen zu Studienbeginn](http://studium.fmi.uni-leipzig.de/fileadmin/Studienbuero/documents/InfoVeranstaltungen_WS14_FMI_04.pdf) im Felix-Klein-Hörsaal (P501, Paulinum), jeweils 15:00 Uhr
  * **7. Oktober**: Informationsveranstaltung Lehramt Informatik
  * **8. Oktober**: Informationsveranstaltung Bachelor Informatik
  * **9. Oktober**: Informationsveranstaltung Master Informatik und Master Bioinformatik
* **13. Oktober**: Vorlesungsbeginn
* **15. Oktober**: FSR-Kneipentour, Treffpunkt 16:45 Uhr nach der Vorlesung "Modellierung und Programmierung 1" im Hörsaal 9
* **17.-19. Oktober**: Studienfahrt [ANMELDUNG HIER](http://studienfahrt.informatiko.net/)

Um auf dem Laufenden zu bleiben, folgt uns bei [Twitter](https://twitter.com/fsinfLeipzig), liked uns bei [Facebook](https://www.facebook.com/FSRinformatikUniLeipzig) oder abonniert unseren [Newsletter](https://fsinf.informatik.uni-leipzig.de/mailman/listinfo/fsinf-newsletter).
