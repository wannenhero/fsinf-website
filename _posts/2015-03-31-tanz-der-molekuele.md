---
title: "22. April: Semesterauftaktparty der Naturwissenschaften"



---

Die Fachschaftsräte Biowissenschaften & Pharmazie, Chemie & Mineralogie, Informatik sowie Physik & Meteorologie laden ein zur Semesterauftaktparty am 22. April ab 22 Uhr in der [Distillery](http://www.distillery.de). DJ Ratzbaddz von der legendären [Vibes Ambassadors](http://www.vibesambassadors.com/index.php?r=artist/view&id=1) Crew wird Elektroswing und Balkanpop auflegen, DJ HabyKey wird den Main Floor bespielen. Wo ihr ab dem 13. April Vorverkaufsbändchen erwerben könnt, steht beim zugehörigen [Facebook-Event](https://www.facebook.com/events/711558735623758/).
