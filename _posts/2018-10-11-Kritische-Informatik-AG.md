---
title: "StuRa-AG: Kritische Informatik"



---
Momentan befindet sich die AG "Kritische Informatik" als Initiative von einigen Informatik-Studierenden im Gründungsprozess. Interessierte Studierende sind herzlich zu einer ersten Vorstellung im Rahmen der [Kritischen Einführungswochen](https://kew-leipzig.de/) am  **24.10 um 15:30 in S17 der WiWi-Fakultät** eingeladen. 

Im Zuge dessen hat uns ein Informationstext erreicht, welchen wir gerne an euch weiter leiten:

>	Als Hochschulgruppe Kritische Informatik wollen wir eine Anlaufstelle für all diejenigen sein, die die Inhalte des Studiums nicht nur als Karrieremittel sehen, sondern als Gegenstand von politischer und gesellschaftlicher Relevanz. So soll Technik nicht nur als Mittel zum Profit, sondern humanistisch und pro-demokratisch eingesetzt werden.

>    Technische Expertise und Mittel sollen nicht nur wenigen Spezialisten*innen zugänglich sein. Wir wollen eine studiengangsübergreifende Frage- und Lernkultur der Hilfe und Selbsthilfe etablieren.

>    Wir wollen IT-verwandte und netzpolitische Geschehnisse und Veränderungen transparent machen und diskutieren, um in Veranstaltungen in der Universität und im Studium gemeinsam Positionen zu beziehen.

> Bei weiteren Fragen kommt zur Vorstellung am 24.10.

<br>
Als Fachschaft begrüßen wir diese studentische Initiative sehr und wir freuen uns über die Beteiligung an wichtigen Fragestellungen in der Informatik.
