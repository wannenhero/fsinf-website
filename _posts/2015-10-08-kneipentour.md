---
title: Jährliche Kneipentour



---
***for an english version click [here](/news/2015/10/08/2015-10-kneipentour) and scroll down***

Das neue Wintersemester steht vor der Tür und bevor der Studienalltag dich ganz einnimmt, lass nochmal alles liegen und komm mit auf unsere Kneipentour.
Treffpunkt ist am **14.10.** um **16:45 Uhr** vor dem Audimax an der Information.

Gerne könnt ihr auch im Laufe des Abends dazu stoßen, haltet dafür einfach Rücksprache mit euren Kommilitonen oder uns.

<!-- Read more -->

Wir werden euch in drei erlesene Etablissements führen. Um den Abend geeignet ausklingen zu lassen, wird unsere Tour in der Moritzbastei in geselliger Atmosphäre enden. Wer ein wenig das Tanzbein schwingen möchte, kommt auch auf seine Kosten, da zum selben Zeitpunkt dort auch eine Erstiparty statt findet.

Die FSR-Kneiptentour gibt euch die Möglichkeit eure Kommilitonen und  Informatik-Studierende anderer Semester kennen zu lernen. 
Wir sind ebenfalls gespannt, uns euch vorzustellen!

Euer FSR Informatik

### English Version


The new semester is about to start and before you are back at your daily routine, leave your tasks behind and join us on our bar tour.

We'll meet at **4.45PM** on **Oct 10th** at the information desk in front of the Audimax .

Feel free to join us later. Just stay in touch with your fellow students or us and you'll know where we'll be.

We will visit three locations and the evening will finish in the Moritzbastei, which has a really pleasant atmosphere. If you feel like dancing, you will be happy to know that there will be a semester party.

The bar tour will give you the opportunity to get to know your fellow students or students of other semesters.
We are excited to introduce ourselves as well!
  
Your student council for Computer Science.
