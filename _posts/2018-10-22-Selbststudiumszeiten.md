---
title: Selbststudiumszeiten  



---
Wir möchten kurz auf die seit 1.5 Jahren bestehenden Selbststudiumszeiten in Räumlichkeiten an der Fakultät aufmerksam machen:

Montag bis Freitag   
A-531 von 15:00 - 22:00 Uhr     
P-701 von 17:00 - 22:00 Uhr   
 
Zutritt erhälst Du mit Deinem Studienausweis im genannten Zeitraum. Voraussetzung ist, dass Du bereits Zugang zu den PC-Pools hast. Ist dem nicht der Fall, wende Dich bitte an die Zentralen Dienste (Fabian Heusel, A 527 und Fabian Schmidt, A 529). 

Im Zuge unserer Absprache mit dem Dekanat haben wir uns verpflichtet darauf hinzuweisen, dass die Fakultät die ordnungsgemäße Nutzung der Räume prüfen wird und sich eine Sperrung der Räume vorbehält, wenn Müll oder das fehlende Zurückstellen der Tische in die übliche parlamentarische Bestuhlung dies erfordern.
Der Zugang zu den Räumen und die Selbststudiumszeiten sind große Zugeständnisse der Fakultät. Deshalb bitten wir Dich, diesen Hinweis zu beachten und auch andere gegebenenfalls daran zu erinnern, um Probleme zu vermeiden. Eine Annullierung der Regelung wäre äußerst ärgerlich. Wir bleiben mit der Fakultät im Gespräch und wenn alles gut läuft, wird es eine erneute Fortsetzung und auch Ausweitung der Selbststudiumszeiten in den Folgesemestern geben.
