---
title: Altklausuren in TI2



---
Wir haben gestern einige TI2-Altklausuren von Prof. Bogdan zur Verfügung gestellt bekommen und ins gitlab ( https://git.fsinf.informatik.uni-leipzig.de/material/Grundlagen_der_Technischen_Informatik_2 ) eingepflegt. 

An dieser Stelle möchten wir darauf hinweisen, dass die Altklausuren von Prof. Bogdan einem Copyright unterliegen und die Einhaltung dessen eine Bedingung für Bereitstellung der Altklausuren im gitlab ist. **Jegliche kommerzielle Nutzung der Altklausuren durch den FSR Informatik, seine Mitglieder oder Dritte ist daher ausgeschlossen.**

Wir bedanken uns bei Prof. Bogdan für die Bereitsstellung und wünschen euch viel Erfolg in der Prüfungsphase!

Dein FSR Informatik
