---
title: Wahlen des Referats Ausländischer Studierender des StuRa



---

Am 1.-3. Juli 2014 sind alle ausländische Studierende aufgerufen, das [Referat Ausländischer Studierender des StuRa](http://stura.uni-leipzig.de/referat-auslaendischer-studierender) zu wählen. Die [zugelassenen Wahlvorschläge](https://stura.uni-leipzig.de/sites/stura.uni-leipzig.de/files/dokumente/2014/06/wahlausschuss_bekanntmachung_wahlvorschlaege_ras_14.06.17.pdf) findet ihr auf der StuRa-Webseite. Wählen könnt ihr zwischen 10:00 und 17:00 Uhr im Foyer des Hörsaalgebäudes. Das Kleingedruckte dazu steht in der [Wahlausschreibung](http://stura.uni-leipzig.de/sites/stura.uni-leipzig.de/files/dokumente/2014/05/wahlausschreibung_2014_05_17_ras_online.pdf).
