---
title: Moduleinschreibung für das Sommersemester



---

Nachdem ihr hoffentlich alle an die Rückmeldung für das Sommersemester gedacht habt,
beginnt in den nächsten Tagen der Einschreibezeitraum für zahlreiche Informatikmodule.
Detaillierte Informationen findet ihr auf der [Webseite des Prüfungsamts](http://studium.fmi.uni-leipzig.de/startseite.html)
oder direkt in dieser [PDF-Datei](http://studium.fmi.uni-leipzig.de/fileadmin/Studienbuero/documents/Ankuendigung_Moduleinschreibung_SS15.pdf).
