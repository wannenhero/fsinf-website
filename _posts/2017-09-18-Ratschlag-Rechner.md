---
title: Hardwarekauf und Lizenzerwerb



---
Uns erreichen regelmäßig Anfragen zum Hardwarekauf und zu Lizenzerwerb. Wir können dazu ein paar Ratschläge geben.

### Gebrauchte Hardware
Gebrauchte Hardware zu kaufen ist nicht nur umweltbewusst, sondern genügt in den meisten Fällen den Ansprüchen fürs Studium bei weitem. Dass bei Privatkäufen ein gewisses Risiko besteht, ist verständlich. Für alle, die auf Nummer sicher gehen wollen, gibt es zahlreiche seriöse Anbieter im Internet, die generalüberholte Geräte inkl. einjähriger Garantie vertreiben.

### Neue Rechner und Hardware
Solltest Du dennoch einen neuen Rechner oder Hardware für Dein Studium anschaffen, informierst Du dich am besten über Sonderangebote für Studierende. Die meisten Hersteller haben spezielle Campusprogramme und stellen oftmals sogar Sondereditionen und -baureihen her. In jedem Fall lohnt es sich, beim Erwerb auf eine Betriebssystemslizenz und zusätzliche Software ab Werk zu verzichten. Näheres erfährst Du dazu im nächsten Absatz.

### Betriebssystem und Lizenzen
Wir empfehlen Dir, Dich während des Studiums mit freien Betriebssystemen, zum Beispiel [Linux-Derivaten](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen) , auseinander zu setzen und diese zu benutzen. 

Solltest Du dennoch Microsoft-Lizenzen, zum Beispiel Windows 10, benötigen, kannst du diese kostenfrei über das [Microsoft-Imagine Programm](https://catalog.imagine.microsoft.com/de-de/institutions/enroll)  erhalten. Wende dich dazu bitte an die Zentralen Dienste im 5. Stock im Augusteum, da diese einen Zugang für dich beantragen können. Wichtig ist, dass eine kommerzielle Nutzung der Software unzulässig ist.

Das Office-Paket erhälst du über [Campus Sachsen](https://campussachsen.tu-dresden.de/) . Du musst dich lediglich alle 180 Tage dort einloggen, damit deine Lizenz nicht verfällt.

### Sonstiges
Empfehlenswert sind Businessmodelle, da diese in der Regel eine bessere Qualität und manchmal auch eine längere Garantie aufweisen.

Anschaffungen fürs Studium sind unter Umständen steuerlich absetzbar.
