---
title: Studentische Mitglieder für verschiedene Berufungskommissionen gesucht



---
In den nächsten Monaten starten mehrere Berufungskommissionen für zu besetzende Professuren und wir sind noch auf der Suche nach 3 Studierenden, die wir entsenden können.
Eine Berufungskommission besteht in der Regel aus etwa 12-14 Mitgliedern und setzt sich aus verschiedenen Vertretern universitärer Organe zusammen, u.a. Professoren, Mitarbeiter und Studierende. Der Fachschaftsrat ist hierbei für die Entsendung der studentischen Vertreter zuständig. Zeitlich nimmt eine Berufungskommission im Zeitraum von 6 Monaten in etwa 4 Termine in Anspruch. Wenn du Interesse hast, entsendet zu werden, schreib uns eine Mail ([fsinf@fsinf.informatik.uni-leipzig.de](mailto:fsinf@fsinf.informatik.uni-leipzig.de)) oder komm zu unserer Sitzung.
