---
title: "Wahlen: Euer FSR braucht Nachwuchs!"



---

Am 3. und 4. Dezember finden die nächsten Fachschaftsratswahlen statt. Dann entscheidet ihr, wer von April 2015 bis <del>März 2016</del> September 2015 im FSR Informatik sitzt. Wenn ihr euch vergewissern wollt, dass ihr als Wähler_in des FSR Informatik zugelassen seid, könnt ihr bis zum 5. November bei [Andreas Zerbst](http://www.informatik.uni-leipzig.de/ifi/index.php?id=287&person=AZerbst) im Raum A523 Einblick in das Wähler_innenverzeichnis nehmen.

Ein paar derzeitige FSR-Mitglieder werden nicht noch einmal kandidieren, weil sie kurz vor dem Ende ihres Studiums stehen. Deshalb sind wir mehr denn je auf Nachwuchs angewiesen, um die neun Plätze mit motivierten Menschen zu besetzen. Falls du Zeit und Lust hast, dich auf Institutsebene für die Belange deiner Mitstudierenden einzusetzen, dann bist du im Fachschaftsrat genau richtig. Wir veranstalten nicht nur gelegentlich Partys und eine jährliche Studienfahrt zum Kennenlernen, sondern vertreten außerdem studentische Interessen in universitären Gremien, versorgen euch mit Informationen und sind bei Problemen ansprechbar.

**Bis zum 12. November um 16:00 Uhr** kannst du im StuRa deine Kandidatur einreichen, das Formular dafür findest du [hier](http://stura.uni-leipzig.de/sites/stura.uni-leipzig.de/files/dokumente/2014/03/wahlvorschlag_fsr.pdf). Oder du kommst zur nächsten FSR-Sitzung am Dienstag, dem 11. November, um 15:15 Uhr im Glaskasten im 5. Obergeschoss des Augusteums vorbei.

Zusammen mit den FSR-Wahlen finden auch die Wahlen des Senats und des erweiterten Senats der Universität statt. Außerdem haben wir Pfannkuchen im Wahllokal – es lohnt sich also dreifach, den 3. und 4. Dezember in euren Kalender einzutragen. Schaut einfach an einem der beiden Tage zwischen 9:00 und 16:00 Uhr im Ziegenledersaal des StuRa (auf dem Campus-Innenhof, gegenüber vom Mensa-Eingang) vorbei und vergesst eure Unicard nicht.
