---
title: "8. Mai: Durchleuchtet, analysiert und einsortiert. Big Data, Privacy und kommerzielle digitale Überwachung."



---

Der am 3. Februar aus gesundheitlichen Gründen ausgefallene Vortrag **Durchleuchtet, analysiert und einsortiert. Big Data, Privacy und kommerzielle digitale Überwachung** mit [Wolfie Christl](http://wolfie.crackedlabs.org/) wird am 8. Mai nachgeholt.

**8. Mai 2015, 19 Uhr, Universität Leipzig (Hörsaalgebäude), Hörsaal 1**

Den Ankündigungstext findet ihr auf der Webseite des [AK Gesellschaftskritik](https://akgesellschaftskritik.wordpress.com/2015/04/15/christl-digitale-ueberwachung/) oder hier:

<!-- Read more -->

<blockquote>Durch die rasante Weiterentwicklung der Informations- und Kommunikationstechnologien dringt die Erfassung persönlicher Daten immer mehr in den Alltag ein. Unsere Vorlieben und Abneigungen werden heute in einem Ausmaß digital gespeichert, verarbeitet und verwertet, das bis vor wenigen Jahren undenkbar war. Einzelne Personen werden über Geräte und Plattformen hinweg wiedererkannt, deren Verhalten und Bewegungen detailliert ausgewertet, Persönlichkeit und Interessen akribisch analysiert. Immer mehr Geräte sind heute mit Sensoren ausgestattet, mit dem Internet verbunden und ermöglichen so umfassende Einblicke in unser Leben. Gleichzeitig lassen sich im Zeitalter von Big Data mit automatisierten Methoden schon aus rudimentären Metadaten über Kommunikations- und Online-Verhalten umfangreiche Persönlichkeitsprofile erstellen. Aufstrebende Firmen in den Feldern soziale Netzwerke, Online-Werbung, mobile Apps oder Fitness arbeiten mit Hochdruck an Geschäftsmodellen, die auf der kommerziellen Verwertung der gesammelten Profile beruhen. Internationale Unternehmen agieren dabei teils unter Missachtung regionaler Datenschutzgesetze, oft gilt die Devise: Gemacht wird, was technisch möglich ist und angenommen wird. In vielen Wirtschaftssektoren von Marketing und Handel bis Versicherungs-, Finanz- und Personalwirtschaft herrscht Goldgräberstimmung – und gleichzeitig die Angst, den Anschluss zu verlieren. Während die Einzelnen immer transparenter werden, agieren viele Unternehmen hochgradig intransparent – deren Services, Apps, Plattformen und Algorithmen sind zentralisiert und kaum durchschaubar. Darüber hinaus haben nicht nur die Enthüllungen von Edward Snowden gezeigt, dass auch staatliche Behörden und Geheimdienste gern auf die gesammelten Daten zugreifen. Die Privatsphäre ist heute gleichermaßen durch Unternehmen wie auch durch staatliche Behörden bedroht.</blockquote>
