---
title: Sommerfest FSRä Mathematik und Informatik, 15. Juni, ab 15 Uhr, Friedenspark



---
Gönn dir doch mal eine Auszeit und genieße die Sonne!
Wir reichen dir ein Bier, eine Köstlichkeit vom Grill und bringen mit Volleyball und Flunky- alias Bierball Abwechslung in deinen Studienalltag.


![Sommerfest Plakat]({{"/images/2016/Sommerfest.jpg" | relative_url}} "Sommerfest Plakat")
