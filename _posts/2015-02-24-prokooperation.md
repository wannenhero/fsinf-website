---
title: "ProKooperation: Teilnahme an HTWK-Mastermodulen"



---

Das Projekt "ProKooperation" ermöglicht Studierenden des **Informatik-Masterstudiengangs** an der Uni Leipzig die Teilnahme an einzelnen Informatik- und Medieninformatik-Modulen der [HTWK Leipzig](http://www.htwk-leipzig.de). Die so erworbenen Leistungspunkte werden euch im Kern- oder Ergänzungsbereich angerechnet. Im Sommersemester beginnt die Pilotphase des Projekts, in der ihr noch nicht dazu verpflichtet seid, euch die an der HTWK absolvierten Module anrechnen zu lassen. Die zur Auswahl stehenden Module werden demnächst bekanntgegeben, alle anderen Informationen und Kontaktmöglichkeiten findet ihr in dieser [PDF-Datei]({{"/images/2015/ProKoop_Info_Studierende.pdf" | relative_url}}).
