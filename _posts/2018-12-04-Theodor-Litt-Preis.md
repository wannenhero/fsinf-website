---
title: Fachschaftsrat Informatik kritisiert Vergabe des Theodor-Litt-Preises



---
*Pressemitteilung*

Der Fachschaftsrat Informatik hält die Vergabe des Förderpreises für gute Lehre an Monica Berti für verfehlt. Der mit 1000€ dotierte Preis wurde gestern im Rahmen des „Dies academicus“ an Monica Berti und Franziska Naether verliehen. Er soll besonderes Engagement in der Lehre, die Entwicklung und Umsetzung innovativer Lehrmethoden sowie die herausragende Betreuung von Studierenden auszeichnen.

"Monica Berti und Franziska Naether wurden von Professor Gregory Crane, an dessen Lehrstuhl Monica Berti angestellt ist, für ihre Leistung in der Forschung nominiert." erklärt Paul Reinhardt, Sprecher der studentischen Vertretung der Informatikstudierenden. Im aktuellen Lehrbericht der Fakultät für Mathematik und Informatik wurde ein Modul der Preisträgerin explizit als Negativbeispiel für die Probleme der Lehre und Betreuungssituation im Studiengang Digital Humanities genannt. "Auch wurde an den Fachschaftsrat Informatik wiederholt Kritik an den Lehrveranstaltungen von Monica Berti in Bezug auf Didaktik, Betreuung und Datenschutz herangetragen." so Reinhardt weiter.

"Von Seiten der Studierenden gab es mehrere Nominierungen Dozierender, welche die Kriterien im Gegensatz zu den beiden Mitarbeiterinnen tatsächlich erfüllen. Die Gründe der Jury für die Auswahl der beiden Nominierten sind uns ein Rätsel. Die Meinung der Studierenden, welche von der Lehre akut betroffen sind, wurde offensichtlich nicht berücksichtigt." schließt Reinhardt.

Bei Rückfragen steht der Fachschaftsrat Informatik der Universität Leipzig mit Freude zur Verfügung: fsinf@fsinf.informatik.uni-leipzig.de
