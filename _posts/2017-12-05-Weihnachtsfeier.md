---
title: Weihnachtsfeier, 11.12.17, ab 19 Uhr im Felix-Klein-Hörsaal



---
![Weihnachtsflyer]({{ "/images/2017/Weihnachten.PNG" | relative_url }} "Weihnachtsflyer"){: height="500px"}


Auch in diesem Jahr laden wir euch wieder herzlich zur Weihnachtsfeier des FSR ein. Weihnachtliche Knabbereien und vorzugsweise dampfende Getränke erwarten euch zu kleinen Preisen. Verbringt mit uns und euren Kommiliton_innen einen entspannten Abend in liebevoll dekorierter Atmosphäre mit wunderschönem Blick auf den festlich geschmückten Augustusplatz. Auch in diesem Jahr wird uns der Chor der Erziehungswissenschaftlichen Fakultät wieder musikalisch erfreuen.
