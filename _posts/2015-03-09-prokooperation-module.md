---
title: "ProKooperation: Module und Einschreibefrist"



---

Informatik-Masterstudierende können im Sommersemester im Rahmen des Pilotprojekts "ProKooperation" an ausgewählten Informatik- und Medieninformatik-Modulen der [HTWK Leipzig](http://www.htwk-leipzig.de) teilnehmen:

* Human Computer Interaction
* Robotik
* Thread-Programmierung
* 3D-Design und -Dynamik

Eine Anrechnung der erworbenen Leistungspunkte ist möglich. Die Einschreibung erfolgt per E-Mail zwischen dem 22.03.2015, 12.00 Uhr, und dem 29.03.2015, 18.00 Uhr. Beeilt euch, denn die Plätze sind begrenzt.

**Alle Informationen findet ihr in dieser [PDF-Datei]({{ "/images/2015/ProKoop_Bekanntgabe_Module_HTWK.pdf" |  relative_url }}).**
