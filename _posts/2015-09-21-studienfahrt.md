---
title: Studienfahrt 2015



---
***for an english version click [here](/news/2015/09/21/2015-09-studienfahrt) and scroll down***

Alle Studierenden der Fakultät für Mathematik und Informatik sind herzlich zu einer gemeinsamen Studienfahrt vom 16.10. - 18.10.2015 nach Schloss Oberau bei Meißen eingeladen.  
Was erwartet dich auf der Fahrt und warum solltest du mitfahren?  
Du hast die Gelegenheit den kommenden Semesterstress beiseite zu schieben, neue Leute zu treffen und Spaß zu haben.  
Natürlich versuchen wir, deine FSRä, dir durch ein lockeres Rahmenprogramm ein unvergessliches Wochenende zu gestalten.  
Wir haben für bis zu 45 Personen Platz, weshalb wir eine rechtzeitige Anmeldung empfehlen.  
Die Kosten für Fahrt, Unterkunft, nicht-alkoholische Getränke und Verpflegung belaufen sich auf 35€. Es ist für alles gesorgt, packe lediglich deine Tasche mit etwas Wechselkleidung und sonstigem unverzichtbaren Kram. Komm wie du bist und habe Spaß!

Anmelden kannst du dich ab sofort unter folgender URL: [https://fsinf.informatik.uni-leipzig.de/registration/](https://fsinf.informatik.uni-leipzig.de/registration/)

**Bitte beachte, dass die Plätze neu vergeben werden, wenn der Unkostenbeitrag nicht rechtzeitig bei uns eingeht.**  
Wenn du sonst noch Fragen hast wende dich an: [fsinf@fsinf.informatik.uni-leipzig.de](mailto:fsinf@fsinf.informatik.uni-leipzig.de)

Wir freuen uns auf deine Anmeldung!  
FSR Mathe und FSR Informatik

<!-- Read more -->
### English Version

All students belonging to the Faculty for Mathematics and Computer Science are invited to attend a study trip taking place Oct 16th – 18th at Castle Oberau near Meißen.
Now, what will expect you there and why should you come?  
It will be an opportunity to forget about the upcoming semester duties, meet new people and to have fun. Of course we, your student councils, are giving all we can to provide an unforgettable weekend with a nice, relaxed program.  
Unfortunately, space is limited to 45 people, which is why we recommend to register on time.   
The costs will be 35€ and will include traveling, accommodation, non-alcoholic drinks and meals. Everything will be taken care of, just pack your bag with warm clothes and other personal necessities. Come as you are and be ready to have fun!

You can register here [https://fsinf.informatik.uni-leipzig.de/registration/](https://fsinf.informatik.uni-leipzig.de/registration/)

**Please note that there will be a waiting list as soon as we are fully booked. Your spot will be reassigned in case the registration fee was not paid on time.**  
If you have any questions, feel free to send us a message to [fsinf@fsinf.informatik.uni-leipzig.de](mailto:fsinf@fsinf.informatik.uni-leipzig.de)

We are excited!  
Student councils Math and Computer Science

