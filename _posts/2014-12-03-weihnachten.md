---
title: "Weihnachten: Picknick und Party"



---

Es wird weihnachtlich: Am 11. Dezember laden wir euch Informatik-Studierende zum Weihnachtspicknick in den Raum P901 ein. Bringt weihnachtliche Sachen (Glühwein, Punsch, Spekulatius usw.) mit und genießt den Blick über die Stadt. 17 Uhr geht's los!

Eine Woche später, am 18. Dezember 2014, feiern wir eine große Party im E35 (Wittenberger Straße 62, 04129 Leipzig). Zusammen mit den Fachschaftsräten [FaRAO](http://stura.uni-leipzig.de/fsr-farao) und [MuWi/KuPäd](http://stura.uni-leipzig.de/fsr-kupaed-muwi) laden wir euch und all eure Freundinnen und Freunde ab 21 Uhr zu Livemusik, DJs und günstigen Getränken ein. Der Spaß wird 4 Euro Eintritt kosten, weitere Details folgen.
