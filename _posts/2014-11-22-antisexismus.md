---
title: Sexistischer "Witz" vor der Kneipentour



---

Wir wurden darüber informiert, dass während der Auftaktveranstaltung unserer Ersti-Kneipentour am 15. Oktober 2014 im Hörsaal beim Spiel "Powerpoint-Karaoke" jemand einen "Witz" geäußert hat, der von einigen Personen als sexistisch und trans-feindlich wahrgenommen wurde und wegen dem ein paar Studierende die Veranstaltung verlassen haben.

Uns als Fachschaftsrat ist bewusst, dass einige von uns das nicht bemerkt und andere nicht eingegriffen haben. Uns mangelte es teils an einer Sensibilisierung für diskriminierende Äußerungen und außerdem an einem Plan, wie auf solche zu reagieren ist.

Sowohl bei [unserer letzten Sitzung](https://fsinf.informatik.uni-leipzig.de/protocols/2014/11/11/) als auch FSR-intern haben wir begonnen, die Geschehnisse nachzubereiten und Konsequenzen für unsere Arbeit zu ziehen. Ein erster Schritt war die Teilnahme am Anti-Sexismus-Workshop des StuRa am 20. November, der regelmäßig vom Stura angeboten wird und den wir auch euch empfehlen können.

Durch die Teilnahme an diesem Workshop haben wir viel gelernt und sind nun etwas vertrauter mit diesem Thema. Gleichzeitig haben wir gelernt, dass es wichtig ist miteinander zu reden. Denn wenn jemand sich durch eine gefallene Äußerung diskriminiert fühlt, dann ist es wichtig, dass eine Entschuldigung ausgesprochen wird. Es ist aber auch wichtig, dass darüber soweit gesprochen wird, dass die 'diskriminierende' Seite lernt, was genau falsch war und wie das in Zukunft vermieden werden kann. Deswegen wünschen wir uns, dass ihr in solchen Fällen mit uns Kontakt aufnehmt. Wer sich nicht gleich an den ganzen FSR wenden möchte, kann auch direkt Nancy (nancy at bioinf.uni-leipzig.de) ansprechen, die auch bei dem Workshop war und nun in engem Kontakt zum [Referat für Gleichstellung und Lebensweisenpolitik](http://stura.uni-leipzig.de/gleichstellung-lebensweisenpolitik) des StuRa steht.

Vor zukünftigen Veranstaltungen werden wir uns absprechen, wie mit beleidigenden Äußerungen umzugehen ist. Auf der Suche nach Denkanstößen, um solche zu erkennen, sind wir auf [eine Broschüre](https://antisexismusbroschuere.files.wordpress.com/2014/11/2.pdf) gestoßen, in der u.a. beleidigende Alltagssituationen geschildert werden – im Kapitel "Alltagssituationen" ab Seite 47.

Dass sexistische und anderweitig dumme Sprüche nicht geduldet werden, sollte eigentlich Konsens sein. Wir hoffen, dass die Situation am 15. Oktober niemanden dauerhaft abgeschreckt hat, möchten uns für unseren Umgang mit der Situation entschuldigen und geloben Besserung. Gleichzeitig wünschen wir uns, dass auch ihr den Mund aufmacht, wenn euch dumme Sprüche auffallen. Gerade in einem männerdominierten Bereich wie der Informatik ist das wichtig.

Euer FSR Informatik

