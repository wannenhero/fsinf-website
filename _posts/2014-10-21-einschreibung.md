---
title: "Wichtig: Einschreibung in Pflichtmodule"



---

Seit diesem Semester gibt es eine Änderung bei der Moduleinschreibung, die auch uns überrascht hat. Für eure Pflichtmodule müsst ihr euch neuerdings schon am Semesterbeginn anmelden, und zwar bis zum 26. Oktober. (Bisher reichte meist eine Anmeldung zur Prüfung und gegebenenfalls zu den Übungen aus.) Diese Moduleinschreibung ist gleichzeitig Prüfungsanmeldung. Abmeldungen sind voraussichtlich zwischen dem 28.10.2014 und 25.01.2015 online möglich. Alle Infos erhaltet ihr in dieser [PDF-Datei auf der Universitätswebseite](http://www.fmi.uni-leipzig.de/Media/News/2014/10/Ankuendigung_Einschreibung.pdf).

Die Einschreibung erfolgt über [das TOOL](https://almaweb.uni-leipzig.de/einschreibung/). Dort wird auch eine Anzahl freier Plätze angezeigt. In der Vergangenheit waren die Plätze in den Pflichtmodule nicht begrenzt, und das war auch gut so. Falls ihr in einem Pflichtmodul keinen Platz erhaltet, dann [meldet euch bitte bei uns](https://fsinf.informatik.uni-leipzig.de/pages/impressum/).

Welche Module Pflichtmodule und damit von dieser Änderung betroffen sind, erfahrt ihr in den [Modulbeschreibungen sowie im Studienverlaufsplan bzw. Studienablaufplan](http://www.informatik.uni-leipzig.de/ifi/studium/studiengnge.html) und natürlich im TOOL selbst.

Weitere Informationen zur Einschreibung, u.a. in Module des Ergänzungsfachs Biologie und Wirtschaftswissenschaften, findet ihr auf der [Fakultätswebseite](http://www.fmi.uni-leipzig.de).
