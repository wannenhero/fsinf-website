---
title: Sommerparty am 3. Juli!



---

Die Fachschaftsräte Informatik, Kunstgeschichte, Musikwissenschaften + Kunstpädagogik, Theaterwissenschaften und FaRAO laden am 3. Juli zur Sommerparty ein. Ab 15 Uhr gibt's für euch am Elsterflutbecken Ecke Jahnallee Livemusik und DJs, bezahlbare Getränke und leckeres veganes Essen. Mehr Infos findet ihr bei [Facebook](https://www.facebook.com/events/325875797537331/).
