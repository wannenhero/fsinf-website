---
title: Selbststudiumszeiten im WiSe 2017/18



---
Aufgrund der guten Resonanz konnten wir uns erfolgreich für eine Fortsetzung und Erweiterung der Selbststudiumszeiten einsetzen.

Werktags kannst Du von 15:00-22:00 die A-531 und 17:00-22:00 Uhr die P-701 zum gemeinsamen oder stillen Lernen nutzen. **Zutritt erhälst Du mit Deinem Studienausweis im genannten Zeitraum.** Voraussetzung ist, dass Du bereits Zugang zu den PC-Pools hast. Ist dem nicht der Fall, wende Dich bitte an die Zentralen Dienste.

Im Zuge unserer Absprache mit dem Dekanat haben wir uns verpflichtet darauf hinzuweisen, dass die Fakultät die ordnungsgemäße Nutzung der Räume prüfen wird und sich eine Sperrung der Räume vorbehält, wenn Müll oder das fehlende Zurückstellen der Tische in die übliche parlamentarische Bestuhlung dies erfordern.

Der Zugang zu den Räumen und die Selbststudiumszeiten sind große Zugeständnisse der Fakultät. Deshalb bitten wir Dich, diesen Hinweis zu beachten und auch andere gegebenenfalls daran zu erinnern, um Probleme zu vermeiden. Eine Annullierung der Regelung wäre äußerst ärgerlich. Wir bleiben mit der Fakultät im Gespräch und wenn alles gut läuft, wird es eine erneute Fortsetzung und auch Ausweitung der Selbststudiumszeiten in den Folgesemestern geben.
