---
title: Sommerparty



---
Die lauen Sommernächte häufen sich und so haben wir keine Kosten und Mühen gescheut, um euch auch dieses Jahr wieder ein kleines Sommerspektakel zu kredenzen:

Neben 15 (!) Acts gibt es natürlich auch wieder die traditionelle Hüpfburg, den Kicker und eine lauschige Feuershow im Dunkel des Hains. Ebenso könnt ihr, dank des reichhaltigen Angebots an Speis und Trank, dieses Jahr dabei auch wieder einige gemeinnützige Projekte unterstützen – oder ihr genießt einfach die Zeit und tanzt mit uns bis ins Morgengrauen. Alles ist möglich. Alles kann - nichts muss!

Diese Veranstaltung finanziert sich über einen Solibeitrag von 1- 3 Euro, welchen ihr auf euer erstes Getränk draufgerechnet bekommt. Das die Party überhaupt so einen Umfang haben kann, verdanken wir damit eurem Beitrag zum ersten Getränk. 

Sexist_innen, Homo- Inter- und Transhasser_innen, Antisemit_innen, Rassist_innen, Nationalist_innen und Faschist_innen werden nicht toleriert und sollen zu Hause bleiben, denn jede Form von Diskriminierung und Grenzüberschreitung wird nicht toleriert!
Nein heißt Nein!

**am 02. Juli, ab 16 Uhr**

<!-- Read more -->

 Mit dabei dieses Jahr: 

**Bands/Live-Acts:**
* [The Polluters (Damn Fresh Rock Music)](http://www.thepolluters.de/media.htm)
* [The Evil Machine (Folkrock/Americana)](http://theevilmachine.bandcamp.com/)
* [Brazzbandit_innen (alternativlose Blasmusik)](http://soundcloud.com/brassbanditen)
* [Lipsko Kosmos Orkestr (World/Folk/Alternativ)](http://facebook.com/lipskokosmosorkestr)
* [On the Verge of Sanity (Heavy Fusion)](http://www.onthevergeofsanity.de/)
* [Der Täubling (Prof. für Angewandte Misanthropie)](https://soundcloud.com/dertaeubling)
* [irq7 (Chiptune/Gameboy Breaks)](http://soundcloud.com/irq7)

**Djs**
* [L|Allure (Deepdubtechno|RuS/Resonance)](http://soundcloud.com/lallure)
* [lún (Deephouse/Electro|Erythrosin)](http://soundcloud.com/lun-3)
* [L.U.I. (DnB|Dpbsffgah.ahtu/Basssucht)](http://soundcloud.com/l_u_i)
* [Jonn Dark (Tech House|In Höchsten Höhen)](http://soundcloud.com/jonn-dark)
* [Firle (Acidhouse/Techno|exLEpäng)](http://soundcloud.com/firlemusic)
* [kïba (Techno|RuS/Resonance)](http://soundcloud.com/ki_ba)
* [Dentrid (Deephouse|MOTTT.FM)](http://soundcloud.com/dentrid/)
* [Dj Tschüss Ey! (Eurodance/Trash/90ies)](http://facebook.com/TschuessEy)

**und sonst:**
* Feuershow mit Freaks on Fire
* Soli-Cocktailbar
* veganes Essen
* Hüpfburg
* Planschbecken

**Ort:** wie in den letzten Jahren
