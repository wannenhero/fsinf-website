---
title: Studienfahrt 2017



---
Alle Studierenden der Informatik und Digital Humanities sind herzlich zu einer gemeinsamen Studienfahrt vom **10. - 12.11.17** in das ökologische Landschulheim Dreiskau-Muckern eingeladen. 
Du hast die Gelegenheit neue Leute zu treffen und dich erwartet ein vielfältiges Programm. Daneben bietet das Gelände viele Möglichkeiten wie eine Mehrzweckturnhalle und eine große Grünfläche mit Grillplatz. Der Störmthaler See ist in naher Reichweite, sodass auch dort Spaziergänge und Wanderungen möglich sind.
Wir haben für bis zu **60 Personen** Platz, weshalb wir eine rechtzeitige Anmeldung empfehlen. 
Die Kosten für Fahrt, Unterkunft, nicht-alkoholische Getränke und Verpflegung belaufen sich auf **30€**. Es ist für alles gesorgt, pack lediglich Deine Tasche mit Wechselkleidung, festen Schuhen und sonstigem unverzichtbaren Kram. Komm wie Du bist!

Anmelden kannst Du Dich ab dem **25.9. um 18 Uhr** auf folgender Seite: https://events.fsinf.informatik.uni-leipzig.de

Bitte beachte, dass die Plätze neu vergeben werden, wenn der Unkostenbeitrag nicht rechtzeitig bei uns eingeht. 
Wenn Du sonst noch Fragen hast wende dich an uns: fsinf@fsinf.informatik.uni-leipzig.de. 

Wir freuen uns! 
FSR Informatik 

 
Englisch: 

All Computer Science and Digital Humanities students are invited to attend a study trip taking place **Nov 10th – 12th** at ökologisches Landschulheim Dreiskau-Muckern. 
You will have the opportunity to meet new people and to have fun by attending fun activities. The location provides many recreational options such as a gymn to play sports and an outdoor BBQ area. As the living quarters are located closely to lake Störmthal, nice walks and hikes will be an option as well.
Unfortunately, space is limited to **60 people**, which is why we recommend to register on time. 
The costs will be **30€** and will include traveling, accommodation, non-alcoholic drinks and meals. Everything will be taken care of, just pack your bag with warm clothes and other personal necessities. Come as you are and be ready to have fun! 

Registration will start on the **25th of September at 6PM** on this site: https://events.fsinf.informatik.uni-leipzig.de

Please note that your spot will be given to someone else on the waiting list if you don't transfer the registration fee on time. 
If you have any questions, feel free to send us a message fsinf@fsinf.informatik.uni-leipzig.de 

We are excited! 

Student council Computer Science 
