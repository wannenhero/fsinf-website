---
title: Anmeldung zur Studienfahrt gestartet



---
***for an english version click [here](/news/2016/07/08/2016-07-studienfahrt) and scroll down***


## Deutsch
Alle Informatik-Studierenden sind herzlich zu einer gemeinsamen Studienfahrt vom 05. - 07.08.16 zur Strobel-Mühle Pockautal eingeladen. Bisher fand die Studienfahrt immer zum Beginn des Wintersemesters statt. Oft hat es geregnet oder es war kalt. Deshalb haben wir uns entschieden dieses Jahr etwas neues auszuprobieren und die diesjährige Studienfahrt auf den Beginn der vorlesungsfreien Zeit zu legen.
Was erwartet Dich auf der Fahrt und warum solltest Du mitfahren?
Du hast die Gelegenheit neue Leute zu treffen und Spaß zu haben. Daneben bietet das Gelände der Mühle viele Möglichkeiten wie einen Tischtennisraum oder ein Beachvolleyballfeld. Außerdem gibt es direkt nebenan einen Hochseilgarten oder bei schönem Wetter kann man auch wandern gehen.
Wir haben für bis zu 30 Personen Platz, weshalb wir eine rechtzeitige Anmeldung empfehlen.
Die Kosten für Fahrt, Unterkunft, nicht-alkoholische Getränke und Verpflegung belaufen sich auf 35€. Es ist für alles gesorgt, pack lediglich Deine Tasche mit etwas Wechselkleidung, festen Schuhen und sonstigem unverzichtbaren Kram. Komm wie Du bist!

Anmelden kannst Du Dich ab sofort unter folgender URL: https://fsinf.informatik.uni-leipzig.de/registration/

Bitte beachte, dass die Plätze neu vergeben werden, wenn der Unkostenbeitrag nicht rechtzeitig bei uns eingeht.
Wenn Du sonst noch Fragen hast wende dich an uns: [fsinf@fsinf.informatik.uni-leipzig.de](mailto:fsinf@fsinf.informatik.uni-leipzig.de)


<!-- Read more -->

## English
All Computer Science students are invited to attend a study trip taking place Aug 5th – 7th at Strobel-Mühle Pockautal. Past study trips took place at the beginning of the winter term. It was often rainy and cold. This is why we have decided to try something new this year and have it at the beginning of summer break.
Now, what will expect you there and why should you come?
It will be an opportunity to meet new people and to have fun. The location provides many recreational options such as a table tennis room or a beach volleyball field. Close to our location is an outdoor climbing facility and if the weather is nice, we will also offer a hiking trip.
Unfortunately, space is limited to 30 people, which is why we recommend to register on time.
The costs will be 35€ and will include traveling, accommodation, non-alcoholic drinks and meals. Everything will be taken care of, just pack your bag with warm clothes and other personal necessities. Come as you are and be ready to have fun!

You can register here https://fsinf.informatik.uni-leipzig.de/registration/

Please note that your spot will be given to someone else on the waiting list if you don't transfer the registration fee on time.
If you have any questions, feel free to send us a message [fsinf@fsinf.informatik.uni-leipzig.de](mailto:fsinf@fsinf.informatik.uni-leipzig.de)
