---
title: Veranstaltungen im Herbst   



---
In den kommenden Wochen haben wir einige Veranstaltungen für euch geplant und nun zur besseren Übersicht zusammengefasst. 

Informationen zu Einführungsveranstaltungen findest du [hier](https://www.uni-leipzig.de/fileadmin/user_upload/Studium/zentrale_studienberatung/pdf/event/Broschuere_fuer_Neuimmatrikulierte.pdf).

Alle Veranstaltungen findest du auch in unserem [Kalender](https://fsinf.informatik.uni-leipzig.de/pages/calendar/). 

Bei Fragen, kannst du dich jederzeit an uns, z.B. per [Email](mailto:fsinf@fsinf.informatik.uni-leipzig.de), wenden.

### 11.10, ab 20 Uhr | Masterkneipenabend im Beyerhaus
Alle Studierenden in unseren Masterstudiengängen Bioinformatik, Informatik und Digital Humanities, sowie Informatik-Lehrämter höherer Semester sind am 11.10 herzlich ins Beyerhaus eingeladen. Los geht's um 20 Uhr.

### 16.10, ab 19 Uhr | Bachelorkneipentour, Treffpunkt vor dem Audimax
Am 16.10 sind alle Erstsemester und interessierte Studierende höherer Semester zu unserer alljährlichen Kneipentour eingeladen. Los geht's direkt nach der "Diskrete Strukturen" Vorlesung um 19 Uhr vor dem Audimax am Infopoint.

### 02. - 4.11 | Studienfahrt Forsthaus Sayda
Die alljährliche [Studienfahrt](https://fsinf.informatik.uni-leipzig.de/news/2018/09/26/2018-09-26-Studienfahrt/) führt dich dieses Mal ins [Forsthaus Sayda](http://www.forsthaus-sayda.de/). Alle Studierenden der Informatik, Bioinformatik und Digital Humanities sind herzlich eingeladen teilzunehmen. Es wartet ein spannendes Programm aus Workshops, Sport und Freizeitaktivitäten. Der Teilnehmerbeitrag beläuft sich auf 35€.

Für eine bequeme An- und Abreise haben wir einen exklusiven **Flixbus** organisiert.

Anmeldung und mehr Informationen unter https://events.fsinf.informatik.uni-leipzig.de
