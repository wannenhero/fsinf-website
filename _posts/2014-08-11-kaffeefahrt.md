---
title: Kaffeefahrt und Petitionsübergabe am 13. August



---

Am Mittwoch, dem 13. August, wird in Dredsden die [Petition zum Erhalt der Wissenschaft in Sachsen](http://kuerzung.wordpress.com/petition/) [übergeben](https://www.facebook.com/events/306869426157095). Dazu organisiert der StuRa eine kostenlose Kaffeefahrt nach Dresden. Der Bus fährt pünktlich 8:00 Uhr an der Goethestraße ab und wird etwa 15:30 Uhr wieder in Leipzig ankommen. Tickets erhaltet ihr für drei Euro Pfand im Campus Service auf dem Campusinnenhof.

Mehr Infos: [StuRa](http://stura.uni-leipzig.de/news/schoen-hier-aber-warst-du-schon-einmal-dresden) + [Facebook](https://www.facebook.com/events/337133746451365)
