---
title: Einführungswochen Oktober 2019



---
Liebe Neueingeschriebenen, Wechsler\*innen, Masterand\*innen und alle Interessierten!

Wir freuen uns, dass ihr zu uns an die Uni und zu uns auf die Website gefunden habt. Um den Einstieg ins Studium zu erleichtern haben wir ein kleines Programm mit Infoveranstaltungen, Essen und Spaß zusammengestellt. Wir freuen uns euch dort kennenzulernen. Erfahrungsgemäß sind die Gesellschaftabende sowie die Studienfahrt eine gute Möglichkeit sich mit Studierenden aus den höheren Semestern sowie der ersten Lerngruppe zu vernetzen. Die Studienfahrt wird dieses Jahr im November stattfinden, Informationen dazu findet ihr dann ab Oktober auf unserer Website.


<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-basic{text-align:center;vertical-align:top}
.tg .tg-fsr{color:#cb38e5;border-color:#000000;text-align:center;vertical-align:top}
.tg .tg-head{background-color:#b055ed;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-fak{color:#3166ff;text-align:center;vertical-align:top;border-color:#000000}
.tg .tg-head2{background-color:#f03cd8;border-color:inherit;text-align:center;vertical-align:top}
</style>


<table class="tg">
  <tr>
    <th class="tg-head2">23.09.2019 - 04.10.2019 Propädeutikum</th>
  </tr>
  <tr>
    <td class="tg-fak"><a href="#Spieleabend" style="color:#cb38e5">Mittwoch 25.09.19 Spieleabend</a> <br><span style="color:gray">(19.00 P801)</span><br></td>
  </tr>
</table>



<table class="tg">
  <tr>
    <th class="tg-head">Montag 07.10.19 </th>
    <th class="tg-head">Dienstag 08.10.19 <br></th>
    <th class="tg-head">Mittwoch 09.10.19</th>
    <th class="tg-head">Donnerstag 10.10.19</th>
  </tr>
  <tr>
    <td class="tg-fak"><a href="#Fak">Zentraler<br>Einführungstag</a> <br><span style="color:gray">(ganztägig)</span><br></td>
    <td class="tg-fak"><a href="#Fak">Einführung<br>des Instituts</a><br><span style="color:gray">(ganztägig)</span><br></td>
    <td class="tg-fak"><a href="#Fak">Sprechstunde<br>Modul-<br>einschreibung</a><br><span style="color:gray">9.00 - 12.00 &amp;<br>13.00 - 16.00<br>Raum A508</span><br></td>
    <td class="tg-basic"></td>
  </tr>
  <tr>
    <td class="tg-fsr"><a href="#CampusTour" style="color:#cb38e5">Campus Tour I</a> <br><span style="color:gray">Treffpunkt: 13.00 <br> TBA </span></td>
    <td class="tg-basic"></td>
    <td class="tg-fsr"><a href="#InfoFSR" style="color:#cb38e5">Infoveranstaltung<br>des FSR</a><br><span style="color:gray">16.00 TBA <br></span></td>
    <td class="tg-fsr"><a href="#Ralley" style="color:#cb38e5">Campus-Rallye</a><br><span style="color:gray">12.00 <br> Mensaeingang <br></span></td>
  </tr>
  <tr>
    <td class="tg-basic"></td>
    <td class="tg-fsr"><a href="#Spieleabend" style="color:#cb38e5">Spieleabend</a><br><span style="color:gray">19.00 <br>Felix-Klein-Hörsaal</span></td>
    <td class="tg-fsr"><a href="#Grillen" style="color:#cb38e5">Gemeinsames<br>Grillen</a><br><span style="color:gray">19.00 TBA <br></span></td>
    <td class="tg-basic"></td>
  </tr>
</table>




<table class="tg">
  <tr>
    <th class="tg-head">Montag 14.10.19 </th>
    <th class="tg-head">Dienstag 15.10.19 <br></th>
    <th class="tg-head">Mittwoch 16.10.19</th>
    <th class="tg-head">Donnerstag 17.10.19</th>
  </tr>
  <tr>
    <td class="tg-basic" colspan="4">Beginn der Vorlesungen <br></td>
  </tr>
  <tr>
    <td class="tg-fsr"><a href="#CampusTour" style="color:#cb38e5">Campus Tour II</a><br><span style="color:gray">Treffpunkt: 13.00 <br> TBA </span></td>
    <td class="tg-basic"></td>
    <td class="tg-basic"></td>
    <td class="tg-basic"></td>
  </tr>
  <tr>
    <td class="tg-basic"></td>
    <td class="tg-fsr">Kneipentour <br><span style="color:gray">Treffpunkt: 19.00 <br> AudiMax </span></td>
    <td class="tg-fsr"><br><span style="color:gray"> </span></td>
    <td class="tg-fsr"><a href="#Quiz" style="color:#cb38e5">Hörsaal-Quiz<br></a><span style="color:gray">19.00 <br> Felix-Klein-Hörsaal </span></td>
  </tr>
</table>



### Zentraler Einführungstag  <a name="Fak"> 
Die Uni bietet verschiedene (auch fakultätsspezifische) Einführungsveranstaltungen an. Alle Informationen findest du <a href="https://www.uni-leipzig.de/studium/im-studium/immatrikulation-und-studienstart/einfuehrungswoche/einfuehrungsveranstaltungen/">hier</a>.

### Campus Tour <a name = "CampusTour">

Wir treffen uns mit euch im Augusteum (genaueres folgt noch). Aufgeteilt in Gruppen besuchen wir dann zusammen die wichtigsten Plätze im Institut und am Campus. Einige Orte werden euch im Studium wiederholt begegnen, wie die Poststelle, das FSR Büro oder der Felix-Klein-Hörsaal, es lohnt sich am Anfang die wichtigsten Wege schon einmal gegangen zu sein. Die beiden Touren haben den selben Inhalt. Tendenziell hoffen wir, dass Ihr euch gut auf die beiden Termine verteilt. Für Digital Humanities Studierende bietet sich der zweite Termin besonders an, er ist direkt nach eurer erste Vorlesung!

### Infoveranstaltung der Fachschaft <a name = "InfoFSR">

In der Fachschaftsarbeit begegnen uns verschiedene Situationen in denen wir uns mit Studien- und Prüfungsordnung intensiver beschäftigen. Dabei haben wir verschiedene Tips und Tricks zusammen getragen und möchten euch die verschiedenen Ordnungen einfach erklären und näher bringen. Außerdem wird es die Möglichkeit geben in kleineren Gruppen Fragen zu stellen, dafür teilen wir euch in die jeweiligen Studiengänge ein und eine Person aus einem höheren Semester wird euch alle Fragen versuchen zu beantworten. 






### Campus-Rallye <a name = "Ralley">

Was ist der Unterschied zwischen einer Campus-Tour und einer Campus-Rally? Na ganz einfach: der Unterschied ist der selbe wie der zwischen einer Führung und einem Spiel-Spaß-Erlebnis Rundgang! Als Gruppe bekommt ihr verschiedene Aufgaben die euch an verschiedene Orte auf dem Campus führen und an verschiedenen Stationen spielerisch die Uni kennenlernen. Natürlich gibt es für die beste Gruppe auch einen Preis. 
Wir treffen uns um 12 Uhr vor der Mensa um dort gemeinsam zu essen. Danach geht es dann in Kleingruppen weiter. Wer es also nicht um 12 Uhr schafft kann den zweiten Treffpunkt um 13 Uhr vor dem AudiMax im Augusteum wahrnehmen. 


### Spieleabend <a name = "Spieleabend">

Bring doch einfach dein Lieblingsspiel mit oder sucht euch eins in unserer großen Sammlung aus. Zusammen wollen wir einen gemütlichen Abend verbringen, bringt gerne Knabbereien mit!


### Gemeinsamer Grillabend <a name = "Grillen">

Wir möchten wieder alle zusammen Grillen - wo und wann genau werden wir hier noch bekannt geben. 

### Hörsaal-Quiz <a name="Quiz">

Schnappt euch ein paar Freunde oder kommt alleine und findet neue, denn beim Quizabend treten Teams von bis zu 6 Spielern gegeneinander an. Bei Getränken, Snacks und guter Laune beantwortet euer Tisch die 30 Fragen und kann am Ende einen tollen Preis gewinnen. Doch Vorsicht: es wird nicht nur breites Wissen sondern auch cleveres Um-die-Ecke-Denken verlangt und nicht zuletzt etwas Überredungskunst um den Spielleiter für euer Antwort zu begeistern.

Treff 19:00 im Felix-Klein-Hörsaal (direkt nach der Analysis Vorlesung).



