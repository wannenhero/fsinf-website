---
title: "Befragung: Leihfahrräder für alle?"



---
Die Firma nextbike ist mit einem Kooperationsangebot für einen niederschwelligen Zugang zu ihrem Fahrradverleihsystem auf den Semesterticketausschuss zugekommen. Das Plenum des Student_innenRates der Uni Leipzig hat am 16.06.2015 beschlossen eine Befragung aller Studierenden zu einem Angebot der Firma nextbike zu starten. Wir möchten mit dieser Umfrage eure Meinung dazu einholen. Auf Basis der Umfrage-Ergebnisse wird das Plenum des Student_innenRates über das Kooperationsangebot entscheiden.

<!-- Read more -->

Das Unternehmen nextbike mit Sitz in Leipzig betreibt öffentliche Fahrradverleihsysteme in 16 Ländern und mehr als 80 Städten (u.a. Köln, Warschau, Budapest). Das System in Leipzig wird im kommenden Jahr erneuert und ausgebaut:  Nach einer möglichen Kooperation mit dem Semesterticketausschuss würden dann z.B. insgesamt 500 Fahrräder an festen Standorten zur Verfügung stehen. Zu den bisherigen Standorten kommen noch einmal 20 neue hinzu, welche sich im direkten Umfeld von studentischen Einrichtungen befinden werden. Die Fahrräder können an einer Station ausgeliehen und an einer anderen zurückgegeben werden, und zwar per App, am Bordcomputer oder über die Kundenhotline. Eine Integration des Studienausweises der Uni Leipzig ist möglich, damit wäre die Ausleihe direkt am Fahrrad noch einfacher.

Das Kooperationsangebot sieht vor, dass alle Leipziger Studierenden 1€ pro Semester als Umlage zahlen und dafür pro Ausleihe in Leipzig 30 Minuten Freifahrt bekommen. Es kann jedoch so oft hintereinander ein Rad ausgeliehen und abgegeben werden wie gewünscht, aber immer nur an einer der nextbike-Stationen. Pro Person können vier Räder gleichzeitig ausgeliehen werden, allerdings gelten die Sonderkonditionen nur für das erste ausgeliehene Rad.

Um die Fahrräder auszuleihen, müsst ihr euch einmalig bei nextbike registrieren. Die Verifizierung erfolgt entweder über die Uni-E-Mail-Adresse oder (falls gewünscht) über den Studierendenausweis. Bei der Registrierung wird eine Handynummer angegeben, die Angabe einer Bankverbindung ist nicht notwendig.

Falls ihr bei einer Ausleihe über das Freiminuten-Kontingent hinaus fahrt, wird euer Konto vorübergehend gesperrt und ihr werdet darüber per SMS informiert. Wenn ihr dann in eurem Kundenkonto eine Bankverbindung hinterlegt, wird das Konto wieder freigeschaltet und auch das Freikontingent steht wieder zur Verfügung. Nach den 30 Freiminuten zahlt ihr dann je 1€ für weitere 30 Minuten und maximal 9€ am Tag.

**Nun ist eure Meinung zu diesem Kooperationsangebot gefragt. Vom 06. Juli (8 Uhr) bis zum 12. Juli 2015 (23:59) könnt ihr über das Online-Einschreibesystem TOOL eure Stimme abgeben: https://almaweb.uni-leipzig.de/einschreibung**

Weitere Informationen von nextbike findet ihr [hier]({{"/images/2015/20150702_konzept_nextbike_leipzig.pdf" | relative_url}}).
