---
title: Studienfahrt 2018    



---
*English description below*
Alle Studierenden der Informatik, Bioinformatik und Digital Humanities sind herzlich zu einer gemeinsamen Studienfahrt vom 02. - 04.11.17 in das Forsthaus Sayda eingeladen.
Du hast die Gelegenheit neue Leute zu treffen und dich erwartet ein vielfältiges Programm.
Das Gruppenhaus ist von einem weitläufigen Gelände umgeben. Im Innenhof befinden sich eine große Terrasse mit befestigtem Grillplatz, ein kleiner Spielplatz mit Schaukel und Wippe, eine Feuerstelle, ein Volleyballfeld, Fußballplatz sowie eine Tischtennisplatte. Die umliegenden Wiesen können für verschiedene Aktivitäten genutzt werden.
Wir haben für bis zu 53 Personen Platz, weshalb wir eine rechtzeitige Anmeldung empfehlen.
Die Kosten für Fahrt, Unterkunft, nicht-alkoholische Getränke und Verpflegung belaufen sich auf 35€. Es ist für alles gesorgt, pack lediglich Deine Tasche mit Wechselkleidung, festen Schuhen und sonstigem unverzichtbaren Kram. Komm wie Du bist!


Die Anmeldung erfolgt über https://events.fsinf.informatik.uni-leipzig.de/ mit deiner Uni-Emailadresse (NUTZER@studserv.uni-leipzig.de)
Weitere Informationen zur Fahrt erhältst du nach der Anmeldung. 


Bitte beachte, dass die Plätze neu vergeben werden, wenn der Unkostenbeitrag nicht rechtzeitig bei uns eingeht.


Wenn Du sonst noch Fragen hast wende dich an uns: fsinf@fsinf.informatik.uni-leipzig.de.


Wir freuen uns!
FSR Informatik



**English:**


All Computer Science, Bioinformatics and Digital Humanities students are invited to attend a study trip taking place Nov 02th – 04th at Forsthaus Sayda.
You will have the opportunity to meet new people and to have fun by attending fun activities. The house is located on spacious premises. There is a grill, a playground, a fireplace, a volleyball and football court and a table tennis table in the patio. The grassland around the house can be used for a wide range of activities.
Unfortunately, space is limited to 60 people, which is why we recommend to register on time.
The costs will be 35€ and will include traveling, accommodation, non-alcoholic drinks and meals. Everything will be taken care of, just pack your bag with warm clothes and other personal necessities. Come as you are and be ready to have fun!


You can sign up at https://events.fsinf.informatik.uni-leipzig.de/ via your University account (USER@studserv.uni-leipzig.de)
Once you signed up you will receive further information.


Please note that your spot will be given to someone else on the waiting list if you don't transfer the registration fee on time.


If you have any questions, feel free to send us a message fsinf@fsinf.informatik.uni-leipzig.de


We are excited!
Student council Computer Science
