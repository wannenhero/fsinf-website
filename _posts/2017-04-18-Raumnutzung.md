---
title: "Raumnutzung Augusteum und Paulinum: Selbststudiumszeiten"



---

Um die Lernplatzsituation am Institut zu verbessern, haben wir mit dem Dekanat eine vorerst für dieses Semester geltende Regelung getroffen.

Ab sofort kannst du **Mo-Fr von 17:00-22:00** die **A-531** und die **P-701** (Achtung, mittwochs erst ab 19 Uhr) zum gemeinsamen oder stillen Lernen nutzen. Zutritt erhälst Du mit Deinem Studienausweis im genannten Zeitraum. Voraussetzung ist, dass Du bereits Zugang zu den PC-Pools hast. Ist dem nicht der Fall, wende Dich bitte an die [Zentralen Dienste](https://www.informatik.uni-leipzig.de/ifi/oeffentlichkeit/zd/cip-pools/mitarbeiter-zd.html).

Im Zuge unserer Absprache haben wir uns verpflichtet darauf hinzuweisen, dass die Fakultät die ordnungsgemäße Nutzung der Räume prüfen wird und sich eine Sperrung der Räume vorbehält, wenn Müll oder das fehlende Zurückstellen der Tische in die übliche parlamentarische Bestuhlung dies erfordern.

Der Zugang zu den Räumen und die Selbststudiumszeiten sind große Zugeständnisse der Fakultät. Deshalb bitten wir Dich, diesen Hinweis zu beachten und auch andere gegebenenfalls daran zu erinnern, um Probleme zu vermeiden. Eine Annullierung der Regelung wäre äußerst ärgerlich.
Wir bleiben mit der Fakultät im Gespräch und wenn alles gut läuft, werden wir uns um eine Fortsetzung und auch Ausweitung der Selbststudiumszeiten in den Folgesemestern bemühen
können.
