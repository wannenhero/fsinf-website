---
title: Informationsabend zu kooperativen Promotionen und Masterarbeiten



---
Das Institut für Informatik der Universität Leipzig lädt gemeinsam mit der Fakultät für Informatik, Mathematik und Naturwissenschaften der HTWK Leipzig zum gemeinsamen Informationsabend am 13. Januar 2016 ein. Alle interessierten Masterstudierenden der Informatik und Bioinformatik sind hierzu herzlich eingeladen.

[hier der Flyer]({{"/images/2015/infoabend_jan_plakat.pdf" | relative_url}})
