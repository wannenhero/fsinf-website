---
title: Fristen bei der Bewerbung auf einen Masterstudienplatz + "Deutschlandstipendium"



---

Falls ihr im Oktober ein Informatik-Masterstudium an der Uni Leipzig beginnen möchtet, dann beachtet die [Bewerbungsfristen](http://www.informatik.uni-leipzig.de/ifi/studium/studiengnge/ma-inf/bewerbung.html). Bis zum 15. Juli solltet ihr eure Unterlagen zur Eignungsprüfung einreichen. Nach bestandener Eignungsprüfung müsst ihr euch [online für einen Studienplatz bewerben](http://www.zv.uni-leipzig.de/studium/bewerbung/online-bewerbung.html#c703).

Falls ihr den Bachelor erst in ein paar Monaten abschließt, könnt ihr trotzdem schon das Masterstudium beginnen. Maximal ein Semester lang könnt ihr euch in solch einem Bachelor-Master-Doppelstudium befinden. Spätestens Ende März müsstet ihr also nachweisen, dass ihr alle Credit Points eingesammelt bzw. die Bachelor-Abschlussprüfung bestanden habt. Falls ihr das plant, lasst euch aber besser vorher im [Studienbüro](http://studium.fmi.uni-leipzig.de/) beraten.

Am 15. Juli um 12:59 Uhr endet auch die Bewerbungsfrist für ein ["Deutschlandstipendium"](http://www.zv.uni-leipzig.de/studium/angebot/foerderung/finanzierungsmoeglichkeiten/deutschlandstipendium.html). Chancen haben "talentierte und leistungsstarke Studierende, die auf Grund ihres bisherigen Engagements und Werdeganges unter Berücksichtigung sozialer und persönlicher Umstände über ein großes Potential verfügen und bereits herausragende Leistungen in Studium oder Beruf erbracht haben oder erwarten lassen".
