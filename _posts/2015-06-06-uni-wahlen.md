---
title: Universitätswahlen



---
Bald finden wieder die Wahlen für den nächsten FSR, Senat, erweiteren Senat, Promovierenden Rat, Fakultätsrat und des Referats Ausländischer Studierender (RAS) statt.

Diese sind am **09. und 10. Juni 2015** von **9 bis 16 Uhr**. Die Wahl wird für Studierende der Informatik im **Ziegenledersaal** (Campus Augustusplatz, Erdgeschoss des Seminargebäudes, Zugang vom Innenhof, zwischen Sparkasse und StuRa) stattfinden.

Um deinen studentischen Vertretern in den diversen Gremien auch genügend Legitimation zu verleihen, solltest du unbedingt zur Wahl vorbeischauen!
