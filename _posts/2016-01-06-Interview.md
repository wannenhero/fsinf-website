---
title: Interviewpartner/in gesucht



---
Für die Video-Reihe „Das sagt der/die Student/in“ werden noch 
Studierende aus dem Informatik B.Sc. gesucht, die Lust haben, in einem Video Fragen zum Studiengang zu beantworten.

Die Filme sollen Studieninteressierten und -anfängern wertvolle Orientierung bieten und auf dem Weg ins Studium unterstützen. 
Auch ein Dreh zu zweit ist möglich.

Im Video soll ein authentischer Einblick in die eigenen Erfahrungen aus dem Studium an der Uni Leipzig gegeben werden. Der fertige Clip soll anschließend auf dem Studienstartportal der Uni www.leipzig-studieren.de veröffentlicht werden.

Beispiel-Video für den Studiengang Zahnmedizin:
https://youtu.be/YDrkYrLVlo4

Bei Interesse bitte kurze E-Mail mit Namen und Fachsemestern sowie einer kurzen Begründung, warum du gern dabei sein möchtest an [leipzig-studieren@uni-leipzig.de](mailto: leipzig-studieren@uni-leipzig.de).
