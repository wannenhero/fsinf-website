---
title: "Donnerstag: Weihnachtsparty"



---

Am Donnerstag, dem 18. Dezember 2014, feiern wir eine große Party im
**[E35 (Wittenberger Straße 62, 04129 Leipzig)](https://www.google.de/maps/place/Kunst-+und+Kulturverein+E35+e.V./@51.365336,12.385838,15z/data=!4m2!3m1!1s0x0:0xca4a810bc49473a9)**.
Zusammen mit den Fachschaftsräten [FaRAO](http://stura.uni-leipzig.de/fsr-farao) und
[MuWi/KuPäd](http://stura.uni-leipzig.de/fsr-kupaed-muwi) laden wir euch und all eure Freundinnen und Freunde
zu Livemusik, DJs und günstigen Getränken auf zwei Floors ein.

Der Spaß wird 4 Euro Eintritt kosten. Einlass ab 20 Uhr, Musik ab 21 Uhr.

Auf den Bühnen erwarten euch:

* [Tee und Wein](https://soundcloud.com/tee-und-wein) (Swing, Ska, Jazz)
* [The Heroine Whores](http://www.reverbnation.com/theheroinewhores) (Grunge, Riot Grrrl)
* [Serius Bjerkelej](https://www.facebook.com/SeriusBjerkelej) (Deep, Tech, House)
* [L_Sa](https://soundcloud.com/l_sa) (Female Vocalists, Remixgedöns, Deephouse)
* [Buntmeliert](https://soundcloud.com/buntmeliert) (Deep, Tech, Dance)
* [Triac](https://triac8bit.bandcamp.com) (8bit, Gameboycore)
* [Muraks](http://www.mixcloud.com/muraks7/) (exlepäng)
* [The Schnugginators](https://soundcloud.com/francis-tent) (Techno).

Alle Infos findet ihr auch beim
[Facebook-Event](https://www.facebook.com/events/580630852036737/).

Kein Platz für Rassismus, Sexismus etc.
