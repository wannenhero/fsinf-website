---
title: Studienfahrt und neue Eventplattform



---
Wie bereits [mitgeteilt](https://fsinf.informatik.uni-leipzig.de/news/2017/09/18/2017-09-Studienfahrt/), findet dieses Jahr wieder eine Studienfahrt vom 10. - 12.11.17 statt. 

Die Anmeldung erfolgt ab sofort auf unserer neuen Eventplattform [events.fsinf.informatik.uni-leipzig.de](https://events.fsinf.informatik.uni-leipzig.de) mittels [Uni-Login](https://www.urz.uni-leipzig.de/dienste/uni-login/).

Sollte es Probleme geben, melde Dich bitte bei [uns](https://fsinf.informatik.uni-leipzig.de/pages/impressum/).
