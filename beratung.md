---
title: Beratung
permalink: /beratung/
lang: de
ref: beratung
---

Wir entsenden Personen in den Prüfungsausschuss und in die Studienkommission. Im Prüfungsausschuss können wir aktiv eure Rechte aus Prüfungs- und Studienordnung durchsetzen, und in der Studienkommission können wir aktiv zu Verbesserungen beitragen. Falls du Probleme im Studium hast, unfair oder nicht entsprechend der Prüfungs- oder Studienordnung behandelt wurdest sind wir gerne für dich da. Falls du Verbesserungsvorschläge für die Studiengänge am Institut für Informatik hast kannst du diese gerne an uns herantragen.

Bei allgemeinen Anliegen kannst du entweder bei uns im Büro (A541) vorbeischauen oder eine Email an [{{ site.email }}](mailto:{{ site.email }}) schreiben.

Für vertrauliche Anliegen kannst du dich an unsere Vertrauenspersonen Alina oder Georg wenden. Du erreichst sie unter den Emailadressen [alina@fsinf.informatik.uni-leipzig.de](mailto:alina@fsinf.informatik.uni-leipzig.de) und [georg@fsinf.informatik.uni-leipzig.de](mailto:georg@fsinf.informatik.uni-leipzig.de).

Ausländische Studierende können sich gerne an Amin über die Emailadresse [amin@fsinf.informatik.uni-leipzig.de](mailto:amin@fsinf.informatik.uni-leipzig.de) richten. Anfragen sind hier auf Arabisch, Deutsch oder Englisch möglich.

Alle weiteren Wege uns zu erreichen sind unter [Kontakt]({{ "/kontakt/" | relative_url}}) aufgelistet.
