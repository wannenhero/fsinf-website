# fsinf-website

### Installation

##### 1. Clonen

Das Git repository clonen.

##### 2. Ruby und Bundler installieren

Ruby installieren (abhängig vom System). 
Danach mit 
```
$ gem install bundler
```
Bundler installieren.
Damit Bundler aus der Konsole benutzt werden soll muss Ruby in die Pathdatei. Temporär geht das mit
```
export PATH="$PATH:/home/wannenhero/.gem/ruby/2.6.0/bin"
```

##### 3. Gems lokal installieren

Im Repository mit dem Befehl
```
bundle install
```
die benötigten Gems installieren lassen.

##### 4. Lokal laufen lassen

Mit dem Befehl
```
bundle exec jekyll serve
```
kann dann die Seite auf [localhost:4000](localhost:4000) ansehen.

### Inhalte verändern/einfügen

##### Protokolle

Protokolle liegen im Ordner `_protokolle`. Sie werden nach dem Schema JJJJ.MM.DD-protokoll.md benannt, und enhalten folgenden Header:
```
---

---
```
Wenn die Datei auf Master gepusht wurde baut sich die Website automatisch mit der neuen Datei neu.

##### Posts

Posts liegen im Ordner `_posts`. Sie werden nach dem Schema JJJJ.MM.DD-[abstract title].md benannt, und enhalten folgenden Header:
```
---
title: [Überschrift]
---
```
Wenn die Datei auf Master gepusht wurde baut sich die Website automatisch mit der neuen Datei neu. 