---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Willkommen beim Fachschaftsrat Informatik
lang: de
ref: home
---

Der Fachschaftsrat Informatik der Universität Leipzig ist die gewählte Vertretung der Studierenden am Institut für Informatik. Unsere Sitzungstermine findest du im [Kalender](/kalender/). Unseren Newsletter kannst du [hier](https://fsinf.informatik.uni-leipzig.de/mailman/listinfo/fsinf-newsletter) abonnieren. Im Newsletter gibt es alle ein bis zwei Monate Infos aus der Fachschaft und relevante Termine. Außerdem betreiben wir einen Jobverteiler, über welchen Arbeitgebende Jobangebote versenden. Diesen kannst du [hier](https://fsinf.informatik.uni-leipzig.de/mailman/listinfo/jobs) abonnieren. 
    
{% twitter https://twitter.com/fsinfLeipzig  maxwidth=500  limit=2 %}

